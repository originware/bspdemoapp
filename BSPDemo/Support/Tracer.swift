
//
//  Support/Tracing.swift
//  BSPDemo
//
//  Created by Terry Stillone on 23/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation

#if os(iOS) || os(macOS)
import os.log
#endif

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The tracer singleton for the App.
///

public let g_tracer = Tracer(enabled: true, delivererType: .eConsole)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Traceable Protocol to allow classes and structs to be traceable.
///

public protocol Traceable
{
}

extension Traceable
{
    @inlinable
    @inline(__always)
    func trace(_ message: @autoclosure () -> String)
    {
#if !DisableTracing
        g_tracer.trace(message())
#endif
    }
    
    @inlinable
    @inline(__always)
    func trace(title: @autoclosure () -> String, _ message: @autoclosure () -> String)
    {
#if !DisableTracing
        g_tracer.trace(title: title(), message())
#endif
    }
}

///
/// TracerDeliverer, how trace messages are delivered.
///
public protocol TracerDeliverer
{
    var enabled : Bool { get set }
    func deliver(_ message: String)
}

///
/// Tracer, used for monitoring Notifier notifications.
///

public class Tracer
{
#if TraceWithTimestamp
    public static let HeaderTag = "Trace %5d [%@]> "

    public let traceStartTime = Date()
#else
    public static let HeaderTag = "Trace >  "
#endif

    public static let TitleLength = 55
    public static let TagLength   = 22
    
    public var enabled : Bool
    {
        get { deliverer.enabled }
        set { deliverer.enabled = newValue }
    }
                
    public var deliverer : TracerDeliverer
    
    public init(enabled : Bool, delivererType: TracerDelivererBuilder.eDelivererType = .eNull)
    {
        self.deliverer = delivererType.buildDeliverer(enabled: enabled)
    }
    
    @inlinable
    @inline(__always)
    open func trace(_ message: @autoclosure () -> String)
    {
        guard deliverer.enabled else { return }

#if TraceWithTimestamp
        let millisec = Int(-traceStartTime.timeIntervalSinceNow * 1000)
        let paddedTag = String(format: Tracer.HeaderTag, millisec, Thread.getThreadName()).padding(toLength: Tracer.TagLength, withPad: " ", startingAt: 0)

        deliverer.deliver(paddedTag + message())
#else
        deliverer.deliver(message())
#endif
    }
    
    @inlinable
    @inline(__always)
    open func trace(title: @autoclosure () -> String, _ message: @autoclosure () -> String)
    {
        guard deliverer.enabled else { return }

        let paddedTitle = title().padding(toLength: Tracer.TitleLength, withPad: " ", startingAt: 0)

#if TraceWithTimestamp
        let millisec = Int(-traceStartTime.timeIntervalSinceNow * 1000)
        let paddedTag = String(format: Tracer.HeaderTag, millisec, Thread.getThreadName()).padding(toLength: Tracer.TagLength, withPad: " ", startingAt: 0)

        deliverer.deliver(paddedTag + paddedTitle + " " + message())
#else

        deliverer.deliver(paddedTitle + " " + message())
#endif
    }
}

public struct TracerDelivererBuilder
{
    public static func getDefaultType() -> eDelivererType
    {
        eDelivererType.eConsole
    }
    
    public indirect enum eDelivererType
    {
        case eNull
        case eLogging(subsystem : String, category: String, logType: Int, includeConsole: Bool)
        case eConsole
        case eCustom(TracerDeliverer)

        public func buildDeliverer(enabled: Bool) -> TracerDeliverer
        {
            switch self
            {
                case .eLogging(let subsystem, let category, let logTypeRaw, let includeConsole):

                    if #available(iOS 10.0, OSX 10.14, *)
                    {
                        return LoggerDeliverer(enabled: enabled, subsystem: subsystem, category: category, logTypeRaw: logTypeRaw, includeConsole: includeConsole)
                    }
                    else
                    {
                        return ConsoleDeliverer(enabled: enabled)
                    }

                case .eConsole:

                    return ConsoleDeliverer(enabled: enabled)

                case .eCustom(let deliverer):

                    return deliverer

                case .eNull:

                    return NullDeliverer()
            }
        }
    }

    public class LoggerDeliverer: TracerDeliverer
    {
        public var enabled : Bool
        private let m_includeConsole : Bool

#if os(iOS) || os(macOS)

        private let m_osLogger : OSLog
        private let m_osLogType : OSLogType

        @available(iOS 10.0, OSX 10.14, *)
        public init(enabled : Bool = true, subsystem : String, category: String, logTypeRaw: Int, includeConsole: Bool)
        {
            self.enabled = enabled
            self.m_osLogger = OSLog(subsystem: subsystem, category: category)
            self.m_osLogType = OSLogType(UInt8(logTypeRaw))
            self.m_includeConsole = includeConsole
        }

        public func deliver(_ message: String)
        {
            if m_includeConsole
            {
                print(message)
            }

            if #available(iOS 10.0, OSX 10.14, *)
            {
                os_log("%@", log: m_osLogger, type: m_osLogType, message)
            }
        }

#elseif os(Linux)

        private let m_priority : Int32

        public init(enabled : Bool = true, subsystem : String, category: String, logTypeRaw: Int, includeConsole: Bool)
        {
            self.enabled = enabled
            self.m_priority = Int32(logTypeRaw)
            self.m_includeConsole = includeConsole

            openlog(subsystem, LOG_NOWAIT, LOG_USER)
        }

        deinit
        {
            closelog()
        }

        public func deliver(_ message: String)
        {
            if m_includeConsole
            {
                print(message)
            }

            message.withCString {

                withVaList([$0]) { args -> Void in

                    vsyslog(m_priority, "%s", args)
                }
            }
        }
#else
        public init(enabled : Bool = true, subsystem : String, category: String, logTypeRaw: Int, includeConsole: Bool)
        {
            self.enabled = enabled
            self.m_includeConsole = includeConsole
        }

        public func deliver(_ message: String)
        {
            if m_includeConsole
            {
                print(message)
            }
        }
#endif
    }

    public struct ConsoleDeliverer : TracerDeliverer
    {
        public var enabled : Bool

        public init(enabled : Bool = true)
        {
            self.enabled = enabled
        }

        public func deliver(_ message : String)
        {
            print(message)
        }
    }

    public struct NullDeliverer : TracerDeliverer
    {
        public var enabled = false

        public func deliver(_ message : String)
        {
        }
    }
}
