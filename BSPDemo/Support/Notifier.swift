//
//  Support/Notifier.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine

public protocol NotifierType
{
    var uri : String { get }

    init(uri: String)
}

public protocol Notifier: NotifierType, Traceable
{
    associatedtype ValueType
    associatedtype ResultType

    func send(_ value : ValueType) -> ResultType?
    func sendAsync(_ value : ValueType) async -> ResultType
}

public struct Notification<ValueType, ResultType>
{
    public typealias ResultFunc = (ResultType) -> Void
    
    public let value : ValueType
    public let resultFunc : ResultFunc
    
    public func forward(result: ResultType)
    {
        resultFunc(result)
    }
}

public struct Notifiers
{
    public class SyncNotifier<ValueType, ResultType> : Notifier, Publisher
    {
        public typealias Output = Notification<ValueType, ResultType>
        public typealias Failure = Never
        
        public let uri : String

        fileprivate let m_publisher = PassthroughSubject<Output, Never>()
        
#if DEBUG
        public var enableTrace                                                    = true
        public var auditPublisher:       Audit.AuditPublisher<Audit.TraceRecord>? = nil
#endif
        
        public required init(uri: String)
        {
            self.uri = uri
        }
        
        @discardableResult
        public func send(_ value : ValueType) -> ResultType?
        {
            var result : ResultType?
            
            let notification = Notification<ValueType, ResultType>(value: value, resultFunc: { (forwardedResult) in

                result = forwardedResult
            })

#if DEBUG
            if enableTrace
            {
                trace(title: "Notifier[\(uri)]", "send \(String(describing: value))")
            }
            
            auditPublisher?.send(Audit.TraceRecord(uri, "send\(AutomationBehaviourModel.Separator)\(String(describing: value))"))
#endif

            m_publisher.send(notification)
            
            if let result = result
            {
#if DEBUG
                if ResultType.self != Void.self
                {
                    if enableTrace
                    {
                        trace(title: "Notifier[\(uri)]", "send \(String(describing: value)), result: \(String(describing: result))")
                    }
                    
                    auditPublisher?.send(Audit.TraceRecord(uri, "send\(AutomationBehaviourModel.Separator)\(String(describing: value)):\(String(describing: result))"))
                }
#endif
                
                return result
            }
            else if ResultType.self != Void.self
            {
                assertionFailure("SyncNotifier: no result returned for return type \(ResultType.self)")
                
                return nil
            }
            else
            {
                return nil
            }
        }

        public func sendAsync(_ value : ValueType) async -> ResultType
        {
#if DEBUG
            if enableTrace
            {
                trace(title: "Notifier[\(uri)]", "sendAsync \(String(describing: value))")
            }

            await auditPublisher?.sendAsync(Audit.TraceRecord(uri, "sendAsyncNoTrace\(AutomationBehaviourModel.Separator)\(String(describing: value))"))
#endif

            return await withCheckedContinuation { continuation in
         
                m_publisher.send(Output(value: value, resultFunc: { (forwardedResult) in
                    
                    continuation.resume(returning: forwardedResult)
                }))
            }
        }
        
        public func receive<S>(subscriber: S) where S : Subscriber, S.Failure == Never, S.Input == Output
        {
            m_publisher.receive(subscriber: subscriber)
        }
        
        public func sink(receiveValue: @escaping (Output) -> Void) -> AnyCancellable
        {
            m_publisher.sink(receiveValue: receiveValue)
        }

        @inlinable
        @inline(__always)
        public func setNoTrace(_ execute: () -> Void)
        {
#if DEBUG
            enableTrace = false
            execute()
            enableTrace = true
#else
            execute()
#endif
        }
    }

    public class AsyncNotifier<ValueType, ResultType> : Notifier
    {
        public typealias Output         = Notification<ValueType, ResultType>
        public typealias ReceiverFunc   = (Output) async -> ResultType
        public typealias Failure        = Never

        public let uri : String
        
        fileprivate var m_receiverFunc : ReceiverFunc? = nil

#if DEBUG
        public var enableTrace                                                    = true
        public var auditPublisher:       Audit.AuditPublisher<Audit.TraceRecord>? = nil
#endif

        public required  init(uri: String)
        {
            self.uri = uri
        }

        public func send(_ value : ValueType) -> ResultType?
        {
#if DEBUG
            assert(m_receiverFunc != nil, "Expected m_receiverFunc to be valid")

            if enableTrace
            {
                trace(title: "Notifier[\(uri)]", "send \(String(describing: value))")
            }

            auditPublisher?.send(Audit.TraceRecord(uri, "send\(AutomationBehaviourModel.Separator)\(String(describing: value))"))
#endif

            guard let receiverFunc = m_receiverFunc else { return nil }

            let notification = Notification<ValueType, ResultType>(value: value, resultFunc: { (_) in })

            Task {

                await receiverFunc(notification)
            }

            return nil
        }

        public func sendAsync(_ value : ValueType) async -> ResultType
        {
#if DEBUG
            assert(m_receiverFunc != nil, "Expected m_receiverFunc to be valid")

            if enableTrace
            {
                trace(title: "Notifier[\(uri)]", "sendAsync \(String(describing: value))")
            }

            await auditPublisher?.sendAsync(Audit.TraceRecord(uri, "sendAsync\(AutomationBehaviourModel.Separator)\(String(describing: value))"))
#endif
            
            let notification = Notification<ValueType, ResultType>(value: value, resultFunc: { (_) in })

            return await m_receiverFunc!(notification)
        }

        @inlinable
        @inline(__always)
        public func setNoTrace(_ execute: () -> Void)
        {
#if DEBUG
            enableTrace = false
            execute()
            enableTrace = true
#else
            execute()
#endif
        }
        
        public func sinkAsync(receiveValue: @escaping ReceiverFunc) -> Self
        {
            m_receiverFunc = receiveValue

            return self
        }

        public func getCancellable() -> AnyCancellable
        {
            AnyCancellable({ [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.m_receiverFunc = nil
            })
        }

        public func store(in set: inout Set<AnyCancellable>)
        {
            set.insert(getCancellable())
        }
    }


    public class QueuedNotifier<ValueType, ResultType> : Notifier
    {
        public typealias Output = Notification<ValueType, ResultType>
        public typealias ReceiverFunc = (Output) async -> ResultType

        public typealias Failure = Never

        fileprivate actor Queue
        {
            private var m_queue = ContiguousArray<ValueType>()

            fileprivate func queue(_ value: ValueType)
            {
                m_queue.append(value)
            }

            fileprivate func unqueue() -> ValueType?
            {
                guard !m_queue.isEmpty else { return nil }

                return m_queue.removeFirst()
            }

            fileprivate func removeAll()
            {
                return m_queue.removeAll()
            }
        }

        public let uri : String

        fileprivate var m_receiverFunc : ReceiverFunc? = nil
        private var m_queue = Queue()
        
#if DEBUG
        public var auditPublisher : Audit.AuditPublisher<Audit.TraceRecord>? = nil
#endif
        
        public required init(uri: String)
        {
            self.uri = uri
        }
        
        public func send(_ value : ValueType) -> ResultType?
        {
            // This method is not expected to be exercised.
            assertionFailure("Unexpected code point")
            
            return nil
        }
        
        public func sendAsync(_ value : ValueType) async -> ResultType
        {
#if DEBUG
            assert(m_receiverFunc != nil, "Expected m_receiverFunc to be valid")

            trace(title: "Notifier[\(uri)]", "sendAsync \(String(describing: value))")
            
            await auditPublisher?.sendAsync(Audit.TraceRecord(uri, "sendAsync\(AutomationBehaviourModel.Separator)\(String(describing: value))"))
#endif

            while let queuedValue = await m_queue.unqueue()
            {
                let notification = Notification<ValueType, ResultType>(value: queuedValue, resultFunc: { (_) in })

                let _ = await m_receiverFunc!(notification)
            }

            let notification = Notification<ValueType, ResultType>(value: value, resultFunc: { (_) in })

            return await m_receiverFunc!(notification)
        }

        public func removeAll() async
        {
            await m_queue.removeAll()
        }

        public func sink(receiveValue: @escaping ReceiverFunc) -> Self
        {
            m_receiverFunc = receiveValue

            return self
        }

        public func store(in set: inout Set<AnyCancellable>)
        {
            let cancellable = AnyCancellable({ [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.m_receiverFunc = nil

                Task {
                    
                    await strongSelf.removeAll()
                }
            })

            set.insert(cancellable)
        }
    }

    public final class NullSyncNotifier<ValueType, ResultType> : SyncNotifier<ValueType, ResultType>
    {
        private let m_defaultResult : ResultType

        public init(uri: String, defaultResult : ResultType)
        {
            self.m_defaultResult = defaultResult

            super.init(uri: uri)
        }

        public required init(uri: String)
        {
            fatalError("init(uri:) not been implemented")
        }

        public override func send(_ value : ValueType) -> ResultType?
        {
            nil
        }

        public override func sendAsync(_ value : ValueType) async -> ResultType
        {
            m_defaultResult
        }

        public override func sink(receiveValue: @escaping (Output) -> Void) -> AnyCancellable
        {
            AnyCancellable({})
        }
    }

    public final class NullAsyncNotifier<ValueType, ResultType> : AsyncNotifier<ValueType, ResultType>
    {
        private let m_defaultResult : ResultType

        public init(uri: String, defaultResult : ResultType)
        {
            self.m_defaultResult = defaultResult

            super.init(uri: uri)
        }

        public required init(uri: String)
        {
            fatalError("init(uri:) not been implemented")
        }

        public override func send(_ value : ValueType) -> ResultType?
        {
            nil
        }

        public override func sendAsync(_ value : ValueType) async -> ResultType
        {
            m_defaultResult
        }

        public override func sinkAsync(receiveValue: @escaping ReceiverFunc) -> Self
        {
            self
        }

        public override func store(in set: inout Set<AnyCancellable>)
        {
            set.insert(AnyCancellable({}))
        }
    }
    
    public final class NullQueuedNotifier<ValueType, ResultType> : QueuedNotifier<ValueType, ResultType>
    {
        private let m_defaultResult : ResultType

        public init(uri: String, defaultResult : ResultType)
        {
            self.m_defaultResult = defaultResult

            super.init(uri: uri)
        }

        public required init(uri: String)
        {
            fatalError("init(uri:) not been implemented")
        }
        
        public override func send(_ value : ValueType) -> ResultType?
        {
            nil
        }

        public override func sendAsync(_ value : ValueType) async -> ResultType
        {
            m_defaultResult
        }

        public override func sink(receiveValue: @escaping ReceiverFunc) -> Self
        {
            self
        }

        public override func store(in set: inout Set<AnyCancellable>)
        {
            set.insert(AnyCancellable({}))
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Notifier creation and access through a directory
///

public final class NotifierDirectory
{
    public struct Presentation
    {
        public struct Message
        {
            public var event : MessageScope.Presentation.MessageEventNotifier?       { m_notifierStore.presentationMessageEvent }

            public var isActive : Bool
            {
                m_notifierStore.isActive(uri: MessageScope.Presentation.MessageEventNotifierURI)
            }

            private let m_notifierStore: NotifierStore

            public init(notifierStore : NotifierStore)
            {
                m_notifierStore = notifierStore
            }
        }

        public struct WebURL
        {
            public var request : WebURLScope.Presentation.WebURLRequestNotifier?   { m_notifierStore.presentationWebURLRequest }

            public var isActive : Bool
            {
                m_notifierStore.isActive(uri: WebURLScope.Presentation.WebURLRequestNotifierURI)
            }

            private let m_notifierStore: NotifierStore

            public init(notifierStore : NotifierStore)
            {
                m_notifierStore = notifierStore
            }
        }

        public var request : PresentationActor.PresentationRequestNotifier  { m_notifierStore.presentationRequest }
        public var event : PresentationActor.PresentationEventNotifier      { m_notifierStore.presentationEvent }
        internal var stack : PresentationActor.PresentationStackNotifier    { m_notifierStore.presentationStack }

        public let message: Message
        public let webURL: WebURL

        private let m_notifierStore : NotifierStore

        public init(notifierStore : NotifierStore)
        {
            self.message = Message(notifierStore: notifierStore)
            self.webURL = WebURL(notifierStore: notifierStore)
            self.m_notifierStore = notifierStore
        }
    }

    public struct Service
    {
        public var request : ServiceActor.ServiceRequestNotifier                        { m_notifierStore.serviceRequest }
        public var event :                    ServiceActor.ServiceEventNotifier         { m_notifierStore.serviceEvent }
        public var queuedSpeechSynthNotifier: ServiceActor.QueuedSpeechSynthNotifier    { m_notifierStore.serviceQueuedSpeechSynth }

        private let m_notifierStore : NotifierStore

        public init(notifierStore : NotifierStore)
        {
            self.m_notifierStore = notifierStore
        }
    }

    public struct Behaviour
    {
        public struct Meta
        {
            public var request : BehaviourActor.BehaviourMetaRequestNotifier    { m_notifierStore.metaBehaviourRequest }
            public var event : BehaviourActor.BehaviourMetaEventNotifier        { m_notifierStore.metaBehaviourEvent }

            private let m_notifierStore : NotifierStore

            public init(notifierStore : NotifierStore)
            {
                self.m_notifierStore = notifierStore
            }
        }

        public struct WebURL
        {
            public var request : WebURLScope.Behaviour.WebURLRequestNotifier?    { m_notifierStore.behaviourWebURLRequest }

            private let m_notifierStore : NotifierStore

            public init(notifierStore : NotifierStore)
            {
                self.m_notifierStore = notifierStore
            }
        }
        
        internal var stack : BehaviourActor.BehaviourStackNotifier          { m_notifierStore.behaviourStack }

        public let webURL : WebURL
        public let meta :           Meta
        private let m_notifierStore: NotifierStore

        public init(notifierStore : NotifierStore)
        {
            self.webURL = WebURL(notifierStore: notifierStore)
            self.meta = Meta(notifierStore: notifierStore)
            self.m_notifierStore = notifierStore
        }
    }
     
    public fileprivate(set) var behaviour : Behaviour!
    public fileprivate(set) var presentation : Presentation!
    public fileprivate(set) var service : Service!

    public let notifierStore: NotifierStore

    public init(notifierStore : NotifierStore)
    {
        self.notifierStore      = notifierStore
        self.behaviour          = Behaviour(notifierStore: notifierStore)
        self.presentation       = Presentation(notifierStore: notifierStore)
        self.service            = Service(notifierStore: notifierStore)
    }

    public func isActive(uri: String) -> Bool
    {
        notifierStore.isActive(uri: uri)
    }
    
    public func remove(uri: String)
    {
        return notifierStore.remove(uri: uri)
    }
}

public class NotifierStore
{
    public struct StackedNotifier<Value : NotifierType>
    {
        public var isEmpty : Bool       { m_eventNotifierStack.isEmpty }
        public var top : Value?         { m_eventNotifierStack.last }
        
        public let uri : String

        private var m_eventNotifierStack = ContiguousArray<Value>()

        @discardableResult
        public mutating func push() -> Value
        {
            let notifier = Value(uri: uri)

            m_eventNotifierStack.append(notifier)

            return notifier
        }

        public mutating func pop()
        {
            guard !m_eventNotifierStack.isEmpty else { return }

            m_eventNotifierStack.removeLast()
        }
        
        public mutating func removeAll()
        {
            m_eventNotifierStack.removeAll()
        }

        public init(uri: String)
        {
            self.uri = uri
        }
    }

    public private(set) lazy var metaBehaviourRequest       = build(uri: BehaviourActor.BehaviourMetaRequestURI, type: BehaviourActor.BehaviourMetaRequestNotifier.self)
    public private(set) lazy var metaBehaviourEvent         = build(uri: BehaviourActor.BehaviourMetaEventURI, type: BehaviourActor.BehaviourMetaEventNotifier.self)
    internal let behaviourStack                             = BehaviourActor.BehaviourStackNotifier(uri: BehaviourActor.BehaviourStackURI)

    public private(set) lazy var serviceRequest             = build(uri: ServiceActor.ServiceRequestURI, type: ServiceActor.ServiceRequestNotifier.self)
    public private(set) lazy var serviceEvent               = build(uri: ServiceActor.ServiceEventURI, type: ServiceActor.ServiceEventNotifier.self)
    public private(set) lazy var serviceQueuedSpeechSynth   = build(uri: ServiceActor.ServiceSpeechSynthURI, type: ServiceActor.QueuedSpeechSynthNotifier.self)

    public private(set) lazy var presentationRequest        = build(uri: PresentationActor.PresentationRequestURI, type: PresentationActor.PresentationRequestNotifier.self)
    public private(set) lazy var presentationEvent          = build(uri: PresentationActor.PresentationEventURI, type: PresentationActor.PresentationEventNotifier.self)
    internal let presentationStack      = PresentationActor.PresentationStackNotifier(uri: PresentationActor.PresentationStackURI)

    public var presentationMessageEvent : MessageScope.Presentation.MessageEventNotifier?
    {
        m_presentationMessageNotifierStack.top
    }

    public var behaviourWebURLRequest : WebURLScope.Behaviour.WebURLRequestNotifier?
    {
        m_behaviourWebURLNotifierStack.top
    }

    public var presentationWebURLRequest : WebURLScope.Presentation.WebURLRequestNotifier?
    {
        m_presentationWebURLNotifierStack.top
    }

    private var m_behaviourWebURLNotifierStack  = StackedNotifier<WebURLScope.Behaviour.WebURLRequestNotifier>(uri: WebURLScope.Behaviour.WebURLRequestNotifierURI)
    private var m_presentationWebURLNotifierStack  = StackedNotifier<WebURLScope.Presentation.WebURLRequestNotifier>(uri: WebURLScope.Presentation.WebURLRequestNotifierURI)
    private var m_presentationMessageNotifierStack = StackedNotifier<MessageScope.Presentation.MessageEventNotifier>(uri: MessageScope.Presentation.MessageEventNotifierURI)

    public func push(uri: String)
    {
        switch uri
        {
            case WebURLScope.Behaviour.WebURLRequestNotifierURI:

                m_behaviourWebURLNotifierStack.push()

            case WebURLScope.Presentation.WebURLRequestNotifierURI:

                m_presentationWebURLNotifierStack.push()

            case MessageScope.Presentation.MessageEventNotifierURI:

                m_presentationMessageNotifierStack.push()

            default:

                break
        }
    }

    public func pop(uri: String)
    {
        switch uri
        {
            case WebURLScope.Behaviour.WebURLRequestNotifierURI:

                m_behaviourWebURLNotifierStack.pop()

            case WebURLScope.Presentation.WebURLRequestNotifierURI:

                m_presentationWebURLNotifierStack.pop()

            case MessageScope.Presentation.MessageEventNotifierURI:

                m_presentationMessageNotifierStack.pop()

            default:

                break
        }
    }

    public func isActive(uri: String) -> Bool
    {
        switch uri
        {
            case WebURLScope.Behaviour.WebURLRequestNotifierURI:

                return !m_behaviourWebURLNotifierStack.isEmpty

            case WebURLScope.Presentation.WebURLRequestNotifierURI:

                return !m_presentationWebURLNotifierStack.isEmpty

            case MessageScope.Presentation.MessageEventNotifierURI:

                return !m_presentationMessageNotifierStack.isEmpty

            case BehaviourActor.BehaviourMetaRequestURI,
                 BehaviourActor.BehaviourMetaEventURI,
                 ServiceActor.ServiceRequestURI,
                 ServiceActor.ServiceEventURI,
                 PresentationActor.PresentationRequestURI,
                 PresentationActor.PresentationEventURI:

                return true

            default:

                return false
        }
    }

    public func remove(uri: String)
    {
        switch uri
        {
            case WebURLScope.Behaviour.WebURLRequestNotifierURI:

                m_behaviourWebURLNotifierStack.removeAll()

            case WebURLScope.Presentation.WebURLRequestNotifierURI:

                m_presentationWebURLNotifierStack.removeAll()

            case MessageScope.Presentation.MessageEventNotifierURI:

                m_presentationMessageNotifierStack.removeAll()

            default:

                break
        }
    }

    public func build<T>(uri: String, type: T.Type) -> T
    {
        switch uri
        {
            case ServiceActor.ServiceRequestURI:

                return ServiceActor.ServiceRequestNotifier(uri: ServiceActor.ServiceRequestURI) as! T

            case ServiceActor.ServiceEventURI:

                return ServiceActor.ServiceEventNotifier(uri: ServiceActor.ServiceEventURI) as! T

            case ServiceActor.ServiceSpeechSynthURI:

                return ServiceActor.QueuedSpeechSynthNotifier(uri: uri) as! T

            case BehaviourActor.BehaviourMetaRequestURI:

                return BehaviourActor.BehaviourMetaRequestNotifier(uri: BehaviourActor.BehaviourMetaRequestURI) as! T

            case BehaviourActor.BehaviourMetaEventURI:

                return BehaviourActor.BehaviourMetaEventNotifier(uri: BehaviourActor.BehaviourMetaEventURI) as! T

            case PresentationActor.PresentationRequestURI:

                return PresentationActor.PresentationRequestNotifier(uri: PresentationActor.PresentationRequestURI) as! T

            case PresentationActor.PresentationEventURI:

                return PresentationActor.PresentationEventNotifier(uri: PresentationActor.PresentationRequestURI) as! T

            default:

                fatalError("Invalid Notifier URI")
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Notifier auditing, used in unit testing.
///

public protocol NotifierAuditRecord
{
    var notifierURI : String { get }
}

public struct Audit
{
    public enum eNotificationRecord : NotifierAuditRecord
    {
        case eServiceRequest(Notification<ServiceActor.eServiceRequest, ServiceActor.eServiceReply>)
        case eServiceEvent(Notification<ServiceActor.eServiceEvent, Void>)

        case eBehaviourRequest(Notification<BehaviourActor.eBehaviourMetaRequest, Void>)

        case ePresentationRequest(Notification<PresentationActor.ePresentationRequest, Void>)
        case ePresentationEvent(Notification<PresentationActor.ePresentationEvent, Void>)
        
        public var notifierURI : String
        {
            switch self
            {
                case .eServiceRequest:      return ServiceActor.ServiceRequestURI
                case .eServiceEvent:        return ServiceActor.ServiceEventURI
                        
                case .eBehaviourRequest:    return BehaviourActor.BehaviourMetaRequestURI
                    
                case .ePresentationRequest: return PresentationActor.PresentationRequestURI
                case .ePresentationEvent:   return PresentationActor.PresentationEventURI
            }
        }
    }

    public struct TraceRecord : NotifierAuditRecord
    {
        public let notifierURI: String
        public let trace : String

        public init(_ notifierURI: String, _ trace: String)
        {
            self.notifierURI = notifierURI
            self.trace = trace
        }
    }

    public class AuditPublisher<Record : NotifierAuditRecord>
    {
        public typealias ValueType              = Record
 
        public typealias ReceiverFunc           = (ValueType) -> ()
        public typealias ReceiveCompletionFunc  = () -> ()

        fileprivate actor Executor
        {
            fileprivate func run(_ executionFunc: () async -> Void) async
            {
                await executionFunc()
            }
        }
        
        private var m_receiverFunc :            ReceiverFunc? = nil
        private var m_receiveCompletionFunc :   ReceiveCompletionFunc? = nil
        private var m_onReceiveFunc:            ReceiverFunc? = nil
        
        private let m_executor = Executor()
        private var m_isComplete : Bool = false

        public func send(_ value : ValueType)
        {
            guard !m_isComplete else { return }

            m_receiverFunc?(value)
            m_onReceiveFunc?(value)
        }

        public func sendAsync(_ value : ValueType) async
        {
            guard !m_isComplete else { return }

            await m_executor.run({
                m_receiverFunc?(value)
                m_onReceiveFunc?(value)
            })
        }
        
        public func sendCompletion() async
        {
            guard !m_isComplete else { return }

            await m_executor.run({
                
                m_isComplete = true
                m_receiveCompletionFunc?()
            })
        }
        
        public func onReceive(_ receiveFunc : @escaping (ValueType) -> Void)
        {
            m_onReceiveFunc = receiveFunc
        }

        public func sink(receiveCompletion: @escaping ReceiveCompletionFunc, receiveValue: @escaping ReceiverFunc)  -> Self
        {
            m_receiveCompletionFunc = receiveCompletion
            m_receiverFunc = receiveValue

            return self
        }

        public func store(in set: inout Set<AnyCancellable>)
        {
            let cancellable = AnyCancellable({ [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.m_receiverFunc = nil
                strongSelf.m_receiveCompletionFunc = nil
            })

            set.insert(cancellable)
        }
    }
    
    public class AuditStore<Record : NotifierAuditRecord>
    {
        public struct StoreNotificationRecord
        {
            public let timestamp : TimeInterval
            public let sequenceNo : Int
            public let notifierURI : String
            public let auditRecord : Record
        }
        
        public var sequenceNo : Int
        {
            let value = m_sequenceCounter
            
            m_sequenceCounter += 1
            
            return value
        }
        
        public private(set) var logAsyncStream : AsyncStream<StoreNotificationRecord>! = nil
        public let startTime = Date()
        
        private var m_sequenceCounter = 0
        
        private var m_subscribers = Set<AnyCancellable>()
    
        public init(auditPublisher: AuditPublisher<Record>)
        {
            let asyncStream = AsyncStream<StoreNotificationRecord> { [weak self] (continuation) in
             
                guard let strongSelf = self else { return }

                auditPublisher.sink(receiveCompletion: { 

                    continuation.finish()
                    
                }, receiveValue: { (auditRecord) in

                    let storeNotificationRecord = StoreNotificationRecord(timestamp: -strongSelf.startTime.timeIntervalSinceNow,
                                                                          sequenceNo: strongSelf.sequenceNo,
                                                                          notifierURI: auditRecord.notifierURI,
                                                                          auditRecord: auditRecord)

                    continuation.yield(storeNotificationRecord)
                    
                }).store(in: &strongSelf.m_subscribers)
            }
            
            self.logAsyncStream = asyncStream
        }
    }
}
