
//
//  Support/Misc.swift
//  BSPDemo
//
// Created by Terry Stillone on 26/2/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Box-Muller Gaussian distribution number sequence generator.
///
public struct GaussianRandomSequenceGenerator<T : BinaryFloatingPoint> where T.RawSignificand : FixedWidthInteger
{
    private let mu : T
    private let sigma : T

    private var m_previousValue: T? = nil

    public init(mean: T, sigma : T)
    {
        self.mu = mean
        self.sigma = sigma
    }

    public mutating func generate(count: Int) -> ContiguousArray<T>
    {
        guard count > 0 else { return ContiguousArray<T>() }

        var values = ContiguousArray<T>()

        for _ in 0..<count
        {
            values.append(generateValue())
        }

        values.sort()

        // Ensure the first value is the smallest allowed value
        //values[0] = mu - sigma

        return values
    }

    private mutating func generateValue() -> T
    {
        guard m_previousValue == nil else
        {
            let value = m_previousValue!

            m_previousValue = nil

            return value
        }

        let pi2 = T.pi * T(2)
        let u1 = T.random(in: 0.1...1.0)
        let u2 = T.random(in: 0.0...1.0)
        let piu2 = pi2 * u2

        let mag = self.sigma * sqrt(T(-2.0) * T.logfp(u1))
        var z0  = mag * T.cosfp(piu2) + self.mu
        var z1  = mag * T.sinfp(piu2) + self.mu

        // Clamping
        if z0 < 0 { z0 = 0 }
        if z1 < 0 { z1 = 0 }

        self.m_previousValue = z1

        return z0
    }
}

extension BinaryFloatingPoint
{
    @inline(__always)
    public static func cosfp<T : BinaryFloatingPoint>(_ angle : T) -> T
    {
        if T.self == Float.self
        {
            return T(cos(Float(angle)))
        }
        else if T.self == CGFloat.self
        {
            return T(cos(CGFloat(angle)))
        }
        else if T.self == Double.self
        {
            return T(cos(Double(angle)))
        }
        else
        {
            return T(cos(Double(angle)))
        }
    }

    @inline(__always)
    public static func sinfp<T : BinaryFloatingPoint>(_ angle : T) -> T
    {
        if T.self == Float.self
        {
            return T(sin(Float(angle)))
        }
        else if T.self == CGFloat.self
        {
            return T(sin(CGFloat(angle)))
        }
        else if T.self == Double.self
        {
            return T(sin(Double(angle)))
        }
        else
        {
            return T(sin(Double(angle)))
        }
    }

    @inline(__always)
    public static func logfp<T : BinaryFloatingPoint>(_ value: T) -> T
    {
        if T.self == Float.self
        {
            return T(log(Float(value)))
        }
        else if T.self == CGFloat.self
        {
            return T(log(CGFloat(value)))
        }
        else if T.self == Double.self
        {
            return T(log(Double(value)))
        }
        else
        {
            return T(log(Double(value)))
        }
    }
}