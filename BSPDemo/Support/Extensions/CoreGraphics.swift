
//
//  Support/Extensions/CoreGraphics.swift
//  BSPDemo
//
//  Created by Terry Stillone on 3/2/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import UIKit

extension CGPoint
{
    public func distance(to point: CGPoint) -> CGFloat
    {
        let dx = x - point.x
        let dy = y - point.y

        return sqrt(dx * dx + dy * dy)
    }
    
    public static func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint
    {
        CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }

    public static func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint
    {
        CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
}

extension CGSize
{
    public static func +(lhs: CGSize, rhs: CGSize) -> CGSize
    {
        CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
    }

    public static func -(lhs: CGSize, rhs: CGSize) -> CGSize
    {
        CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
    }

    public static func +=(lhs: inout CGSize, rhs: CGSize)
    {
        lhs = CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
    }

    public static func -=(lhs: inout CGSize, rhs: CGSize)
    {
        lhs = CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
    }
}

extension CGRect
{
    public var center : CGPoint
    {
        CGPoint(x: midX, y: midY)
    }

    public func inset(by value: CGFloat) -> CGRect
    {
        inset(by: UIEdgeInsets(top: value, left: value, bottom: value, right: value))
    }

    public init(center: CGPoint, size: CGSize)
    {
        self.init(origin: CGPoint(x: center.x - size.width / 2, y: center.y - size.height / 2), size: size)
    }
}

extension UIColor
{
    public func solidColor(forAlpha alpha : CGFloat) -> UIColor
    {
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)

        guard getRed(&r, green: &g, blue: &b, alpha: &a) else { return self }

        let effectiveAlpha = a * alpha
        let whiteComponent = 1 - effectiveAlpha

        return UIColor(red: r * effectiveAlpha + whiteComponent, green: g * effectiveAlpha + whiteComponent, blue: b * effectiveAlpha + whiteComponent, alpha: 1.0)
    }
}
