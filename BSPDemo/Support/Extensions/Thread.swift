
//
//  Support/Extensions/String.swift
//  BSPDemo
//
//  Created by Terry Stillone on 21/3/22.
//  Copyright © 2022 Originware. All rights reserved.
//


import Foundation

extension Thread
{
    @inlinable
    @inline(__always)
    static func getThreadName() -> String
    {
        if current.isMainThread
        {
            return "main"
        }

        if let threadName = current.name
        {
            if !threadName.isEmpty
            {
                return threadName
            }

            let description = current.description

            if let range = description.range(of: "number = ")
            {
                let startIndex = range.upperBound
                var endIndex = description.index(after: range.upperBound)

                while description[endIndex].isNumber
                {
                    endIndex = description.index(after: endIndex)
                }

                return String(description[startIndex..<endIndex])
            }
            else
            {
                return description
            }
        }

        return "unknown"
    }
}
