//
//  Support/Extensions/String.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    @inlinable
    @inline(__always)
    public var isCapitalized : Bool
    {
        guard let firstCharacter = self.first else { return false }

        return firstCharacter.isUpperCase
    }

    @inlinable
    @inline(__always)
    public func capitalizingFirstLetter() -> String
    {
        prefix(1).capitalized + dropFirst()
    }

    @inlinable
    @inline(__always)
    public mutating func capitalizeFirstLetter()
    {
        self = self.capitalizingFirstLetter()
    }
}

extension Character
{
    @inlinable
    @inline(__always)
    public var isUpperCase : Bool
    {
        CharacterSet.uppercaseLetters.contains(self.unicodeScalars.first!)
    }

    @inlinable
    @inline(__always)
    public var isLowerCase : Bool
    {
        CharacterSet.lowercaseLetters.contains(self.unicodeScalars.first!)
    }
}

extension Optional where Wrapped == String
{
    @inlinable
    @inline(__always)
    public mutating func append(_ element :String)
    {
        switch self
        {
            case .none:      self = Wrapped()
            case .some:      break
        }

        self!.append(element)
    }

    @inlinable
    @inline(__always)
    public var isEmpty : Bool
    {
        switch self
        {
            case .none:                 return true
            case .some(let unwrapped):  return unwrapped.isEmpty
        }
    }

    @inlinable
    @inline(__always)
    public static func ++(lhs: String?, rhs: String?) -> String
    {
        switch (lhs, rhs)
        {
            case (nil, .some(let rhsString)):

                return rhsString

            case (.some(let lhsString), .some(let rhsString)) where lhsString.isEmpty:

                return rhsString

            case (.some(let lhsString), nil):

                return lhsString

            case (.some(let lhsString), .some(let rhsString)) where rhsString.isEmpty:

                return lhsString

            case (.some(let lhsString), .some(let rhsString)):

                return lhsString + " " + rhsString

            case (nil, nil):

                return ""
        }
    }

    @inlinable
    @inline(__always)
    public static func ++=(lhs: inout String, rhs: String?)
    {
        guard let rhs = rhs, !rhs.isEmpty else { return }
        guard !lhs.isEmpty else { lhs = rhs; return }

        lhs = lhs + " " + rhs
    }

    @inlinable
    @inline(__always)
    public static func ++=(lhs: inout String?, rhs: String)
    {
        if let unwrappedLhs = lhs, !unwrappedLhs.isEmpty
        {
            lhs = unwrappedLhs + " " + rhs
        }
        else
        {
            lhs = rhs
        }
    }

    @inlinable
    @inline(__always)
    public static func &&=(lhs: inout String, rhs: String?)
    {
        guard let rhs = rhs, !rhs.isEmpty else { return }
        guard !lhs.isEmpty else { lhs = rhs; return }

        lhs = lhs + ", " + rhs
    }

    @inlinable
    @inline(__always)
    public static func &&=(lhs: inout String?, rhs: String)
    {
        if let unwrappedLhs = lhs, !unwrappedLhs.isEmpty
        {
            lhs = unwrappedLhs + ", " + rhs
        }
        else
        {
            lhs = rhs
        }
    }
}

extension String.StringInterpolation
{
    mutating func appendInterpolation(_ value: AttributedString)
    {
        appendLiteral(String(value.characters))
    }
}

extension NSAttributedString
{
    func components(separatedBy string: String) -> [NSAttributedString]
    {
        var pos = 0
        
        return self.string.components(separatedBy: string).map {
            
            let range = NSRange(location: pos, length: $0.count)

            pos += range.length + string.count

            return self.attributedSubstring(from: range)
        }
    }

    public static func +(lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString
    {
        let mutableLHS = NSMutableAttributedString(attributedString: lhs)

        mutableLHS.append(rhs)

        return mutableLHS
    }

    public static func ++(lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString
    {
        let mutableLHS = NSMutableAttributedString(attributedString: lhs)

        mutableLHS.append(rhs)

        return mutableLHS
    }

    public static func +=(lhs: inout NSMutableAttributedString, rhs: NSAttributedString)
    {
        lhs.append(rhs)
    }
}

infix operator ++: AdditionPrecedence
infix operator ++=: AssignmentPrecedence
infix operator &&=: AssignmentPrecedence

