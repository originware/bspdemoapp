
//
//  Support/Mocks.swift
//  BSPDemo
//
//  Created by Terry Stillone on 1/1/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Speech
import Combine
import CoreLocation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Mocks of various system services.
///
struct Mocks
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// SFSpeechRecognizer mocks.
///
extension Mocks
{
    public final class AuthorizedSFSpeechRecognizer : SFSpeechRecognizer, Traceable
    {
        private static let m_dispatchQueue = DispatchQueue(label: "speechRecog.originware", qos: .background)

        private var m_mockedRecognizedText = ""
        private var m_subscribers = Set<AnyCancellable>()
        
        weak override public var delegate: SFSpeechRecognizerDelegate?
        {
            get { nil } set {}
        }
        
        @available(iOS 13, *)
        public override var supportsOnDeviceRecognition: Bool
        {
            get { true } set {}
        }
        
        public init?(appBuilder : IAppBuilder)
        {
            super.init(locale: Locale.current)
      
            appBuilder.config.testingConfig?.speechRecognitionTextPublisher.sink(receiveValue: { [weak self] (value) in
                
                guard let strongSelf = self else { return }
                
                strongSelf.m_mockedRecognizedText = value
                
            }).store(in: &m_subscribers)

            trace(title: "Mocks.AuthorizedSFSpeechRecognizer", "Construct Service")

            AuthorizedSFSpeechRecognizer.m_dispatchQueue.async { Thread.current.name = "speechRecog" }
        }

        deinit
        {
            trace(title: "Mocks.AuthorizedSFSpeechRecognizer", "Destruct Service")
        }
        
        public override class func requestAuthorization(_ handler: @escaping (SFSpeechRecognizerAuthorizationStatus) -> Void)
        {
            m_dispatchQueue.async { handler(.authorized) }
        }
        
        public override func recognitionTask(with request: SFSpeechRecognitionRequest, resultHandler: @escaping (SFSpeechRecognitionResult?, Error?) -> Void) -> SFSpeechRecognitionTask
        {
            let speechRecognitionTask = MockedSFSpeechRecognitionTask()

            AuthorizedSFSpeechRecognizer.m_dispatchQueue.asyncAfter(deadline: .now() + 0.7, execute: { [weak self] in
            
                guard let strongSelf = self else { return }
                
                let recognitionResult = MockedSFSpeechRecognitionResult(mockedRecognizedText: strongSelf.m_mockedRecognizedText)
        
                resultHandler(recognitionResult, nil)
            })

            return speechRecognitionTask
        }
    }
    
    public class MockedSFSpeechRecognitionTask : SFSpeechRecognitionTask
    {
        public override var error : Error?  { nil }
        
        public override func finish()
        {
            // Do nothing
        }
    }
    
    public class MockedSFSpeechRecognitionResult : SFSpeechRecognitionResult
    {
        public override var isFinal : Bool  { true }
        
        private let m_mockedRecognizedText : String
        
        public init(mockedRecognizedText : String)
        {
            self.m_mockedRecognizedText = mockedRecognizedText

            super.init()
        }

        public required init(coder: NSCoder)
        {
            self.m_mockedRecognizedText = "dummy"
            
            super.init()
        }
        
        public override var bestTranscription : SFTranscription
        {
            MockedSFTranscription(mockedRecognizedText: m_mockedRecognizedText)
        }
    }
    
    public class MockedSFTranscription : SFTranscription
    {
        private let m_mockedRecognizedText : String
        
        public init(mockedRecognizedText : String)
        {
            self.m_mockedRecognizedText = mockedRecognizedText
            
            super.init()
        }
        
        public required init(coder: NSCoder)
        {
            self.m_mockedRecognizedText = "dummy"
            
            super.init()
        }
        
        public override var formattedString: String { m_mockedRecognizedText }
    }
}

///
/// AVSpeechSynthesizer mocks.
///
extension Mocks
{
    public final class MockedAVSpeechSynthesizer : AVSpeechSynthesizer, Traceable
    {
        private let m_dispatchQueue = DispatchQueue(label: "speechSynth.originware", qos: .background)

        weak public override var delegate: AVSpeechSynthesizerDelegate?
        {
            get { m_delegate }
            set(value) { m_delegate = value}
        }
        
        public var nextSequenceID : Int
        {
            if let currentSequenceID = m_currentSequenceID
            {
                m_currentSequenceID = currentSequenceID + 1
                
                return currentSequenceID + 1
            }
            else
            {
                m_currentSequenceID = 0
                
                return 0
            }
        }

        private var m_stopSequenceIDs = ContiguousArray<Int>()
        private var m_currentSequenceID : Int? = nil
        private var m_delegate : AVSpeechSynthesizerDelegate? = nil

        public override init()
        {
            super.init()

            trace(title: "Mocks.MockedAVSpeechSynthesizer", "Construct Service")

            m_dispatchQueue.async { Thread.current.name = "speechSynth" }
        }
        
        deinit
        {
            trace(title: "Mocks.MockedAVSpeechSynthesizer", "Destruct Service")
        }

        public override func speak(_ utterance: AVSpeechUtterance)
        {
            let sequenceID = nextSequenceID
            m_dispatchQueue.asyncAfter(deadline: .now() + 0.2, execute: { [weak self] in

                guard let strongSelf = self,
                      let delegate = strongSelf.m_delegate else { return }

                let string = utterance.speechString
                let range = string.startIndex..<string.endIndex

                string.enumerateSubstrings(in: range, options: .byWords) { (_, subStringRange, _, doStop) -> () in

                    guard let strongSelf2 = self,
                          let delegate2 = strongSelf2.m_delegate,
                          !strongSelf.m_stopSequenceIDs.contains(sequenceID) else
                    {
                        doStop = true
                        return
                    }
             
                    let start = string.distance(from: string.startIndex, to: subStringRange.lowerBound)
                    let length = string.distance(from: subStringRange.lowerBound, to: subStringRange.upperBound)
                    let nsRange = NSMakeRange(start, length)
          
                    delegate2.speechSynthesizer?(strongSelf2, willSpeakRangeOfSpeechString: nsRange, utterance: utterance)
                }

                if !strongSelf.m_stopSequenceIDs.contains(sequenceID)
                {
                    delegate.speechSynthesizer?(strongSelf, didFinish: utterance)
                }
                else
                {
                    strongSelf.m_stopSequenceIDs.removeAll()
                }
            })
        }

        public override func stopSpeaking(at boundary: AVSpeechBoundary) -> Bool
        {
            if let currentSequenceID = m_currentSequenceID
            {
                m_stopSequenceIDs.append(currentSequenceID)
            }
      
            delegate?.speechSynthesizer?(self, didFinish: AVSpeechUtterance(string: "dummy utterance"))
            
            return true
        }

        public override func pauseSpeaking(at boundary: AVSpeechBoundary) -> Bool
        {
            // do nothing
            
            true
        }

        public override func continueSpeaking() -> Bool
        {
            // do nothing

            true
        }
    }
}

///
/// AVSpeechSynthesizer mocks.
///
extension Mocks
{
    public final class MockedCLLocationManager : CLLocationManager, Traceable
    {
        private let m_dispatchQueue = DispatchQueue(label: "location.originware", attributes: .concurrent)

        weak public override var delegate: CLLocationManagerDelegate?
        {
            get { m_delegate }
            set(value) { m_delegate = value}
        }

        public override var authorizationStatus: CLAuthorizationStatus
        {
            .authorizedAlways
        }
        
        private var m_location : CLLocation
        private var m_authorization : CLAuthorizationStatus
        private var m_delegate : CLLocationManagerDelegate? = nil
        
        public init(authorization: CLAuthorizationStatus, location: CLLocation)
        {
            self.m_authorization = authorization
            self.m_location = location

            super.init()

            m_dispatchQueue.async { Thread.current.name = "location" }

            m_dispatchQueue.asyncAfter(deadline: .now() + 0.1, execute: { [weak self] in
            
                guard let strongSelf = self, let delegate = strongSelf.m_delegate else { return }

                delegate.locationManagerDidChangeAuthorization?(strongSelf)
            })

            trace(title: "Mocks.MockedCLLocationManager", "Construct Service")
        }

        deinit
        {
            trace(title: "Mocks.MockedCLLocationManager", "Destruct Service")
        }
        
        public override func requestLocation()
        {
            switch m_authorization
            {
                case .authorizedAlways, .authorizedWhenInUse:
                    
                    m_dispatchQueue.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
                    
                        guard let strongSelf = self, let delegate = strongSelf.m_delegate else { return }

                        delegate.locationManager?(strongSelf, didUpdateLocations: [strongSelf.m_location])
                    })
                    
                default:
                    
                    break
            }
        }
    }
}

///
/// Temporal mocks.
///
extension Mocks
{
    public static func getTime() -> ServiceActor.eServiceReply
    {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        if let date = dateFormatter.date(from: "2016-12-18 14:30:00")
        {
            return .eTime(date)
        }
        else
        {
            return .eFailure("Mock date failure")
        }
    }
}
