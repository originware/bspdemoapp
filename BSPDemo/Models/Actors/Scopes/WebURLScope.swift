
//
//  Models/Actors/Scopes/WebScope.swift
//  BSPDemo
//
//  Created by Terry Stillone on 6/3/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The WebURL Scope, fetches a given URL and presents it in a WebURLView.
///

public struct WebURLScope
{
    public enum eWebPresentProgressEvent
    {
        case eOnFetch(URL)
        case eDownloadingURL(URL)
        case eDownloadProgress(Double)
        case eOnPresent(URL)
        case eOnError(String)
    }

    struct ActorNotifierDirectory
    {
        struct Behaviour
        {
            fileprivate let webURL: NotifierDirectory.Behaviour.WebURL
            var stack : BehaviourActor.BehaviourStackNotifier    { m_notifierStore.behaviourStack }

            private let m_notifierStore : NotifierStore

            fileprivate init(notifierStore: NotifierStore)
            {
                self.webURL = NotifierDirectory.Behaviour.WebURL(notifierStore: notifierStore)
                self.m_notifierStore = notifierStore
            }
        }

        struct Presentation
        {
            fileprivate let webURL: NotifierDirectory.Presentation.WebURL
            var stack : PresentationActor.PresentationStackNotifier    { m_notifierStore.presentationStack }

            private let m_notifierStore : NotifierStore

            fileprivate init(notifierStore: NotifierStore)
            {
                self.webURL = NotifierDirectory.Presentation.WebURL(notifierStore: notifierStore)
                self.m_notifierStore = notifierStore
            }
        }

        let behaviour : Behaviour
        let presentation : Presentation

        private let m_notifierStore : NotifierStore

        fileprivate init(notifierStore: NotifierStore)
        {
            self.behaviour = Behaviour(notifierStore: notifierStore)
            self.presentation = Presentation(notifierStore: notifierStore)
            self.m_notifierStore = notifierStore
        }
    }

    public final class Behaviour : IScope
    {
        public typealias WebURLRequestNotifier         = Notifiers.AsyncNotifier<eWebRequest, Void>

        public static let WebURLRequestNotifierURI     = "/scope/webURL/behaviour/request"
        public static let WebURLScopeURI               = "/scope/webURL/behaviour"

        public enum eWebRequest
        {
            case ePresentURL(URL, (eWebPresentProgressEvent) -> Void)
        }

        public let uri          = Behaviour.WebURLScopeURI
        public let notifierURIs = [WebURLRequestNotifierURI]

        internal private(set) var scopeView: eScopeView? = nil
        internal private(set) var isActive               = false

        private let m_notifiers : ActorNotifierDirectory
        private var m_subscribers = Set<AnyCancellable>()
        private let m_presentationScope : Presentation

        init(notifierStore: NotifierStore)
        {
            self.m_notifiers = ActorNotifierDirectory(notifierStore: notifierStore)
            self.m_presentationScope = Presentation(notifierStore: notifierStore)
        }

        func activate()
        {
            guard !isActive else { return }

            if !m_presentationScope.isActive
            {
                m_notifiers.presentation.stack.send(.ePush(m_presentationScope))

                assert(m_notifiers.behaviour.webURL.request != nil, "WebURLScope presentation notifier not available")

                m_notifiers.behaviour.webURL.request?.sinkAsync(receiveValue: { [weak self] (notification) in

                   guard let strongSelf = self else { return }

                   switch notification.value
                   {
                       case .ePresentURL(let url, let progressFunc):

                           strongSelf.m_notifiers.presentation.webURL.request?.send(.ePresentURL(url, progressFunc))
                   }

               }).store(in: &m_subscribers)
            }

            isActive = true
        }

        func deactivate()
        {
            guard isActive else { return }

            isActive = false

            m_notifiers.presentation.stack.send(.ePop(m_presentationScope.uri))
        }
    }

    public final class Presentation : IScope, ObservableObject
    {
        public typealias WebURLRequestNotifier         = Notifiers.AsyncNotifier<eWebRequest, Void>

        public static let WebURLRequestNotifierURI     = "/scope/webURL/presentation/request"
        public static let WebURLScopeURI               = "/scope/webURL/presentation"
        
        public enum eWebRequest
        {
            case ePresentURL(URL, (eWebPresentProgressEvent) -> Void)
        }

        public let uri          = Presentation.WebURLScopeURI
        public let notifierURIs = [WebURLRequestNotifierURI]

        internal private(set) var isActive = false
        
        private let m_notifiers : ActorNotifierDirectory
        private var m_webViewModel = WebViewModel()
        private var m_subscribers = Set<AnyCancellable>()
        
        internal private(set) var scopeView: eScopeView? = nil

        fileprivate init(notifierStore: NotifierStore)
        {
            self.m_notifiers = ActorNotifierDirectory(notifierStore: notifierStore)
        }

        func activate()
        {
            guard !isActive else { return }

            m_notifiers.presentation.webURL.request?.sinkAsync(receiveValue: { [weak self] (notification) in

               guard let strongSelf = self else { return }

               switch notification.value
               {
                   case .ePresentURL(let url, let progressEventFunc):

                       for await webEvent in await strongSelf.m_webViewModel.load(url: url)
                       {
                           switch webEvent
                           {
                               case .eFetchingURL(let url):

                                   progressEventFunc(.eOnFetch(url))

                               case .eDownloadingURL:

                                   progressEventFunc(.eDownloadingURL(url))

                               case .eDownloadProgress(let progress):

                                   progressEventFunc(.eDownloadProgress(progress))

                               case .eLoadedURL:

                                   progressEventFunc(.eOnPresent(url))

                               case .eError(let error):
                                   
                                   let nsError = error as NSError
                                   
                                   if nsError.code == NSFileNoSuchFileError
                                       || nsError.code == CFNetworkErrors.cfHostErrorUnknown.rawValue
                                   {
                                       progressEventFunc(.eOnError("Sorry, network failure, I think I am not connected to the net."))
                                   }
                                   else
                                   {
                                       progressEventFunc(.eOnError("Alert: \(error.localizedDescription)"))
                                   }
                           }
                       }
               }

            }).store(in: &m_subscribers)

            let view = Views.WebWithDismissButtonView(webViewModel: m_webViewModel, notifierDirectory: m_notifiers, webContentHeight: 0)
            
            scopeView = .eWebView(view)
            isActive = true
        }

        func deactivate()
        {
            guard isActive else { return }

            isActive = false
            scopeView = nil
        }
    }
}


