
//
//  Models/Actors/Scopes/MessageScope.swift
//  BSPDemo
//
//  Created by Terry Stillone on 4/3/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import MessageUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Message scope, messages the current location. Obtains the current location from the ServiceActor and presents the location in a MessageComposeView
///

public struct MessageScope
{
    enum eMessageRequest
    {
        case ePresentMessageView
        case eDismissMessageView
    }

    struct MessageModel
    {
        public var subject:    String = ""
        public var recipients: [String]?
        public var body:       String = ""
    }

    fileprivate struct ActorNotifierDirectory
    {
        fileprivate struct Behaviour
        {
            fileprivate struct Meta
            {
                public var request : BehaviourActor.BehaviourMetaRequestNotifier    { m_notifierStore.metaBehaviourRequest }
                public var event : BehaviourActor.BehaviourMetaEventNotifier        { m_notifierStore.metaBehaviourEvent }

                private let m_notifierStore : NotifierStore

                fileprivate init(notifierStore : NotifierStore)
                {
                    self.m_notifierStore = notifierStore
                }
            }

            fileprivate let meta :           Meta
            fileprivate var stack : BehaviourActor.BehaviourStackNotifier    { m_notifierStore.behaviourStack }

            private let m_notifierStore : NotifierStore

            fileprivate init(notifierStore: NotifierStore)
            {
                self.meta = Meta(notifierStore: notifierStore)
                self.m_notifierStore = notifierStore
            }
        }

        fileprivate struct Presentation
        {
            fileprivate struct Message
            {
                fileprivate var event : MessageScope.Presentation.MessageEventNotifier? { m_notifierStore.presentationMessageEvent
                }

                private let m_notifierStore : NotifierStore
                
                fileprivate init(notifierStore: NotifierStore)
                {
                    self.m_notifierStore = notifierStore
                }
            }

            fileprivate var message : Message
            fileprivate var stack : PresentationActor.PresentationStackNotifier    { m_notifierStore.presentationStack }

            private let m_notifierStore : NotifierStore

            fileprivate init(notifierStore: NotifierStore)
            {
                self.message = Message(notifierStore: notifierStore)
                self.m_notifierStore = notifierStore
            }
        }

        fileprivate let behaviour : Behaviour
        fileprivate let presentation : Presentation

        fileprivate let m_notifierStore : NotifierStore

        fileprivate init(notifierStore: NotifierStore)
        {
            self.behaviour = Behaviour(notifierStore: notifierStore)
            self.presentation = Presentation(notifierStore: notifierStore)
            self.m_notifierStore = notifierStore
        }
    }

    final class Behaviour : IScope
    {
        public static let MessageScopeURI             = "/scope/message/behaviour"

        public let uri          = Behaviour.MessageScopeURI
        public let notifierURIs = [String]()
        
        private(set) var scopeView: eScopeView? = nil

        internal private(set) var isActive = false

        private let m_messageTemplate : MessageModel
        private let m_notifiers : ActorNotifierDirectory
        private var m_subscribers = Set<AnyCancellable>()
        private let m_presentationScope : Presentation

        init(messageTemplate : MessageModel, notifierStore: NotifierStore)
        {
            self.m_messageTemplate = messageTemplate
            self.m_notifiers = ActorNotifierDirectory(notifierStore: notifierStore)
            self.m_presentationScope = Presentation(messageTemplate: messageTemplate)
        }

        func activate()
        {
            if !m_presentationScope.isActive
            {
                m_notifiers.presentation.stack.send(.ePush(m_presentationScope))

                assert(m_notifiers.presentation.message.event != nil, "MessageScope presentation notifier not available")

                m_notifiers.presentation.message.event?.sinkAsync(receiveValue: { [weak self] (notification) in

                   guard let strongSelf = self else { return }

                   func dismissPresentation()
                   {
                       Task {
                           await MainActor.run {
                               strongSelf.m_notifiers.behaviour.stack.send(.ePop(MessageScope.Behaviour.MessageScopeURI))
                           }
                       }
                   }

                   switch notification.value
                   {
                       case .eMessageResult(let result):

                           switch result
                           {
                               case .cancelled:

                                   strongSelf.m_notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: 0.1, .eLowPriority))))
                                   dismissPresentation()

                               case .sent:

                                   dismissPresentation()
                                   strongSelf.m_notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: 0.1, .eLowPriority))))

                                   // Speak after the vocal cancellation window.
                                   try? await Task.sleep(nanoseconds: 1_000_000_000)
                                   await strongSelf.m_notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak("Message sent", isAlert: true))))

                               case .failed:

                                   strongSelf.m_notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: nil, .eLowPriority))))

                                   // Speak after the vocal cancellation window.
                                   try? await Task.sleep(nanoseconds: 1_000_000_000)
                                   await strongSelf.m_notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak("Message failed", isAlert: true))))

                               @unknown default:

                                   await strongSelf.m_notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak("Message UI gave an unknown result", isAlert: true))))
                           }
                   }

               }).store(in: &m_subscribers)
            }

            isActive = true
        }

        func deactivate()
        {
            isActive = false
            m_notifiers.presentation.stack.send(.ePop(m_presentationScope.uri))
        }
    }

    public final class Presentation : IScope, ObservableObject
    {
        public typealias MessageEventNotifier         = Notifiers.AsyncNotifier<eMessageEvent, Void>

        public static let MessageEventNotifierURI     = "/scope/message/presentation/event"
        public static let MessageScopeURI             = "/scope/message/presentation"

        public enum eMessageEvent
        {
            case eMessageResult(MessageComposeResult)
        }

        public let uri            = Presentation.MessageScopeURI
        public let notifierURIs   = [MessageEventNotifierURI]

        internal private(set) var isActive = false
        
        @Published internal private(set) var scopeView: eScopeView? = nil

        private let m_messageTemplate : MessageModel

        fileprivate init(messageTemplate: MessageModel)
        {
            self.m_messageTemplate = messageTemplate
        }

        func activate()
        {
            scopeView = .eMessageView(Views.MessageView(messageModel: m_messageTemplate))
            isActive = true
        }

        func deactivate()
        {
            isActive = false
            scopeView = nil
        }
    }
}

