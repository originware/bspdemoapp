
//
//  Models/Actors/Scopes/Scope.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/3/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IScope, the base protocol for Scopes.
///

protocol IScope : IActor
{
    // Indicator that the scope is Active
    var isActive : Bool         { get }

    // The view associated with this scope, is only valid for presentation scopes.
    var scopeView: eScopeView?  { get }

    // Activate the scope.
    func activate()

    // Deactivate the scope.
    func deactivate()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The scope stack.
///

class ScopeStack : IActor, ObservableObject
{
    enum eRequest
    {
        case ePush(IScope)
        case ePop(String)
    }
    
    public var top : IScope?                    { stack.last }
    public var topIndex : Int                   { stack.count }

    public let          uri : String
    public let notifierURIs : [String]

    @Published public var topView = eScopeView.eGraph
    public private(set) var stack = ContiguousArray<IScope>()

    private let m_notifiers : NotifierDirectory

    public init(uri: String, notifierDirectory: NotifierDirectory)
    {
        self.uri = uri
        self.notifierURIs = [uri]
        self.m_notifiers = notifierDirectory
    }

    public func push(scope: IScope)
    {
        if !scope.isActive
        {
            // Set the resident notifiers for the scope into the NotifierStore
            for notifierURI in scope.notifierURIs
            {
                m_notifiers.notifierStore.push(uri: notifierURI)
            }
        }
        
        // Push the scope onto the stack.
        stack.append(scope)

        if !scope.isActive
        {
            scope.activate()
        }
    }

    public func pop(uri: String)
    {
        guard let topScope = top, topScope.uri == uri else { return }

        // Pop the scope from the stack.
        stack.removeLast()

        if topScope.isActive
        {
            topScope.deactivate()
        }

        // Remove the resident notifiers for the scope from the NotifierStore
        for notifierURI in topScope.notifierURIs
        {
            m_notifiers.notifierStore.pop(uri: notifierURI)
        }
    }

    public func has(uri: String) -> Bool
    {
        stack.contains(where: { $0.uri == uri })
    }

    public func hasTop(uri: String) -> Bool
    {
        top?.uri == uri
    }
}