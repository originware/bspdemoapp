
//
//  Models/Actors/BehaviourActor.swift
//  BSPDemo
//
//  Created by Terry Stillone on 20/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import PencilKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Behaviour Actor
///

public final class BehaviourActor: IActor, Traceable
{
    public typealias BehaviourMetaRequestNotifier   = Notifiers.AsyncNotifier<BehaviourActor.eBehaviourMetaRequest, Void>
    public typealias BehaviourMetaEventNotifier     = Notifiers.AsyncNotifier<BehaviourActor.eBehaviourMetaEvent, Void>
    internal typealias BehaviourStackNotifier       = Notifiers.SyncNotifier<ScopeStack.eRequest, Void>
    public typealias RecognitionStats               = eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.RecognitionStats

    public static let ActorURI          = "/actor/behaviour"
    public static let BehaviourStackURI = "/actor/behaviour/stack"

    public static let BehaviourMetaRequestURI = "/actor/behaviour/notifier/meta/request"
    public static let BehaviourMetaEventURI   = "/actor/behaviour/notifier/meta/event"
    public static let AutomationRequestURI    = "/actor/behaviour/automation/request"
    
    ///
    /// The Behaviour Internal Events
    ///

    public enum eBehaviourMetaEvent
    {
        case eBehaviourCycleEvent(eBehaviourCycle)
        case eFacilityEvent(eFacilityStatusEvent)
        case ePresentationEvent(PresentationActor.ePresentationEvent)
        
        public enum eBehaviourCycle
        {
            case eOnHostingCycleChange(ServiceActor.eServiceEvent.eAppStatusChange)
            case eOnOperationalCycleChange(eOperationalCycle)

            public enum eFacilityCycle
            {
                case eSpeechRecognition(ServiceActor.ServiceDirectory.eSpeechRecognitionStatus)
            }

            public enum eOperationalCycle
            {
                case eRecognitionCycle(eRecognitionCycleEvent)
                case eResponseCycle(eResponseCycleEvent)

                public enum eRecognitionCycleEvent
                {
                    case eBeginListening
                    case eEndListening
                }

                public enum eResponseCycleEvent
                {
                    public struct VocalResult : Equatable
                    {
                        public let vocalOps: ContiguousArray<SpeechSynthController.eVocalOp>
                        public let isAlert : Bool

                        public init(_ vocalOp : SpeechSynthController.eVocalOp, isAlert: Bool = false)
                        {
                            self.vocalOps = [vocalOp]
                            self.isAlert = isAlert
                        }

                        public init(_ text : String, isAlert: Bool = false)
                        {
                            self.vocalOps = [.eSpeak(text)]
                            self.isAlert = isAlert
                        }

                        public init(vocalOps : ContiguousArray<SpeechSynthController.eVocalOp>, isAlert: Bool = false)
                        {
                            self.vocalOps = vocalOps
                            self.isAlert = isAlert
                        }
                    }
                    
                    public enum eResult : Equatable
                    {
                        case eSuccess(VocalResult?)
                        case eFailure(String)
                    }
                    
                    public struct RecognitionStats : CustomStringConvertible
                    {
                        public let recognitionDuration: TimeInterval

                        public var description : String
                        {
                            "RecognitionStats(recognitionDuration: \(recognitionDuration))"
                        }
                    }

                    case eBegin(command: String, stats: RecognitionStats?)
                    case eEnd(result: eResult)
                }
            }
        }

        public enum eFacilityStatusEvent
        {
            case eSpeechRecognitionAvailabilityChange(ServiceActor.ServiceDirectory.eSpeechRecognitionStatus)
        }
    }

    ///
    /// The Behaviour Internal Requests
    ///

    public enum eBehaviourMetaRequest
    {
        case eAvatar(eAvatarRequest)

        public enum eAvatarRequest
        {
            case eSpeaking(eSpeakingRequest)
            case eGesturing(AvatarBehaviourModel.eAvatarGestureOp)
        }

        public enum eSpeakingRequest
        {
            case eSpeak(String, isAlert: Bool)
            case eCancelSpeaking(forDuration: TimeInterval? = 0.4, ServiceActor.eSpeechSynthRequest.eCancelPriority)
        }
    }

    ///
    /// The State of the Behaviour Actor
    ///

    struct State
    {
        public var appIsFunctional = false
        public var haveAuthorizedServices = false

        public var appActiveSinceTimestamp : Date? = nil
        public var appInactiveSinceTimestamp : Date? = nil

        public var enableAnnouncements : Bool
    }

    ///
    /// The Actor Directory, access to notifiers and the behaviour stack state.
    ///

    final class ActorDirectory
    {
        let notifiers : NotifierDirectory
        let scopeStack: ScopeStack?

        fileprivate init(notifierDirectory: NotifierDirectory, stack : ScopeStack?)
        {
            self.notifiers = notifierDirectory
            self.scopeStack = stack
        }
    }

    public let uri          = BehaviourActor.ActorURI
    public let notifierURIs = [String]()

    var state:                    State
    let scopeStack:               ScopeStack?
    let commandBehaviourModel:    CommandBehaviourModel
    let avatarBehaviourModel:     AvatarBehaviourModel
    let automationBehaviourModel: AutomationBehaviourModel

    private let m_directory:                            ActorDirectory
    private let m_facilityAvailabilityBehaviourModel:   FacilityAvailabilityBehaviourModel

    private var m_subscribers                       = Set<AnyCancellable>()
    
    init(appBuilder: IAppBuilder)
    {
        let notifierDirectory                       = appBuilder.notifierDirectory
        let actorDirectory                          = ActorDirectory(notifierDirectory: notifierDirectory, stack: appBuilder.behaviourScopeStack)

        self.state                                  = State(enableAnnouncements: appBuilder.config.enableAnnouncements)
        self.scopeStack                             = appBuilder.behaviourScopeStack

        self.m_directory                            = actorDirectory

        self.commandBehaviourModel                  = CommandBehaviourModel(appBuilder: appBuilder, actorDirectory: actorDirectory)
        self.avatarBehaviourModel                   = AvatarBehaviourModel(notifierDirectory: notifierDirectory)
        self.automationBehaviourModel               = AutomationBehaviourModel(appBuilder: appBuilder)
        self.m_facilityAvailabilityBehaviourModel   = FacilityAvailabilityBehaviourModel(notifierDirectory: notifierDirectory)

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Stack Requests.
        //

        m_directory.notifiers.behaviour.stack.sink(receiveValue: { [weak self] (notification) in

           guard let strongSelf = self else { return }

           switch notification.value
           {
               case .ePush(let scope):

                   strongSelf.scopeStack?.push(scope: scope)

               case .ePop(let uri):

                   strongSelf.scopeStack?.pop(uri: uri)
           }

        }).store(in: &m_subscribers)

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Internal Behaviour Events.
        //

        m_directory.notifiers.behaviour.meta.event.sinkAsync(receiveValue: { [weak self] (notification) in

           guard let strongSelf = self else { return }

           let notifiers               = strongSelf.m_directory.notifiers
           let avatarBehaviourModel    = strongSelf.avatarBehaviourModel

           switch notification.value
           {
               case .eBehaviourCycleEvent(let behaviourCycleEvent):

                   switch behaviourCycleEvent
                   {
                       case .eOnHostingCycleChange(let hostingEvent):

                           switch hostingEvent
                           {
                               case .eOnAppActive:

                                   strongSelf.state.appActiveSinceTimestamp = Date()

                                   if !strongSelf.state.haveAuthorizedServices
                                   {
                                       strongSelf.state.haveAuthorizedServices = true

                                       notifiers.service.request.send(.eAuthorizeServices)
                                   }

                               case .eOnAppInActive:

                                   notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: nil, .eImmediatePriority))))

                                   strongSelf.state.appInactiveSinceTimestamp = Date()
                                   strongSelf.state.appIsFunctional = false
                           }

                       case .eOnOperationalCycleChange(let operationalCycleEvent):

                           switch operationalCycleEvent
                           {
                               case .eRecognitionCycle(.eBeginListening):

                                   // Terminate any current vocalisations.
                                   await avatarBehaviourModel.cancelSpeaking(forDuration: nil, priority: .eImmediatePriority)
                                   await avatarBehaviourModel.perform(.eBeginListening())

                               case .eRecognitionCycle(.eEndListening):

                                   await avatarBehaviourModel.perform(.eEndListening())

                               case .eResponseCycle(.eBegin(let vocalCommand, let stats)):

                                   let tag = AttributedString("Recognized: ", attributes: Styles.annotationTextStyle)

                                   await notifiers.presentation.request.sendAsync(.eSetTextField(.eRecognitionTextField(tag + AttributedString(vocalCommand, attributes: Styles.bodyTextStyle))))

                                   do { // Process command cycle

                                       let result = await strongSelf.commandBehaviourModel.getResponse(vocalCommand: vocalCommand, recogStats: stats)

                                       await notifiers.behaviour.meta.event.sendAsync(.eBehaviourCycleEvent(.eOnOperationalCycleChange(.eResponseCycle(.eEnd(result: result)))))
                                   }

                               case .eResponseCycle(.eEnd(let result)):

                                   switch result
                                   {
                                       case .eSuccess(let vocalResult):

                                           if let vocalResult = vocalResult
                                           {
                                               for vocalOp in vocalResult.vocalOps
                                               {
                                                   switch vocalOp
                                                   {
                                                       case .eSpeak(let vocalText, _):

                                                           await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak(vocalText, isAlert: vocalResult.isAlert))))

                                                       case .ePause(let interval):

                                                           await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eGesturing(.ePause(duration: interval))))
                                                                          
                                                       case .eApply:

                                                           // ignore
                                                           break
                                                   }
                                               }
                                           }

                                       case .eFailure(let failure):

                                           await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak(failure, isAlert: true))))
                                   }
                           }
                   }

               case .eFacilityEvent(.eSpeechRecognitionAvailabilityChange(let speechRecognitionStatus)):

                   switch speechRecognitionStatus
                   {
                       case .eOnlineOperation, .eOfflineOperation:

                           if !strongSelf.state.appIsFunctional
                           {
                               strongSelf.state.appIsFunctional = true
                           }

                           if strongSelf.state.enableAnnouncements
                           {
                               func doAnnounce() -> Bool
                               {
                                   guard let appInactiveSinceTimestamp = strongSelf.state.appInactiveSinceTimestamp else { return true }

                                   return -appInactiveSinceTimestamp.timeIntervalSinceNow > 60
                               }

                               if doAnnounce()
                               {
                                   // The Avatar assembles itself and then announces itself.
                                   try? await Task.sleep(nanoseconds: 1_000_000_000)
                                   await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eGesturing(.eSaluteAndVocalize(vocalize: "Agent online"))))
                               }

                               if UserDefaults.standard.object(forKey: "Read the README") == nil
                               {
                                   UserDefaults.standard.set(true, forKey: "Read the README")

                                   try? await Task.sleep(nanoseconds: 1_000_000_000)

                                   await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eGesturing(.eSpeakAndVocalize(vocalize: "By the way, please read the README on how to operate this app.", isAlert: true))))
                               }
                           }

                       case .eIsNotAvailable(let failure):

                           await avatarBehaviourModel.perform(.eSpeakAndVocalize(vocalize: "Sorry, speech recognition is not functional: \(failure)", isAlert: true))

                           try? await Task.sleep(nanoseconds: 2_000_000_000)

                           await avatarBehaviourModel.perform(.eSpeakAndVocalize(vocalize: "This app cannot operate until speech recognition is available", isAlert: true))

                       case .eIsNotReady:

                           await avatarBehaviourModel.perform(.eSpeakAndVocalize(vocalize: "Sorry, speech recognition is not ready yet", isAlert: true))

                           try? await Task.sleep(nanoseconds: 2_000_000_000)

                           await avatarBehaviourModel.perform(.eSpeakAndVocalize(vocalize: "This app cannot operate until speech recognition is available", isAlert: true))
                   }

               case .ePresentationEvent(let presentationEvent):

                   switch presentationEvent
                   {
                       case .eOnListenButtonEvent(let listenButtonEvent):

                           switch listenButtonEvent
                           {
                               case .ePressDown:

                                   notifiers.service.request.send(.eSpeechRecognition(.eBeginListening))

                               case .ePressUp:

                                   notifiers.service.request.send(.eSpeechRecognition(.eEndListening))
                           }

                       case .eOnCancelButtonPressed:

                           notifiers.behaviour.meta.request.send(.eAvatar(.eGesturing(.eEndListening())))
                           notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(.eImmediatePriority))))

                           notifiers.presentation.request.send(.ePencil(.eClearDrawing))

                       case .eOnAutomationRequest(let request):

                           strongSelf.automationBehaviourModel.onAutomationRequest(request: request)
                   }
           }

       }).store(in: &m_subscribers)


        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Internal Behaviour Requests.
        //

        m_directory.notifiers.behaviour.meta.request.sinkAsync(receiveValue: { [weak self] (notification) in

            guard let strongSelf = self else { return }

            let avatarBehaviourModel    = strongSelf.avatarBehaviourModel

            switch notification.value
            {
                case .eAvatar(let avatarRequest):

                    switch avatarRequest
                    {
                        case .eSpeaking(let speakingRequest):

                            switch speakingRequest
                            {
                                case .eSpeak(let vocalText, isAlert: let isAlert):

                                    await avatarBehaviourModel.perform(.eSpeakAndVocalize(vocalize: vocalText, isAlert: isAlert))

                                case .eCancelSpeaking(let forDuration, let priority):

                                    await avatarBehaviourModel.cancelSpeaking(forDuration: forDuration, priority: priority)
                            }

                        case .eGesturing(let gestureRequest):

                            await avatarBehaviourModel.perform(gestureRequest)
                    }
            }

        }).store(in: &m_subscribers)

        
        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Service Event notifications.
        //

        m_directory.notifiers.service.event.sink(receiveValue: { [weak self] (notification) in
                    
            guard let strongSelf = self else { return }
            
            let notifiers = strongSelf.m_directory.notifiers
            
            switch notification.value
            {
                case .eOnAppStatusChange(let appStatusEvent):

                    notifiers.behaviour.meta.event.send(.eBehaviourCycleEvent(.eOnHostingCycleChange(appStatusEvent)))

                case .eOnSpeechRecognition(let recognitionEvent):
                    
                    switch recognitionEvent
                    {
                        case .eBeginListening:

                            notifiers.behaviour.meta.event.send(.eBehaviourCycleEvent(.eOnOperationalCycleChange(.eRecognitionCycle(.eBeginListening))))
    
                        case .eEndListening:

                            notifiers.behaviour.meta.event.send(.eBehaviourCycleEvent(.eOnOperationalCycleChange(.eRecognitionCycle(.eEndListening))))
          
                        case .eRecognized(let recognizedText, let stats):

                            notifiers.behaviour.meta.event.send(.eBehaviourCycleEvent(.eOnOperationalCycleChange(.eResponseCycle(.eBegin(command: recognizedText, stats: stats)))))
                     
                        case .eRecognitionFailed(let error):

                            Task {

                                await notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak(error, isAlert: true))))
                            }
                    }
                    
                case .eOnServiceAvailabilityChange(let facilityStatus):
                    
                    strongSelf.m_facilityAvailabilityBehaviourModel.onFacilityChange(facilityStatus: facilityStatus)
            }
            
        }).store(in: &m_subscribers)

        
        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Presentation Event notifications.
        //
        //      These events are routed to the internal behaviour event notifier.
        //

        m_directory.notifiers.presentation.event.sink(receiveValue: { [weak self] (notification) in
                    
            guard let strongSelf = self else { return }

            let notifiers = strongSelf.m_directory.notifiers

            // Route presentation events to the behaviour event handling.
            notifiers.behaviour.meta.event.send(.ePresentationEvent(notification.value))
            
        }).store(in: &m_subscribers)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
//  CustomStringConvertible conformance.
//

//            case .eFacilityStatus(let request):     return "eFacilityStatus(\(request))"

extension BehaviourActor.eBehaviourMetaRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eAvatar(let request):             return "eAvatar(\(request))"
        }
    }
}

extension BehaviourActor.eBehaviourMetaRequest.eAvatarRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eSpeaking(let request):       return "eSpeaking(\(request))"
            case .eGesturing(let gestureOp):    return "eGesturing(\(gestureOp))"
        }
    }
}

extension BehaviourActor.eBehaviourMetaRequest.eSpeakingRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eSpeak(let vocalText, isAlert: let isAlert):
                
                return "eSpeak(\(vocalText), isAlert: \(isAlert))"
                
            case .eCancelSpeaking:
                
                return "eCancelSpeaking"
        }
    }
}

extension BehaviourActor.eBehaviourMetaEvent.eFacilityStatusEvent : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eSpeechRecognitionAvailabilityChange(let status):

                return "eSpeechRecognitionAvailabilityChange(\(status))"
        }
    }
}

extension BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eOnHostingCycleChange(let cycle):         return "eHostingLifeCycle(\(cycle))"
            case .eOnOperationalCycleChange(let cycle):     return "eOperationalCycle(\(cycle))"
        }
    }
}

extension BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eRecognitionCycle(let recognitionCycle):    return "eRecognitionCycle(\(recognitionCycle))"
            case .eResponseCycle(let responseCycle):          return "eResponseCycle(\(responseCycle))"
        }
    }
}

extension BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eRecognitionCycleEvent : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eBeginListening:   return "eBeginListening"
            case .eEndListening:     return "eEndListening"
        }
    }
}

extension BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eBegin(let command, let stats):   return "eBegin(\(command), \(String(describing: stats))"
            case .eEnd(let result):                 return "eEnd(\(result))"
        }
    }
}


extension BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eFacilityCycle : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eSpeechRecognition(let status):  return "eSpeechRecognition(\(status))"
        }
    }
}
