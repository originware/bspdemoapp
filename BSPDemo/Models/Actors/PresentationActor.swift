
//
//  Models/Actors/PresentationActor.swift
//  BSPDemo
//
//  Created by Terry Stillone on 20/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import MessageUI
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Presentation Actor
///

public class PresentationActor: IActor, ObservableObject, Traceable
{
    public typealias PresentationRequestNotifier = Notifiers.AsyncNotifier<PresentationActor.ePresentationRequest, Void>
    public typealias PresentationEventNotifier   = Notifiers.SyncNotifier<PresentationActor.ePresentationEvent, Void>
    internal typealias PresentationStackNotifier = Notifiers.SyncNotifier<ScopeStack.eRequest, Void>

    public typealias eResult                     = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult

    public static let ActorURI                   = "/actor/presentation"
    public static let PresentationStackURI       = "/actor/presentation/stack"

    public static let PresentationRequestURI     = "/actor/presentation/notifier/request"
    public static let PresentationEventURI       = "/actor/presentation/notifier/event"
    
    ///
    /// The Presentation Actor Requests
    ///

    public enum ePresentationRequest
    {
        case eAvatarGesture(eGestureRequest)
        case eSetTextField(eTextFields)
        case eGraph(ContiguousArray<GraphViewModel.Node>, ContiguousArray<GraphViewModel.Edge>)
        case ePencil(ePencilRequest)
        case eAutomation(eAutomationRequest)

        public enum eGestureRequest
        {
            case ePerformGestureAnimation(duration: TimeInterval, fromGesture: eAvatarGestureAnimationOp, toGesture: eAvatarGestureAnimationOp)
            case eCancelGestures
        }
        
        public enum eTextFields
        {
            case eRecognitionTextField(AttributedString)
            case eResponseTextField(AttributedString)
        }
         
        public enum ePencilRequest
        {
            case ePresentControls((eResult) -> Void)
            case eDismissControls((eResult) -> Void)
            case eClearDrawing
        }

        public enum eWebURLRequest
        {
            case ePresentURL(URL)
            case eDismiss
        }
         
        public enum eAutomationRequest
        {
            case eClearRequest
            case eClearReply
            case eReply(String)
        }
    }
    
    ///
    /// The Presentation Actor Events
    ///

    public enum ePresentationEvent
    {
        case eOnListenButtonEvent(eListenButtonEvent)
        case eOnCancelButtonPressed
        case eOnAutomationRequest(String)
      
        public enum eListenButtonEvent
        {
            case ePressDown
            case ePressUp
        }
    }

    //
    // The notifiers that the Presentation Actor is allowed to access
    //

    class ActorNotifierDirectory
    {
        let presentation : NotifierDirectory.Presentation

        fileprivate init(notifierStore : NotifierStore)
        {
            self.presentation = NotifierDirectory.Presentation(notifierStore: notifierStore)
        }
    }

    public let uri          = PresentationActor.ActorURI
    public let notifierURIs = [PresentationRequestURI, PresentationEventURI]

    let mainViewModel : MainViewModel
    var scopeStack: ScopeStack?

    // Non-scoped view models.
    let avatarUIView : AvatarUIView
    let graphViewModel                       = GraphViewModel()
    let graphRenderPipeline                  = GraphRenderPipeline()
    let pencilCanvasModel                    = PencilCanvasViewModel()
    var automationViewModel : AutomationViewModel?

    let notifiers:               ActorNotifierDirectory
    
    private let m_avatarAnimationModel: AvatarAnimationViewModel
    private var m_messageViewController : MFMessageComposeViewController? = nil
    private var m_subscribers = Set<AnyCancellable>()

    init(appBuilder: IAppBuilder, mainViewModel : MainViewModel)
    {
        let notifierDirectory = appBuilder.notifierDirectory
        let avatarUIView = AvatarUIView(frame: .zero)
        
        self.mainViewModel = mainViewModel
        self.scopeStack   = appBuilder.presentationScopeStack
        self.avatarUIView = avatarUIView

        self.notifiers = ActorNotifierDirectory(notifierStore: notifierDirectory.notifierStore)
        self.m_avatarAnimationModel = AvatarAnimationViewModel(avatarUIView: avatarUIView)

        self.automationViewModel = appBuilder.config.testingConfig?.showAutomationView == true ? AutomationViewModel(appBuilder: appBuilder) : nil

        Task {

            await MainActor.run {
                self.m_messageViewController = MFMessageComposeViewController()
            }
        }

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Stack Requests.
        //

        notifiers.presentation.stack.sink(receiveValue: { [weak self] (notification) in

           guard let strongSelf = self else { return }

           switch notification.value
           {
               case .ePush(let scope):

                   guard let scopeStack = strongSelf.scopeStack else { return }

                   scopeStack.push(scope: scope)

                   if !scope.isActive
                   {
                       scope.activate()
                   }

                   scopeStack.topView = scope.scopeView ?? .eGraph
                   
               case .ePop(let scopeURI):

                   guard let scopeStack = strongSelf.scopeStack, let topScope = scopeStack.top, topScope.uri == scopeURI else { return }

                   scopeStack.pop(uri: scopeURI)

                   scopeStack.topView = scopeStack.top?.scopeView ?? .eGraph
           }

        }).store(in: &m_subscribers)

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Presentation Service requests.
        //

        notifiers.presentation.request.sinkAsync(receiveValue: { [weak self] (notification) in
            
            guard let strongSelf = self else { return }

            switch notification.value
            {
                case .eAvatarGesture(let gestureRequest):

                    switch gestureRequest
                    {
                        case .ePerformGestureAnimation(let duration, let fromGesture, let toGesture):

                            await strongSelf.m_avatarAnimationModel.performAsync(duration: duration, fromGesture: fromGesture, toGesture: toGesture)

                        case .eCancelGestures:

                            await strongSelf.m_avatarAnimationModel.cancelGestures()
                    }

                case .eSetTextField(let setTextFieldRequest):

                    await MainActor.run {

                        switch setTextFieldRequest
                        {
                            case .eRecognitionTextField(let attributedText):

                                strongSelf.mainViewModel.recognitionText = attributedText

                            case .eResponseTextField(let attributedText):

                                strongSelf.mainViewModel.responseText = attributedText
                        }
                    }

                case .eGraph(let nodes, let edges):

                    await MainActor.run {

                        strongSelf.graphViewModel.clear()

                        for node in nodes
                        {
                            strongSelf.graphViewModel.add(node: node)
                        }

                        for edge in edges
                        {
                            strongSelf.graphViewModel.add(edge: edge)
                        }


                        let _ = strongSelf.graphRenderPipeline.request(graphViewModel: strongSelf.graphViewModel)
                    }

                case .ePencil(let pencilRequest):

                    await MainActor.run {

                        switch pencilRequest
                        {
                            case .ePresentControls(let progressFunc):

                                strongSelf.pencilCanvasModel.presentPencilPicker(enable: true, progressFunc: progressFunc)

                            case .eDismissControls(let progressFunc):

                                strongSelf.pencilCanvasModel.presentPencilPicker(enable: false, progressFunc: progressFunc)

                            case .eClearDrawing:

                                strongSelf.pencilCanvasModel.clearDrawing()
                        }
                    }
                     
                case .eAutomation(let request):
                    
                    switch request
                    {
                        case .eClearRequest:
                            
                            strongSelf.automationViewModel?.automationRequest.wrappedValue = ""
                            
                        case .eClearReply:
                            
                            strongSelf.automationViewModel?.automationReply.wrappedValue = ""
                            
                        case .eReply(let replyText):
                 
                            strongSelf.automationViewModel?.automationReply.wrappedValue = replyText
                    }
            }
            
        }).store(in: &m_subscribers)
    }

    public func createMFMessageComposeViewController() -> MFMessageComposeViewController?
    {
        let currentMessageViewController = m_messageViewController
        
        m_messageViewController = nil
         
        return currentMessageViewController
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
//  CustomStringConvertible conformance.
//

extension PresentationActor.ePresentationRequest: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eAvatarGesture(let request):              return "eGestureAnimation(\(request))"
            case .eSetTextField(let request):               return "eSet(\(request))"
            case .eGraph:                                   return "eGraph"
            case .ePencil(let request):                     return "ePencil(\(request))"
            case .eAutomation(let request):                 return "eAutomation(\(request))"
        }
    }
}

extension PresentationActor.ePresentationRequest.eGestureRequest: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .ePerformGestureAnimation(let duration, let fromGesture, let toGesture):

                return "ePerformAnimation(duration: \(duration), fromGesture: \(fromGesture), toGesture: \(toGesture))"

            case .eCancelGestures:

                return "eCancelGestures"
        }
    }
}

extension PresentationActor.ePresentationRequest.eTextFields: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eRecognitionTextField(let attributedString):
             
                return "eRecognitionTextField(\(attributedString))"

            case .eResponseTextField(let attributedString):
              
                return "eResponseTextField(\(attributedString))"
        }
    }
}

extension PresentationActor.ePresentationRequest.ePencilRequest: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .ePresentControls:     return "ePresentControls"
            case .eDismissControls:     return "eDismissControls"
            case .eClearDrawing:        return "eClearDrawing"
        }
    }
}

extension PresentationActor.ePresentationRequest.eWebURLRequest: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .ePresentURL(let url): return "ePresent(\(url))"
            case .eDismiss:             return "eDismiss"
        }
    }
}

extension PresentationActor.ePresentationEvent: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eOnListenButtonEvent(let event):                  return "eOnListenButton(\(event))"
            case .eOnCancelButtonPressed:                           return "eOnCancelButtonPressed"
            case .eOnAutomationRequest(let request):                return "eOnAutomationRequest(\(request))"
        }
    }
}

extension PresentationActor.ePresentationEvent.eListenButtonEvent: CustomStringConvertible
{  
    public var description: String
    {
        switch self
        {
            case .ePressDown:               return "ePressDown"
            case .ePressUp:                 return "ePressUp"
        }
    }
}
