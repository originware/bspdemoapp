
//
//  Models/Actors/ServiceActor.swift
//  BSPDemo
//
//  Created by Terry Stillone on 20/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import MessageUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Service Actor
///

public class ServiceActor: IActor, Traceable
{
    public typealias ServiceRequestNotifier     = Notifiers.SyncNotifier<ServiceActor.eServiceRequest, eServiceReply>
    public typealias ServiceEventNotifier       = Notifiers.SyncNotifier<ServiceActor.eServiceEvent, Void>
    public typealias QueuedSpeechSynthNotifier  = Notifiers.QueuedNotifier<eSpeechSynthRequest, Void>

    public static let ActorURI              = "/actor/service"

    public static let ServiceRequestURI     = "/actor/service/notifier/request"
    public static let ServiceEventURI       = "/actor/service/notifier/event"
    public static let ServiceSpeechSynthURI = "/actor/service/notifier/speechSynthRequest"
    
    ///
    /// The Service Actor Requests
    ///

    public enum eServiceRequest
    {
        case eAuthorizeServices
        case eSpeechRecognition(eSpeechRecognitionRequest)
        case eSpeechSynth(eSpeechSynthRequest)
        case eLocation(eLocationRequest)
        case eTemporal(eTemporalRequest)
        
        public enum eSpeechRecognitionRequest
        {
            case eBeginListening
            case eEndListening
        }
        
        public enum eLocationRequest
        {
            case eGetCurrentAddress
        }

        public enum eTemporalRequest
        {
            case eGetTime
        }
    }
    
    ///
    /// The Service Actor Request Replies
    ///
    
    public enum eServiceReply
    {
        case eAddress(String)
        case eTime(Date)
        case eFailure(String)
        case eAcknowledge
    }
    
    ///
    /// The Service Actor Events
    ///
    
    public enum eServiceEvent
    {
        case eOnAppStatusChange(eAppStatusChange)
        case eOnSpeechRecognition(eSpeechRecognitionEvent)
        case eOnServiceAvailabilityChange(ServiceActor.ServiceDirectory.eFacilityStatus)
        
        public enum eAppStatusChange
        {
            case eOnAppActive
            case eOnAppInActive
        }
        
        public enum eSpeechRecognitionEvent
        {
            case eBeginListening
            case eEndListening
            case eRecognized(String, BehaviourActor.RecognitionStats?)
            case eRecognitionFailed(String)
        }
        
        public enum eSpeechVocalizationEvent
        {
            case eOnUtterance(SpeechSynthService.eInteraction, AttributedString, Range<AttributedString.Index>?)
            case eEndSpeaking(SpeechSynthService.eInteraction, AttributedString)
            case eCancelSpeaking(SpeechSynthService.eInteraction, AttributedString)
        }
    }

    ///
    /// Directory of Service Facilities.
    ///
    
    public class ServiceDirectory : Traceable
    {
        public enum eFacility : String
        {
            public static let SpeechRecogDependentFacilities = Set<eFacility>([.eMicrophone, .eSpeechRecognitionService, .eReachabilityService])
            
            case eMicrophone                = "Microphone"
            case eSpeechRecognitionService  = "Speech Recognition Service"
            case eSpeechSynthService        = "Speech Synthesis Service"
            case eReachabilityService       = "Reachability Service"
            case eLocationService           = "Location Service"
        }
      
        public enum eSpeechRecognitionStatus: Equatable
        {
            case eOnlineOperation(String?)
            case eOfflineOperation
            case eIsNotAvailable(String)
            case eIsNotReady
            
            public var isAvailable : Bool
            {
                switch self
                {
                    case .eIsNotReady, .eIsNotAvailable:            return false
                    case .eOnlineOperation, .eOfflineOperation:     return true
                }
            }
        }

        public enum eMicrophoneStatus: Equatable
        {
            case eIsAvailable
            case eFailure(String)
            
            public var isAvailable : Bool
            {
                switch self
                {
                    case .eIsAvailable:                             return true
                    case .eFailure:                                 return false
                }
            }
        }
        
        public enum eFacilityStatus
        {
            case eMicrophone(eMicrophoneStatus)
            case eSpeechRecognitionService(eSpeechRecognitionStatus)
            case eSpeechSynthService(Bool)
            case eReachabilityService(NetworkReachabilityService.eReachabilityStatus)
            case eLocationService(LocationService.eLocationAvailabilityStatus)
 
            public var isAvailable : Bool
            {
                switch self
                {
                    case .eMicrophone(let status):                      return status.isAvailable
                    case .eSpeechRecognitionService(let status):        return status.isAvailable
                    case .eSpeechSynthService(let status):              return status
                    case .eReachabilityService(let status):             return status.isReachable
                    case .eLocationService(let status):                 return status.isAvailable
                }
            }
            
            public var facility : eFacility
            {
                switch self
                {
                    case .eMicrophone:                                  return .eMicrophone
                    case .eSpeechRecognitionService:                    return .eSpeechRecognitionService
                    case .eSpeechSynthService:                          return .eSpeechSynthService
                    case .eReachabilityService:                         return .eReachabilityService
                    case .eLocationService:                             return .eLocationService
                }
            }
        }

        @SpeechSynthActor   public let speechSynthService:          SpeechSynthService
                            public let locationService:             LocationService
                            public let reachabilityService :        NetworkReachabilityService
                            public let speechRecognitionService:    SpeechRecognitionService

        @MainActor
        init(appBuilder: IAppBuilder)
        {
            self.speechSynthService = SpeechSynthService(appBuilder: appBuilder)
            self.locationService = LocationService(appBuilder: appBuilder)
            self.reachabilityService = NetworkReachabilityService(appBuilder: appBuilder)
            self.speechRecognitionService = SpeechRecognitionService(appBuilder: appBuilder)

            trace(title: "ServiceDirectory", "Construct Service")
        }

        deinit
        {
            trace(title: "ServiceDirectory", "Destruct Service")
        }
    }

    public enum eSpeechSynthRequest
    {
        public enum eCancelPriority
        {
            case eImmediatePriority
            case eLowPriority
        }

        case eSpeak(String, progressReportFunc: (eServiceEvent.eSpeechVocalizationEvent) -> Void)
        case eCancelSpeaking(TimeInterval?, eCancelPriority)
    }

    //
    // The notifiers that the Service Actor is allowed to access
    //

    struct ActorNotifierDirectory
    {
        public let service : NotifierDirectory.Service

        fileprivate init(notifierStore : NotifierStore)
        {
            self.service = NotifierDirectory.Service(notifierStore: notifierStore)
        }
    }

    public let uri          = ServiceActor.ActorURI
    public let notifierURIs = [ServiceRequestURI, ServiceEventURI, ServiceSpeechSynthURI]

    let notifiers: ActorNotifierDirectory
    var serviceDirectory :           ServiceDirectory? = nil
    
    private var m_subscribers = Set<AnyCancellable>()
    private var m_recognitionStartTime: Date? = nil

    @MainActor
    init(appBuilder : IAppBuilder)
    {
        let notifierDirectory   = appBuilder.notifierDirectory

        self.notifiers = ActorNotifierDirectory(notifierStore: notifierDirectory.notifierStore)

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Service Actor requests.
        //
        
        notifiers.service.request.sink(receiveValue: { [weak self] (notification) in
            
            guard let strongSelf = self else { return }
            
            let notifiers           = strongSelf.notifiers
            let serviceDirectory    = strongSelf.serviceDirectory

            switch notification.value
            {
                case .eAuthorizeServices:

                    notification.forward(result: .eAcknowledge)
                    
                    Task {
                        
                        await serviceDirectory?.speechRecognitionService.requestMicrophoneAuthorization()
                        await serviceDirectory?.speechRecognitionService.requestSpeechRecogAuthorization()
                    }

                case .eSpeechRecognition(let speechRecognitionRequest):
                    
                    switch speechRecognitionRequest
                    {
                        case .eBeginListening:

                            strongSelf.m_recognitionStartTime = Date()

                            notification.forward(result: .eAcknowledge)
                                            
                            Task {
                                
                                await serviceDirectory?.speechRecognitionService.startStreaming(resultHandler: { (result, error) in
                                 
                                    if let error = error
                                    {
                                        let code = (error as NSError).code

                                        switch code
                                        {
                                            case 1107:

                                                notifiers.service.event.send(.eOnSpeechRecognition(.eRecognitionFailed("Intermittent speech recognition error, please try again")))

                                            case 203:

                                                notifiers.service.event.send(.eOnSpeechRecognition(.eRecognitionFailed("Sorry, I didn't hear anything, please try again")))

                                            default:

                                                let errorDescription = error.localizedDescription

                                                notifiers.service.event.send(.eOnSpeechRecognition(.eRecognitionFailed(errorDescription)))
                                        }
                                    }
                                    else if let result = result, result.isFinal
                                    {
                                        let formattedString = result.bestTranscription.formattedString

                                        if let recognitionStartTime = strongSelf.m_recognitionStartTime
                                        {
                                            let recognitionStats = BehaviourActor.RecognitionStats(recognitionDuration: -recognitionStartTime.timeIntervalSinceNow)
                                            
                                            notifiers.service.event.send(.eOnSpeechRecognition(.eRecognized(formattedString, recognitionStats)))
                                        }
                                        else
                                        {
                                            notifiers.service.event.send(.eOnSpeechRecognition(.eRecognized(formattedString, nil)))
                                        }
                                    }
                                })
                
                                notifiers.service.event.send(.eOnSpeechRecognition(.eBeginListening))
                            }
                       
                        case .eEndListening:
                    
                            notification.forward(result:.eAcknowledge)
                            
                            Task {
                                
                                await strongSelf.serviceDirectory?.speechRecognitionService.stopStreaming {
      
                                    notifiers.service.event.send(.eOnSpeechRecognition(.eEndListening))
                                }
                            }
                    }

                case .eSpeechSynth(let speechSynthRequest):

                    switch speechSynthRequest
                    {
                        case .eSpeak(let vocalText, let progressReportFunc):

                            Task {

                                await strongSelf.notifiers.service.queuedSpeechSynthNotifier.sendAsync(.eSpeak(vocalText, progressReportFunc: progressReportFunc))
                            }

                            notification.forward(result:.eAcknowledge)

                        case .eCancelSpeaking(let duration, let priority):

                            Task {

                                await strongSelf.notifiers.service.queuedSpeechSynthNotifier.removeAll()
                                await serviceDirectory?.speechSynthService.cancelSpeaking(duration: duration, priority: priority)
                            }
                            
                            notification.forward(result:.eAcknowledge)
                    }

                case .eLocation(.eGetCurrentAddress):
     
                    if let serviceDirectory = strongSelf.serviceDirectory
                    {
                        if let currentCoordinate = serviceDirectory.locationService.currentLocation
                        {
                            Task {

                                notification.forward(result: await serviceDirectory.locationService.getAddress(forCoordinate: currentCoordinate))
                            }
                        }
                        else if case .eIsNotAvailable(let description) = serviceDirectory.locationService.status
                        {
                            switch description
                            {
                                case nil:

                                    notification.forward(result: .eFailure("Sorry, the current location has not yet been determined, please try again"))

                                default:

                                    notification.forward(result: .eFailure("Sorry, the location service is not available on this device"))
                            }
                        }
                    }

                case .eTemporal(.eGetTime):
                    
                    if appBuilder.config.testingConfig?.mockServices == true
                    {
                        notification.forward(result: Mocks.getTime())
                    }
                    else
                    {
                        notification.forward(result: .eTime(Date()))
                    }
            }
            
        }).store(in: &m_subscribers)

        //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
        //
        //  Handle Internal Speech Synth requests.
        //

        notifiers.service.queuedSpeechSynthNotifier.sink(receiveValue: { [weak self] (notification) in

            guard let strongSelf = self,
                  let serviceDirectory = strongSelf.serviceDirectory else { return }

            switch notification.value
            {
                case .eSpeak(let text, let progressReportFunc):

                    await serviceDirectory.speechSynthService.speak(interaction: .eSpeakToUser(nil), text: text, reportProgress: progressReportFunc)

                    notification.forward(result: ())

                case .eCancelSpeaking:

                    // Not used
                    break
            }
            
        }).store(in: &m_subscribers)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
//  CustomStringConvertible conformance.
//

extension ServiceActor.eServiceRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eAuthorizeServices:               return "AuthorizeServices"
            case .eSpeechRecognition(let request):  return "eSpeechRecognition(\(request))"
            case .eSpeechSynth(let request):        return "eSpeechSynth(\(request))"
            case .eLocation(let request):           return "eLocation(\(request))"
            case .eTemporal(let request):           return "eTemporal(\(request))"
        }
    }
}

extension ServiceActor.eServiceRequest.eSpeechRecognitionRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eBeginListening:              return "eBeginListening"
            case .eEndListening:                return "eEndListening"
        }
    }
}

extension ServiceActor.eServiceRequest.eLocationRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eGetCurrentAddress:           return "eGetCurrentAddress"
        }
    }
}

extension ServiceActor.eServiceReply : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eAddress(let text):               return "eAddress(\(text))"
            case .eTime(let text):                  return "eTime(\(text)"
            case .eFailure(let message):            return "eFailure(\(message))"
            case .eAcknowledge:                     return "eAcknowledge"
        }
    }
}

extension ServiceActor.eServiceRequest.eTemporalRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eGetTime:                         return "eGetTime"
        }
    }
}

extension ServiceActor.eServiceEvent : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eOnAppStatusChange(let event):            return "eOnAppStatusChange(\(event))"
            case .eOnSpeechRecognition(let event):          return "eOnSpeechRecognition(\(event))"
            case .eOnServiceAvailabilityChange(let event):  return "eOnServiceAvailabilityChange(\(event))"
        }
    }
}

extension ServiceActor.eServiceEvent.eAppStatusChange : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eOnAppActive:             return "eOnAppActive"
            case .eOnAppInActive:           return "eOnAppInActive"
        }
    }
}

extension ServiceActor.eServiceEvent.eSpeechRecognitionEvent : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eBeginListening:                                  return "eBeginListening"
            case .eEndListening:                                    return "eEndListening"
            case .eRecognized(let vocalText, let stats):            return "eRecognized(\(vocalText), \(stats != nil ? stats!.description : "nil"))"
            case .eRecognitionFailed(let message):                  return "eRecognitionFailed(\(message))"
        }
    }
}

extension ServiceActor.eServiceEvent.eSpeechVocalizationEvent : CustomStringConvertible
{
    public var description : String
    {
        switch self
        {
            case .eOnUtterance(let interaction, let attributedString, let characterRange):
                
                if let characterRange = characterRange
                {
                    let utteranceAttributedString = AttributedString(attributedString[characterRange])
                    
                    return "eOnUtterance(\(interaction), \(utteranceAttributedString)"
                }
                else
                {
                    return "eOnUtterance(\(interaction), \(attributedString)"
                }

            case .eEndSpeaking(let interaction, let attributedString):
                
                return "eEndSpeaking(\(interaction), \(attributedString))"
                
            case .eCancelSpeaking(let interaction,let attributedString):
                
                return "eCancelSpeaking(\(interaction), \(attributedString))"
        }
    }
}

extension ServiceActor.ServiceDirectory.eSpeechRecognitionStatus: CustomStringConvertible
{
    public var description : String
    {
        switch self
        {
            case .eOnlineOperation(nil):                    return "eOnlineOperation(nil)"
            case .eOnlineOperation(let status?):            return "eOnlineOperation(\(status))"
            case .eIsNotAvailable:                          return "eIsNotAvailable"
            case .eIsNotReady:                              return "eIsNotReady"
            case .eOfflineOperation:                        return "eOfflineOperation"
        }
    }
}

extension ServiceActor.ServiceDirectory.eMicrophoneStatus: CustomStringConvertible
{
    public var description : String
    {
        switch self
        {
            case .eIsAvailable:
                
                return "eIsAvailable"
                
            case .eFailure(let failure):
                
                return "eFailure(\(failure))"
        }
    }
}

extension ServiceActor.ServiceDirectory.eFacilityStatus : CustomStringConvertible
{
    public var description : String
    {
        switch self
        {
            case .eMicrophone(let status):
                
                return "eMicrophone(\(status))"
                
            case .eSpeechRecognitionService(let status):
                
                return "eSpeechRecognitionService(\(status))"
                
            case .eSpeechSynthService(let status):
                
                return "eSpeechSynthService(\(status))"
                
            case .eReachabilityService(let status):
                
                return "eReachabilityService(\(status))"
                
            case .eLocationService(let status):
               
                return "eLocationService(\(status))"
        }
    }
}

extension ServiceActor.eSpeechSynthRequest : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eSpeak(let vocalText, _):     return "eSpeak(\(vocalText))"
            case .eCancelSpeaking:              return "eCancelSpeaking"
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
//  Equatable conformance.
//

extension ServiceActor.ServiceDirectory.eFacilityStatus : Equatable
{
    public static func == (lhs: ServiceActor.ServiceDirectory.eFacilityStatus, rhs: ServiceActor.ServiceDirectory.eFacilityStatus) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eSpeechRecognitionService(let lhsStatus), .eSpeechRecognitionService(let rhsStatus)):
                
                return lhsStatus == rhsStatus
                
            case (.eSpeechSynthService(let lhsStatus), .eSpeechSynthService(let rhsStatus)):
                
                return lhsStatus == rhsStatus
                
            case (.eReachabilityService(let lhsStatus), .eReachabilityService(let rhsStatus)):
                
                return lhsStatus == rhsStatus
                  
            default:
                  
                return false
        }
    }
}

