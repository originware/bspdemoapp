
//
//  Models/Actors/Actor.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/3/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IActor, the base protocol for Actors.
///

public protocol IActor
{
    // URI of the Actor
    var uri : String            { get }

    // URIs of resident Notifiers
    var notifierURIs : [String] { get }
}
