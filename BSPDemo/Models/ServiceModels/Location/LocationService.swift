
//
//  LocationService.swift
//  BSPDemo
//
//  Created by Terry Stillone on 27/6/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation
import CoreLocation
import CFNetwork

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Location Service. Generates Service Actor events for location service failures.
///

public final class LocationService: NSObject, Traceable, CLLocationManagerDelegate
{
    public struct Location2D : Hashable
    {
        public let latitude :   Double
        public let longitude :  Double

        public init(_ latitude: Double, _ longitude: Double)
        {
            self.latitude   = latitude
            self.longitude  = longitude
        }
    }
    
    public enum eLocationAvailabilityStatus : CustomStringConvertible
    {
        case eIsAvailable(Location2D)
        case eIsNotAvailable(String?)
        
        public var isAvailable : Bool
        {
            switch self
            {
                case .eIsAvailable:     return true
                case .eIsNotAvailable:  return false
            }
        }
        
        public var description : String
        {
            switch self
            {
                case .eIsAvailable(let location):        return "eIsAvailable(\(location))"
                case .eIsNotAvailable(nil):              return "eIsNotAvailable(nil)"
                case .eIsNotAvailable(let message?):     return "eIsNotAvailable(\(message))"
            }
        }
    }

    public private(set) var currentLocation : Location2D? = nil
    public private(set) var status = eLocationAvailabilityStatus.eIsNotAvailable(nil)
    
    private let m_serviceEventNotifier : ServiceActor.ServiceEventNotifier
    private var m_locationManager                              : CLLocationManager
    private let m_mockAddress : String?

    init(appBuilder : IAppBuilder)
    {
        let locationManager = appBuilder.buildLocationManager()
        
        self.m_serviceEventNotifier = appBuilder.notifierDirectory.service.event
        self.m_locationManager      = locationManager
        self.m_mockAddress          = appBuilder.config.testingConfig?.mockAddress
        
        super.init()

        locationManager.delegate = self

        if CLLocationManager.locationServicesEnabled()
        {
            let status : CLAuthorizationStatus
            
            if #available(iOS 14.0, OSX 11.0, *)
            {
                status = m_locationManager.authorizationStatus
            }
            else
            {
                status = CLLocationManager.authorizationStatus()
            }

            switch status
            {
                case .authorizedWhenInUse, .authorizedAlways:

                    break

                case .denied:

                    onServiceFailure("Location service is not authorized")

                case .notDetermined:

                    m_locationManager.requestAlwaysAuthorization()

                case .restricted:

                    onServiceFailure("Location service is restricted")

                @unknown default:

                    onServiceFailure("Location service is in an unknown state")
            }
        }
        else
        {
            onServiceFailure("Location service is not enabled on this device")
        }

        trace(title: "LocationService", "Construct Service")
    }

    deinit
    {
        trace(title: "LocationService", "Destruct Service")
    }
    
    public func getAddress(forCoordinate coordinate: Location2D) async -> ServiceActor.eServiceReply
    {
        if let mockAddress = m_mockAddress
        {
            return .eAddress(mockAddress)
        }
        
        let clocation = CLLocation(latitude: CLLocationDegrees(coordinate.latitude), longitude: CLLocationDegrees(coordinate.longitude))
        let geocoder = CLGeocoder()
        
        return await withCheckedContinuation { continuation in

            geocoder.reverseGeocodeLocation(clocation, completionHandler: { (placemarks, error) in

                func getScore(placemark: CLPlacemark) -> Int
                {
                    var result = 0

                    if placemark.subThoroughfare != nil
                    {
                        result += 2
                    }

                    if placemark.thoroughfare != nil
                    {
                        result += 1
                    }

                    return result
                }
                
                func format(placemark: CLPlacemark) -> String
                {
                    var result = ""
                    
                    result ++= placemark.subThoroughfare
                    result ++= placemark.thoroughfare
                    result ++= placemark.subLocality
                    result &&= placemark.locality
                    result &&= placemark.country
                    
                    return result
                }

                if error == nil
                {
                    if let placemarks = placemarks
                    {
                        var scores = [Int : CLPlacemark]()

                        for placemark in placemarks
                        {
                            let score = getScore(placemark: placemark)

                            scores[score] = placemark
                        }

                        if !scores.isEmpty
                        {
                            let topScore = scores.keys.sorted().first!
                            let topPlacemark      = scores[topScore]!
                            let formattedAddress  = format(placemark: topPlacemark)
                            
                            if !formattedAddress.isEmpty
                            {
                                continuation.resume(returning: .eAddress(formattedAddress))
                            }
                            else
                            {
                                continuation.resume(returning: .eFailure("Sorry, could not resolve address"))
                            }
                            
                            return
                        }
                    }

                    continuation.resume(returning: .eFailure("Sorry, could not format the address for coordinates latitude \(coordinate.latitude), longitude \(coordinate.longitude)"))
                    
                    return
                }
                else if let error = error
                {
                    let nserror = error as NSError
                    
                    if nserror.code == NSFileNoSuchFileError
                        || nserror.code == CFNetworkErrors.cfHostErrorUnknown.rawValue
                    {
                        continuation.resume(returning: .eFailure("Sorry, location network failure. I think I am not connected to the net."))
                    }
                    else
                    {
                        let failure = nserror.localizedDescription
                        
                        continuation.resume(returning: .eFailure(failure))
                    }
                }
                else
                {
                    continuation.resume(returning: .eFailure("Sorry, unknown internal error"))
                }
            })
        }
    }
    
    @available(iOS 14.0, *)
    public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager)
    {
        onServiceChange(manager.authorizationStatus)
    }
    
    @available(iOS, introduced: 4.2, deprecated: 14.0)
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        onServiceChange(status)
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let clocation = locations.first
        {
            let location = Location2D(Double(clocation.coordinate.latitude), Double(clocation.coordinate.longitude))

            currentLocation = location
            status = .eIsAvailable(location)
            m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eLocationService(.eIsAvailable(location))))
        }
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        let errorDescription = error.localizedDescription
        let serviceStatus = eLocationAvailabilityStatus.eIsNotAvailable(errorDescription)
                          
        trace("ServiceModel.LocationService didFailWithError error: \(errorDescription)")

        status = serviceStatus
        m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eLocationService(serviceStatus)))
    }

    private func onServiceChange(_ status : CLAuthorizationStatus)
    {
        switch status
        {
            case .denied:

                onServiceFailure("Location service is not authorized")

            case .notDetermined:

                onServiceFailure("Location service state could not be determined")

            case .authorizedWhenInUse:

                m_locationManager.requestLocation()

            case .authorizedAlways:

                m_locationManager.requestLocation()

                if let currentLocation = currentLocation
                {
                    m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eLocationService(.eIsAvailable(currentLocation))))
                }

            case .restricted:

                onServiceFailure("Location service is restricted")

            default:

                onServiceFailure("Location service is in an unknown state")
        }
    }

    private func onServiceFailure(_ description : String)
    {
        let serviveStatus = eLocationAvailabilityStatus.eIsNotAvailable(description)

        self.status = serviveStatus
        m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eLocationService(serviveStatus)))
    }
}

