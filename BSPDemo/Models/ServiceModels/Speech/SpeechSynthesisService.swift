//
//  SpeechSynthesisService.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/12/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The synchronization Actor for the Speech Synth Service and sub-systems.
//

@globalActor
struct SpeechSynthActor
{
    actor ActorType {}

    static let shared: ActorType = ActorType()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The Speech Synthesis Service. Generates Service Actor events.
//

@SpeechSynthActor
public class SpeechSynthService : Traceable
{
    fileprivate typealias SpeechSynthTask = () -> Void
    
    public enum eInteraction
    {
        case eSpeakToUser(String?)
        case eConverse(String, String)

        public var speaker : String?
        {
            switch self
            {
                case .eSpeakToUser(let name):   return name
                case .eConverse(let name, _):   return name
            }
        }
    }

    private let m_serviceEventNotifier: ServiceActor.ServiceEventNotifier
    private let m_speechSynthController : SpeechSynthController

    nonisolated init(appBuilder : IAppBuilder)
    {
        self.m_serviceEventNotifier = appBuilder.notifierDirectory.service.event
        self.m_speechSynthController = SpeechSynthController(appBuilder: appBuilder)

        trace(title: "SpeechSynthService", "Construct Service")
    }
    
    deinit
    {
        trace(title: "SpeechSynthService", "Destruct Service")
    }

    public func speak(interaction: eInteraction, text: String, reportProgress: (ServiceActor.eServiceEvent.eSpeechVocalizationEvent) -> Void) async
    {
        @SpeechSynthActor
        func isCancelled(utteranceOp: UtteranceSequencer.eUtteranceOp?) async -> Bool
        {
            guard let cancellation = m_speechSynthController.utteranceSequencer.cancellation else { return false }

            if case .eCompleted = utteranceOp
            {
                m_speechSynthController.utteranceSequencer.removeAll()
                m_speechSynthController.utteranceSequencer.updateCancellation()

                return true
            }
            else if cancellation.isCancellationWindowActive
            {
                m_speechSynthController.utteranceSequencer.removeAll()

                return true
            }
            else
            {
                m_speechSynthController.utteranceSequencer.clearCancellation()

                return false
            }
        }

        let vocalOp = SpeechSynthController.eVocalOp.eSpeak(text)

        var vocalIndex          = 0
        var vocalTextOffset     = 0
        
        let (vocalOpTextOffsetMap, attributedReply) = formatAttributedText(vocalOps: [vocalOp])
        let utteranceOpAsyncStream = m_speechSynthController.utteranceSequencer.getStream()

        if await isCancelled(utteranceOp: nil)
        {
            reportProgress(.eCancelSpeaking(interaction, attributedReply))
            return
        }

        m_speechSynthController.startSpeaking(interaction: interaction, vocalOp: vocalOp)

        loop: for await utteranceOp in utteranceOpAsyncStream
        {
            if await isCancelled(utteranceOp: utteranceOp)
            {
                reportProgress(.eCancelSpeaking(interaction, attributedReply))
                break loop
            }

            switch utteranceOp
            {
                case .eVocalOp(let interaction, _):

                    vocalIndex += 1
                    vocalTextOffset = vocalOpTextOffsetMap[vocalIndex]

                    reportProgress(.eOnUtterance(interaction, attributedReply, nil))

                case .eUtterance(_, let characterRange):

                    guard let characterRange = characterRange else { return }

                    let highlightAttributes = Styles.highlightBodyTextStyle
                    let startOffset = characterRange.location + vocalTextOffset
                    let endOffset = startOffset + characterRange.length
                    var bodyText : AttributedString = attributedReply

                    if startOffset <= bodyText.characters.count,
                       endOffset <= bodyText.characters.count
                    {
                        let startIndex = bodyText.characters.index(bodyText.startIndex, offsetBy: startOffset)
                        let endIndex = bodyText.characters.index(bodyText.startIndex, offsetBy: endOffset)
                        let highlightRange = startIndex..<endIndex

                        bodyText[startIndex..<endIndex].setAttributes(highlightAttributes)

                        reportProgress(.eOnUtterance(interaction, bodyText, highlightRange))
                    }

                case .eCompleted:

                    reportProgress(.eEndSpeaking(interaction, attributedReply))
            }
        }

        // Not required, but exercised as safety code.
        m_speechSynthController.utteranceSequencer.streamContinuation = nil
    }

    public func cancelSpeaking(duration: TimeInterval?, priority: ServiceActor.eSpeechSynthRequest.eCancelPriority)
    {
        m_speechSynthController.cancelSpeaking(duration: duration, priority: priority)
    }
    
    private func formatAttributedText(vocalOps: ContiguousArray<SpeechSynthController.eVocalOp>) -> (ContiguousArray<Int>, AttributedString)
    {
        func getLines(fromVocalOps vocalOps: ContiguousArray<SpeechSynthController.eVocalOp>) -> ContiguousArray<String>?
        {
            func getSpeakText(forVocalOp vocalOp: SpeechSynthController.eVocalOp) -> String?
            {
                switch vocalOp
                {
                    case .eSpeak(let speakText, _):

                        if !speakText.isEmpty
                        {
                            return speakText
                        }

                    case .ePause(let duration):

                        return "(p\(duration))"

                    case .eApply(let commands):

                        return "(\(commands))"
                }

                return nil
            }

            var displayText         = ContiguousArray<String>()

            for vocalOp in vocalOps
            {
                if let adjustedText = getSpeakText(forVocalOp: vocalOp)
                {
                    displayText.append(adjustedText)
                }
            }

            return !displayText.isEmpty ? displayText : nil
        }

        guard let lines = getLines(fromVocalOps: vocalOps) else { return (ContiguousArray<Int>(), AttributedString()) }

        var cumulativeTextCount = 0
        var vocalOpIndexMap     = ContiguousArray<Int>()
        var attributedText = AttributedString()

        vocalOpIndexMap.append(0)

        for (index, line) in lines.enumerated()
        {
            let isNotLastLine = index != lines.count - 1
            var mutableLine = line

            if isNotLastLine, !mutableLine.hasSuffix("\n")
            {
                mutableLine += " "
            }

            cumulativeTextCount += mutableLine.count
            vocalOpIndexMap.append(cumulativeTextCount)
            attributedText.append(AttributedString(mutableLine, attributes: Styles.bodyTextStyle))
        }

        return (vocalOpIndexMap, attributedText)
    }
}

@SpeechSynthActor
public class SpeechSynthController : NSObject, AVSpeechSynthesizerDelegate
{
    public enum eVocalOp : Equatable
    {
        case eSpeak(String, String? = nil)
        case ePause(TimeInterval)
        case eApply(String)

        public var speakingText : String?
        {
            switch self
            {
                case .eSpeak(let text, nil):        return text
                case .eSpeak(_, let text?):         return text
                case .ePause:                       return nil
                case .eApply:                       return nil
            }
        }
    }

    fileprivate var isSpeaking : Bool
    {
        m_speechSynth.isSpeaking
    }

    fileprivate let utteranceSequencer                   : UtteranceSequencer
    
    private var m_voiceDirectory                         = AvatarPersonaDirectory()
    private let m_speechSynth                            : AVSpeechSynthesizer
    
    fileprivate nonisolated init(appBuilder: IAppBuilder)
    {
        self.utteranceSequencer = UtteranceSequencer()
        self.m_speechSynth = appBuilder.buildSpeechSynthesizer()

        super.init()

        m_speechSynth.delegate = self

        func listAvailableVoices()
        {
            // Debug: List available voices.
            let voices = AVSpeechSynthesisVoice.speechVoices()
            let voiceDescriptions = voices.map({ String(describing: $0)}).joined(separator: "\n")
            
            print(">> voices descriptions: \(voiceDescriptions)")
            print(">> voices: \(voices)")
        }
       
        // Uncomment to use
        //listAvailableVoices()
    }

    fileprivate func speak(interaction: SpeechSynthService.eInteraction, vocalOp: eVocalOp)
    {
        switch vocalOp
        {
            case .eSpeak(let text, nil),
                 .eSpeak(_, let text?):

                let utterance = AVSpeechUtterance(string: text)

                if let voiceCharacteristics = m_voiceDirectory[interaction.speaker ?? "Default"]
                {
                    utterance.voice = voiceCharacteristics.voice
                    utterance.rate = voiceCharacteristics.rate
                    utterance.pitchMultiplier = voiceCharacteristics.pitch
                    utterance.volume = voiceCharacteristics.volume
                }

                m_speechSynth.speak(utterance)

            case .ePause(let timeInterval):

                let utterance = AVSpeechUtterance(string: "")

                usleep(UInt32(timeInterval * 1_000_000))

                // Perform a dummy utterance to trigger the next utterance cycle.
                m_speechSynth.speak(utterance)

            case .eApply(let commands):

                if var characteristics = m_voiceDirectory[interaction.speaker]
                {
                    var currentIndex            = commands.startIndex
                    var doUpdateCharacteristics = false

                    func consumeCharCmd(_ command: () -> Void)
                    {
                        currentIndex = commands.index(after: currentIndex)
                        doUpdateCharacteristics = true

                        command()
                    }

                    func consumeTimeIntervalCmd(_ command: (TimeInterval) -> Void)
                    {
                        let bufferStartIndex = commands.index(after: currentIndex)
                        let buffer           = String(commands[bufferStartIndex...])
                        let scanner = Scanner(string: buffer)

                        if #available(iOS 13, *)
                        {
                            if let value = scanner.scanDouble()
                            {
                                let distance = buffer.distance(from: buffer.startIndex, to: scanner.currentIndex)

                                command(value)

                                currentIndex = commands.index(bufferStartIndex, offsetBy: distance)
                            }
                            else
                            {
                                // skip to end
                                currentIndex = commands.endIndex
                            }
                        }
                        else
                        {
                            var value = Double(0)
                            
                            if scanner.scanDouble(&value)
                            {
                                command(value)

                                currentIndex = commands.index(currentIndex, offsetBy: scanner.scanLocation + 1)
                            }
                            else
                            {
                                // skip to end
                                currentIndex = commands.endIndex
                            }
                        }
                    }

                    while currentIndex < commands.endIndex
                    {
                        let character = commands[currentIndex]

                        switch character
                        {
                            case ">":   consumeCharCmd({ characteristics.rate += 0.04 })
                            case "<":   consumeCharCmd({ characteristics.rate -= 0.04 })
                            case "+":   consumeCharCmd({ characteristics.volume += 0.2 })
                            case "-":   consumeCharCmd({ characteristics.volume -= 0.2 })
                            case "p":   consumeTimeIntervalCmd({ usleep(UInt32($0 * 1_000_000)) })
                            default:    break
                        }
                    }

                    if doUpdateCharacteristics
                    {
                        m_voiceDirectory[interaction.speaker] = characteristics
                    }
                }

                let utterance = AVSpeechUtterance(string: "")
                
                if let voiceCharacteristics = m_voiceDirectory[interaction.speaker]
                {
                    utterance.voice = voiceCharacteristics.voice
                    utterance.rate = voiceCharacteristics.rate
                    utterance.pitchMultiplier = voiceCharacteristics.pitch
                    utterance.volume = voiceCharacteristics.volume
                }

                // Perform a dummy utterance to trigger the next utterance cycle.
                m_speechSynth.speak(utterance)
        }
    }

    fileprivate func startSpeaking(interaction: SpeechSynthService.eInteraction, vocalOp: eVocalOp)
    {
        speak(interaction: interaction, vocalOp: vocalOp)
    }

    fileprivate func cancelSpeaking(duration: TimeInterval?, priority: ServiceActor.eSpeechSynthRequest.eCancelPriority)
    {
        switch priority
        {
            case .eImmediatePriority:   m_speechSynth.stopSpeaking(at: .immediate)
            case .eLowPriority:         m_speechSynth.stopSpeaking(at: .word)
        }

        utteranceSequencer.cancel(duration: duration, priority: priority)

        switch priority
        {
            case .eImmediatePriority:   m_speechSynth.stopSpeaking(at: .immediate)
            case .eLowPriority:         m_speechSynth.stopSpeaking(at: .word)
        }
    }
    
    public nonisolated func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance)
    {
        Task {
          
            let text = utterance.speechString
            let count = text.count
            let range : Range<Int> = 0..<count
            
            // Workaround for Apple bug not giving valid character range.
            if !range.contains(characterRange.lowerBound) || !range.contains(characterRange.upperBound)
            {
                if range.contains(characterRange.lowerBound) && count - characterRange.lowerBound > 0
                {
                    // Attempt to correct range.
                    let characterRange = NSRange(location: characterRange.lowerBound, length: count - characterRange.lowerBound)

                    await utteranceSequencer.queue(.eUtterance(text, characterRange))
                }
                else
                {
                    // Supply null range.
                    await utteranceSequencer.queue(.eUtterance(text, NSRange(location: 0, length: 0)))
                }
            }
            else
            {
                await utteranceSequencer.queue(.eUtterance(text, characterRange))
            }
        }
    }

    public nonisolated func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance)
    {
        Task {
            
            await utteranceSequencer.queue(.eCompleted)
        }
    }

    public nonisolated func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance)
    {
        Task {

            await utteranceSequencer.queue(.eCompleted)
        }
    }
}

@SpeechSynthActor
fileprivate class UtteranceSequencer : Traceable
{
    fileprivate enum eUtteranceOp
    {
        case eVocalOp(SpeechSynthService.eInteraction, SpeechSynthController.eVocalOp)
        case eUtterance(String?, NSRange?)
        case eCompleted
    }
    
    fileprivate struct Cancellation
    {
        fileprivate let priority : ServiceActor.eSpeechSynthRequest.eCancelPriority
        fileprivate let threshold : TimeInterval
        fileprivate private(set) var timestamp : Date?

        fileprivate init(duration: TimeInterval?, priority : ServiceActor.eSpeechSynthRequest.eCancelPriority)
        {
            self.priority = priority
            self.threshold = duration == nil ? 0.0 : 0.7
            
            switch priority
            {
                case .eImmediatePriority:       self.timestamp = nil
                case .eLowPriority:             self.timestamp = Date()
            }
        }

        fileprivate var isCancellationWindowActive : Bool
        {
            guard let timestamp = timestamp else { return false }
            
            let duration = -timestamp.timeIntervalSinceNow

            return duration <= threshold
        }

        fileprivate mutating func update()
        {
            guard timestamp != nil else { return }

            self.timestamp = Date()
        }
    }

    fileprivate var streamContinuation: AsyncStream<eUtteranceOp>.Continuation? = nil
    fileprivate private(set) var cancellation: Cancellation? = nil

    private var m_asyncStream : AsyncStream<eUtteranceOp>! = nil
    private var m_utteranceOpQueue                         = ContiguousArray<eUtteranceOp>()

    fileprivate nonisolated init()
    {
    }

    fileprivate func getStream() -> AsyncStream<eUtteranceOp>
    {
        streamContinuation = nil

        let asyncStream = AsyncStream<eUtteranceOp> { [weak self] (continuation) in

            guard let strongSelf = self else { return }

            if strongSelf.streamContinuation == nil
            {
                strongSelf.streamContinuation = continuation
            }

            if let op = strongSelf.m_utteranceOpQueue.first
            {
                strongSelf.m_utteranceOpQueue.removeFirst()

                switch op
                {
                    case .eCompleted:

                        continuation.finish()

                    default:

                        continuation.yield(op)
                }
            }
        }
        
        m_asyncStream = asyncStream
   
        return asyncStream
    }
    
    fileprivate func queue(_ op: eUtteranceOp)
    {
        if !m_utteranceOpQueue.isEmpty
        {
            m_utteranceOpQueue.append(op)
        }
        else if let continuation = streamContinuation
        {
            switch op
            {
                case .eCompleted:

                    continuation.yield(op)
                    continuation.finish()
                    streamContinuation = nil
                     
                default:

                    continuation.yield(op)
            }
        }
    }
    
    fileprivate func cancel(duration: TimeInterval?, priority: ServiceActor.eSpeechSynthRequest.eCancelPriority)
    {
        m_utteranceOpQueue.removeAll()

        setCancellation(duration: duration, priority: priority)
    }

    fileprivate func removeAll()
    {
        m_utteranceOpQueue.removeAll()
    }

    @inline(__always)
    fileprivate func setCancellation(duration: TimeInterval?, priority: ServiceActor.eSpeechSynthRequest.eCancelPriority)
    {
        cancellation = Cancellation(duration: duration, priority: priority)
    }

    @inline(__always)
    fileprivate func clearCancellation()
    {
        cancellation = nil
    }

    @inline(__always)
    fileprivate func updateCancellation()
    {
        cancellation?.update()
    }
}

//
// The Avatar Persona Directory, which details the vocal characteristics of various Avatar personas.
//

public struct AvatarPersonaDirectory
{
    public struct VoiceCharacteristics
    {
        public static let SpeechSynthesisRate = Float(0.52)
        
        public let voice: AVSpeechSynthesisVoice?
        public var rate : Float
        public var pitch : Float
        public var volume : Float

        public init(identifier: String, rate: Float = VoiceCharacteristics.SpeechSynthesisRate, pitch: Float = 1.0, volume: Float = 0.8)
        {
            self.voice = AVSpeechSynthesisVoice(identifier: identifier) ?? AVSpeechSynthesisVoice(language: nil)
            self.rate = rate
            self.pitch = pitch
            self.volume = volume
        }
    }

    public struct Voice
    {
        public static var availableVoices : [AVSpeechSynthesisVoice]
        {
            AVSpeechSynthesisVoice.speechVoices()
        }
        
        public let defaultCharacteristics : VoiceCharacteristics
        public var current : VoiceCharacteristics
       
        public init(_ characteristics : VoiceCharacteristics)
        {
            self.current = characteristics
            self.defaultCharacteristics = characteristics
        }

        public mutating func reset()
        {
            current = defaultCharacteristics
        }
    }
    
    public private(set) var voiceDirectory : [String : Voice]
    
    public init()
    {
        
#if targetEnvironment(macCatalyst)

        self.voiceDirectory = [

            "Default"          : Voice(VoiceCharacteristics(identifier: "com.apple.speech.synthesis.voice.daniel.premium", rate: 0.52)),
            "Timetable"        : Voice(VoiceCharacteristics(identifier: "com.apple.speech.synthesis.voice.kate.premium", rate: 0.52)),
            "Home Services"    : Voice(VoiceCharacteristics(identifier: "com.apple.speech.synthesis.voice.Alex", rate: 0.52, pitch: 1.0)),
            "Kitchen Services" : Voice(VoiceCharacteristics(identifier: "com.apple.speech.synthesis.voice.daniel.premium", rate: 0.52, pitch: 1.0)),
        ]

#else

        self.voiceDirectory = [
            
            "Default"          : Voice(VoiceCharacteristics(identifier: "com.apple.ttsbundle.Daniel-compact", rate: 0.52)),
            "Timetable"        : Voice(VoiceCharacteristics(identifier: "com.apple.ttsbundle.siri_female_en-GB_compact", rate: 0.52)),
            "Home Services"    : Voice(VoiceCharacteristics(identifier: "com.apple.ttsbundle.siri_male_en-US_compact", rate: 0.53, pitch: 1.0)),
            "Kitchen Services" : Voice(VoiceCharacteristics(identifier: "com.apple.ttsbundle.Daniel-compact", rate: 0.52, pitch: 1.0)),
        ]
#endif
    }
    
    public subscript(name : String?) -> VoiceCharacteristics?
    {
        get {
         
            guard let name = name else { return nil }
            
            return voiceDirectory[name]?.current
        }
        
        set {
           
            guard let name = name, let newValue = newValue else { return }
            
            voiceDirectory[name]?.current = newValue
        }
    }
    
    public mutating func resetVoice(forName name: String?)
    {
        guard let name = name else { return }
        
        voiceDirectory[name]?.reset()
    }
}
