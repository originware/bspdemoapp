//
//  Models/AvatarSpeechRecognitionBehaviourModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Combine
import Speech
import Contacts

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The synchronisation Actor for the Speech Recognition Service and sub-systems.
//

@globalActor
struct SpeechRecognitionActor
{
    actor ActorType {}

    static let shared: ActorType = ActorType()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The Speech Recognition Service. Generates Service Actor events.
//

@SpeechRecognitionActor
public final class SpeechRecognitionService: NSObject, Traceable, SFSpeechRecognizerDelegate
{
    public struct Constant
    {
        public static let SpeechRecognitionOperationalOnline  = "speech recognition operating online"
        public static let SpeechRecognitionOperationalOffline = "Speech recognition operating offline"
        public static let SpeechRecognitionNotAvailable       = "speech recognition is not available"
        public static let SpeechRecognitionNotAuthorized      = "speech recognition is not authorised"
        public static let SpeechRecognitionInUnknownState     = "speech recognition is in an unknown state"

        public static let MicrophoneAccessAuthorized          = "microphone access authorized"
        public static let MicrophoneAccessNotAuthorized       = "microphone access not authorized"
        public static let MicrophoneAccessRestricted          = "microphone access not authorized"
        public static let MicrophoneAccessInUnknownState      = "microphone access is in an unknown state"
    }

    public var isAvailable: Bool                    { speechRecognizer?.isAvailable ?? false }
    public var isStreaming: Bool                    { m_streamingStartTime != nil }

    private let m_serviceEventNotifier: ServiceActor.ServiceEventNotifier
    
    public var lastStreamingDuration                 = TimeInterval(0)
    public var speechRecognizer: SFSpeechRecognizer?

    private var m_recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private let m_audioEngine                       = AVAudioEngine()
    private let m_audioMixer                        = AVAudioMixerNode()
    private let m_speechRecogAudioFormat: AVAudioFormat
    private var m_currentSpeechRecognitionUnavailabilityStatus : String?
    private var m_currentMicrophoneUnavailabilityStatus : String?

    private var m_streamingStartTime: Date?         = nil
    private var m_recognitionTask: SFSpeechRecognitionTask?

    @MainActor
    init(appBuilder: IAppBuilder)
    {
#if targetEnvironment(macCatalyst)
        
        // iOS/MacCatalyst bug fix, some versions didn't set the recognition audio channel properties.
        switch UIDevice.current.model
        {
            case "iPhone":

                self.m_speechRecogAudioFormat = AVAudioFormat(commonFormat: .pcmFormatInt16, sampleRate: 16000, channels: 1, interleaved: true)!

            default:

                self.m_speechRecogAudioFormat = AVAudioFormat(commonFormat: .pcmFormatInt16, sampleRate: 16000, channels: 2, interleaved: true)!
        }

#else
        self.m_speechRecogAudioFormat = AVAudioFormat(commonFormat: .pcmFormatInt16, sampleRate: 16000, channels: 2, interleaved: true)!
#endif

        self.speechRecognizer = appBuilder.buildSpeechRecognizer()
        self.m_serviceEventNotifier = appBuilder.notifierDirectory.service.event

        super.init()

        self.speechRecognizer?.delegate = self

        if #available(iOS 13, macCatalyst 13.4, *),
           let speechRecognizer = speechRecognizer,
           speechRecognizer.supportsOnDeviceRecognition
        {
            trace("SpeechRecognitionService: device supports on device (offline) speech recognition")
        }
        
        do
        {
            let session = AVAudioSession.sharedInstance()
            try session.setActive(false, options: .notifyOthersOnDeactivation)

            switch UIDevice.current.model
            {
                case "iPhone":

                    try session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.spokenAudio, options: [.interruptSpokenAudioAndMixWithOthers, .defaultToSpeaker])

                default:

                    try session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.spokenAudio, options: [.interruptSpokenAudioAndMixWithOthers])
           }
            
           try session.setActive(true, options: .notifyOthersOnDeactivation)

            if let availableInputs = session.availableInputs
            {
                // Select mic input as a preference
                for input in availableInputs where input.portType == AVAudioSession.Port.builtInMic
                {
                    try session.setPreferredInput(input)
                }
            }
        }
        catch let error
        {
            trace("SpeechRecognitionService:error: \(String(describing: error))")
        }

        let inputNode   = m_audioEngine.inputNode
        let inputFormat = inputNode.outputFormat(forBus: 0)

        m_audioEngine.attach(m_audioMixer)
        m_audioEngine.connect(m_audioEngine.inputNode, to: m_audioMixer, format: inputFormat)
        m_audioEngine.disconnectNodeOutput(m_audioMixer)
        m_audioMixer.volume = 1.0
        m_audioMixer.outputVolume = 1.0

        trace(title: "SpeechRecognitionService", "Construct Service")
    }

    deinit
    {
        trace(title: "SpeechRecognitionService", "Destruct Service")
    }
    
    public func startStreaming(resultHandler: @escaping (SFSpeechRecognitionResult?, Error?) -> Void)
    {
        guard let speechRecognizer = speechRecognizer else { return }
        
        let recognitionRequest = SFSpeechAudioBufferRecognitionRequest()

        do // Audio Engine
        {
            let inputFormat               = m_audioEngine.outputNode.outputFormat(forBus: 0)
            let speechRecogFormat         = m_speechRecogAudioFormat
            let sampleRateConversionRatio = inputFormat.sampleRate / speechRecogFormat.sampleRate
                                                         
            func resample(buffer: AVAudioPCMBuffer, format: AVAudioFormat) -> AVAudioPCMBuffer
            {
                let convertedCapacity = AVAudioFrameCount(Double(buffer.frameCapacity) / sampleRateConversionRatio)

                guard  let converter = AVAudioConverter(from: format, to: speechRecogFormat),
                   let convertedBuffer = AVAudioPCMBuffer(pcmFormat: speechRecogFormat, frameCapacity: convertedCapacity) else { return buffer }

                var error: NSError? = nil
                var gotData         = false

                converter.convert(to: convertedBuffer, error: &error, withInputFrom: { (_, outStatus) -> AVAudioBuffer? in

                    guard !gotData else
                    {
                        outStatus.pointee = .noDataNow
                        return nil
                    }

                    gotData = true
                    outStatus.pointee = .haveData

                    return buffer
                })

                return error == nil ? convertedBuffer : buffer
            }

            // Uncomment for debug.
            //print(">> recognition request native audio format: \(recognitionRequest.nativeAudioFormat) conversion format \(speechRecogFormat)")
            
            m_audioMixer.installTap(onBus: 0, bufferSize: 1024, format: inputFormat) { (buffer, _) in

                let convertedBuffer = resample(buffer: buffer, format: inputFormat)

                recognitionRequest.append(convertedBuffer)
            }

            try? m_audioEngine.start()
        }
        
        do // Speech recog Tasking
        {
            // Cancel previous task.
            if let task = m_recognitionTask
            {
                task.cancel()
                m_recognitionTask = nil
            }

            let recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: resultHandler)

            // For debug purposes:
            //recognitionRequest.requiresOnDeviceRecognition = true
            recognitionRequest.shouldReportPartialResults = false
            recognitionRequest.contextualStrings = ["Computer", "Home services", "House services", "Timetable agent", "Conversion agent", "Kitchen services",
                                                    "google", "web page",
                                                    "wikipedia",
                                                    "show", "hide", "display", "remove", "dismiss"
            ]

            m_recognitionRequest = recognitionRequest
            m_recognitionTask = recognitionTask
        }
    }

    public func stopStreaming(onCompletion: @escaping () -> Void)
    {
        if let recognitionRequest = m_recognitionRequest
        {
            recognitionRequest.endAudio()
        }
        
        if let task = m_recognitionTask
        {
            task.finish()
            m_recognitionTask = nil
            m_recognitionRequest = nil
        }

        m_audioEngine.stop()
        m_audioMixer.removeTap(onBus: 0)

        onCompletion()
    }

    public func requestMicrophoneAuthorization() async
    {
        guard #available(macCatalyst 14.0, *) else { return }
        
        let _ : Void = await withCheckedContinuation { continuation in
            
            switch AVCaptureDevice.authorizationStatus(for: .audio)
            {
                case .authorized:

                    m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessAuthorized
                    m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eIsAvailable)))
                    
                    continuation.resume()

                case .notDetermined:

                    AVCaptureDevice.requestAccess(for: .audio) { [weak self] granted in

                        guard let strongSelf = self else { return }

                        if !granted
                        {
                            strongSelf.m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessNotAuthorized
                            strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eFailure("Microphone authorization denied"))))
                        }
                        else
                        {
                            strongSelf.m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessAuthorized
                            strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eIsAvailable)))
                        }
                        
                        continuation.resume()
                    }

                case .denied:

                    m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessNotAuthorized
                    m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eFailure("Microphone authorization denied"))))
                    continuation.resume()

                case .restricted:

                    m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessRestricted
                    m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eFailure("Microphone authorization restricted"))))
                    continuation.resume()

                default:

                    m_currentMicrophoneUnavailabilityStatus = Constant.MicrophoneAccessInUnknownState
                    m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eMicrophone(.eFailure("Microphone authorization unknown"))))
                    continuation.resume()
            }
        }
    }

    public func requestSpeechRecogAuthorization() async
    {
        let _ : Void = await withCheckedContinuation { continuation in
            
            SFSpeechRecognizer.requestAuthorization { [weak self] (authStatus) in

                guard let strongSelf = self else { return }

                switch authStatus {

                    case .authorized, .restricted:

                        strongSelf.m_currentSpeechRecognitionUnavailabilityStatus = nil

                        if strongSelf.speechRecognizer?.isAvailable == true
                        {
                            strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eOfflineOperation)))
                        }
                        
                        continuation.resume()

                    case .denied:

                        strongSelf.m_currentSpeechRecognitionUnavailabilityStatus = Constant.SpeechRecognitionNotAuthorized
                        strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eIsNotAvailable("Speech Recognition authorization denied"))))
                        
                        continuation.resume()
                 
                    case .notDetermined:

                        strongSelf.m_currentSpeechRecognitionUnavailabilityStatus = Constant.SpeechRecognitionNotAuthorized
                        strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eIsNotAvailable("Speech Recognition authorization not determined"))))
                        
                        continuation.resume()
                    
                    @unknown default:

                        strongSelf.m_currentSpeechRecognitionUnavailabilityStatus = Constant.SpeechRecognitionInUnknownState
                        strongSelf.m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eIsNotAvailable("Speech Recognition authorization unknown"))))
                        
                        continuation.resume()
                }
            }
        }
    }

    public nonisolated func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool)
    {
        if available
        {
            m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eOfflineOperation)))
        }
        else
        {
            m_serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eSpeechRecognitionService(.eIsNotAvailable("unknown reason"))))
        }
    }
}
