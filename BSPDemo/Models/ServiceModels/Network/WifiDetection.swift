//
//  Models/ServiceModels/Network/WifiDetection.swift
//  BSPDemo
//
//  Created by Terry Stillone on 24/12/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation

import Network
import UIKit
import SystemConfiguration
import SystemConfiguration.CaptiveNetwork

extension UIDevice
{
    @objc var wifiSSID: String?
    {
        guard #available(macCatalyst 14.0, macOS 10.12, iOS 12, *) else { return nil }
        guard let interfaces = CNCopySupportedInterfaces() as? [String] else { return nil }
        
        let key = kCNNetworkInfoKeySSID as String

        for interface in interfaces
        {
            guard let interfaceInfo = CNCopyCurrentNetworkInfo(interface as CFString) as NSDictionary? else { continue }

            return interfaceInfo[key] as? String
        }

        return nil
    }
}

