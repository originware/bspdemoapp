//
//  Models/ServiceModels/Network/NetworkReachabilityService.swift
//  BSPDemo
//
//  Created by Terry Stillone on 24/12/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation
import Network
import Combine
import UIKit
import SystemConfiguration

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Network Reachability Service. Generates Service Actor events.
///

public final class NetworkReachabilityService: Traceable
{
    public enum eReachabilityStatus : Equatable, CustomStringConvertible
    {
        case eIsReachable(String, String?)
        case eIsNotReachable
        case eFailure

        public var isReachable : Bool
        {
            guard case .eIsReachable = self else { return false }

            return true
        }
        
        public var description : String
        {
            switch self
            {
                case .eIsReachable(let description, _):         return "eIsReachable(\(description))"
                case .eIsNotReachable:                          return "eIsNotReachable"
                case .eFailure:                                 return "eFailure"
            }
        }
    }

    fileprivate static let NetReachabilityServiceHostName = "www.google.com"

    public var currentReachabilityStatus : eReachabilityStatus { m_reachabilityHandler.currentReachabilityStatus }
    
    private let m_reachabilityHandler: NWPathNetworkReachabilityHandler

    init(appBuilder : IAppBuilder)
    {
        self.m_reachabilityHandler = NWPathNetworkReachabilityHandler(appBuilder: appBuilder)

        trace(title: "NetworkReachabilityService", "Construct Service")
    }

    deinit
    {
        trace(title: "NetworkReachabilityService", "Destruct Service")
    }
}

fileprivate class NWPathNetworkReachabilityHandler
{
    private static let InterfacePrecedence : [NWInterface.InterfaceType : Int] = [

        .wiredEthernet : 0,
        .cellular : 1,
        .wifi : 2
    ]

    /// The last reachability event observed.
    var currentReachabilityStatus = NetworkReachabilityService.eReachabilityStatus.eFailure
    
    private let m_monitor : NWPathMonitor
    private let m_monitorQueue = DispatchQueue(label: "reachability.originware", qos: .background)

    public init(appBuilder: IAppBuilder)
    {
        let serviceEventNotifier    = appBuilder.notifierDirectory.service.event
        let monitor                 = appBuilder.buildNWPathMonitor()

        self.m_monitor = monitor

        monitor.pathUpdateHandler = { [weak self] path in

            guard let strongSelf = self else { return }

            var preferredInterfaceType: NWInterface.InterfaceType? = nil

            func updatePreferredInterface(_ interface: NWInterface)
            {
                guard let currentPreferredInterfaceType = preferredInterfaceType else
                {
                    preferredInterfaceType = interface.type
                    return
                }

                if let currentPrecedence = NWPathNetworkReachabilityHandler.InterfacePrecedence[currentPreferredInterfaceType],
                   let interfacePrecedence = NWPathNetworkReachabilityHandler.InterfacePrecedence[interface.type],
                   currentPrecedence < interfacePrecedence
                {
                    preferredInterfaceType = interface.type
                }
            }

            func getAvailability() -> (Bool, String)
            {
                guard case .satisfied = path.status else    { return (false, "") }
        
                switch preferredInterfaceType
                {
                    case .wifi:

                        if let ssid = UIDevice.current.wifiSSID
                        {
                            strongSelf.currentReachabilityStatus = .eIsReachable("wifi", ssid)
              
                            return (true, ssid)
                        }
                        else
                        {
                            strongSelf.currentReachabilityStatus = .eIsReachable("wifi", nil)
                            
                            return (true, "wifi")
                        }

                    case .cellular:

                        strongSelf.currentReachabilityStatus = .eIsReachable("cellular", nil)
                        
                        return (true, "cellular")

                    case .wiredEthernet:

                        strongSelf.currentReachabilityStatus = .eIsReachable("ethernet", nil)

                        return (true, "ethernet")
                        
                    case .loopback, .other, nil:

                        break

                    default:

                        break
                }
                
                return (false, "")
            }
            
            for interface in path.availableInterfaces
            {
                switch interface.type
                {
                    case .wifi, .cellular, .wiredEthernet:

                        updatePreferredInterface(interface)

                    case .loopback, .other, nil:

                        break

                    default:

                        break
                }
            }

            let (isAvailable, _) = getAvailability()
         
            if !isAvailable || preferredInterfaceType == nil
            {
                strongSelf.currentReachabilityStatus = .eIsNotReachable
            }
  
            serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eReachabilityService(strongSelf.currentReachabilityStatus)))
        }

        monitor.start(queue: m_monitorQueue)
    }
}

// Legacy Network Reachability is not being used.
fileprivate class LegacyNetworkReachabilityHandler
{
    typealias NetworkReachabilityNotifier = (NetworkReachabilityService.eReachabilityStatus) -> Void

    var m_reachabilityNotifier : NetworkReachabilityNotifier? = nil

    /// The last reachability event observed.
    var currentReachabilityStatus = NetworkReachabilityService.eReachabilityStatus.eFailure

    /// The reachability context.
    fileprivate var m_contextPointer = UnsafeMutablePointer<SCNetworkReachabilityContext>.allocate(capacity: 1)

    fileprivate init(appBuilder: IAppBuilder)
    {
        let serviceEventNotifier = appBuilder.notifierDirectory.service.event

        m_reachabilityNotifier = { (reachability) in

            serviceEventNotifier.send(.eOnServiceAvailabilityChange(.eReachabilityService(reachability)))
        }

        registerForReachabilityChanges()
    }

    /// Register with SCNetworkReachability for reachability changes.
    @discardableResult func registerForReachabilityChanges() -> Bool
    {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, NetworkReachabilityService.NetReachabilityServiceHostName) else
        {
            currentReachabilityStatus = .eFailure
            m_reachabilityNotifier?(.eFailure)
            return false
        }

        let selfPointer = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        let context = SCNetworkReachabilityContext(version: 0, info: selfPointer, retain: nil, release: nil, copyDescription: nil)

        let callback : SCNetworkReachabilityCallBack = { (reachability: SCNetworkReachability,
                                                          flags: SCNetworkReachabilityFlags,
                                                          info: UnsafeMutableRawPointer?) in

            let isReachable : Bool = flags.contains(SCNetworkReachabilityFlags.reachable)
            let needsConnection : Bool = flags.contains(SCNetworkReachabilityFlags.connectionRequired)
            let ssid : String? = UIDevice.current.wifiSSID
            let currentReachability : NetworkReachabilityService.eReachabilityStatus = (isReachable && !needsConnection) ? .eIsReachable("wifi", ssid) : .eIsNotReachable;

            let handler = Unmanaged<LegacyNetworkReachabilityHandler>.fromOpaque(info!).takeUnretainedValue()

            if (handler.currentReachabilityStatus != currentReachability)
            {
                handler.currentReachabilityStatus = currentReachability
                handler.m_reachabilityNotifier?(currentReachability)
            }
        }

        m_contextPointer.initialize(to: context)

        SCNetworkReachabilitySetCallback(reachability, callback, m_contextPointer)
        SCNetworkReachabilityScheduleWithRunLoop(reachability, CFRunLoopGetMain(), CFRunLoopMode.commonModes.rawValue)

        return true
    }

    /// De-register with SCNetworkReachability for reachability changes.
    func deregisterForReachabilityChanges(_ hostName: String)
    {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, hostName) else { return }

        SCNetworkReachabilitySetCallback(reachability, nil, nil)
        m_contextPointer.deinitialize(count: 1)
    }
}

extension NetworkReachabilityService.eReachabilityStatus
{
    public static func ==(lhs: NetworkReachabilityService.eReachabilityStatus, rhs: NetworkReachabilityService.eReachabilityStatus) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eIsReachable(let lhsNetwork, let lhsSSID), .eIsReachable(let rhsNetwork, let rhsSSID)):

                return lhsNetwork == rhsNetwork && lhsSSID == rhsSSID

            case (.eIsNotReachable, .eIsNotReachable):

                return true

            case (.eFailure, .eFailure):

                return true

            default:

                return false
        }
    }
}
