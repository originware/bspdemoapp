
//
//  Models/BehaviourModels/Automation/AutomationBehaviourModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 3/1/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation

public class AutomationBehaviourModel : Traceable
{
    public static let AutomationNotifierURI = "/behaviour/automation/request"
    public static let Separator = ";"
    public static let Terminator = "\n"

    fileprivate var m_auditPublisher : Audit.AuditPublisher<Audit.TraceRecord>? = nil
    fileprivate var m_storePublisher : Audit.AuditStore<Audit.TraceRecord>? = nil

    private let m_appBuilder : IAppBuilder
  
    init(appBuilder: IAppBuilder)
    {
        self.m_appBuilder = appBuilder
    }
    
    func onAutomationRequest(request: String)
    {
#if DEBUG
        if request.last == "."
        {
            Task {
                await process(request: request)
            }
        }
#endif
    }
    
    func process(request: String) async
    {
        guard m_appBuilder.config.testingConfig?.showAutomationView == true  else { return }
        
        let components = request.components(separatedBy: ":")
        
        if let index = components.lastIndex(of: "begin"),
           index == components.count - 2
        {
            let recognitionText = components[index + 1]

            let auditPublisher = Audit.AuditPublisher<Audit.TraceRecord>()
            let storePublisher = Audit.AuditStore<Audit.TraceRecord>(auditPublisher: auditPublisher)
      
            m_auditPublisher = auditPublisher
            m_storePublisher = storePublisher
            
            m_appBuilder.notifierDirectory.behaviour.meta.request.auditPublisher    = auditPublisher
            m_appBuilder.notifierDirectory.behaviour.meta.event.auditPublisher      = auditPublisher
            m_appBuilder.notifierDirectory.service.request.auditPublisher           = auditPublisher
            m_appBuilder.notifierDirectory.service.event.auditPublisher             = auditPublisher
            m_appBuilder.notifierDirectory.presentation.request.auditPublisher      = auditPublisher
            m_appBuilder.notifierDirectory.presentation.event.auditPublisher        = auditPublisher
            
            m_appBuilder.config.testingConfig?.speechRecognitionTextPublisher.send(recognitionText)
            
            m_appBuilder.notifierDirectory.presentation.request.send(.eAutomation(.eReply(request + ":ack")))
            
            return
        }
        else if let index = components.lastIndex(of: "end."),
                index == components.count - 1
        {
            var reply = ""
            
            if let auditRecords = await getAuditRecords()
            {
                reply = auditRecords + "\n"
            }
            
            m_auditPublisher = nil
            m_storePublisher = nil
            
            trace(title: "AutomationBehaviourModel", reply + request + ":ack")
            
            m_appBuilder.notifierDirectory.presentation.request.send(.eAutomation(.eReply(reply + request + ":ack")))
            
            return
        }
        else if let index = components.lastIndex(of: "clear."),
                index == components.count - 1
        {
            m_appBuilder.notifierDirectory.presentation.request.send(.eAutomation(.eClearRequest))
            m_appBuilder.notifierDirectory.presentation.request.send(.eAutomation(.eClearReply))
            
            return
        }
        
        trace(title: "AutomationBehaviourModel", "Invalid automation request: \(request)")
    }
    
    func getAuditRecords() async -> String?
    {
        guard let storePublisher = m_storePublisher,
              let auditPublisher = m_auditPublisher else { return nil }
        
        var auditTraceRecords = ""

        // Complete audit publishing.
        await auditPublisher.sendCompletion()
        
        // Collect the audit records
        for await record in storePublisher.logAsyncStream
        {
            let fields : ContiguousArray<String> = [
                
                String(record.sequenceNo),
                record.notifierURI,
                record.auditRecord.trace
            ]

            auditTraceRecords += fields.joined(separator: AutomationBehaviourModel.Separator) + AutomationBehaviourModel.Terminator
        }
        
        // Populate the auditTextField in the UI with the audit records.
        return !auditTraceRecords.isEmpty ? auditTraceRecords : nil
    }
}


