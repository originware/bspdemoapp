//
//  Models/BehaviourModels/Facility/FacilityAvailabilityBehaviourModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 24/12/21.
//  Copyright (c) 2022 Originware. All rights reserved.
//

import Foundation
import Combine

///
/// The Behaviour model for handling facility availability.
///

public class FacilityAvailabilityBehaviourModel: Traceable
{
    public typealias AvailabilityPublisher = PassthroughSubject<eNotification, Never>
    public typealias eFacility = ServiceActor.ServiceDirectory.eFacility
  
    public enum eNotification
    {
        case eAvailability(eFacility, String?, Bool)
    }

    public var effectiveSpeechRecognitionStatus = ServiceActor.ServiceDirectory.eSpeechRecognitionStatus.eIsNotAvailable("not initialized")

    public var hasResolvedSpeechRecognitionStatus : Bool
    {
        for facility in ServiceActor.ServiceDirectory.eFacility.SpeechRecogDependentFacilities
        {
            if serviceStatus[facility] == nil
            {
                return false
            }
        }
 
        return true
    }
    
    private(set) var serviceStatus = [eFacility : ServiceActor.ServiceDirectory.eFacilityStatus]()

    private let m_behaviourMetaRequestNotifier: BehaviourActor.BehaviourMetaRequestNotifier
    private let m_behaviourMetaEventNotifier:   BehaviourActor.BehaviourMetaEventNotifier
    private var m_hasResolvedSpeechRecognitionStatus = false
    
    public init(notifierDirectory: NotifierDirectory)
    {
        self.m_behaviourMetaRequestNotifier = notifierDirectory.behaviour.meta.request
        self.m_behaviourMetaEventNotifier = notifierDirectory.behaviour.meta.event
    }
    
    ///
    /// The handler for a facility availability change.
    ///
    
    public func onFacilityChange(facilityStatus: ServiceActor.ServiceDirectory.eFacilityStatus)
    {
        let facility = facilityStatus.facility
        
        if serviceStatus[facility] != facilityStatus
        {
            switch facilityStatus
            {
                case .eSpeechRecognitionService:
            
                    let fromSpeechRecogStatus = resolveEffectiveSpeechRecognitionStatus()
  
                    serviceStatus[facility] = facilityStatus
                    
                    let toSpeechRecogStatus = resolveEffectiveSpeechRecognitionStatus()
           
                    if case .eIsNotReady = toSpeechRecogStatus
                    {
                        // Ignore
                    }
                    else
                    {
                        if let vocalAlert = describeSpeechRecognitionStatusChange(from: fromSpeechRecogStatus, to: toSpeechRecogStatus)
                        {
                            m_behaviourMetaRequestNotifier.send(.eAvatar(.eSpeaking(.eSpeak(vocalAlert, isAlert: true))))
                        }

                        effectiveSpeechRecognitionStatus = toSpeechRecogStatus
                    }

                case .eReachabilityService:
                    
                    let fromSpeechRecogStatus = resolveEffectiveSpeechRecognitionStatus()
                    
                    serviceStatus[facility] = facilityStatus
                    
                    let toSpeechRecogStatus = resolveEffectiveSpeechRecognitionStatus()
                    
                    if let vocalAlert = describeSpeechRecognitionStatusChange(from: fromSpeechRecogStatus, to: toSpeechRecogStatus)
                    {
                        m_behaviourMetaRequestNotifier.send(.eAvatar(.eSpeaking(.eSpeak(vocalAlert, isAlert: true))))
                    }
                    
                    effectiveSpeechRecognitionStatus = toSpeechRecogStatus
                    
                case .eMicrophone:

                    serviceStatus[facility] = facilityStatus
                    effectiveSpeechRecognitionStatus = resolveEffectiveSpeechRecognitionStatus()
                    
                case .eSpeechSynthService:
                    
                    serviceStatus[facility] = facilityStatus
                    
                case .eLocationService:
                    
                    serviceStatus[facility] = facilityStatus
            }
            
            if !m_hasResolvedSpeechRecognitionStatus && hasResolvedSpeechRecognitionStatus
            {
                m_hasResolvedSpeechRecognitionStatus = true

                m_behaviourMetaEventNotifier.send(.eFacilityEvent(.eSpeechRecognitionAvailabilityChange(effectiveSpeechRecognitionStatus)))
            }
        }
    }
   
    private func resolveEffectiveSpeechRecognitionStatus() -> ServiceActor.ServiceDirectory.eSpeechRecognitionStatus
    {
        let microphoneStatus : Bool = serviceStatus[.eMicrophone]?.isAvailable ?? false
        let networkReachability = serviceStatus[.eReachabilityService]
        let speechRecognitionStatus = serviceStatus[.eSpeechRecognitionService]

        switch (microphoneStatus, networkReachability, speechRecognitionStatus)
        {
            case (true, .eReachabilityService(let networkReachability), .eSpeechRecognitionService(.eOnlineOperation)),
                (true, .eReachabilityService(let networkReachability), .eSpeechRecognitionService(.eOfflineOperation)):
                
                switch networkReachability
                {
                    case .eIsReachable(let network, nil):

                        return .eOnlineOperation("\(network) network")

                    case .eIsReachable(let network, let ssid?):

                        return .eOnlineOperation("\(network) SSID \(ssid)")
   
                    case .eIsNotReachable, .eFailure:
                        
                        return .eOfflineOperation
                }
       
            case (false, _, _):
                
                return .eIsNotAvailable("Microphone not accessible")
                
            case (_, _, nil):
                
                return .eIsNotReady

            case (_, nil, _):
                
                return .eIsNotAvailable("Speech recognition authorization currently unknown")
                
            case (_, _?, .eSpeechRecognitionService(let status)):
                
                return status
                
            case (true, _, _?):
                
                // Unexpected code point
                return .eIsNotAvailable("internal error")
        }
    }

    private func describeSpeechRecognitionStatusChange(from fromStatus : ServiceActor.ServiceDirectory.eSpeechRecognitionStatus,
                                                       to toStatus: ServiceActor.ServiceDirectory.eSpeechRecognitionStatus) -> String?
    {
        switch (fromStatus, toStatus)
        {
            case (.eIsNotAvailable, .eOnlineOperation(let networkInterface)),
                (.eIsNotReady, .eOnlineOperation(let networkInterface)):
                
                if let networkInterface = networkInterface
                {
                    return "Attention, online speech recognition is now available on the \(networkInterface) interface"
                }
                else
                {
                    return "Attention, online speech recognition is now available"
                }

            case (.eIsNotAvailable, .eOfflineOperation),
                (.eIsNotReady, .eOfflineOperation):

                return "Attention, offline speech recognition is now available."

            case (.eOfflineOperation, .eOnlineOperation(let networkInterface)):

                if let networkInterface = networkInterface
                {
                    return "Alert, switching to online speech recognition on the \(networkInterface) interface"
                }
                else
                {
                    return "Alert, switching to online speech recognition"
                }
    
            case (.eOnlineOperation(let fromNetworkInterface), .eOnlineOperation(let toNetworkInterface)):

                if fromNetworkInterface != toNetworkInterface
                {
                    if let toNetworkInterface = toNetworkInterface
                    {
                        return "Alert, switching online speech recognition to the \(toNetworkInterface) interface"
                    }
                    else
                    {
                        return "Alert, network has changed"
                    }
                }

            case (.eOnlineOperation, .eIsNotAvailable),
                 (.eOfflineOperation, .eIsNotAvailable):

                return "Alert, speech recognition is now unavailable"

            case (.eOnlineOperation, .eOfflineOperation):

                return "Alert, switching to offline speech recognition"

            default:

                break
        }

        return nil
    }
}




