
//
//  Models/BehaviourModels/Avatar/AvatarBehaviourModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 19/12/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine

///
/// Avatar Behaviour Model
/// 

public class AvatarBehaviourModel: Traceable
{
    public static let GestureRequestNotifierURI = "/behaviour/avatar/gestureRequest"
    
    public enum eAvatarGestureOp : CustomStringConvertible
    {
        case eAssemble(duration: TimeInterval = 0.7)
        
        case eBeginSalute(duration: TimeInterval = 0.5)
        case eEndSalute(duration: TimeInterval = 0.5)
        
        case eBeginListening(duration: TimeInterval = 0.05)
        case eEndListening(duration: TimeInterval = 0.2)
        case eCancelListening(duration: TimeInterval = 0.3)
        
        case eBeginSpeaking(duration: TimeInterval = 0.05, isAlert: Bool)
        case eEndSpeaking(duration: TimeInterval = 0.1)
        case eCancelSpeaking(duration: TimeInterval = 0.1)
        
        case eSpeakAndVocalize(vocalize: String, isAlert: Bool)
        case eSaluteAndVocalize(vocalize: String)
        
        case eVocalize(String)
        case ePause(duration: TimeInterval)
        
        public var description : String
        {
            switch self
            {
                case .eAssemble(duration: let duration):                    return "eAssemble(\(duration))"
                case .eBeginSalute(duration: let duration):                 return "eBeginSalute(\(duration))"
                case .eEndSalute(duration: let duration):                   return "eEndSalute(\(duration))"
                case .eBeginListening(duration: let duration):              return "eBeginListening(\(duration))"
                case .eEndListening(duration: let duration):                return "eEndListening(\(duration))"
                case .eCancelSpeaking(duration: let duration):              return "eCancelSpeaking(\(duration))"
                case .eCancelListening(duration: let duration):             return "eCancelListening(\(duration))"
                case .eEndSpeaking(duration: let duration):                 return "eEndSpeaking(\(duration))"
                case .eSaluteAndVocalize(vocalize: let vocalize):           return "eSaluteAndVocalize(\(vocalize))"
                case .eVocalize(let vocalString):                           return "eVocalize(\(vocalString))"
                case .ePause(duration: let duration):                       return "ePause(\(duration))"
                   
                case .eBeginSpeaking(duration: let duration, isAlert: let isAlert):
                    
                    return "eBeginSpeaking(\(duration), isAlert(\(isAlert)))"
    
                case .eSpeakAndVocalize(vocalize: let vocalize, isAlert: let isAlert):
                    
                    return "eSpeakAndVocalize(\(vocalize), isAlert(\(isAlert)))"
            }
        }
    }
    
    public enum eAvatarListenState : CustomStringConvertible
    {
        case eIsNotListening
        case eIsListening
        
        public var description : String
        {
            switch self
            {
                case .eIsNotListening:      return "eIsNotListening"
                case .eIsListening:         return "eIsListening"
            }
        }
    }

    fileprivate actor Executor
    {
        private var m_isInExecution = false

        fileprivate func run(_ executionFunc: () async -> Void) async
        {
            let startTimestamp = Date()

            // Work around for reentrancy deficiencies in Actors.
            // The executed threads are sometimes yielded/slept and this causes the Actors thread atomicity to be broken.
            // So we perform incremental sleeps until the closure has returned.
            while m_isInExecution && -startTimestamp.timeIntervalSinceNow < 20
            {
                try? await Task.sleep(nanoseconds: 50_000_000)
            }

            m_isInExecution = true

            await executionFunc()

            m_isInExecution = false
        }
    }

    public var listenState = PassthroughSubject<eAvatarListenState, Never>()

    private let m_executor = Executor()
    private let m_notifiers : NotifierDirectory

    private var m_cancelThresholdTimestamp: Date? = nil

    public init(notifierDirectory: NotifierDirectory)
    {
        self.m_notifiers = notifierDirectory
    }
    
    public func perform(_ gesture: AvatarBehaviourModel.eAvatarGestureOp) async
    {
        func isCancelled() -> Bool
        {
            if case .eCancelSpeaking = gesture
            {
                return false
            }
            else if let cancelThresholdTimestamp = m_cancelThresholdTimestamp
            {
                if Date() < cancelThresholdTimestamp
                {
                    return true
                }
                else
                {
                    m_cancelThresholdTimestamp = nil

                    return false
                }
            }
            else
            {
                return false
            }
        }

        func performGestureAnimation(duration: TimeInterval, fromGesture: eAvatarGestureAnimationOp, toGesture: eAvatarGestureAnimationOp)
        {
            m_notifiers.presentation.request.send(.eAvatarGesture(.ePerformGestureAnimation(duration: duration, fromGesture: fromGesture, toGesture: toGesture)))
        }

        func performVocalize(vocalText: String, onComplete: @escaping () async -> Void) async
        {
            await m_notifiers.service.queuedSpeechSynthNotifier.sendAsync(.eSpeak(vocalText, progressReportFunc: { [weak self] (progressEvent) in

                func formatAnnotation(interaction: SpeechSynthService.eInteraction) -> AttributedString
                {
                    switch interaction
                    {
                        case .eConverse(let fromName, let toName):

                            return AttributedString("\(fromName) -> \(toName): ", attributes: Styles.annotationTextStyle)

                        case .eSpeakToUser(let speaker):

                            if let speaker = speaker
                            {
                                return AttributedString("\(speaker): ", attributes: Styles.annotationTextStyle)
                            }
                            else
                            {
                                return AttributedString("Remark:       ", attributes: Styles.annotationTextStyle)
                            }
                    }
                }

                guard let strongSelf = self else { return }

                switch progressEvent
                {
                    case .eOnUtterance(let interaction, let vocalText, let characterRange):

                        let highlightAttributes = Styles.highlightBodyTextStyle
                        let annotationText = formatAnnotation(interaction: interaction)

                        // When an utterance is beginning.
                        var bodyText : AttributedString = vocalText
                        let notifiers = strongSelf.m_notifiers

                        if let characterRange = characterRange
                        {
                            let highlightedText = AttributedString(bodyText[characterRange])

                            bodyText[characterRange].setAttributes(highlightAttributes)
                            
                            let traceMessage = "progressEvent(eOnUtterance(" + NSAttributedString(highlightedText).string + "))"

                            strongSelf.trace(title: "    AvatarBehaviourModel", traceMessage)

                            let taskBodyText = bodyText

                            Task {

                                await MainActor.run {

                                    notifiers.presentation.request.setNoTrace({

                                        notifiers.presentation.request.send(.eSetTextField(.eResponseTextField(annotationText + " " + taskBodyText)))
                                    })
                                }
                            }
                        }
                        else
                        {
                            let taskBodyText = bodyText

                            Task {

                                await MainActor.run {

                                    notifiers.presentation.request.send(.eSetTextField(.eResponseTextField(annotationText + " " + taskBodyText)))
                                }
                            }
                        }

                    case .eEndSpeaking(let interaction, let vocalText):

                        let annotationText = formatAnnotation(interaction: interaction)
                        let appendingAnnotationText = AttributedString(" (voiced)", attributes: Styles.annotationTextStyle)
                        
                        // When all sentences have been voiced.
                        var bodyText = vocalText
                        let notifiers = strongSelf.m_notifiers

                        if bodyText.characters.count > 0
                        {
                            bodyText.append(appendingAnnotationText)
                        }

                        let traceMessage = "progressEvent(eEndSpeaking(\(bodyText)))"

                        strongSelf.trace(title: "    AvatarBehaviourModel", traceMessage)

                        notifiers.behaviour.meta.event.auditPublisher?.send(Audit.TraceRecord(notifiers.behaviour.meta.event.uri, traceMessage))

                        let taskBodyText = bodyText
                        
                        Task {

                            await onComplete()

                            await MainActor.run {

                                notifiers.presentation.request.send(.eSetTextField(.eResponseTextField(annotationText + " " + taskBodyText)))
                            }
                        }

                    case .eCancelSpeaking(let interaction, let vocalText):

                        let appendingText = AttributedString(" (cancelled)", attributes: Styles.annotationTextStyle)
                        let annotationText = formatAnnotation(interaction: interaction)
                    
                        // Vocalization cancel.
                        var bodyText = vocalText
                        let notifiers = strongSelf.m_notifiers

                        bodyText.append(appendingText)

                        strongSelf.trace(title: "    AvatarBehaviourModel", "progressEvent(eCancelSpeaking(\(bodyText)))")
                        notifiers.behaviour.meta.event.auditPublisher?.send(Audit.TraceRecord(notifiers.behaviour.meta.event.uri, "[Cancel Vocal Command]"))

                        let taskBodyText = bodyText
                        
                        Task {

                            await MainActor.run {

                                notifiers.presentation.request.send(.eSetTextField(.eResponseTextField(annotationText + " " + taskBodyText)))
                            }

                            await onComplete()
                        }
                }
            }))
        }
        
        func internalPerform(_ gesture: AvatarBehaviourModel.eAvatarGestureOp) async
        {
            switch gesture
            {
                case .eAssemble(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eDisassemble, toGesture: .eAssemble)

                case .eBeginSalute(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eWait, toGesture: .eSalute)

                case .eEndSalute(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eSalute, toGesture: .eWait)

                case .eBeginListening(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eWait, toGesture: .eListen)

                case .eEndListening(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eListen, toGesture: .eWait)

                case .eCancelListening(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eListen, toGesture: .eWait)

                case .eBeginSpeaking(let duration, let isAlert):

                    performGestureAnimation(duration: duration, fromGesture: .eWait, toGesture: isAlert ? .eAlert : .eSpeak)

                case .eEndSpeaking(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eSpeak, toGesture: .eWait)

                case .eCancelSpeaking(let duration):

                    m_notifiers.presentation.request.send(.eAvatarGesture(.eCancelGestures))

                    performGestureAnimation(duration: duration, fromGesture: .eSpeak, toGesture: .eWait)

                case .ePause(let duration):

                    performGestureAnimation(duration: duration, fromGesture: .eSpeak, toGesture: .ePause)

                case .eSpeakAndVocalize(let vocalText, let isAlert):

                    await internalPerform(.eBeginSpeaking(isAlert: isAlert))

                    await performVocalize(vocalText: vocalText, onComplete: {

                        await internalPerform(.eEndSpeaking())
                                
                    })

                case .eSaluteAndVocalize(let vocalText):

                    await internalPerform(.eBeginSalute())

                    await performVocalize(vocalText: vocalText, onComplete: {

                        let _ = await internalPerform(.eEndSalute())
                    })

                case .eVocalize(let vocalText):

                    await performVocalize(vocalText: vocalText, onComplete: {} )
            }
        }

        guard !isCancelled() else {

            m_notifiers.behaviour.meta.event.auditPublisher?.send(Audit.TraceRecord(m_notifiers.behaviour.meta.event.uri, "[Cancel Vocal Command]"))

            return
        }

        await m_executor.run {

            if !isCancelled()
            {
                await internalPerform(gesture)
            }
            else
            {
                m_notifiers.behaviour.meta.event.auditPublisher?.send(Audit.TraceRecord(m_notifiers.behaviour.meta.event.uri, "[Cancel Vocal Command]"))
            }
        }
    }

    public func cancelSpeaking(forDuration duration: TimeInterval?, priority: ServiceActor.eSpeechSynthRequest.eCancelPriority) async
    {
        if let duration = duration
        {
            m_cancelThresholdTimestamp = Date(timeIntervalSinceNow: duration)
        }
        else
        {
            m_cancelThresholdTimestamp = nil
        }

        m_notifiers.service.request.send(.eSpeechSynth(.eCancelSpeaking(duration, priority)))
        await perform(.eCancelSpeaking())
    }
}

