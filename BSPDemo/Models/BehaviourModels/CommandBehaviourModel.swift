
//
//  Models/BehaviourModels/Automation/CommandBehaviourModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 13/1/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Command Behaviour Model. Runs vocal commands.
///

protocol ICommandResponder
{
    func getResponse(toVocalCommand: String, recogStats: BehaviourActor.RecognitionStats?) async -> BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult
}

class CommandBehaviourModel
{
    enum eCommandResponderType
    {
        case eSample
        case eSemanticTranscriptionSDK

        func createCommandResponder(appBuilder: IAppBuilder, actorDirectory: BehaviourActor.ActorDirectory) -> ICommandResponder
        {
            switch self
            {
                case .eSample:

                    return SampleCommandResponder(appBuilder: appBuilder, actorDirectory: actorDirectory)

                case .eSemanticTranscriptionSDK:

                     fatalError("SemanticTranscriptionSDK is not available in the BSP Demo product")
            }
        }
    }

    enum eCommandRequest : CustomStringConvertible
    {
        case eTemporal(ServiceActor.eServiceRequest.eTemporalRequest)
        case eLocation(ServiceActor.eServiceRequest.eLocationRequest)
        case eMessageLocation(MessageScope.eMessageRequest)
        case eWebURL(PresentationActor.ePresentationRequest.eWebURLRequest)
        case ePerformSoundCheck
        case eRecite(String)
        case ePencil(PresentationActor.ePresentationRequest.ePencilRequest)

        public var description: String
        {
            switch self
            {
                case .eTemporal(let request):           return "eTemporal(\(request))"
                case .eMessageLocation(let request):    return "eMessageLocation(\(request))"
                case .eLocation(let request):           return "eLocation(\(request))"
                case .eWebURL(let request):             return "eWeb(\(request))"
                case .ePerformSoundCheck:               return "ePerformSoundCheck"
                case .eRecite(let title):               return "eRecite(\(title))"
                case .ePencil(let request):             return "ePencil(\(request))"
            }
        }
    }

    private let m_commandResponder: ICommandResponder

    init(appBuilder: IAppBuilder, actorDirectory: BehaviourActor.ActorDirectory)
    {
        self.m_commandResponder = appBuilder.config.commandResponderType.createCommandResponder(appBuilder: appBuilder, actorDirectory: actorDirectory)
    }

    func getResponse(vocalCommand: String, recogStats: BehaviourActor.RecognitionStats?) async -> BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult
    {
        await m_commandResponder.getResponse(toVocalCommand: vocalCommand, recogStats: recogStats)
    }
}

class SampleCommandResponder : ICommandResponder, Traceable
{
    public typealias VocalResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.VocalResult
    public typealias eResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult

    private let m_directory:         BehaviourActor.ActorDirectory
    private let m_dateFormatter:     DateFormatter

    init(appBuilder: IAppBuilder, actorDirectory: BehaviourActor.ActorDirectory)
    {
        let dateFormatter       = DateFormatter()

        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short

        self.m_directory     = actorDirectory
        self.m_dateFormatter = dateFormatter
    }

    ///
    ///  Parse a recognized vocal text command.
    ///  Note: This is a simple stub and mock for the actual semantic transcription SDK. This code performs simple word matching.
    ///

    func getResponse(toVocalCommand vocalText: String, recogStats: BehaviourActor.RecognitionStats?) async -> eResult
    {
        let specialCharacters: Set<Character> = [".", "!", "?", ",", ":", "\"", "\'"]

        let words = vocalText
                .filter({ !specialCharacters.contains($0) })
                .lowercased()
                .components(separatedBy: " ")

        var stats = GraphFormatter.Stats(words: words, recogStats: recogStats)

        trace("RequestBehaviourModel process vocalCommand: words: \(words)")

        func match(_ matchWords: ContiguousArray<String>) -> Bool
        {
            for (index, word) in words.enumerated()
            {
                if matchWords.contains(word)
                {
                    stats.matchedWordIndexes.insert(index)
                    return true
                }
            }

            return false
        }

        switch vocalText
        {
                // Wikipedia page search

            case _ where match(["wikipedia"]):

                if match(["search"]),
                   match(["for"]),
                   let index = words.firstIndex(of: "for"),
                   index < words.count - 1
                {
                    let searchText = words[(index+1)...].joined(separator: "%20")
                    let urlString = "https://en.wikipedia.org/wiki/Special:Search?search=" + searchText

                    if let url = URL(string: urlString)
                    {
                        return await graphAndGetResponse(toCommand: .eWebURL(.ePresentURL(url)), stats: stats)
                    }
                    else
                    {
                        let failure = "Sorry, the generated URL was invalid"

                        await m_directory.notifiers.behaviour.meta.request.sendAsync(.eAvatar(.eSpeaking(.eSpeak(failure, isAlert: true))))

                        return .eFailure(failure)
                    }
                }
                
                // Google page display

            case _ where match(["google"]):

                if match(["display", "show"])
                {
                    let url = "https://www.google.com/"

                    return await graphAndGetResponse(toCommand: .eWebURL(.ePresentURL(URL(string: url)!)), stats: stats)
                }

            case _ where match(["web page", "webpage", "web"]):

                if match(["remove", "hide", "dismiss"])
                {
                    return await graphAndGetResponse(toCommand: .eWebURL(.eDismiss), stats: stats)
                }

                // Pencil control

            case _ where match(["controls", "pencil", "pen"]):

                if match(["display", "show"])
                {
                    return await graphAndGetResponse(toCommand: .ePencil(.ePresentControls({ _ in })), stats: stats)
                }
                else if match(["remove", "hide", "dismiss"])
                {
                    return await graphAndGetResponse(toCommand: .ePencil(.eDismissControls({ _ in })), stats: stats)
                }

                // Location or time request

            case _ where match(["what", "get"]):

                if match(["location", "address"])
                {
                    return await graphAndGetResponse(toCommand: .eLocation(.eGetCurrentAddress), stats: stats)
                }
                else if match(["time"])
                {
                    return await graphAndGetResponse(toCommand: .eTemporal(.eGetTime), stats: stats)
                }

                // Message location.

            case _ where match(["message", "messaging", "messages", "send"]):

                if match(["location", "position"])
                {
                    return await graphAndGetResponse(toCommand: .eMessageLocation(.ePresentMessageView), stats: stats)
                }
                else if match(["remove", "hide", "dismiss"])
                {
                    return await graphAndGetResponse(toCommand: .eMessageLocation(.eDismissMessageView), stats: stats)
                }

                // Sound check.

            case _ where match(["give", "perform"]):

                if match(["soundcheck", "sound", "check"])
                {
                    return await graphAndGetResponse(toCommand: .ePerformSoundCheck, stats: stats)
                }

                // Recite

            case _ where match(["recite"]):

                var titleFound: String? = nil

                for (index, word) in words.enumerated()
                {
                    if let title = ReciteExtracts.Keywords[word]
                    {
                        stats.matchedWordIndexes.insert(index)

                        titleFound = title
                    }
                }

                if let titleFound = titleFound
                {
                    return await graphAndGetResponse(toCommand: .eRecite(titleFound), stats: stats)
                }

            default:

                break
        }

        await graph(stats: stats)

        return .eFailure("Sorry, the command \"\(vocalText)\" is not recognized")
    }

    private func graph(stats: GraphFormatter.Stats) async
    {
        guard !stats.words.isEmpty else { return }

        let graphFormatter = GraphFormatter(stats: stats)

        await m_directory.notifiers.presentation.request.sendAsync(.eGraph(graphFormatter.nodes, graphFormatter.edges))
    }

    private func graphAndGetResponse(toCommand command: CommandBehaviourModel.eCommandRequest, stats: GraphFormatter.Stats) async -> eResult
    {
        func failure(_ message : String) async -> eResult
        {
            await graph(stats: stats)

            return .eFailure(message)
        }

        func success(_ vocalResult : VocalResult?) async -> eResult
        {
            await graph(stats: stats)

            return .eSuccess(vocalResult)
        }

        let notifiers = m_directory.notifiers

        switch command
        {
            case .eLocation(.eGetCurrentAddress):

                await graph(stats: stats)

                switch await notifiers.service.request.sendAsync(.eLocation(.eGetCurrentAddress))
                {
                    case .eAddress(let address):

                        return .eSuccess(VocalResult(.eSpeak("The address is" ++ address)))

                    case .eFailure(let failure):

                        return .eFailure(failure)

                    case .eAcknowledge:

                        // Used in unit testing.
                        return .eFailure("Location request not issued")

                    default:

                        return .eFailure("Invalid get address reply")
                }

            case .eMessageLocation(.ePresentMessageView):

                switch await notifiers.service.request.sendAsync(.eLocation(.eGetCurrentAddress))
                {
                    case .eAddress(let address):
                        
                        guard RepresentedUIControllers.MessageComposeView.canSendMessage else
                        {
                            return await failure("Sorry, text messaging is not available on this device.")
                        }
                        
                        if let scopeStack = m_directory.scopeStack
                        {
                            if !scopeStack.hasTop(uri: MessageScope.Behaviour.MessageScopeURI)
                            {
                                let messageTemplate = MessageScope.MessageModel(
                                        subject: "My current location",
                                        body: "My current location is: \(address)"
                                )


                                notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("Your current location is: \(address)", isAlert: false))))

                                let messageScope = MessageScope.Behaviour(messageTemplate: messageTemplate, notifierStore: m_directory.notifiers.notifierStore)

                                await MainActor.run {
                                   notifiers.behaviour.stack.send(.ePush(messageScope))
                                }

                                return await success(VocalResult(.eSpeak("Please enter the message recipients")))
                            }
                            else
                            {
                                return await failure("The message view is already being displayed")
                            }
                        }
                        else
                        {
                            return await failure("Message request not issued")
                        }

                    case .eFailure(let failureMessage):

                        return await failure(failureMessage)

                    case .eAcknowledge:

                        // Used in unit testing.
                        return await failure("Location request not issued")

                    default:

                        return await failure("Invalid get address reply")
                }

            case .eMessageLocation(.eDismissMessageView):

                if let scopeStack = m_directory.scopeStack
                {
                    if scopeStack.hasTop(uri: MessageScope.Behaviour.MessageScopeURI)
                    {
                        await MainActor.run {
                            notifiers.behaviour.stack.send(.ePop(MessageScope.Behaviour.MessageScopeURI))
                        }
                        
                        return await success(VocalResult(.eSpeak("Dismissing message view")))
                    }
                    else
                    {
                        return await failure("Sorry, the message view is not being displayed")
                    }
                }
                else
                {
                    return await failure("Message dismiss request not issued")
                }


            case .eTemporal(.eGetTime):

                await graph(stats: stats)

                switch await notifiers.service.request.sendAsync(.eTemporal(.eGetTime))
                {
                    case .eTime(let time):

                        let result = "The time is" ++ m_dateFormatter.string(from: time)

                        return .eSuccess(VocalResult(.eSpeak(result)))

                    case .eFailure(let failure):

                        return .eFailure(failure)

                    case .eAcknowledge:

                        // Used in unit testing.
                        return .eFailure("Temporal request not issued")

                    default:

                        return .eFailure("Invalid get time reply")
                }

            case .eWebURL(let webRequest):

                switch webRequest
                {
                    case .ePresentURL(let url):

                        var isFinished = false
                        var isFirstProgressVocalisation = true
                        
                        let progressEventFunc: (WebURLScope.eWebPresentProgressEvent) -> Void = { (progressEvent) in

                            switch progressEvent
                            {
                                case .eOnFetch:

                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("Fetching web page", isAlert: false))))

                                case .eDownloadingURL:

                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("Waiting for web page to download", isAlert: false))))

                                case .eDownloadProgress(let progress):

                                    if !isFinished
                                    {
                                        let percentage = Int(progress * 100)
                                        let clampedPercentage = percentage > 0 ? percentage : 1

                                        if isFirstProgressVocalisation
                                        {
                                            isFirstProgressVocalisation = false
                                            notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("The download is \(clampedPercentage)% complete", isAlert: false))))
                                        }
                                        else
                                        {
                                            notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("\(clampedPercentage)%", isAlert: false))))
                                        }
                                    }

                                case .eOnPresent:

                                    isFinished = true
                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: nil, .eLowPriority))))
                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak("Presenting web page", isAlert: false))))

                                case .eOnError(let error):

                                    isFinished = true
                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eCancelSpeaking(forDuration: nil, .eLowPriority))))
                                    notifiers.behaviour.meta.request.send(.eAvatar(.eSpeaking(.eSpeak(error, isAlert: false))))
                            }
                        }

                        if let scopeStack = m_directory.scopeStack
                        {
                            if !scopeStack.hasTop(uri: WebURLScope.Behaviour.WebURLScopeURI)
                            {
                                let webScope = WebURLScope.Behaviour(notifierStore: m_directory.notifiers.notifierStore)

                                await MainActor.run {
                                    notifiers.behaviour.stack.send(.ePush(webScope))
                                }
                            }

                            await notifiers.behaviour.webURL.request?.sendAsync(.ePresentURL(url, progressEventFunc))

                            return await success(nil)
                        }
                        else
                        {
                            return await failure("Web request not issued")
                        }

                    case .eDismiss:

                        if let scopeStack = m_directory.scopeStack
                        {
                            if scopeStack.hasTop(uri: WebURLScope.Behaviour.WebURLScopeURI)
                            {
                                await MainActor.run {
                                    notifiers.behaviour.stack.send(.ePop(WebURLScope.Behaviour.WebURLScopeURI))
                                }

                                return await success(VocalResult("Removing web page", isAlert: true))
                            }
                            else
                            {
                                return await failure("Sorry, there is no web page to dismiss")
                            }
                        }
                        else
                        {
                            return await failure("Web request not issued")
                        }
                }

            case .ePerformSoundCheck:

                let soundCheckVocalOps = VocalResult(vocalOps : [.eSpeak("Sound check"), .ePause(0.3), .eSpeak("Supercalifragilisticexpialidocious")])

                await graph(stats: stats)

                return .eSuccess(soundCheckVocalOps)

            case .eRecite(let title):

                await graph(stats: stats)

                if let vocalOps = ReciteExtracts.ReciteTexts[title]
                {
                    return .eSuccess(vocalOps)
                }
                else
                {
                    return .eFailure("Sorry, I don;t know the extract titled: \(title)")
                }

            case .ePencil(let pencilRequest):

                await graph(stats: stats)

                switch pencilRequest
                {
                    case .ePresentControls:

                        var result = eResult.eFailure("Pencil request not issued")

                        await notifiers.presentation.request.sendAsync(.ePencil(.ePresentControls({ (progressEvent) in

                            result = progressEvent
                        })))

                        return result

                    case .eDismissControls:

                        var result = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult.eFailure("Pencil controls not dismissed")

                        await notifiers.presentation.request.sendAsync(.ePencil(.eDismissControls({ (progressEvent) in

                            result = progressEvent
                        })))

                        return result

                    case .eClearDrawing:

                        return .eSuccess(VocalResult("Clear drawing"))
                }
        }
    }
}

extension SampleCommandResponder
{
    fileprivate struct GraphFormatter
    {
        typealias Node              = GraphViewModel.Node
        typealias Edge              = GraphViewModel.Edge
        typealias eShape            = GraphViewModel.Node.Style.eShape
        typealias RectangleStyle    = Node.Style.RectangleStyle
        typealias EllipseStyle      = Node.Style.EllipseStyle
        typealias eBorderStyle      = Node.Style.eBorderStyle

        fileprivate enum eNodeType
        {
            case eTerminal
            case eBase
        }

        fileprivate struct NonMatchedTerminal
        {
            private static let borderStyle = eBorderStyle.eLine(
                    color:             Styles.Graph.NonMatchedWordTokenColor.solidColor(forAlpha: 0.8),
                    borderWidth:       3
            )

            private static let nodeShape = eShape.eRectangle(RectangleStyle(
                    fillColor:         Styles.Graph.NonMatchedWordTokenColor.solidColor(forAlpha: 0.13),
                    borderStyle:       borderStyle,
                    cornerRadius:      20
            ))

            fileprivate static let nodeStyle = Node.Style(
                    shape:              nodeShape,
                    paddingSize:        nodePaddingSize.size
            )
        }

        fileprivate struct MatchedTerminal
        {
            private static let borderStyle = eBorderStyle.eThreeStripe(
                    color:             Styles.Graph.MatchedWordTokenColor.solidColor(forAlpha: 0.6),
                    borderWidth:       5,
                    stripeColor:       Styles.Graph.MatchedWordStripeColor,
                    stripeWidth:       1.0
            )

            private static let nodeShape = eShape.eRectangle(RectangleStyle(
                    fillColor:         Styles.Graph.NonMatchedWordTokenColor.solidColor(forAlpha: 0.13),
                    borderStyle:       borderStyle,
                    cornerRadius:      20
            ))

            fileprivate static let nodeStyle = Node.Style(
                    shape:              nodeShape,
                    paddingSize:        nodePaddingSize.size
            )
        }

        fileprivate struct Base
        {
            private static let borderStyle       = eBorderStyle.eLine(
                    color:             Styles.Graph.BaseColor.solidColor(forAlpha: 0.8),
                    borderWidth:       3
            )

            private static let nodeShape = eShape.eEllipse(EllipseStyle(
                    fillColor:         Styles.Graph.BaseColor.solidColor(forAlpha: 0.13),
                    borderStyle:       borderStyle
            ))

            fileprivate static let nodeStyle = Node.Style(
                    shape:               nodeShape,
                    paddingSize:         nodePaddingSize.size
            )
        }

        fileprivate struct Stats
        {
            let words              : [String]
            var matchedWordIndexes = Set<Int>()
            let recogStats: BehaviourActor.RecognitionStats?
        }

        private static let edgeStyle = Edge.Style(
                strokeColor:        Styles.Graph.EdgeColor,
                strokeLineWidth:    3
        )

        private static let nodePaddingSize = GraphLayoutModel.Padding(font: Styles.Graph.getNodeFont(forSize: 12))

        fileprivate var nodes = ContiguousArray<Node>()
        fileprivate var edges = ContiguousArray<Edge>()

        fileprivate init(stats: Stats)
        {
            func formatRecognitionStats(_ stats: BehaviourActor.RecognitionStats?) -> String?
            {
                guard let duration = stats?.recognitionDuration else { return nil }

                return "\n[Apple speech recognition time: \(Int(duration * 1000.0)) ms]"
            }

            let baseTitleFormatter = AttributedStringFormatter(nodeType: .eBase)

            baseTitleFormatter.font(Styles.Graph.getNodeBoldFont(forSize: 16))
                              .text("Sentence")
                              .font(Styles.Graph.getNodeFont(forSize: 15))
                              .text(formatRecognitionStats(stats.recogStats))

            let baseNode = Node(groupId: 0, title: baseTitleFormatter.attributedString, style: GraphFormatter.Base.nodeStyle)

            nodes.append(baseNode)

            for (index, word) in stats.words.enumerated()
            {
                let wordTitleFormatter = AttributedStringFormatter(nodeType: .eTerminal)

                wordTitleFormatter.font(Styles.Graph.getNodeBoldFont(forSize: 13))
                                  .text(word)
                                  .font(Styles.Graph.getNodeFont(forSize: 12))
                                  .text("\n(Word Token)")

                let isMatchedWord = stats.matchedWordIndexes.contains(index)
                let nodeStyle : Node.Style? = isMatchedWord ? GraphFormatter.MatchedTerminal.nodeStyle : GraphFormatter.NonMatchedTerminal.nodeStyle
                let wordNode = Node(groupId: 0, title: wordTitleFormatter.attributedString, style: nodeStyle)
                let edge = Edge(from: baseNode, to: wordNode, fromNodeEdgeIndex: edges.count, style: GraphFormatter.edgeStyle)

                nodes.append(wordNode)
                edges.append(edge)
            }
        }
    }

    fileprivate class AttributedStringFormatter
    {
        fileprivate let attributedString = NSMutableAttributedString()
        fileprivate var attributes: [NSAttributedString.Key : Any]

        fileprivate init(nodeType: GraphFormatter.eNodeType)
        {
            let paragraphStyle = NSMutableParagraphStyle()

            paragraphStyle.alignment     = .center
            paragraphStyle.lineBreakMode = .byTruncatingTail

            switch nodeType
            {
                case .eTerminal:

                    self.attributes = [
                        NSAttributedString.Key.paragraphStyle:     paragraphStyle,
                        NSAttributedString.Key.foregroundColor :   Styles.Graph.NonMatchedWordTokenColor,
                        NSAttributedString.Key.font :              Styles.Graph.getNodeFont(forSize: 12),
                        NSAttributedString.Key.baselineOffset :    NSNumber(value: Float(2))
                    ]

                case .eBase:

                    self.attributes = [
                        NSAttributedString.Key.paragraphStyle:     paragraphStyle,
                        NSAttributedString.Key.foregroundColor :   Styles.Graph.BaseColor,
                        NSAttributedString.Key.font :              Styles.Graph.getNodeFont(forSize: 16),
                        NSAttributedString.Key.baselineOffset :    NSNumber(value: Float(0))
                    ]
            }
        }

        @discardableResult
        public func text(_ text: String?) -> Self
        {
            guard let text = text, !text.isEmpty else { return self }

            attributedString.append(NSAttributedString(string: text, attributes: attributes))

            return self
        }

        @discardableResult
        public func font(_ font: UIFont) -> Self
        {
            attributes[NSAttributedString.Key.font] = font

            return self
        }

        @discardableResult
        public func color(_ color: UIColor) -> Self
        {
            attributes[NSAttributedString.Key.foregroundColor] = color

            return self
        }

        @discardableResult
        public func paragraphStyle(_ paragraphStyle: NSParagraphStyle) -> Self
        {
            attributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle

            return self
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Extracts that can be recited.
///

public struct ReciteExtracts
{
    public typealias VocalResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.VocalResult
    
    static internal let ReciteTexts: [String : VocalResult] = [

        "the loose moose" : VocalResult(vocalOps: [.eApply(">"), .eSpeak("Reciting The Loose Moose ..."),
                                         .ePause(0.2),  .eSpeak("Can I tell you about the tale of the Loose Moose ?"),
                                         .ePause(0.15), .eSpeak("He got loose from the rear caboose, just past Toulouse."),
                                         .ePause(0.15), .eSpeak("In the second class booth, he loosened the goose, then he tried to smooch a papoose."),
                                         .ePause(0.15), .eSpeak("Fortunately they caught him in a noose. And he went vamoose, right back to that caboose."),
                                         .ePause(0.15), .eSpeak("So, the moral of the story is, a loose noose is handy to catch a smoochy moose."), .eApply("<")]),

        "the walrus and the carpenter" : VocalResult(vocalOps: [.eSpeak("Reciting from 'The Walrus and the Carpenter' by Lewis Carroll ..."),
                                          .ePause(0.2), .eSpeak("The time has come, the Walrus said, To speak of many things: of shoes and ships and sealing wax of cabbages and kings. And why the sea is boiling hot, and whether pigs have wings."),
                                          .ePause(0.2), .eSpeak("But wait a bit, the Oysters cried, before we have our chat;"),
                                          .ePause(0.2), .eSpeak("For some of us are out of breath, and all of us are fat!"),
                                          .ePause(0.2), .eSpeak("No hurry!' said the Carpenter, They thanked him much for that."),
                                          .ePause(0.3), .eSpeak("A loaf of bread, the Walrus said. Is what we chiefly need:"),
                                          .ePause(0.2), .eSpeak("Pepper and vinegar besides, Are very good indeed."),
                                          .ePause(0.3), .eSpeak("Now if you're ready, Oysters dear. We can begin to feed!"),
                                          .ePause(0.2), .eSpeak("But not on us! the Oysters cried, turning a little blue"),
                                          .ePause(0.2), .eSpeak("After such kindness, that would be ..."),
                                          .ePause(0.1), .eSpeak("A most dismal thing to do!")]),

        "silvie and bruno" : VocalResult(vocalOps: [.eSpeak("Reciting from Sylvie and Bruno by Lewis Carroll ..."),
                                          .ePause(0.2), .eSpeak("In the scene, Mein Herr is talking to Sylvie ..."),
                                          .ePause(0.4), .eSpeak("That's another thing we've learned from your Nation, Said Mein Herr."),
                                          .eApply(">"), .ePause(0.4), .eSpeak("Map making!"), .eApply("<"),
                                          .ePause(0.3), .eSpeak("But we've carried it much further than you! What do you consider the largest map that would be really useful?"),
                                          .ePause(0.4), .eSpeak("I remarked, About six inches to the mile."),
                                          .ePause(0.2), .eSpeak("Only six inches! exclaimed Mein Herr. We very soon got to six yards to the mile. Then later, a hundred yards to the mile. And then came the grandest idea of all! We actually made a map of the country, on the exact scale of one mile to the same mile!"),
                                          .ePause(0.3), .eSpeak("Have you used it much? I enquired."),
                                          .ePause(0.4), .eSpeak("Well, it has never been spread out as yet, said Mein Herr. The farmers objected: they said it would cover the whole country, and shut out the sunlight!"),
                                          .ePause(0.4), .eSpeak("So we now use the country itself, as its own map, and I assure you it does nearly as well!")]),
    ]

    static internal let Keywords: [String : String] = [

        "moose" : "the loose moose",
        "walrus" : "the walrus and the carpenter",
        "carpenter" : "the walrus and the carpenter",
        "silvio" : "silvie and bruno",
        "silvie" : "silvie and bruno",
        "bruno" : "silvie and bruno",
    ]

    public subscript(index: String) -> VocalResult?
    {
        let lowercasedIndex = index.lowercased()

        if let VocalResult = ReciteExtracts.ReciteTexts[lowercasedIndex]
        {
            return VocalResult
        }

        let keywords = lowercasedIndex.components(separatedBy: " ")
                                      .filter({ !$0.isEmpty && $0 != "the" && $0 != "and" && $0 != "poem" && $0 != "title" })

        for keyword in keywords
        {
            if let effectiveIndex = ReciteExtracts.Keywords[keyword]
            {
                return ReciteExtracts.ReciteTexts[effectiveIndex]
            }
        }

        return nil
    }
}