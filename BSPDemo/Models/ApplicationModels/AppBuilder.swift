
//
//  Models/ApplicationModels/AppBuilder.swift
//  BSPDemo
//
//  Created by Terry Stillone on 1/1/22.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Speech
import Network
import CoreLocation
import Combine

///
/// The IAppBuilder protocol assists in building and configuring the services and components of the App.
///

protocol IAppBuilder
{
    var notifierDirectory : NotifierDirectory   { get }
    var behaviourScopeStack : ScopeStack?       { get }
    var presentationScopeStack : ScopeStack?    { get }

    var config : AppConfig { get }

    func buildSpeechRecognizer() -> SFSpeechRecognizer?
    func buildSpeechSynthesizer() -> AVSpeechSynthesizer
    func buildNWPathMonitor() -> NWPathMonitor
    func buildLocationManager() -> CLLocationManager
}

///
/// AppBuilder assists in building and configuring the services and components of the App.
///

class AppBuilder: IAppBuilder
{
    let notifierDirectory : NotifierDirectory
    let behaviourScopeStack : ScopeStack?
    let presentationScopeStack : ScopeStack?
    
    let config : AppConfig
    
    init(config: AppConfig, notifierDirectory: NotifierDirectory, behaviourScopeStack : ScopeStack?, presentationScopeStack : ScopeStack?)
    {
        self.config = config
        self.notifierDirectory = notifierDirectory
        self.behaviourScopeStack = behaviourScopeStack
        self.presentationScopeStack = presentationScopeStack
    }
    
    func buildSpeechRecognizer() -> SFSpeechRecognizer?
    {
        if config.testingConfig?.mockServices == true
        {
            guard let speechRecognizer = Mocks.AuthorizedSFSpeechRecognizer(appBuilder: self) else { return nil }

            return speechRecognizer
        }
        else
        {
            guard let speechRecognizer = SFSpeechRecognizer(locale: Locale.current) else { return nil }

            speechRecognizer.defaultTaskHint = .unspecified

            return speechRecognizer
        }
    }

    func buildSpeechSynthesizer() -> AVSpeechSynthesizer
    {
        if config.testingConfig?.mockServices == true
        {
            return Mocks.MockedAVSpeechSynthesizer()
        }
        else
        {
            return AVSpeechSynthesizer()
        }
    }

    func buildNWPathMonitor() -> NWPathMonitor
    {
        NWPathMonitor()
    }
    
    func buildLocationManager() -> CLLocationManager
    {
        if config.testingConfig?.mockServices == true
        {
            return Mocks.MockedCLLocationManager(authorization: .authorizedAlways, location: config.testingConfig!.mockLocation)
        }
        else
        {
            return CLLocationManager()
        }
    }
}

///
/// AppConfig, the operational and testing configuration.
///

struct AppConfig
{
    struct TestingConfig
    {
        let showAutomationView:  Bool
        let mockServices:        Bool
        let speechRecognitionTextPublisher = PassthroughSubject<String, Never>()
        
        let mockLocation:        CLLocation
        let mockAddress:         String
    }

    let commandResponderType : CommandBehaviourModel.eCommandResponderType
    let enableAnnouncements: Bool
    let testingConfig : TestingConfig?
}

