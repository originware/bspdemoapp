//
//  Models/ApplicationModels/ApplicationModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 30/11/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import Speech

///
/// The Application Life Cycle Model. Handles the application subsystem construction, destruction  and authorization.
///

class ApplicationModel: ObservableObject
{
    // App Identification.
    let appIdentifier :      String?
    
    // AppBuilder
    let appBuilder:          IAppBuilder
    
    // Models
    let serviceActor:      ServiceActor
    let behaviourActor:    BehaviourActor
    let presentationActor: PresentationActor
    
    // Main View View Model
    let mainViewModel:       MainViewModel
    
    // Directory of Notifiers
    let notifierDirectory:   NotifierDirectory

    @MainActor
    init(appBuilder : IAppBuilder)
    {
        func getAppIdentifier() -> String?
        {
            Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String
                    ?? Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String
        }
        
        // Create the main view View Model.
        let mainViewModel       = MainViewModel()

        // Create the BSP Models
        let serviceActor      = ServiceActor(appBuilder: appBuilder)
        let presentationActor = PresentationActor(appBuilder : appBuilder, mainViewModel: mainViewModel)
        let behaviourActor    = BehaviourActor(appBuilder : appBuilder)
        
        self.appIdentifier      = getAppIdentifier()!
        self.appBuilder         = appBuilder
        self.serviceActor       = serviceActor
        self.behaviourActor     = behaviourActor
        self.presentationActor  = presentationActor
        self.mainViewModel      = mainViewModel
        self.notifierDirectory  = appBuilder.notifierDirectory
    }
}
