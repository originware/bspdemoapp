
//
//  ViewModels/MainViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 30/11/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Main View, View Model.
///

@MainActor
public class MainViewModel : ObservableObject
{
    @Published var recognitionText :             AttributedString
    @Published var responseText :                AttributedString
    @Published var indentTemplate :              AttributedString

    init()
    {
        self.recognitionText            = AttributedString("Recognized:", attributes: Styles.annotationTextStyle)
        self.responseText               = AttributedString("", attributes: Styles.annotationTextStyle)
        self.indentTemplate             = AttributedString("Recognized: ", attributes: Styles.annotationTextStyle)
    }
}
