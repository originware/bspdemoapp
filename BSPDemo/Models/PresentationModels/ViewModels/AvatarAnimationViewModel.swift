//
//  ViewModels/AvatarAnimationViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 30/11/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Avatar Animation View Model
/// 

@MainActor 
public class AvatarAnimationViewModel : Traceable
{
    fileprivate actor AnimationQueue : Traceable
    {
        private var m_queue = ContiguousArray<IAnimationModelItem>()

        fileprivate func queue(animation: IAnimationModelItem)
        {
            if m_queue.isEmpty
            {
                animation.run()

                m_queue.append(animation)
            }
            else
            {
                m_queue.append(animation)
            }
        }

        fileprivate func unqueue(animation: IAnimationModelItem)
        {
            guard let indexOfAnimation = getIndex(of: animation) else { return }

            m_queue.remove(at: indexOfAnimation)

            if let firstItem = m_queue.first
            {
                trace("AnimationQueue id: \(firstItem.animationSequenceID) from animation: \(animation.animationSequenceID)")

                firstItem.run()
            }
        }

        fileprivate func removeAll()
        {
            m_queue.removeAll()
        }

        fileprivate func getIndex(of animation: IAnimationModelItem) -> Int?
        {
            for (index, queueAnimation) in m_queue.enumerated()
            {
                if queueAnimation.animationSequenceID == animation.animationSequenceID
                {
                    return index
                }
            }

            return nil
        }
    }
    
    let avatarUIView: AvatarUIView

    private var m_animationQueue = AnimationQueue()

    public nonisolated init(avatarUIView: AvatarUIView)
    {
        self.avatarUIView = avatarUIView
    }
    
    @discardableResult
    func performAsync(duration: TimeInterval, fromGesture: eAvatarGestureAnimationOp, toGesture: eAvatarGestureAnimationOp) async -> Bool
    {
        let animation = avatarUIView.animations.createGestureAnimation(duration, fromGesture, toGesture)
        
        trace(title: "    AvatarAnimationModel", "performAsync animationID(\(animation.animationSequenceID)) \(fromGesture) -> \(toGesture)")
        
        await m_animationQueue.queue(animation: animation)

        let result : Bool = await withCheckedContinuation { continuation in

            animation.animationNotifyHandler = { [weak self, weak animation] (reply) in

                guard let strongSelf = self, let strongAnimation = animation else { return }
                
                Task {
                 
                    switch reply
                    {
                        case .eDidEndAnimation:
                            
                            strongSelf.trace(title: "    AvatarAnimationModel", "eDidEndAnimation   \(strongAnimation.animationSequenceID): \(fromGesture) -> \(toGesture)")
                    
                            await strongSelf.m_animationQueue.unqueue(animation: strongAnimation)
                            
                            continuation.resume(returning: true)

                        case .eDidCancelGesture:

                            strongSelf.trace(title: "    AvatarAnimationModel", "eDidCancelGesture  \(strongAnimation.animationSequenceID): \(fromGesture) -> \(toGesture)")
                           
                            await strongSelf.m_animationQueue.removeAll()
          
                            continuation.resume(returning: false)
      
                        case .eDidBeginAnimation:
                            
                            strongSelf.trace(title: "    AvatarAnimationModel", "eDidBeginAnimation \(strongAnimation.animationSequenceID): \(fromGesture) -> \(toGesture)")
                            break
                    }
                }
            }
        }

        return result
    }

    func cancelGestures() async
    {
        await m_animationQueue.removeAll()
    }
}
