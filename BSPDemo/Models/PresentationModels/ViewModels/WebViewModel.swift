//
//  ViewModels/WebViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 30/11/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import WebKit
import SwiftUI
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The WebKit View Model. Loads a given URL.
///

public class WebViewModel : NSObject, WKNavigationDelegate
{
    public typealias CompleteNotifyFunc = (Error?) -> Void

    enum eWebModelEvent
    {
        case eFetchingURL(URL)
        case eDownloadingURL(URL)
        case eDownloadProgress(Double)
        case eLoadedURL
        case eError(Error)
    }

    var webView : WKWebView! = nil
    var contentHeight : Binding<CGFloat>? = nil

    private var m_webEventAsyncStream : AsyncStream<eWebModelEvent>? = nil
    private var m_asyncStreamContinuation : AsyncStream<eWebModelEvent>.Continuation? = nil
    private var m_timer : Timer? = nil

    private var resized = false

    override init()
    {
        super.init()
    }

    @MainActor
    func createWKWebView()
    {
        guard webView == nil else { return }

       let webView = WKWebView(frame: .zero)

        webView.scrollView.isScrollEnabled = false
        webView.isUserInteractionEnabled = true
        webView.contentMode = .top
        webView.scrollView.contentOffset = CGPoint.zero
        webView.scrollView.contentInset = UIEdgeInsets.zero
        webView.scrollView.backgroundColor = .white
        webView.backgroundColor = .white

        webView.navigationDelegate = self

        self.webView = webView
    }

    @MainActor
    func load(url : URL) async -> AsyncStream<eWebModelEvent>
    {
        m_asyncStreamContinuation = nil

        createWKWebView()

        let asyncStream = AsyncStream<eWebModelEvent> { [weak self] (continuation) in
         
            guard let strongSelf = self else { return }

            if strongSelf.m_asyncStreamContinuation == nil
            {
                strongSelf.m_asyncStreamContinuation = continuation
                
                continuation.yield(.eFetchingURL(url))

                strongSelf.webView.load(URLRequest(url: url))
                
                m_timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { timer in
            
                    let progress = strongSelf.webView.estimatedProgress
                    
                    if progress < 0.01
                    {
                        continuation.yield(.eDownloadingURL(url))
                    }
                    else
                    {
                        continuation.yield(.eDownloadProgress(strongSelf.webView.estimatedProgress))
                    }
                }
            }
        }

        m_webEventAsyncStream = asyncStream

        return asyncStream
    }

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        guard let continuation = m_asyncStreamContinuation else { return }
            
        webView.evaluateJavaScript("document.readyState") { [weak webView, weak self] complete, _ in

            guard complete != nil, let strongWebView = webView else { return }

            strongWebView.contentMode = .top
            strongWebView.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

            strongWebView.evaluateJavaScript("document.body.scrollHeight") { height, _ in
                
                guard let strongSelf = self,
                      let height = height as? CGFloat else { return }

                // Clamp height to 2000.
                let clampedheight = height > 2000 ? 2000 : height

                // Bug workaround: increase the height by 100 to cater for page clipping.
                strongSelf.contentHeight?.wrappedValue = clampedheight + 100
            }
        }
        
        m_timer?.invalidate()
     
        continuation.yield(.eLoadedURL)
        continuation.finish()
            
        m_asyncStreamContinuation = nil
        m_webEventAsyncStream = nil
        m_timer = nil
    }

    public func webView(_ webView: WKWebView, didFailProvisionalNavigation: WKNavigation!, withError error: Error)
    {
        guard let continuation = m_asyncStreamContinuation else { return }
            
        m_timer?.invalidate()
   
        continuation.yield(.eError(error))
        continuation.finish()
        
        m_asyncStreamContinuation = nil
        m_webEventAsyncStream = nil
        m_timer = nil
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        guard let continuation = m_asyncStreamContinuation else { return }
            
        m_timer?.invalidate()
 
        continuation.yield(.eError(error))
        continuation.finish()
        
        m_asyncStreamContinuation = nil
        m_webEventAsyncStream = nil
        m_timer = nil
    }
}
