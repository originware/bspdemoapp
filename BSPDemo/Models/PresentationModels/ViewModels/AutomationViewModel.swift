
//
//  ViewModels/AutomationViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 3/1/22.
//

import Foundation
import Combine
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Automation Model allows the App to be controlled in UI Unit Tests.
///

public class BoolStateObject : ObservableObject, DynamicProperty
{
    public var value : Bool
    {
        didSet(newValue) {
         
            m_publisher.send(newValue)
        }
    }

    public var objectWillChange: PassthroughSubject<Bool, Never>
    {
        m_publisher
    }

    private let m_publisher = PassthroughSubject<Bool, Never>()

    public init(value : Bool)
    {
        self.value = value
    }
}

public class AutomationViewModel : ObservableObject
{
    public var automationRequest: Binding<String>!
    public var automationReply:   Binding<String>!

    private var m_automationRequest = ""
    private var m_automationReply   = ""
 
    init(appBuilder: IAppBuilder)
    {
        self.automationRequest = Binding<String>(
            
            get: { self.m_automationRequest },
            set: { (newValue) in
                
                if newValue != self.m_automationRequest
                {
                    self.m_automationRequest = newValue
                    
                    appBuilder.notifierDirectory.presentation.event.send(.eOnAutomationRequest(newValue))
                }
            }
        )
         
        self.automationReply = Binding<String>(
        
            get: { self.m_automationReply },
            set: { (newValue) in
                
                if newValue != self.m_automationReply
                {
                    self.m_automationReply = newValue
                }
            }
        )
    }
}
