
//
//  ViewModels/GraphViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 26/1/2.
//  Copyright © 2022 Originware. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import Combine

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Graph node-edge model together with their style decorations.
/// 

public class GraphViewModel
{
    fileprivate struct HashIdCounter
    {
        fileprivate static var counter = 0

        fileprivate static var next: Int
        {
            let value = counter

            counter += 1

             return value
        }
    }

    public class Node : Hashable
    {
        typealias Element = Node

        public struct Style
        {
            public struct RectangleStyle
            {
                public let fillColor : UIColor
                public let borderStyle : eBorderStyle
                public let cornerRadius : CGFloat

                public init(fillColor: UIColor = .clear,
                            borderStyle: eBorderStyle = .eLine(color: .black, borderWidth: 1.0),
                            cornerRadius: CGFloat = 0)
                {
                    self.fillColor = fillColor
                    self.borderStyle = borderStyle
                    self.cornerRadius = cornerRadius
                }
            }

            public struct EllipseStyle
            {
                public let fillColor : UIColor
                public let borderStyle : eBorderStyle

                public init(fillColor: UIColor = .clear,
                            borderStyle: eBorderStyle = .eLine(color: .black, borderWidth: 1.0))
                {
                    self.fillColor = fillColor
                    self.borderStyle = borderStyle
                }
            }

            public enum eShape
            {
                case eRectangle(RectangleStyle)
                case eEllipse(EllipseStyle)

                public var fillColor : UIColor
                {
                    switch self
                    {
                        case .eRectangle(let rectangleStyle):

                            return rectangleStyle.fillColor

                        case .eEllipse(let ellipseStyle):

                            return ellipseStyle.fillColor
                    }
                }

                public var borderStyle : eBorderStyle
                {
                    switch self
                    {
                        case .eRectangle(let rectangleStyle):

                            return rectangleStyle.borderStyle

                        case .eEllipse(let ellipseStyle):

                            return ellipseStyle.borderStyle
                    }
                }

                public func createBezier(rect: CGRect) -> UIBezierPath
                {
                    switch self
                    {
                        case .eEllipse:

                            return Graphics.Shapes.makeEllipse(rect: rect)

                        case .eRectangle(let rectangleStyle):

                            return Graphics.Shapes.makeRectangle(rect: rect, cornerRadius: rectangleStyle.cornerRadius)
                    }
                }
            }

            public enum eBorderStyle
            {
                case eLine(color: UIColor, borderWidth: CGFloat)
                case eThreeStripe(color: UIColor, borderWidth: CGFloat, stripeColor: UIColor, stripeWidth: CGFloat)

                public var strokeWidth : CGFloat
                {
                    switch self
                    {
                        case .eLine(_, let borderWidth):

                            return borderWidth

                        case .eThreeStripe(_, let borderWidth, _, _):

                            return borderWidth
                    }
                }
            }

            public let shape : eShape
            public let paddingSize : CGSize
            
            public init(shape: eShape = .eRectangle(Style.RectangleStyle()), paddingSize: CGSize = CGSize.zero)
            {
                self.shape = shape
                self.paddingSize = paddingSize
            }
        }

        public let id : Int = HashIdCounter.next
        public var groupId : Int
        public let title : NSAttributedString
        public let style : Style?

        public var edges = ContiguousArray<Edge>()

        public init(groupId: Int, title: NSAttributedString, style: Style? = nil)
        {
            self.groupId = groupId
            self.title = title
            self.style = style
        }

        public func hash(into hasher: inout Hasher)
        {
            hasher.combine(id)
        }

        public static func ==(lhs: Node, rhs: Node) -> Bool
        {
            lhs.id == rhs.id
        }
    }

    public struct Edge : Hashable
    {
        public struct Style
        {
            public let color : UIColor
            public let lineWidth : CGFloat

            public init(strokeColor: UIColor = .black, strokeLineWidth: CGFloat = 1.0)
            {
                self.color = strokeColor
                self.lineWidth = strokeLineWidth
            }
        }

        public let id : Int = HashIdCounter.next
        public let fromNode: Node
        public let toNode : Node
        public let fromNodeEdgeIndex: Int
        public var style : Style?

        public init(from fromNode: Node, to toNode : Node, fromNodeEdgeIndex: Int, style: Style? = nil)
        {
            self.fromNode = fromNode
            self.toNode = toNode
            self.style = style
            self.fromNodeEdgeIndex = fromNodeEdgeIndex
        }

        public func hash(into hasher: inout Hasher)
        {
            hasher.combine(id)
        }

        public static func ==(lhs: Edge, rhs: Edge) -> Bool
        {
            lhs.id == rhs.id
        }
    }

    public var isEmpty : Bool { nodes.isEmpty }

    public private(set) var nodes = ContiguousArray<Node>()
    public private(set) var edges = ContiguousArray<Edge>()
    public              var needsLayout = true

    public var graphLayoutModel: GraphLayoutModel? = nil

    public func add(node: Node)
    {
        nodes.append(node)
    }

    public func add(edge: Edge)
    {
        assert(nodes.contains(edge.fromNode), "From node does not exist")
        assert(nodes.contains(edge.toNode), "To node does not exist")

        edge.fromNode.edges.append(edge)

        edges.append(edge)
    }

    public func clear()
    {
        nodes.removeAll()
        edges.removeAll()

        needsLayout = true
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Graph Layout node sizing and positioning model. Also includes edge routing and shaping.
///

public final class GraphLayoutModel
{
    public typealias Node = GraphViewModel.Node
    public typealias Edge = GraphViewModel.Edge

    public struct RankPosition : Hashable
    {
        public let row : Int
        public let column : Int
        public let isVirtualColumn : Bool

        public func hash(into hasher: inout Hasher)
        {
            hasher.combine(row)
            hasher.combine(column)
        }

        public static func ==(lhs: RankPosition, rhs: RankPosition) -> Bool
        {
            lhs.row == rhs.row && lhs.column == rhs.column
        }
    }

    public final class NodeCell : Hashable & CustomStringConvertible
    {
        public struct ChildIndex
        {
            public let index : Int
            public let count : Int

            public var leftRatio: CGFloat
            {
                guard count > 0 else { return 0.5 }

                return CGFloat(index + 1) / CGFloat(count + 2)
            }

            public var rightRatio : CGFloat
            {
                guard count > 0 else { return 0.5 }

                return CGFloat(index + 1 - count / 2) / CGFloat(count + 2)
            }
        }

        public struct Sizing
        {
            public let cellBounds :  CGSize
            public let titleYOffset: CGFloat
        }

        public struct Positioning
        {
            public let cellFrame:              CGRect
            public let titleRelativeCellFrame: CGRect
        }
       
        public let id : Int = GraphViewModel.HashIdCounter.next
        public let title : NSAttributedString
        public let style : Node.Style
        public fileprivate(set) var isRootNodeCell :                Bool = false
        public fileprivate(set) var rankPosition :                  RankPosition? = nil
        public fileprivate(set) var sizing :                        Sizing?      = nil
        public fileprivate(set) var positioning:                    Positioning? = nil

        public private(set) var childNodeCells = ContiguousArray<NodeCell>()

        public var frame : CGRect?         { positioning?.cellFrame }
        public var hasNoChildren : Bool    { childNodeCells.isEmpty }
        public var childCount : Int        { childNodeCells.count }

        public var description : String
        {
            guard let rankPosition = rankPosition else { return "NodeCell(id: \(id))" }

            if rankPosition.isVirtualColumn
            {
                return "NodeCell(row: \(rankPosition.row), virtual column: \(rankPosition.column))"
            }
            else
            {
                return "NodeCell(row: \(rankPosition.row), column: \(rankPosition.column))"
            }
        }
        
        fileprivate var centerPoint : CGPoint?
        {
            guard let frame = frame else { return nil }

            return CGPoint(x: frame.midX, y: frame.midY)
        }

        fileprivate init(node: Node)
        {
            self.title = node.title
            self.style = node.style ?? Node.Style()
        }

        fileprivate func add(nodeCell: NodeCell)
        {
            assert(nodeCell != self)
            assert(!childNodeCells.contains(nodeCell))
            assert(!nodeCell.childNodeCells.contains(self))
            
            childNodeCells.append(nodeCell)
        }

        public func getChildIndex(ofChild child: NodeCell) -> ChildIndex?
        {
            guard let childIndex = childNodeCells.firstIndex(of: child) else { return nil }

            return ChildIndex(index: childIndex, count: childNodeCells.count)
        }

        public func hash(into hasher: inout Hasher)
        {
            hasher.combine(id)
        }

        public static func ==(lhs: NodeCell, rhs: NodeCell) -> Bool
        {
            lhs.id == rhs.id
        }
    }

    public final class EdgeCell : Hashable
    {
        fileprivate let id :                    Int = GraphViewModel.HashIdCounter.next
        fileprivate let fromNodeCell :          NodeCell
        fileprivate let toNodeCell :            NodeCell
        fileprivate let fromNodeCellEdgeIndex:  Int

        public var shape : Graphics.Shapes.eShape? = nil
        public let style : Edge.Style

        fileprivate init(fromNodeCell: NodeCell, toNodeCell: NodeCell, fromNodeCellEdgeIndex: Int, style: Edge.Style)
        {
            self.fromNodeCell = fromNodeCell
            self.toNodeCell = toNodeCell
            self.fromNodeCellEdgeIndex = fromNodeCellEdgeIndex
            self.style = style
        }

        fileprivate func doesIntersectWithChildren(nodeCell: NodeCell, omit: NodeCell, line: Graphics.Shapes.Line) -> Bool
        {
            for childNode in nodeCell.childNodeCells
            {
                if childNode != omit,
                   let childRankPosition = childNode.rankPosition,
                   !childRankPosition.isVirtualColumn,
                   Graphics.Library.doesIntersectFromBelow(line: line, withRect: childNode.frame!)
                {
                    return true
                }
            }

            return false
        }

        fileprivate func createRectangularEdgeLine() -> Graphics.Shapes.Line?
        {
            guard let fromCenterPoint = fromNodeCell.centerPoint,
                  let toCenterPoint = toNodeCell.centerPoint else { return nil }

            let line = Graphics.Shapes.Line(start: fromCenterPoint, end: toCenterPoint)

            guard let fromCrossPoint = getBorderIntersectionPoint(forShape: .eLine(line), nodeCell: fromNodeCell, isReversed: false),
                  let toCrossPoint   = getBorderIntersectionPoint(forShape: .eLine(line), nodeCell: toNodeCell, isReversed: true) else { return nil }

            return Graphics.Shapes.Line(start: fromCrossPoint, end: toCrossPoint)
        }

        fileprivate func createRectangularEdgeShape() -> Graphics.Shapes.eShape?
        {
            guard let line = createRectangularEdgeLine() else { return nil }

            return .eLine(line)
        }
        
        fileprivate func createQuadBezierEdgeShape() -> Graphics.Shapes.eShape?
        {
            let fromRect = fromNodeCell.frame!.inset(by: -fromNodeCell.style.shape.borderStyle.strokeWidth * 2)
            let toRect = toNodeCell.frame!.inset(by: -toNodeCell.style.shape.borderStyle.strokeWidth * 2)

            if let quadBezier = Graphics.Library.getQuadBezier(fromRect: fromRect, toRect: toRect)
            {
                guard let fromIntersectionPoint = getBorderIntersectionPoint(forShape: .eQuadBezier(quadBezier), nodeCell: fromNodeCell, isReversed: false),
                      let toIntersectionPoint = getBorderIntersectionPoint(forShape: .eQuadBezier(quadBezier), nodeCell: toNodeCell, isReversed: true) else { return nil }

                let fromIntersectionPointRect = CGRect(center: fromIntersectionPoint, size: CGSize(width: 3, height: 3))
                let toIntersectionPointRect   = CGRect(center: toIntersectionPoint, size: CGSize(width: 3, height: 3))

                if let intersectionBezier = Graphics.Library.getQuadBezier(fromRect: fromIntersectionPointRect, toRect: toIntersectionPointRect)
                {
                    return .eQuadBezier(intersectionBezier)
                }
            }

            return nil
        }

        fileprivate func determineEdgeShape() -> Graphics.Shapes.eShape?
        {
            func createQuadBezierShape(quadBezier: Graphics.Shapes.QuadBezier) -> Graphics.Shapes.eShape?
            {
                guard let fromIntersectionPoint = getBorderIntersectionPoint(forShape: .eQuadBezier(quadBezier), nodeCell: fromNodeCell, isReversed: false),
                      let toIntersectionPoint = getBorderIntersectionPoint(forShape: .eQuadBezier(quadBezier), nodeCell: toNodeCell, isReversed: true) else { return nil }

                let fromIntersectionPointRect = CGRect(center: fromIntersectionPoint, size: CGSize(width: 3, height: 3))
                let toIntersectionPointRect   = CGRect(center: toIntersectionPoint, size: CGSize(width: 3, height: 3))

                if let intersectionBezier = Graphics.Library.getQuadBezier(fromRect: fromIntersectionPointRect, toRect: toIntersectionPointRect)
                {
                    return .eQuadBezier(intersectionBezier)
                }

                return nil
            }

            let fromRect = fromNodeCell.frame!.inset(by: -fromNodeCell.style.shape.borderStyle.strokeWidth * 2)
            let toRect = toNodeCell.frame!.inset(by: -toNodeCell.style.shape.borderStyle.strokeWidth * 2)

            if let quadBezier = Graphics.Library.getQuadBezier(fromRect: fromRect, toRect: toRect)
            {
                let angle = Graphics.Library.getAngle(from: quadBezier.start, to: quadBezier.end)

                // If the line edge does not intersect at the top or bottom, (i.e. to the side) then use a bezier shape.
                if case .eRectangle = toNodeCell.style.shape
                {
                    switch Graphics.Library.getIntersectionSide(angle: angle, forRect: toRect)
                    {
                        case .eRight, .eLeft:

                            if let shape = createQuadBezierShape(quadBezier: quadBezier)
                            {
                                return shape
                            }

                        case .eTop, .eBottom:

                            break
                    }
                }

                if let line = createRectangularEdgeLine()
                {
                    // If line edge intersects with a child edge, then force it to be a bezier.
                    if fromNodeCell.childCount > 1 && doesIntersectWithChildren(nodeCell: fromNodeCell, omit: toNodeCell, line: line),
                       let shape = createQuadBezierShape(quadBezier: quadBezier)
                    {
                        return shape
                    }

                    return .eLine(line)
                }
            }

            // Safety code
            return createRectangularEdgeShape()
        }
        
        fileprivate func getBorderIntersectionPoint(forShape shape: Graphics.Shapes.eShape, nodeCell: NodeCell, isReversed: Bool) -> CGPoint?
        {
            guard let rect = nodeCell.frame else { return nil }

            switch (shape, nodeCell.style.shape)
            {
                case (.eLine, .eRectangle):

                    if let fromCenterPoint = fromNodeCell.centerPoint,
                       let toCenterPoint = toNodeCell.centerPoint
                    {
                        var centerLineAngle = Graphics.Library.getAngle(from: fromCenterPoint, to: toCenterPoint)

                        if isReversed
                        {
                            centerLineAngle += CGFloat.pi
                        }

                        let rectWithSeparator = rect.inset(by: -nodeCell.style.shape.borderStyle.strokeWidth * 2)

                        return Graphics.Library.getRectangleLineIntersectionPoint(angle: centerLineAngle, forRect: rectWithSeparator)
                    }
                    else
                    {
                        assertionFailure("Expected frame to be set")
                    }

                case (.eLine, .eEllipse):

                    if let fromCenterPoint = fromNodeCell.centerPoint,
                       let toCenterPoint = toNodeCell.centerPoint
                    {
                        var centerLineAngle = Graphics.Library.getAngle(from: fromCenterPoint, to: toCenterPoint)

                        if isReversed
                        {
                            centerLineAngle += CGFloat.pi
                        }

                        let rectWithSeparator = rect.inset(by: -nodeCell.style.shape.borderStyle.strokeWidth * 2)

                        return Graphics.Library.getEllipseToLineIntersectionPoint(angle: centerLineAngle, forRect: rectWithSeparator)
                    }
                    else
                    {
                        assertionFailure("Expected frame to be set")
                    }

                case (.eQuadBezier(let quadBezier), .eRectangle):

                    if let fromCenterPoint = fromNodeCell.centerPoint,
                       let toCenterPoint = toNodeCell.centerPoint
                    {
                        var centerLineAngle = Graphics.Library.getAngle(from: fromCenterPoint, to: toCenterPoint)

                        if isReversed
                        {
                            centerLineAngle += CGFloat.pi
                        }

                        let rectWithSeparator = rect.inset(by: -nodeCell.style.shape.borderStyle.strokeWidth * 1.7)

                        return Graphics.Library.getQuadBezierToRectangleIntersectionPoint(quadBezier: quadBezier, angle: centerLineAngle, forRect: rectWithSeparator)
                    }
                    else
                    {
                        assertionFailure("Expected frame to be set")
                    }

                case (.eQuadBezier(let quadBezier), .eEllipse):

                    if let fromCenterPoint = fromNodeCell.centerPoint,
                       let toCenterPoint = toNodeCell.centerPoint
                    {
                        let centerLineAngle = Graphics.Library.getAngle(from: fromCenterPoint, to: toCenterPoint)
                        let effectiveQuadBezier = isReversed ? quadBezier.reversed() : quadBezier
                        let rectWithSeparator = rect.inset(by: -nodeCell.style.shape.borderStyle.strokeWidth * 1.2)

                        return Graphics.Library.getQuadBezierToEllipseIntersectionPoint(quadBezier: effectiveQuadBezier, angle: centerLineAngle, forRect: rectWithSeparator)
                    }
                    else
                    {
                        assertionFailure("Expected frame to be set")
                    }
            }

            return nil
        }

        public func hash(into hasher: inout Hasher)
        {
            hasher.combine(id)
        }

        public static func ==(lhs: EdgeCell, rhs: EdgeCell) -> Bool
        {
            lhs.id == rhs.id
        }
    }

    public final class GraphLayout
    {
        public struct NodeCellTreeLayout
        {
            public                  let leftColumn : Int
            public fileprivate(set) var rowCount = 0
            public fileprivate(set) var columnCount = 0
        }

        fileprivate var nonVirtualColumnCount: Int
        {
            var result = 0

            for (column, _) in columnWidths
            {
                if !virtualColumns.contains(column)
                {
                    result += 1
                }
            }

            return result
        }

        fileprivate var totalColumnWidth: CGFloat
        {
            var result = CGFloat(0)

            for (column, width) in columnWidths
            {
                if !virtualColumns.contains(column)
                {
                    result += width
                }
            }

            return result
        }
        
        fileprivate var totalRowHeight: CGFloat
        {
            var result = CGFloat(0)

            for height in rowHeights.values
            {
                result += height
            }

            return result
        }

        public var graphSize : CGSize
        {
            let graphHeight = totalRowHeight * 1.6
            var graphWidth = totalColumnWidth

            if !columnWidths.isEmpty
            {
                graphWidth += columnPadding * CGFloat(nonVirtualColumnCount + 1)
            }

            return CGSize(width: graphWidth, height: graphHeight)
        }
        
        public fileprivate(set) var columnWidths = [Int : CGFloat]()
        public fileprivate(set) var rowHeights = [Int : CGFloat]()
        public fileprivate(set) var virtualColumns = Set<Int>()
        public fileprivate(set) var rowCount = 0
        public fileprivate(set) var columnCount = 0
        public                  let columnPadding = CGFloat(25)
        public                  let margin = CGFloat(0)
    }

    public struct Padding
    {
        public let size: CGSize

        public init(font: UIFont)
        {
            let paddingText = NSAttributedString(string: "x", attributes: [NSAttributedString.Key.font: font])
            let paddingTextSize = paddingText.size()

            self.size = CGSize(width: paddingTextSize.width, height: paddingTextSize.height * 0.7)
        }
    }

    public private(set) var graphLayout:            GraphLayout
    public fileprivate(set) var rankToNodeCell:     [GraphLayoutModel.RankPosition: NodeCell]

    public private(set) var nodeCells:              ContiguousArray<NodeCell>
    public private(set) var edgeCells:              ContiguousArray<EdgeCell>
    public private(set) var rootNodeCells:          ContiguousArray<NodeCell>

    fileprivate init(nodes: ContiguousArray<Node>, edges: ContiguousArray<Edge>)
    {
        var nodeToNodeCell = [Node : NodeCell]()
        var nodeCells = ContiguousArray<NodeCell>()
        var edgeCells  = ContiguousArray<EdgeCell>()
   
        for node in nodes
        {
            let nodeCell = NodeCell(node: node)

            nodeToNodeCell[node] = nodeCell
            nodeCells.append(nodeCell)
        }

        for edge in edges
        {
            if let fromNodeCell = nodeToNodeCell[edge.fromNode],
               let toNodeCell = nodeToNodeCell[edge.toNode]
            {
                let edgeCell = EdgeCell(fromNodeCell: fromNodeCell, toNodeCell: toNodeCell, fromNodeCellEdgeIndex: edge.fromNodeEdgeIndex, style: edge.style ?? Edge.Style())

                edgeCells.append(edgeCell)
                
                fromNodeCell.add(nodeCell: toNodeCell)
            }
            else
            {
                assertionFailure("Cannot determine node for edge")
            }
        }
        
        var rootNodeCellSet = Set<NodeCell>(nodeCells)

        for nodeCell in nodeCells
        {
            for childNodeCell in nodeCell.childNodeCells
            {
                rootNodeCellSet.remove(childNodeCell)
            }
        }
        
        self.graphLayout = GraphLayout()
        self.rankToNodeCell = [GraphLayoutModel.RankPosition: NodeCell]()
        self.nodeCells = nodeCells
        self.edgeCells = edgeCells
        self.rootNodeCells = ContiguousArray<NodeCell>(rootNodeCellSet)

        for rootNodeCell in rootNodeCellSet
        {
            rootNodeCell.isRootNodeCell = true
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Graph Render Pipeline, expressed as a sequential composition of operators, together with the render models.
///
/// Operators include:
///
///  * The Node ranking operator
///  * The Node sizing operator
///  * The Node positioning operator
///  * The Edge shaping operator
///

public final class GraphRenderPipeline
{
    public typealias RenderService = ViewRenderService
    //public typealias RenderService = GraphicsContextRenderService

    fileprivate struct RankingOperator
    {
        fileprivate var output : SizingOperator

        fileprivate init(output: SizingOperator)
        {
            self.output = output
        }

        fileprivate mutating func request(model: GraphViewModel) -> GraphLayoutModel
        {
            let graphLayoutModel = GraphLayoutModel(nodes: model.nodes, edges: model.edges)

            if !model.nodes.isEmpty
            {
                model.graphLayoutModel = graphLayoutModel

                rankNodeCells(graphLayoutModel: graphLayoutModel)
                calculateIntrinsicSizeOfNodeCells(graphLayoutModel: graphLayoutModel)

                output.request(graphLayoutModel: graphLayoutModel)
            }

            return graphLayoutModel
        }
    }

    fileprivate struct SizingOperator
    {
        fileprivate var output : PositioningOperator

        fileprivate func request(graphLayoutModel: GraphLayoutModel)
        {
            var canvasFrame = CGRect(origin: CGPoint.zero, size: graphLayoutModel.graphLayout.graphSize)

            // Modify the graph size to form a margin around graphs that have minimal content.
            if graphLayoutModel.graphLayout.columnCount <= 5
            {
                canvasFrame.size.height = canvasFrame.height * 2
            }

            output.request(canvasFrame: canvasFrame, graphLayoutModel: graphLayoutModel)
        }
    }

    fileprivate struct PositioningOperator
    {
        fileprivate var output : EdgeShapingOperator
        
        fileprivate func request(canvasFrame: CGRect, graphLayoutModel: GraphLayoutModel)
        {
            positionNodeCells(canvasFrame: canvasFrame, graphLayoutModel: graphLayoutModel)

            output.request(canvasFrame: canvasFrame, graphLayoutModel: graphLayoutModel)
        }
    }
    
    fileprivate struct EdgeShapingOperator
    {
        @StateObject fileprivate var output : RenderService

        fileprivate func request(canvasFrame: CGRect, graphLayoutModel: GraphLayoutModel)
        {
            determineEdgeCellShape(graphLayoutModel: graphLayoutModel)

            output.request(canvasFrame: canvasFrame, graphLayoutModel: graphLayoutModel)
        }
    }

    public final class GraphicsContextRenderService
    {
        public private(set) var canvasFrame: CGRect? = nil
        fileprivate var m_graphLayoutModel: GraphLayoutModel? = nil

        fileprivate func request(canvasFrame: CGRect, graphLayoutModel: GraphLayoutModel)
        {
            self.canvasFrame = canvasFrame
            m_graphLayoutModel = graphLayoutModel
        }

        public func draw(rect: CGRect)
        {
            guard let graphLayoutModel = m_graphLayoutModel,
                  let context = UIGraphicsGetCurrentContext() else { return }
            
            let margin = graphLayoutModel.graphLayout.margin
            let graphSize = graphLayoutModel.graphLayout.graphSize

            let scaleX = (rect.size.width - margin * 2) / graphSize.width
            let scaleY = (rect.size.height - margin * 2) / graphSize.height
            let scale  = min(scaleX, scaleY)

            context.saveGState()

            do {
                context.translateBy(x: margin, y: margin)
                context.scaleBy(x: scale, y: scale)

                /*
                // Draw red border around graph frame, for debugging purposes.
                let border = UIBezierPath(rect: CGRect(origin: CGPoint.zero, size: graphSize))

                UIColor.red.setStroke()
                border.lineWidth = 4
                border.stroke()
                */

                for nodeCell in graphLayoutModel.nodeCells
                {
                    draw(nodeCell: nodeCell, scale: scale)
                }

                for edgeCell in graphLayoutModel.edgeCells
                {
                    draw(edgeCell: edgeCell)
                }
            }
            
            context.restoreGState()
        }
    }

    public final class ViewRenderService : ObservableObject
    {
        public var              nodeViews               = [Views.NodeView]()
        public var              edgeViews               = [Views.EdgeView]()
        public private(set) var canvasFrame: CGRect?    = nil

        @Published public var   graphIdentifier: Int    = 0
        
        private var m_animationParamSequenceGenerator   = GaussianRandomSequenceGenerator<CGFloat>(mean: 0.5, sigma: 0.7)

        fileprivate var m_graphLayoutModel: GraphLayoutModel? = nil

        fileprivate func request(canvasFrame: CGRect, graphLayoutModel: GraphLayoutModel)
        {
            let animationParamSequence = m_animationParamSequenceGenerator.generate(count: graphLayoutModel.nodeCells.count)
            var nodeViews              = [Views.NodeView]()
            var edgeViews              = [Views.EdgeView]()

            self.canvasFrame = canvasFrame
            
            m_graphLayoutModel = graphLayoutModel

            for (index, nodeCell) in graphLayoutModel.nodeCells.enumerated()
            {
                let animationParams = Views.NodeView.AnimationParams(duration: 0.7, baseParam: 0.1 + animationParamSequence[index])

                nodeViews.append(Views.NodeView(nodeCell: nodeCell, animationParams: animationParams))
            }

            for edgeCell in graphLayoutModel.edgeCells
            {
                edgeViews.append(Views.EdgeView(edgeCell: edgeCell))
            }

            self.nodeViews = nodeViews
            self.edgeViews = edgeViews

            self.graphIdentifier += 1
        }
    }

    public var renderService : RenderService

    private var m_layoutOperator : RankingOperator
    
    public init()
    {
        let renderService = RenderService()
        let edgeShapingOperator = EdgeShapingOperator(output: renderService)
        let positioningOperator = PositioningOperator(output: edgeShapingOperator)
        let sizingOperator = SizingOperator(output: positioningOperator)
        let layoutOperator = RankingOperator(output: sizingOperator)

        self.m_layoutOperator = layoutOperator
        self.renderService = renderService
    }

    public func request(graphViewModel: GraphViewModel) -> GraphLayoutModel
    {
        m_layoutOperator.request(model: graphViewModel)
    }
}

extension GraphRenderPipeline.RankingOperator
{
    fileprivate mutating func calculateIntrinsicSizeOfNodeCells(graphLayoutModel: GraphLayoutModel)
    {
        let graphLayout = graphLayoutModel.graphLayout

        func updateCellSize(nodeCell: GraphLayoutModel.NodeCell)
        {
            let cellBounds   = nodeCell.sizing!.cellBounds
            let rankPosition = nodeCell.rankPosition!

            if !rankPosition.isVirtualColumn
            {
                graphLayout.columnWidths[rankPosition.column] = cellBounds.width
            }

            graphLayout.rowHeights[rankPosition.row]      = max(cellBounds.height, graphLayout.rowHeights[rankPosition.row, default: 0])
        }

        func getSizing(nodeCell: GraphLayoutModel.NodeCell) -> GraphLayoutModel.NodeCell.Sizing
        {
            let attributedString = nodeCell.title
            let attributedStringSize = attributedString.size()

            switch nodeCell.style.shape
            {
                case .eRectangle:

                    let paddingSize = nodeCell.style.paddingSize
                    let cellSize = CGSize(width: attributedStringSize.width + paddingSize.width * 4,
                                          height: attributedStringSize.height + paddingSize.height * 1.5)

                    return GraphLayoutModel.NodeCell.Sizing(cellBounds: cellSize, titleYOffset: 0)

                case .eEllipse:

                    let paddingSize = nodeCell.style.paddingSize
                    let baseSize = CGSize(width: attributedStringSize.width + paddingSize.width,
                                          height: attributedStringSize.height + paddingSize.height * 2)

                    let lineSizes = attributedString.components(separatedBy: "\n").map {

                        $0.size()
                    }

                    func maxWidthLineIsLast() -> Bool
                    {
                        guard lineSizes.count > 1 else { return false }

                        var maxLineWidthSize  = CGSize.zero
                        var maxWidthLineIndex = 0

                        for (index, lineSize) in lineSizes.enumerated()
                        {
                            if lineSize.width > maxLineWidthSize.width
                            {
                                maxLineWidthSize = lineSize
                                maxWidthLineIndex = index
                            }
                        }

                        return maxWidthLineIndex == lineSizes.count - 1
                    }

                    let xRadius = baseSize.width / 2
                    let yRadius = baseSize.height / 2

                    var currentXRadius = xRadius
                    var y = yRadius - CGFloat(lineSizes[0].height)

                    for lineSize in lineSizes
                    {
                        let ry          = y / yRadius
                        let ryFactor    = 1 - ry * ry

                        // Safety code
                        if ryFactor <= 0
                        {
                            continue
                        }
                        
                        let projectedXRadius = (lineSize.width + lineSize.height * 2) / (2 * sqrt(ryFactor))

                        if projectedXRadius > currentXRadius
                        {
                            currentXRadius = projectedXRadius
                        }

                        y -= lineSize.height
                    }

                    let lastLineIsMaxLine = maxWidthLineIsLast() && nodeCell.isRootNodeCell
                    let adjustedSize      = lastLineIsMaxLine ? CGSize(width: baseSize.width, height: baseSize.height + paddingSize.height * 2.5) : baseSize
                    let cellSize          = CGSize(width: currentXRadius * 2, height: adjustedSize.height)

                    return GraphLayoutModel.NodeCell.Sizing(cellBounds: cellSize, titleYOffset: lastLineIsMaxLine ? -paddingSize.height * 0.7 : 0)
            }
        }

        func sizeNodeCellTree(nodeCell: GraphLayoutModel.NodeCell, nodeCellTreeLayout: inout GraphLayoutModel.GraphLayout.NodeCellTreeLayout)
        {
            for childNodeCell in nodeCell.childNodeCells
            {
                if let rankPosition = childNodeCell.rankPosition
                {
                    let column = nodeCellTreeLayout.leftColumn + rankPosition.column

                    if rankPosition.row + 1 > nodeCellTreeLayout.rowCount
                    {
                        nodeCellTreeLayout.rowCount = rankPosition.row + 1
                    }

                    if column + 1 > nodeCellTreeLayout.columnCount
                    {
                        nodeCellTreeLayout.columnCount = column + 1
                    }

                    childNodeCell.sizing = getSizing(nodeCell: childNodeCell)

                    updateCellSize(nodeCell: childNodeCell)

                    //print(">>> rank \(rankPosition) \(String(describing: childNodeCell.sizing?.cellBounds))")
                }

                sizeNodeCellTree(nodeCell: childNodeCell, nodeCellTreeLayout: &nodeCellTreeLayout)
            }
        }

        for rootCellNode in graphLayoutModel.rootNodeCells
        {
            var leftColumn         = 0
            var nodeCellTreeLayout = GraphLayoutModel.GraphLayout.NodeCellTreeLayout(leftColumn: leftColumn)

            nodeCellTreeLayout.rowCount     = 1
            nodeCellTreeLayout.columnCount  = 1

            if rootCellNode.rankPosition != nil
            {
                rootCellNode.sizing = getSizing(nodeCell: rootCellNode)
                updateCellSize(nodeCell: rootCellNode)
            }
     
            sizeNodeCellTree(nodeCell: rootCellNode, nodeCellTreeLayout: &nodeCellTreeLayout)

            graphLayout.rowCount    = max(graphLayout.rowCount, nodeCellTreeLayout.rowCount)
            graphLayout.columnCount += nodeCellTreeLayout.columnCount

            leftColumn = nodeCellTreeLayout.leftColumn + nodeCellTreeLayout.columnCount
        }
    }

    private func rankNodeCells(graphLayoutModel: GraphLayoutModel)
    {
        func resolveNodeCellRanking(nodeCell: GraphLayoutModel.NodeCell, branchRow: Int, branchColumn: Int, maxRow: Int) -> (Int, Int)
        {
            if nodeCell.childNodeCells.isEmpty
            {
                let rankPosition = GraphLayoutModel.RankPosition(row: maxRow, column: branchColumn, isVirtualColumn: false)

                graphLayoutModel.rankToNodeCell[rankPosition] = nodeCell
                nodeCell.rankPosition = rankPosition

                return (branchColumn, branchColumn)
            }
            else
            {
                let childCount       = nodeCell.childNodeCells.count
                let isEvenChildCount = childCount % 2 == 0
                let rankIndexOffset  = isEvenChildCount ? 1 : 0
                let midIndex        = (childCount / 2) - rankIndexOffset
                var midColumn       = branchColumn
                var maxColumn       = branchColumn - 1

                for (index, childCellNode) in nodeCell.childNodeCells.enumerated()
                {
                    func getRankIndex() -> Int
                    {
                        if index == midIndex + 1,
                           rankIndexOffset == 1
                        {
                            return maxColumn + 1 + rankIndexOffset
                        }
                        else
                        {
                            return maxColumn + 1
                        }
                    }

                    let rankIndex = getRankIndex()
                    let (branchMidColumn, branchMaxColumn) = resolveNodeCellRanking(nodeCell: childCellNode, branchRow: branchRow + 1, branchColumn: rankIndex, maxRow: maxRow)

                    if index == midIndex
                    {
                        let midColumnValue  = isEvenChildCount ? branchMaxColumn + 1 : branchMidColumn
                        let rankPosition    = GraphLayoutModel.RankPosition(row: branchRow, column: midColumnValue, isVirtualColumn: isEvenChildCount)

                        if isEvenChildCount
                        {
                            graphLayoutModel.graphLayout.virtualColumns.insert(rankPosition.column)
                        }
                        
                        graphLayoutModel.rankToNodeCell[rankPosition] = nodeCell
                        nodeCell.rankPosition = rankPosition

                        midColumn = midColumnValue
                        maxColumn = branchMaxColumn
                    }
                    else if maxColumn < branchMaxColumn
                    {
                        maxColumn = branchMaxColumn
                    }
                }

                return (midColumn, maxColumn)
            }
        }

        func getMaxRow(nodeCell: GraphLayoutModel.NodeCell, depth: Int) -> Int
        {
            var maxDepth = depth

            for childCellNode in nodeCell.childNodeCells
            {
                let maxTreeDepth = getMaxRow(nodeCell: childCellNode, depth: depth + 1)

                if maxTreeDepth > maxDepth
                {
                    maxDepth = maxTreeDepth
                }
            }

            return maxDepth
        }

        var leftColumn = 0

        for rootNodeCell in graphLayoutModel.rootNodeCells
        {
            let maxRow = getMaxRow(nodeCell: rootNodeCell, depth: 0)
            let (_, maxColumn) = resolveNodeCellRanking(nodeCell: rootNodeCell, branchRow: 0, branchColumn: leftColumn, maxRow: maxRow)

            if leftColumn < maxColumn
            {
                leftColumn = maxColumn
            }
        }
    }
}

extension GraphRenderPipeline.PositioningOperator
{
    fileprivate func positionNodeCells(canvasFrame: CGRect, graphLayoutModel: GraphLayoutModel)
    {
        let columnWidthForPadding = (canvasFrame.size.width - graphLayoutModel.graphLayout.totalColumnWidth) / CGFloat(graphLayoutModel.graphLayout.nonVirtualColumnCount + 1)
        var xOffset = CGFloat(columnWidthForPadding / 2)

        for rootNodeCell in graphLayoutModel.rootNodeCells
        {
            xOffset += positionNodeCells(canvasFrame: canvasFrame, rootNodeCell: rootNodeCell, rootNodeCellXOffset: xOffset, graphLayoutModel: graphLayoutModel)
        }
    }

    fileprivate func positionNodeCells(canvasFrame: CGRect, rootNodeCell: GraphLayoutModel.NodeCell, rootNodeCellXOffset: CGFloat, graphLayoutModel: GraphLayoutModel) -> CGFloat
    {
        let rowHeightForPadding   = (canvasFrame.size.height - graphLayoutModel.graphLayout.totalRowHeight) / CGFloat(graphLayoutModel.graphLayout.rowCount + 1)
        let columnWidthForPadding = (canvasFrame.size.width - graphLayoutModel.graphLayout.totalColumnWidth) / CGFloat(graphLayoutModel.graphLayout.nonVirtualColumnCount + 1)

        func visit(nodeCell: GraphLayoutModel.NodeCell, row: Int, xOffset: CGFloat, yOffset: CGFloat) -> (CGFloat, CGFloat)
        {
            let hasEvenChildren     = nodeCell.childCount % 2 == 0
            let centerIndex         = nodeCell.childCount / 2
            let childRow            = row + 1
            let cellWidth           = nodeCell.sizing!.cellBounds.width

            var childXOffset               = xOffset
            var childXCenterOffset         = xOffset + (cellWidth + columnWidthForPadding) / 2
            var previousChildXCenterOffset = childXCenterOffset

            if nodeCell.hasNoChildren
            {
                return (cellWidth + columnWidthForPadding, childXCenterOffset)
            }
            else
            {
                for (index, childCellNode) in nodeCell.childNodeCells.enumerated()
                {
                    guard let sizing = childCellNode.sizing else { continue }

                    let childCellWidth      = sizing.cellBounds.width + columnWidthForPadding
                    let childCellHeight     = sizing.cellBounds.height + rowHeightForPadding
                    let childYOffset        = childCellNode.hasNoChildren ? 0 : yOffset - childCellHeight

                    let (childBranchWidth, midChildXOffset) = visit(nodeCell: childCellNode, row: childRow, xOffset: childXOffset, yOffset: childYOffset)

                    if index == centerIndex
                    {
                        if hasEvenChildren
                        {
                            childXCenterOffset = (previousChildXCenterOffset + childXOffset + childCellWidth / 2) / 2

                            position(nodeCell: childCellNode, xOffset: midChildXOffset - childCellWidth / 2, yOffset: childYOffset, sizing: sizing)
                        }
                        else
                        {
                            childXCenterOffset = midChildXOffset

                            position(nodeCell: childCellNode, xOffset: midChildXOffset - childCellWidth / 2, yOffset: childYOffset, sizing: sizing)
                        }
                    }
                    else
                    {
                        if index < centerIndex
                        {
                            childXCenterOffset = midChildXOffset
                        }

                        position(nodeCell: childCellNode, xOffset: midChildXOffset - childCellWidth / 2, yOffset: childYOffset, sizing: sizing)
                    }

                    previousChildXCenterOffset  = childXCenterOffset
                    childXOffset                += childBranchWidth
                }

                return (childXOffset - xOffset, childXCenterOffset)
            }
        }

        func position(nodeCell: GraphLayoutModel.NodeCell, xOffset: CGFloat, yOffset: CGFloat, sizing: GraphLayoutModel.NodeCell.Sizing)
        {
            let cellOrigin   = CGPoint(x: xOffset + columnWidthForPadding / 2, y: yOffset + rowHeightForPadding / 2)
            let cellBounds   = sizing.cellBounds
            let titleYOffset = sizing.titleYOffset
            let titleOffset  = CGPoint(x: 0, y: titleYOffset)

            nodeCell.positioning = GraphLayoutModel.NodeCell.Positioning(

                    cellFrame: CGRect(origin: cellOrigin, size: cellBounds),
                    titleRelativeCellFrame: CGRect(origin: titleOffset, size: cellBounds)
            )
        }

        let sizing = rootNodeCell.sizing!
        let rootNodeCellYOffset = canvasFrame.size.height - sizing.cellBounds.height - rowHeightForPadding
        let (branchXOffset, centerXOffset) = visit(nodeCell: rootNodeCell, row: 0, xOffset: rootNodeCellXOffset, yOffset: rootNodeCellYOffset)

        position(nodeCell: rootNodeCell, xOffset: centerXOffset - (sizing.cellBounds.width + columnWidthForPadding) / 2, yOffset: rootNodeCellYOffset, sizing: sizing)

        return branchXOffset
    }
}

extension GraphRenderPipeline.EdgeShapingOperator
{
    fileprivate func determineEdgeCellShape(graphLayoutModel: GraphLayoutModel)
    {
        var nodeCellToEdgeCells = [GraphLayoutModel.NodeCell : ContiguousArray<GraphLayoutModel.EdgeCell>]()

        mainLoop: for edgeCell in graphLayoutModel.edgeCells
        {
            if var edgeCells = nodeCellToEdgeCells[edgeCell.fromNodeCell]
            {
                for (index, nodeEdgeCell) in edgeCells.enumerated()
                {
                    if nodeEdgeCell.fromNodeCellEdgeIndex > edgeCell.fromNodeCellEdgeIndex
                    {
                        edgeCells.insert(edgeCell, at: index)

                        nodeCellToEdgeCells[edgeCell.fromNodeCell] = edgeCells
                        continue mainLoop
                    }
                }

                nodeCellToEdgeCells[edgeCell.fromNodeCell]!.append(edgeCell)
            }
            else
            {
                nodeCellToEdgeCells[edgeCell.fromNodeCell] = [edgeCell]
            }
        }

        for (_, edgeCells) in nodeCellToEdgeCells
        {
            let childCount = edgeCells.count
            let centerIndex = childCount / 2
            let isOdd = childCount % 2 == 1

            // Make sure edge shapes are balanced (i.e. match pair sibling edges have the same shape)

            for (index, edgeCell) in edgeCells.enumerated()
            {
                let lhsEdge : GraphLayoutModel.EdgeCell
                let rhsEdge : GraphLayoutModel.EdgeCell

                if index < centerIndex
                {
                    if isOdd
                    {
                        lhsEdge = edgeCell
                        rhsEdge = edgeCells[centerIndex * 2 - index]
                    }
                    else
                    {
                        lhsEdge = edgeCell
                        rhsEdge = edgeCells[centerIndex * 2 - index  - 1]
                    }
                }
                else if index == centerIndex
                {
                    if isOdd
                    {
                        edgeCell.shape = edgeCell.createRectangularEdgeShape()

                        continue
                    }
                    else
                    {
                        lhsEdge = edgeCell
                        rhsEdge = edgeCells[centerIndex * 2 - index - 1]
                    }
                }
                else
                {
                    // All edges set in previous enumerations.
                    break
                }

                let lhsEdgeShape = childCount > 2 ? lhsEdge.determineEdgeShape() : nil
                let rhsEdgeShape = childCount > 2 ? rhsEdge.determineEdgeShape() : nil

                switch (lhsEdgeShape, rhsEdgeShape)
                {
                    case (.eQuadBezier, .eLine), (.eQuadBezier, nil):

                        lhsEdge.shape = lhsEdgeShape
                        rhsEdge.shape = rhsEdge.createQuadBezierEdgeShape()

                    case (.eLine, .eQuadBezier), (nil, .eQuadBezier):

                        lhsEdge.shape = lhsEdge.createQuadBezierEdgeShape()
                        rhsEdge.shape = rhsEdgeShape

                    case (.eQuadBezier, .eQuadBezier), (.eLine, .eLine):

                        lhsEdge.shape = lhsEdgeShape
                        rhsEdge.shape = rhsEdgeShape

                    default:

                        lhsEdge.shape = lhsEdge.createRectangularEdgeShape()
                        rhsEdge.shape = rhsEdge.createRectangularEdgeShape()
                }
            }
        }
    }
}

extension GraphRenderPipeline.GraphicsContextRenderService
{
    private func draw(edgeCell: GraphLayoutModel.EdgeCell)
    {
        let headWidth = 4 * edgeCell.style.lineWidth
        let lineWidth = edgeCell.style.lineWidth

        switch edgeCell.shape
        {
            case .eLine(let line):

                let lineArrowPath = Graphics.Shapes.makeLineArrow(line: line, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                edgeCell.style.color.setFill()
                lineArrowPath.fill()

                let lineBezierPath = Graphics.Shapes.makeLine(line: line, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                edgeCell.style.color.setStroke()
                lineBezierPath.lineWidth = lineWidth
                lineBezierPath.stroke()

            case .eQuadBezier(let bezier):

                let bezierArrowPath = Graphics.Shapes.makeQuadBezierArrow(quadBezier: bezier, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                edgeCell.style.color.setFill()
                bezierArrowPath.fill()

                let quadBezierPath = Graphics.Shapes.makeQuadBezier(quadBezier: bezier, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                edgeCell.style.color.setStroke()

                quadBezierPath.lineWidth = lineWidth
                quadBezierPath.stroke()
                
            case nil:

                assertionFailure("Expected Edge Cell to have an assigned shape")
                break
        }
    }

    private func draw(nodeCell: GraphLayoutModel.NodeCell, scale: CGFloat)
    {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        guard let frame = nodeCell.frame else { return }

        let verticalPadding   = nodeCell.style.paddingSize.height
        let shadowOffset      = frame.width * scale * 0.02
        let blur              = shadowOffset * 2.5
        let titleRelativeFrame = nodeCell.positioning?.titleRelativeCellFrame ?? CGRect.zero
        
        switch nodeCell.style.shape
        {
            case .eEllipse(let ellipseStyle):

                let textFrameOrigin    = CGPoint(x: frame.origin.x + titleRelativeFrame.origin.x, y: frame.origin.y + titleRelativeFrame.origin.y + verticalPadding / 2)
                let textFrameSize      = CGSize(width: frame.width, height: frame.height - verticalPadding)
                let textFrame          = CGRect(origin: textFrameOrigin, size: textFrameSize)
                let ellipse            = Graphics.Shapes.makeEllipse(rect: frame)

                context.setShadow(
                        offset: CGSize(width: shadowOffset, height: shadowOffset),
                        blur: blur,
                        color: UIColor.gray.cgColor
                )

                ellipseStyle.fillColor.setFill()
                ellipse.fill()

                context.setShadow(
                        offset: CGSize(width: 0, height: 0),
                        blur: 0.0,
                        color: UIColor.clear.cgColor
                )
                
                drawBorder(bezierPath: ellipse, borderStyle: ellipseStyle.borderStyle)

                nodeCell.title.draw(in: textFrame)

            case .eRectangle(let rectangleStyle):

                let textFrameOrigin    = CGPoint(x: frame.origin.x + titleRelativeFrame.origin.x, y: frame.origin.y + titleRelativeFrame.origin.y + verticalPadding * 0.1)
                let textFrameSize      = CGSize(width: frame.width, height: frame.height - verticalPadding)
                let textFrame          = CGRect(origin: textFrameOrigin, size: textFrameSize)
                let rectangle          = Graphics.Shapes.makeRectangle(rect: frame, cornerRadius: rectangleStyle.cornerRadius)

                context.setShadow(
                        offset: CGSize(width: shadowOffset, height: shadowOffset),
                        blur: blur,
                        color: UIColor.gray.cgColor
                )

                rectangleStyle.fillColor.setFill()
                rectangle.fill()

                context.setShadow(
                        offset: CGSize(width: 0, height: 0),
                        blur: 0.0,
                        color: UIColor.clear.cgColor
                )

                drawBorder(bezierPath: rectangle, borderStyle: rectangleStyle.borderStyle)

                nodeCell.title.draw(in: textFrame)
        }
    }

    private func drawBorder(bezierPath: UIBezierPath, borderStyle: GraphViewModel.Node.Style.eBorderStyle)
    {
        switch borderStyle
        {
            case .eLine(let color, let lineWidth):

                bezierPath.lineWidth = lineWidth
                color.setStroke()

                bezierPath.stroke()

            case .eThreeStripe(let color, let lineWidth, let stripeColor, let stripeWidth):

                bezierPath.lineWidth = lineWidth
                color.setStroke()

                bezierPath.stroke()

                bezierPath.lineWidth = stripeWidth
                stripeColor.setStroke()

                bezierPath.stroke()
        }
    }
}

extension GraphRenderPipeline.ViewRenderService
{
    public struct Views
    {
        public struct GraphView : View
        {
            fileprivate enum eAnimationState
            {
                case eOffScreen
                case eAnimateForwards
                case eAnimateBackwards
            }

            private let m_graphSize : CGSize?
            private let m_nodeViews : [NodeView]
            private let m_edgeViews : [EdgeView]

            @State private var animationState = eAnimationState.eOffScreen
            @State private var opacity = CGFloat(1)

            public init(graphSize : CGSize?, nodeViews : [NodeView], edgeViews : [EdgeView])
            {
                self.m_graphSize = graphSize
                self.m_nodeViews = nodeViews
                self.m_edgeViews = edgeViews
            }
            
            public var body: some View {
                
                GeometryReader { geometry in

                    let (offset, scale) = calcOffsetAndScale(geometry.size)

                    ForEach(m_nodeViews) { (nodeView) in

                        nodeView
                            .frame(width: nodeView.frame.width, height: nodeView.frame.height, alignment: .center)
                            .modifier(Effects.FlyInNodeEffect(param: calcParam(nodeView), position: nodeView.frame.origin + offset, scale: scale))
                            .opacity(opacity)
                            .onAppear {

                                if animationState == .eOffScreen
                                {
                                    opacity = 1

                                    withAnimation(Animation.easeIn(duration: nodeView.animationParams.duration)) {

                                        animationState = .eAnimateForwards
                                    }
                                }
                                else
                                {
                                    opacity = 0
                                    animationState = .eAnimateBackwards
                                    
                                    withAnimation(Animation.easeIn(duration: nodeView.animationParams.duration)) {

                                        opacity = 1
                                        animationState = .eAnimateForwards
                                    }
                                }
                            }
                    }

                    ForEach(m_edgeViews) { (edgeView) in

                        edgeView
                            .modifier(Effects.FlyInEdgeEffect(offset: offset, scale: scale))
                            .transition(.asymmetric(
                                    insertion: AnyTransition.opacity.animation(.easeIn(duration: 0.6).delay(0.6)),
                                    removal: AnyTransition.opacity.animation(.easeOut(duration: 0.1))))
                    }
                }
            }

            @inline(__always)
            private func calcParam(_ nodeView: NodeView) -> CGFloat
            {
                switch animationState
                {
                    case .eOffScreen:            return nodeView.animationParams.baseParam
                    case .eAnimateForwards:      return 0
                    case .eAnimateBackwards:     return nodeView.animationParams.baseParam
                }
            }

            @inline(__always)
            private func calcOffsetAndScale(_ viewPortSize: CGSize) -> (CGPoint, CGFloat)
            {
                guard let graphSize = m_graphSize else { return (CGPoint.zero, 1.0) }

                let xScale = viewPortSize.width / graphSize.width
                let yScale = viewPortSize.height / graphSize.height

                if xScale < yScale
                {
                    return (CGPoint(x: 0, y: (viewPortSize.height / xScale - graphSize.height) / 2), xScale)
                }
                else
                {
                    return (CGPoint(x: (viewPortSize.width / yScale - graphSize.width) / 2, y: 0), yScale)
                }
            }
        }

        public struct NodeView : View, Identifiable
        {
            public struct AnimationParams
            {
                public let duration : CGFloat
                public let baseParam:  CGFloat
            }

            public let id = UUID()
            public let frame:             CGRect
            public let text:              AttributedString
            public let textRelativeFrame: CGRect
            public let style:             GraphLayoutModel.Node.Style
            public let borderShape:       Shapes.NodeShape
            public let animationParams:   AnimationParams

            public init(nodeCell: GraphLayoutModel.NodeCell, animationParams: AnimationParams)
            {
                self.frame = nodeCell.positioning!.cellFrame
                self.text = AttributedString(nodeCell.title)
                self.textRelativeFrame = nodeCell.positioning!.titleRelativeCellFrame
                self.style = nodeCell.style
                self.borderShape = Shapes.NodeShape(nodeCell: nodeCell)
                self.animationParams = animationParams
            }

            public var body: some View {

                borderShape
                   .fill(Color(uiColor: style.shape.fillColor))
                   .shadow(color: Color(uiColor: UIColor(white: 0.5, alpha: 0.5)), radius: 6, x: 2, y: 4)

                if case .eThreeStripe(let strokeColor, let strokeWidth, let stripeColor, let stripeWidth) = style.shape.borderStyle
                {
                    borderShape
                        .stroke(Color(uiColor: strokeColor), lineWidth: strokeWidth)

                    borderShape
                        .stroke(Color(uiColor: stripeColor), lineWidth: stripeWidth)
                }
                else if case .eLine(let strokeColor, let strokeWidth) = style.shape.borderStyle
                {
                    borderShape
                        .stroke(Color(uiColor: strokeColor), lineWidth: strokeWidth)
                }

                Text(text)
                    .offset(x: textRelativeFrame.origin.x, y: textRelativeFrame.origin.y)
                    .multilineTextAlignment(.center)
                    .minimumScaleFactor(0.8)
                    .allowsTightening(true)
                    .frame(width: textRelativeFrame.size.width, height: textRelativeFrame.size.height, alignment: .center)
            }
        }

        public struct EdgeView : View, Identifiable
        {
            public let id = UUID()
            public let frame: CGRect

            private let m_edgeShape: Shapes.EdgeShape
            private let m_edgeArrow: Shapes.EdgeArrow

            public init(edgeCell: GraphLayoutModel.EdgeCell)
            {
                let edgeShape = Shapes.EdgeShape(edgeCell: edgeCell)
                let arrowShape = Shapes.EdgeArrow(edgeCell: edgeCell)

                self.m_edgeShape = edgeShape
                self.m_edgeArrow = arrowShape
                self.frame = edgeShape.graphicsEdgeShape.frame.union(arrowShape.graphicsEdgeShape.frame)
            }

            public var body: some View {

                m_edgeShape
                    .stroke(Color(uiColor: m_edgeShape.style.color), lineWidth: m_edgeShape.style.lineWidth)
                
                m_edgeArrow
                        .fill(Color(uiColor: m_edgeShape.style.color))

            }
        }
    }

    public struct Shapes
    {
        public struct NodeShape : Shape
        {
            public var strokeWidth : CGFloat
            {
                m_graphNodeShape.borderStyle.strokeWidth
            }

            private let m_graphNodeShape: GraphLayoutModel.Node.Style.eShape

            public init(nodeCell: GraphLayoutModel.NodeCell)
            {
                self.m_graphNodeShape = nodeCell.style.shape
            }

            public func path(in rect: CGRect) -> Path
            {
                Path(m_graphNodeShape.createBezier(rect: rect).cgPath)
            }
        }

        public struct EdgeShape : Shape
        {
            public let graphicsEdgeShape: Graphics.Shapes.eShape
            public let style:             GraphLayoutModel.Edge.Style

            public init(edgeCell: GraphLayoutModel.EdgeCell)
            {
                self.graphicsEdgeShape = edgeCell.shape!
                self.style = edgeCell.style
            }

            public func path(in rect: CGRect) -> Path
            {
                Path(graphicsEdgeShape.createBezier(style: style).cgPath)
            }
        }

        public struct EdgeArrow : Shape
        {
            public let graphicsEdgeShape: Graphics.Shapes.eShape
            public let style:             GraphLayoutModel.Edge.Style

            public init(edgeCell: GraphLayoutModel.EdgeCell)
            {
                self.graphicsEdgeShape = edgeCell.shape!
                self.style = edgeCell.style
            }

            public func path(in rect: CGRect) -> Path
            {
                Path(graphicsEdgeShape.createArrow(style: style).cgPath)
            }
        }
    }

    public struct Effects
    {
        public struct FlyInNodeEffect: GeometryEffect
        {
            private var m_param:        CGFloat
            private var m_position:     CGPoint
            private var m_scale:        CGFloat

            public init(param: CGFloat, position : CGPoint, scale: CGFloat)
            {
                self.m_param        = param
                self.m_position     = position
                self.m_scale        = scale
            }

            public var animatableData: CGFloat
            {
                get { m_param }
                set { m_param = newValue }
            }

            public func effectValue(size: CGSize) -> ProjectionTransform
            {
                var transform3d = CATransform3DIdentity;

                transform3d.m34 = -1 / 500.0
                transform3d = CATransform3DTranslate(transform3d, 500 * m_param, 250 * m_param, 900 * m_param)

                let affineTransform = ProjectionTransform(CGAffineTransform(translationX: m_position.x, y: m_position.y)
                                                                  .concatenating(CGAffineTransform(scaleX: m_scale, y: m_scale)))

                return ProjectionTransform(transform3d).concatenating(affineTransform)
            }
        }

        public struct FlyInEdgeEffect: GeometryEffect
        {
            private var m_offset: CGPoint
            private var m_scale: CGFloat

            public init(offset: CGPoint, scale: CGFloat)
            {
                self.m_offset = offset
                self.m_scale = scale
            }

            public func effectValue(size: CGSize) -> ProjectionTransform
            {
                ProjectionTransform(CGAffineTransform(translationX: m_offset.x, y: m_offset.y)
                                            .concatenating(CGAffineTransform(scaleX: m_scale, y: m_scale)))
            }
        }
    }
}

public struct Graphics
{
    public struct Shapes
    {
        public enum eShape
        {
            case eLine(Line)
            case eQuadBezier(QuadBezier)

            public var frame : CGRect
            {
                switch self
                {
                    case .eLine(let line):

                        return line.frame

                    case .eQuadBezier(let bezier):

                        return bezier.frame
                }
            }

            public func createBezier(style: GraphViewModel.Edge.Style) -> UIBezierPath
            {
                let headWidth = 4 * style.lineWidth
                let lineWidth = style.lineWidth

                switch self
                {
                    case .eLine(let line):

                        return Graphics.Shapes.makeLine(line: line, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                    case .eQuadBezier(let bezier):

                        return Graphics.Shapes.makeQuadBezier(quadBezier: bezier, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)
                }
            }

            public func createArrow(style: GraphViewModel.Edge.Style) -> UIBezierPath
            {
                let headWidth = 4 * style.lineWidth
                let lineWidth = style.lineWidth

                switch self
                {
                    case .eLine(let line):

                        return Graphics.Shapes.makeLineArrow(line: line, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)

                    case .eQuadBezier(let bezier):

                        return Graphics.Shapes.makeQuadBezierArrow(quadBezier: bezier, lineWidth: lineWidth, headWidth: headWidth, headLength: headWidth)
                }
            }
        }

        public struct Line : Traceable
        {
            public let start: CGPoint
            public let end:   CGPoint

            @inline(__always)
            public var length : CGFloat
            {
                hypot(end.x - start.x, end.y - start.y)
            }

            @inline(__always)
            public var angle : CGFloat
            {
                CGFloat(atan2f(Float(end.y - start.y), Float(end.x - start.x)))
            }

            @inline(__always)
            public var frame : CGRect
            {
                let minX = min(start.x, end.x)
                let minY = min(start.y, end.y)
                let width = abs(start.x - end.x)
                let height = abs(start.y - end.y)

                return CGRect(origin: CGPoint(x: minX, y: minY), size: CGSize(width: width, height: height))
            }

            @inline(__always)
            public func reversed() -> Line
            {
                Line(start: self.end, end: self.start)
            }
            
            @inline(__always)
            func getY(x: CGFloat) -> CGFloat?
            {
                guard start.x != end.x else { return x == start.x ? 0 : nil }

                let m = (start.y - end.y) / (start.x - end.x)
                let y0 = -end.x * m + end.y
                let y = x * m + y0

                return y
            }

            @inline(__always)
            func getX(y: CGFloat) -> CGFloat?
            {
                guard start.x != end.x else { return y == start.y ? 0 : nil }

                let mInv = (start.x - end.x) / (start.y - end.y)
                let x0 = -end.y * mInv + end.x
                let x = y * mInv + x0

                return x
            }
        }

        public struct QuadBezier : Traceable
        {
            public let start:   CGPoint
            public let end:     CGPoint
            public let control: CGPoint

            @inline(__always)
            public var frame : CGRect
            {
                let minX = min(start.x, end.x)
                let minY = min(start.y, end.y)
                let width = abs(start.x - end.x)
                let height = abs(start.y - end.y)

                return CGRect(origin: CGPoint(x: minX, y: minY), size: CGSize(width: width, height: height))
            }

            @inline(__always)
            public var endAngle : CGFloat
            {
                CGFloat(atan2f(Float(end.y - control.y), Float(end.x - control.x)))
            }

            @inline(__always)
            public func x(forT t: CGFloat) -> CGFloat
            {
                let a = (start.x - 2 * control.x + end.x)
                let b = 2 * (control.x - start.x)
                let c = start.x

                return a * t * t + b * t + c
            }

            @inline(__always)
            public func y(forT t: CGFloat) -> CGFloat
            {
                let a = (start.y - 2 * control.y + end.y)
                let b = 2 * (control.y - start.y)
                let c = start.y

                return a * t * t + b * t + c
            }

            @inline(__always)
            public func t(forX x: CGFloat) -> (CGFloat, CGFloat)
            {
                let a = (start.x - 2 * control.x + end.x)
                let b = 2 * (control.x - start.x)
                let c = start.x - x

                return ((-b + sqrt(b * b - 4 * a * c)) / (2 * a), (-b - sqrt(b * b - 4 * a * c)) / (2 * a))
            }

            @inline(__always)
            public func t(forY y: CGFloat) -> (CGFloat, CGFloat)
            {
                let a = (start.y - 2 * control.y + end.y)
                let b = 2 * (control.y - start.y)
                let c = start.y - y

                return ((-b + sqrt(b * b - 4 * a * c)) / (2 * a), (-b - sqrt(b * b - 4 * a * c)) / (2 * a))
            }

            @inline(__always)
            public func t(forEllipse rect: CGRect) -> CGFloat
            {
                // Newton–Raphson approximation of intersection point.
                // Typical cycle count is less than 10.

                let rx = rect.width / 2
                let ry = rect.height / 2
                let center = rect.center

                func distanceMetricFromEllipse(_ point: CGPoint) -> CGFloat
                {
                    let xComponent = (point.x - center.x) / rx
                    let yComponent = (point.y - center.y) / ry

                    return xComponent * xComponent + yComponent * yComponent
                }

                var maxT = CGFloat(0.5)
                var minT = CGFloat(0.0)

                var currentT = CGFloat(0)
                var currentMetric = CGFloat(0)

                var iterationCounter = 0

                while (abs(currentMetric) > 1.04 || abs(currentMetric) < 0.99)
                {
                    let nextT = (maxT + minT) / 2
                    let nextPoint = CGPoint(x: x(forT: nextT), y: y(forT: nextT))

                    currentMetric = distanceMetricFromEllipse(nextPoint)
                    currentT = nextT
                    
                    if currentMetric < 1
                    {
                        minT = max(nextT, minT)
                    }
                    else if currentMetric > 1
                    {
                        maxT = min(nextT, maxT)
                    }
                    else
                    {
                        break
                    }
                    
                    iterationCounter += 1

                    if iterationCounter > 100
                    {
                        return (minT + maxT) / 2
                    }

                    if currentT < 0 || 1.0 < currentT
                    {
                        currentT = 1.0 / CGFloat(iterationCounter)
                    }
                }

                trace(title: "QuadBezier", "intersection end iterations: \(iterationCounter) metric: \(currentMetric) currentT \(currentT) min \(minT) max \(maxT)")

                return currentT
            }

            @inline(__always)
            public func reversed() -> QuadBezier
            {
                QuadBezier(start: self.end, end: self.start, control: self.control)
            }
        }
    }

    fileprivate struct Library
    {
        public enum eAntiClockwiseQuadrant
        {
            case e0
            case e90
            case e180
            case e270
        }

        public enum eSide
        {
            case eRight
            case eTop
            case eLeft
            case eBottom
        }

        fileprivate static func getAngle(from : CGPoint, to: CGPoint) -> CGFloat
        {
            // The y-coordinate runs downward.
            normalize(angle: CGFloat(atan2f(Float(-to.y + from.y), Float(to.x - from.x))))
        }

        fileprivate static func getQuadrant(forAngle angle: CGFloat) -> eAntiClockwiseQuadrant
        {
            let normalizedAngle = normalize(angle: angle)

            if normalizedAngle <= CGFloat.pi / 2
            {
                return .e0
            }
            else if normalizedAngle <= CGFloat.pi
            {
                return .e90
            }
            else if normalizedAngle < CGFloat.pi * 3.0 / 2.0
            {
                return .e180
            }
            else
            {
                return .e270
            }
        }

        fileprivate static func normalize(angle: CGFloat) -> CGFloat
        {
            let pi2 = 2 * CGFloat.pi

            if angle < 0
            {
                let rotations = 1 - Int(angle / pi2)

                return angle + CGFloat(rotations) * pi2
            }
            else if angle >= pi2
            {
                let rotations = Int(angle / pi2)

                return angle - CGFloat(rotations) * pi2
            }
            else
            {
                return angle
            }
        }

        fileprivate static func getIntersectionSide(angle: CGFloat, forRect rect: CGRect) -> eSide
        {
            let quadrant = Library.getQuadrant(forAngle: angle)
            let rectangleAngle = Library.getRectCornerAngle(forQuadrant: quadrant, inRect: rect)

            switch quadrant
            {
                case .e0:

                    return angle < rectangleAngle ? .eRight : .eTop

                case .e90:

                    return angle < rectangleAngle ? .eTop : .eLeft

                case .e180:

                    return angle < rectangleAngle ? .eLeft :.eBottom

                case .e270:

                    return angle < rectangleAngle ? .eBottom : .eRight
            }
        }

        fileprivate static func getRectangleLineIntersectionPoint(angle: CGFloat, forRect rect: CGRect) -> CGPoint
        {
            let crossSide = Library.getIntersectionSide(angle: angle, forRect: rect)

            switch crossSide
            {
                case .eRight:

                    return CGPoint(x: rect.maxX, y:rect.midY - (rect.maxX - rect.midX) * sin(angle))

                case .eTop:

                    return CGPoint(x: rect.midX + (rect.midY - rect.minY) * cos(angle), y: rect.minY)

                case .eLeft:

                    return CGPoint(x: rect.minX, y: rect.midY - (rect.maxX - rect.midX) * sin(angle))

                case .eBottom:

                    return CGPoint(x: rect.midX + (rect.maxY - rect.midY) * cos(angle), y: rect.maxY)
            }
        }

        fileprivate static func doesIntersectFromBelow(line: Graphics.Shapes.Line, withRect rect: CGRect) -> Bool
        {
            if let y = line.getX(y: rect.maxY),
               rect.minY <= y && y <= rect.maxY
            {
                return true
            }
            else if let x = line.getY(x: rect.minX),
                    rect.minX <= x && x <= rect.maxX
            {
                return true
            }
            else if let x = line.getY(x: rect.maxX),
                    rect.minX <= x && x <= rect.maxX
            {
                return true
            }

            return false
        }

        fileprivate static func getQuadBezierToRectangleIntersectionPoint(quadBezier: Graphics.Shapes.QuadBezier, angle: CGFloat, forRect rect: CGRect) -> CGPoint
        {
            if angle < CGFloat.pi
            {
                let (t1, _) = quadBezier.t(forY: rect.minY)

                let (_, x2) = (quadBezier.x(forT: t1), quadBezier.x(forT: t1))

                return CGPoint(x: x2, y: rect.minY)
            }
            else
            {
                let (t1, t2) = quadBezier.t(forY: rect.maxY)

                let (_, x2) = (quadBezier.x(forT: t1), quadBezier.x(forT: t2))

                return CGPoint(x: x2, y: rect.maxY)
            }
        }

        fileprivate static func getQuadBezierToEllipseIntersectionPoint(quadBezier: Graphics.Shapes.QuadBezier, angle: CGFloat, forRect rect: CGRect) -> CGPoint
        {
            let t = quadBezier.t(forEllipse: rect)
            let x2 = quadBezier.x(forT: t)
            let y2 = quadBezier.y(forT: t)

            return CGPoint(x: x2, y: y2)
        }

        fileprivate static func getEllipseToLineIntersectionPoint(angle: CGFloat, forRect rect: CGRect) -> CGPoint
        {
            let xRadius = rect.width / 2
            let yRadius = rect.height / 2
            let rx = cos(angle) / xRadius
            let ry = sin(angle) / yRadius
            let r = 1 / sqrt(rx * rx + ry * ry)

            return CGPoint(x: rect.midX + r * cos(angle), y: rect.midY - r * sin(angle))
        }

        fileprivate static func getRectCornerAngle(forQuadrant quadrant: eAntiClockwiseQuadrant, inRect rect: CGRect) -> CGFloat
        {
            let midPoint = CGPoint(x: rect.midX, y: rect.midY)

            switch quadrant
            {
                case .e0:           return getAngle(from: midPoint, to: CGPoint(x: rect.maxX, y: rect.minY))
                case .e90:          return getAngle(from: midPoint, to: CGPoint(x: rect.minX, y: rect.minY))
                case .e180:         return getAngle(from: midPoint, to: CGPoint(x: rect.minX, y: rect.maxY))
                case .e270:         return getAngle(from: midPoint, to: CGPoint(x: rect.maxX, y: rect.maxY))
            }
        }

        fileprivate static func getQuadBezier(fromRect: CGRect, toRect: CGRect) -> Shapes.QuadBezier?
        {
            let fromCenter          = fromRect.center
            let toCenter            = toRect.center

            let fromToDistance      = fromCenter.distance(to: toCenter)
            let midpoint             = CGPoint(x: (fromCenter.x + toCenter.x) / 2, y: (fromCenter.y + toCenter.y) / 2)
            let fromToNormalGradient = (toCenter.x - fromCenter.x) / (toCenter.y - fromCenter.y)
            let sinNormal            = 1 / sqrt(1 + 1 / fromToNormalGradient * fromToNormalGradient)
            let cosNormal            = 1 / sqrt(1 + fromToNormalGradient * fromToNormalGradient)

            func getControlPoint() -> CGPoint
            {
                let offset = fromToDistance / 4

                if toCenter.x < fromCenter.x
                {
                    return CGPoint(x: midpoint.x - cosNormal * offset, y: midpoint.y + sinNormal * offset)
                }
                else if toCenter.x > fromCenter.x
                {
                    return CGPoint(x: midpoint.x + cosNormal * offset, y: midpoint.y + sinNormal * offset)
                }
                else
                {
                    return midpoint
                }
            }

            return Shapes.QuadBezier(start: fromCenter, end: toCenter, control: getControlPoint())
        }
    }
}

extension Graphics.Shapes
{
    fileprivate static func makeLineArrow(line: Graphics.Shapes.Line, lineWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath
    {
        func p(_ x: CGFloat, _ y: CGFloat) -> CGPoint
        {
            CGPoint(x: x, y: y)
        }

        let arrowPoint = line.end
        let arrowAngle = line.angle

        let points: [CGPoint] = [

            p(-headLength, lineWidth / 2),
            p(-headLength, headWidth / 2),
            p(0, 0),
            p(-headLength, -headWidth / 2),
            p(-headLength, -lineWidth / 2),
        ]

        let cosine = cos(arrowAngle)
        let sine = sin(arrowAngle)
        let transform = CGAffineTransform(a: cosine, b: sine, c: -sine, d: cosine, tx: arrowPoint.x, ty: arrowPoint.y)

        let arrowPath = CGMutablePath()

        arrowPath.addLines(between: points, transform: transform)

        arrowPath.closeSubpath()

        return UIBezierPath(cgPath: arrowPath)
    }

    fileprivate static func makeLine(line: Graphics.Shapes.Line, lineWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath
    {
        let lineAngle   = line.angle
        let cosine      = cos(lineAngle)
        let sine        = sin(lineAngle)
     
        let linePath = CGMutablePath()

        linePath.move(to: line.start)
        linePath.addLine(to: CGPoint(x: line.end.x - headLength * cosine, y: line.end.y - headLength * sine))
         
        return UIBezierPath(cgPath: linePath)
    }

    fileprivate static func makeQuadBezierArrow(quadBezier: Graphics.Shapes.QuadBezier, lineWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath
    {
        func p(_ x: CGFloat, _ y: CGFloat) -> CGPoint
        {
            CGPoint(x: x, y: y)
        }

        let arrowPoint = quadBezier.end
        let arrowAngle = quadBezier.endAngle

        let points: [CGPoint] = [

            p(-headLength, lineWidth / 2),
            p(-headLength, headWidth / 2),
            p(0, 0),
            p(-headLength, -headWidth / 2),
            p(-headLength, -lineWidth / 2),
        ]

        let cosine = cos(arrowAngle)
        let sine = sin(arrowAngle)
        let transform = CGAffineTransform(a: cosine, b: sine, c: -sine, d: cosine, tx: arrowPoint.x, ty: arrowPoint.y)

        let arrowPath = CGMutablePath()

        arrowPath.addLines(between: points, transform: transform)

        arrowPath.closeSubpath()

        return UIBezierPath(cgPath: arrowPath)
    }
    
    fileprivate static func makeQuadBezier(quadBezier: Graphics.Shapes.QuadBezier, lineWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath
    {
        let quadBezierPath = CGMutablePath()

        let arrowAngle = quadBezier.endAngle
        let cosine      = cos(arrowAngle)
        let sine        = sin(arrowAngle)

        quadBezierPath.move(to: quadBezier.start)
        quadBezierPath.addQuadCurve(to: CGPoint(x: quadBezier.end.x - headLength * cosine, y: quadBezier.end.y - headLength * sine), control: quadBezier.control)
        
        return UIBezierPath(cgPath: quadBezierPath)
    }

    fileprivate static func makeEllipse(rect: CGRect) -> UIBezierPath
    {
        UIBezierPath(ovalIn: rect)
    }

    fileprivate static func makeRectangle(rect: CGRect, cornerRadius: CGFloat) -> UIBezierPath
    {
        UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
    }
}
