//
//  ViewModels/PencilCanvasViewModel.swift
//  BSPDemo
//
//  Created by Terry Stillone on 5/12/21.
//

import Foundation
import Combine
import PencilKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Pencil Kit View Model.
///

public class PencilCanvasViewModel: NSObject, ObservableObject, PKToolPickerObserver
{
    public typealias VocalResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.VocalResult
    public typealias eResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult
    
    var subscription : AnyCancellable? = nil
    
    var canvasView : PKCanvasView?
    {
        get { m_canvasView }
        set { m_canvasView = newValue }
    }
    
    private var m_canvasView : PKCanvasView? = nil
    private var m_toolPicker : PKToolPicker? = nil
    
    func clearDrawing()
    {
        m_canvasView?.drawing = PKDrawing()
    }
    
    /// Present or remove  the PencilKit Pen Tool picker.
    
    func presentPencilPicker(enable: Bool, progressFunc: (eResult) -> Void)
    {
        guard #available(iOS 13, *),
              let canvasView = m_canvasView else { return }

        func handleObserver(enable: Bool, pencilPickerTool: PKToolPicker)
        {
            if enable
            {
                pencilPickerTool.addObserver(canvasView)
                pencilPickerTool.addObserver(self)
            }
            else
            {
                pencilPickerTool.removeObserver(canvasView)
                pencilPickerTool.removeObserver(self)
            }
        }

        func handleResult(pencilPickerTool: PKToolPicker) -> eResult
        {
            switch (enable, pencilPickerTool.isVisible)
            {
                case (true, true):

                    return .eSuccess(VocalResult("Displaying pencil controls"))

                case (false, false):

                    return .eSuccess(VocalResult("Removing pencil controls"))

                case (false, true):

                    return .eFailure("Could not remove pencil controls")

                case (true, false):

                    return .eFailure("Could not present pencil control")
            }
        }

        if let pencilPickerTool = m_toolPicker
        {
            pencilPickerTool.setVisible(enable, forFirstResponder: canvasView)
            handleObserver(enable: enable, pencilPickerTool: pencilPickerTool)

            progressFunc(handleResult(pencilPickerTool: pencilPickerTool))
        }
        else if #available(iOS 14.0, *)
        {
            let pencilPickerTool = PKToolPicker()

            pencilPickerTool.setVisible(enable, forFirstResponder: canvasView)
            handleObserver(enable: enable, pencilPickerTool: pencilPickerTool)
            
            canvasView.becomeFirstResponder()

            m_toolPicker = pencilPickerTool

            m_canvasView?.tool = PKInkingTool(.pen, color: .orange, width: 11)

            progressFunc(handleResult(pencilPickerTool: pencilPickerTool))
        }
    }
}

   
