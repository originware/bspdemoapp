//
//  Main/BSPDemoApp.swift
//  BSPDemo
//
//  Created by Terry Stillone on 23/11/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import CoreLocation
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// App Delegate
///

class AppDelegate: NSObject, UIApplicationDelegate
{
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        // Do nothing
        true
    }

#if targetEnvironment(macCatalyst)

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration
    {
         let configuration = UISceneConfiguration(name: nil, sessionRole: connectingSceneSession.role)

        // Configure Scene Delegate class.
         if connectingSceneSession.role == .windowApplication
         {
             configuration.delegateClass = SceneDelegate.self
         }
        
         return configuration
     }
    
#endif
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Scene Delegate, used only for MacCatalyst only.
///

#if targetEnvironment(macCatalyst)

class SceneDelegate: NSObject, UIWindowSceneDelegate, ObservableObject
{
    func windowScene(_ windowScene: UIWindowScene,
                     didUpdate previousCoordinateSpace: UICoordinateSpace,
                     interfaceOrientation previousInterfaceOrientation: UIInterfaceOrientation,
                     traitCollection previousTraitCollection: UITraitCollection)
    {
        guard let windowSize = windowScene.keyWindow?.bounds.size else { return }

        windowScene.titlebar?.titleVisibility = .hidden

        // React to Catalyst window size change.
        if Styles.Sizes.SafeAreaWidth != windowSize.width
           || Styles.Sizes.SafeAreaHeight != windowSize.height
        {
            Styles.Sizes.SafeAreaWidth  = windowSize.width
            Styles.Sizes.SafeAreaHeight = windowSize.height

            Styles.Sizes.WindowWidth    = windowSize.width
            Styles.Sizes.WindowHeight   = windowSize.height
            Styles.Sizes.WindowDiagonal = windowSize.width
        }

        if let titlebar = windowScene.titlebar {
            titlebar.titleVisibility = .visible
            titlebar.toolbar = nil
        }
    }
}
#endif

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// App Main
///

@main
struct BSPDemoApp: App, Traceable
{
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @Environment(\.scenePhase) private var scenePhase

    @ObservedObject private var presentationActor: PresentationActor
    @ObservedObject private var presentationScopeStack: ScopeStack
    @ObservedObject private var mainViewModel :    MainViewModel

    private var m_applicationModel : ApplicationModel
    
    init()
    {
        func buildTestConfig() -> AppConfig.TestingConfig?
        {
            let mockServices = CommandLine.arguments.contains("-mock-services")
            
            guard mockServices else { return nil }
            
            let showAutomationView = CommandLine.arguments.contains("-show-automationView")
            let mockLocation = CLLocation(latitude: -35.0976, longitude: 106.6682)
            let mockAddress = "Sydney, Australia"
            
            return AppConfig.TestingConfig(showAutomationView: showAutomationView,
                                mockServices: mockServices,
                                mockLocation: mockLocation,
                                mockAddress: mockAddress)
        }
        
        let testingConfig = buildTestConfig()
        let commandResponderType = CommandBehaviourModel.eCommandResponderType.eSample
        let enableAnnouncements = !CommandLine.arguments.contains("-disable-announcements")

        let config = AppConfig(commandResponderType: commandResponderType, enableAnnouncements: enableAnnouncements, testingConfig: testingConfig)
        let notifierDirectory = NotifierDirectory(notifierStore: NotifierStore())
        let behaviourScopeStack = ScopeStack(uri: BehaviourActor.BehaviourStackURI, notifierDirectory: notifierDirectory)
        let presentationScopeStack = ScopeStack(uri: PresentationActor.PresentationStackURI, notifierDirectory: notifierDirectory)
        let appBuilder = AppBuilder(config: config, notifierDirectory: notifierDirectory, behaviourScopeStack: behaviourScopeStack, presentationScopeStack: presentationScopeStack)
        let applicationModel = ApplicationModel(appBuilder: appBuilder)
        
        self.m_applicationModel = applicationModel
        self.mainViewModel = applicationModel.mainViewModel
        self.presentationActor = applicationModel.presentationActor
        self.presentationScopeStack = applicationModel.presentationActor.scopeStack ?? ScopeStack(uri: PresentationActor.PresentationStackURI, notifierDirectory: notifierDirectory)
    }
    
    var body: some Scene
    {
        WindowGroup {
            
            // Pass the PresentationModel and MainViewModel to the Content View.

            GeometryReader { geometry in

                ContentView()
                    .statusBar(hidden: true)
                    .environmentObject(presentationActor)
                    .environmentObject(presentationScopeStack)
                    .environmentObject(mainViewModel)
                    .set(windowSize: geometry.size)
            }
        }
        .onChange(of: scenePhase) { (newScenePhase) in
            
            switch newScenePhase
            {
                case .active:

                    // Create Services, if not currently deployed.
                    if m_applicationModel.serviceActor.serviceDirectory == nil
                    {
                        let serviceDirectory = ServiceActor.ServiceDirectory(appBuilder: m_applicationModel.appBuilder)

                        m_applicationModel.serviceActor.serviceDirectory = serviceDirectory
                    }

                    // Publish active hosting notification
                    m_applicationModel.serviceActor.notifiers.service.event.send(.eOnAppStatusChange(.eOnAppActive))

                case .inactive:

                    let appIsFunctional = m_applicationModel.behaviourActor.state.appIsFunctional

                    // Publish inactive hosting notification
                    m_applicationModel.serviceActor.notifiers.service.event.send(.eOnAppStatusChange(.eOnAppInActive))

                    // If the app was in a stable state, free up memory by destructing services.
                    if appIsFunctional
                    {
                        m_applicationModel.serviceActor.serviceDirectory = nil
                    }

                case .background:
                    
                    trace(title: m_applicationModel.appIdentifier ?? "App", "Scene in background")
                        
                @unknown default:
                    
                    trace(title: m_applicationModel.appIdentifier ?? "App", "Scene in unexpected state")
            }
        }
    }
}