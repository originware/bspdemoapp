//
//  Views/Views.swift
//  BSPDemo
//
//  Created by Terry Stillone on 16/12/21.
//  Copyright (c) 2022 Originware. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import WebKit
import MessageUI
import PencilKit


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Scope Stack Views.
///
///

enum eScopeView: Equatable
{
    case eGraph
    case eMessageView(Views.MessageView)
    case eWebView(Views.WebWithDismissButtonView)

    public static func ==(lhs: eScopeView, rhs: eScopeView) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eGraph, .eGraph),
                 (.eMessageView, eMessageView),
                 (.eWebView, eWebView):

                return true

            default:

                return false
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The custom Views.
///

public struct Views
{
    struct HeaderView : View
    {
        @Binding var headerSize : CGSize

        var body: some View {

                HStack {

                    Rectangle()
                            .fill(.clear)
                            .frame(width: headerSize.width * 0.1, height: Styles.Sizes.TitleHeight, alignment: .center)
                            .padding(.leading, headerSize.width * 0.048)

                    Text(AttributedString("Agent", attributes: AttributeContainer.font(Styles.AgentTitleFont)))
                            .scaledToFit()
                            .minimumScaleFactor(0.5)
                            .frame(width: headerSize.width * 0.1)
                            .foregroundColor(Color(uiColor: Styles.AgentTitleColor))
                            .padding(.top, headerSize.height * 0.0005)

                    Spacer(minLength: 1)
                    Image("BSP Demo Title")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: headerSize.width * 0.4)
                            .padding(.top, headerSize.height * 0.0005)

                    Spacer()
                    Image(decorative: "From Originware Image")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: headerSize.width * 0.15)
                            .padding(.trailing, headerSize.width * 0.048)
                            .padding(.top, headerSize.height * 0.0005)
                }
        }
    }

    struct CanvasView: View
    {
        @ObservedObject var pencilCanvasModel : PencilCanvasViewModel
        @State private var canvasView = PKCanvasView()

        var body: some View
        {
            RepresentedUIViews.PencilKitCanvasView(canvasView: $canvasView, pencilCanvasModel: pencilCanvasModel)
        }
    }

    struct WebWithDismissButtonView: View
    {
        let webViewModel : WebViewModel
        let notifierDirectory: WebURLScope.ActorNotifierDirectory

        @State var webContentHeight = CGFloat(0)
        
        var body: some View {

            ZStack {

                GeometryReader { reader in
                    
                    ScrollView(.vertical) {
                     
                        RepresentedUIViews.WebURLView(webViewModel: webViewModel, contentHeight: $webContentHeight)
                        
                            .frame(width: reader.size.width, height: max(reader.size.height, webContentHeight))
                            .edgesIgnoringSafeArea(.vertical)
                            .shadow(radius: 3, x: 2, y: 2)
                            .accessibilityLabel("/webView/webView")
                    }
                }
                
                VStack {
                    
                    HStack {
                        
                        Spacer()
                        Button(action: {

                            notifierDirectory.behaviour.stack.send(.ePop(WebURLScope.Behaviour.WebURLScopeURI))

                        }) {
                            Image("Dismiss")
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                    .padding(10)
                        }
                        .accessibilityLabel("/webView/buttons/dismiss")
                    }

                    Spacer()
                }
            }
        }
    }

    public struct MessageView: View
    {
        @EnvironmentObject var presentationActor: PresentationActor
        @State var messageModel : MessageScope.MessageModel

        public var body: some View {

            RepresentedUIControllers.MessageComposeView(messageModel: $messageModel, messageViewController: presentationActor.createMFMessageComposeViewController()) { result in

                if presentationActor.notifiers.presentation.message.isActive
                {
                    presentationActor.notifiers.presentation.message.event?.send(.eMessageResult(result))
                }
            }
        }
    }

    /// Automation view used in UI Unit Testing. The view provides access to testing control elements..
    struct AutomationView: View
    {
        let presentationActor:    PresentationActor
        let automationViewModel : AutomationViewModel
        
        @Binding public var automationRequest : String
        @Binding public var automationReply : String

        var body: some View {

            VStack(alignment: .leading, spacing: 10) {

                // Title
                Text("Automation Panel")
                    .foregroundColor(.red)
                    .fontWeight(.bold)
                
                // The automation request input element
                TextField("automationRequest", text: $automationRequest)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .modifier(Modifiers.Stylers.AutomationText())
                    .accessibilityLabel("/automationView/textFields/automationRequest")
                
                // The automation reply (reply to request) output element
                TextField("automationReply", text: $automationReply)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .modifier(Modifiers.Stylers.AutomationText())
                    .accessibilityLabel("/automationView/textFields/automationReply")
            }
            .padding(20)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(Color(uiColor: .red), lineWidth: 1.1)
            )
            .padding(20)
        }
    }

    struct SelectiveImage : View
    {
        let iOSImageName : String
        let macCatalystImageName : String

        init(iOS: String, mac : String)
        {
            self.iOSImageName = iOS
            self.macCatalystImageName = mac
        }

        var body: some View {

#if targetEnvironment(macCatalyst)
            Image(decorative: macCatalystImageName)
                    .resizable()
                    .scaledToFill()
#else
            Image(decorative: iOSImageName)
                    .resizable()
                    .scaledToFill()
#endif
        }
    }
}

struct RepresentedUIViews
{
    @MainActor
    struct WebURLView: UIViewRepresentable {

        let webViewModel : WebViewModel

        init(webViewModel: WebViewModel, contentHeight: Binding<CGFloat>)
        {
            self.webViewModel = webViewModel

            webViewModel.createWKWebView()
            webViewModel.contentHeight = contentHeight
        }
        
        func makeUIView(context: Context) -> WKWebView
        {
            webViewModel.webView
        }
        
        func updateUIView(_ webView: WKWebView, context: Context)
        {
        }
    }

    struct AvatarView: UIViewRepresentable {

        let avatarUIView : AvatarUIView

        func makeUIView(context: Context) -> AvatarUIView                    { avatarUIView }
        func updateUIView(_ avatarUIView: AvatarUIView, context: Context)    {}
    }

    struct PencilKitCanvasView: UIViewRepresentable {

        @Binding var canvasView: PKCanvasView
        @ObservedObject var pencilCanvasModel : PencilCanvasViewModel

        func makeUIView(context: Context) -> PKCanvasView
        {
            canvasView.drawingPolicy = .pencilOnly
            canvasView.backgroundColor = .clear

            canvasView.tool = PKInkingTool(.pen, color: .orange, width: 11)

            pencilCanvasModel.canvasView = canvasView

            return canvasView
        }

        func updateUIView(_ canvasView: PKCanvasView, context: Context)
        {
            canvasView.tool = PKInkingTool(.pen, color: .orange, width: 11)
        }
    }
}

struct RepresentedUIControllers
{
    struct UITextViewControllerRepresentable: UIViewControllerRepresentable {

        final class UITextViewController: UIViewController {

            fileprivate var textView : UITextView? = nil
            fileprivate weak var presentationActor : PresentationActor? = nil

#if targetEnvironment(macCatalyst)
            private var m_timer : Timer? = nil
            private var m_extendTimer = false
#endif
            
            override func viewDidLoad()
            {
                view.backgroundColor = .clear

                if let textView = textView
                {
                    let frame = view.frame
                    let textWidth = Styles.Sizes.TextViewWidth
                    let contentSize = textView.sizeThatFits(CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude))
                    let adjustedFrameSize = CGSize(width: textWidth, height: contentSize.height)

                    view.frame = CGRect(origin: frame.origin, size: adjustedFrameSize)
                    textView.frame = CGRect(origin: CGPoint.zero, size: adjustedFrameSize)

                    preferredContentSize = adjustedFrameSize
                }
            }

            override func viewWillLayoutSubviews()
            {
                if let textView = textView,
                   let frame = view.superview?.frame
                {
                    let contentSize = textView.sizeThatFits(CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude))
                    let adjustedFrameSize = CGSize(width: frame.size.width, height: contentSize.height)
                    
                    textView.frame = CGRect(origin: view.frame.origin, size: adjustedFrameSize)
                    preferredContentSize = adjustedFrameSize
                }
            }

            override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
            {
                #if targetEnvironment(macCatalyst)

                    if let textView = textView,
                       let frame = view.superview?.frame
                    {
                        let contentSize = textView.sizeThatFits(CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude))
                        let adjustedFrameSize = CGSize(width: frame.size.width, height: contentSize.height)

                        view.frame = CGRect(origin: view.frame.origin, size: adjustedFrameSize)
                        preferredContentSize = adjustedFrameSize
                    }
                 
                    view.setNeedsLayout()

                    if presentationActor != nil
                    {
                        // inform the avatar to redraw after window resizing has settled.
                        if m_timer != nil
                        {
                            m_extendTimer = true
                        }
                        else
                        {
                            m_timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] timer in

                                guard let strongSelf = self, let presentationActor = strongSelf.presentationActor  else { return }

                                if !strongSelf.m_extendTimer
                                {
                                    timer.invalidate()

                                    strongSelf.m_timer = nil
                                    strongSelf.m_extendTimer = false

                                    presentationActor.notifiers.presentation.request.send(.eAvatarGesture(.ePerformGestureAnimation(duration: 0.7, fromGesture: .eDisassemble, toGesture: .eAssemble)))
                                }
                                else
                                {
                                    strongSelf.m_extendTimer = false
                                }
                            }
                        }
                    }
                
                #endif

                super.viewWillTransition(to: size, with: coordinator)
            }
        }

        private let m_textBinding: Binding<AttributedString>
        private let m_textIndent : CGFloat
        private let m_textHeight : CGFloat
        private let m_presentationActor : PresentationActor?

        init(_ text: Binding<AttributedString>, indentTemplate: AttributedString, presentationActor: PresentationActor?)
        {
            let indentTemplateSize = NSAttributedString(indentTemplate).size()

            self.m_textBinding = text
            self.m_textIndent = indentTemplateSize.width
            self.m_textHeight = indentTemplateSize.height
            self.m_presentationActor = presentationActor
        }

        func makeUIViewController(context: Context) -> UITextViewController
        {
            let layoutManager = NSLayoutManager()
            let textStorage   = NSTextStorage()
            let textContainer = NSTextContainer(size: CGSize.zero)

            textContainer.widthTracksTextView = true
            textContainer.heightTracksTextView = true
            textContainer.maximumNumberOfLines = 0
            textContainer.lineBreakMode = .byWordWrapping

            layoutManager.addTextContainer(textContainer)
            textStorage.addLayoutManager(layoutManager)

            let textView = UITextView(frame: CGRect.zero, textContainer: textContainer)

            textView.isScrollEnabled = false
            textView.autocapitalizationType = .none
            textView.isSelectable = false
            textView.backgroundColor = .clear
            textView.isUserInteractionEnabled = false
            textView.attributedText = NSAttributedString(m_textBinding.wrappedValue)

            let viewController = UITextViewController()

            viewController.textView = textView
            viewController.presentationActor = m_presentationActor
            viewController.view.addSubview(textView)

            return viewController
        }

        func updateUIViewController(_ uiViewController: UITextViewController, context: Context)
        {
            for view in uiViewController.view.subviews
            {
                if let textView = view as? UITextView
                {
                    // Form an exclusion rectangle to give the appearance of a text indent.
                    let frame = textView.frame
                    let exclusionPath = UIBezierPath(rect: CGRect(origin: CGPoint(x: frame.origin.x, y: frame.origin.y + m_textHeight * 1.7), size: CGSize(width: m_textIndent, height: frame.size.height)))

                    textView.attributedText = NSAttributedString(m_textBinding.wrappedValue)
                    textView.textContainer.exclusionPaths = [exclusionPath]
                }
            }
        }
    }
    
    struct MessageComposeView: UIViewControllerRepresentable
    {
        typealias ResultFunc = (MessageComposeResult) -> Void

        var messageModel: Binding<MessageScope.MessageModel>
        let messageViewController : MFMessageComposeViewController
        let resultFunc:   ResultFunc

        class Coordinator: NSObject, MFMessageComposeViewControllerDelegate
        {
            @Environment(\.presentationMode) private var presentationMode
            private var resultFunc: ResultFunc

            init(resultFunc: @escaping ResultFunc)
            {
                self.resultFunc = resultFunc
            }

            func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
            {
                controller.messageComposeDelegate = nil

                resultFunc(result)
            }
        }

        static var canSendMessage: Bool         { MFMessageComposeViewController.canSendText() }

        init(messageModel : Binding< MessageScope.MessageModel>, messageViewController : MFMessageComposeViewController?, resultFunc: @escaping ResultFunc)
        {
            // The first use of MFMessageComposeViewController is pre-created, so as to work around the delay in constructing MFMessageComposeViewController
            let currentMessageViewController = messageViewController ?? MFMessageComposeViewController()

            currentMessageViewController.subject = messageModel.wrappedValue.subject
            currentMessageViewController.body = messageModel.wrappedValue.body
            currentMessageViewController.overrideUserInterfaceStyle = .light

            self.messageModel = messageModel
            self.resultFunc = resultFunc
            self.messageViewController = currentMessageViewController
        }

        func makeCoordinator() -> Coordinator
        {
            Coordinator(resultFunc: resultFunc)
        }

        func makeUIViewController(context: UIViewControllerRepresentableContext<MessageComposeView>) -> MFMessageComposeViewController
        {
            messageViewController
        }

        func updateUIViewController(_ uiViewController: MFMessageComposeViewController, context: UIViewControllerRepresentableContext<MessageComposeView>)
        {
            // Work around to stop the Message UI not coming up the first time called.
            usleep(100_000)

            uiViewController.messageComposeDelegate = context.coordinator
        }
    }
}

extension View {

    func set(windowSize: CGSize)  -> some View {

        if !Styles.Sizes.HaveSetSafeAreaSize
        {
            Styles.Sizes.HaveSetSafeAreaSize = true
            Styles.Sizes.SafeAreaWidth = windowSize.width
            Styles.Sizes.SafeAreaHeight = windowSize.height
        }
        
        return self
    }
     
    func magnifyControlNative(viewPortBounds: CGSize, panZoomModel: PanZoomModel, presentationActor: PresentationActor) -> some View {

        self.modifier(Modifiers.Controllers.MagnifyControlNative(
                viewPortBounds: viewPortBounds,
                renderService: presentationActor.graphRenderPipeline.renderService,
                panZoomModel: panZoomModel)
       )
    }
     
    func printDebug(_ message : String) -> some View {
        self.modifier(Modifiers.Debug.Print(message))
    }

    func perform(_ toPerform : () -> Void) -> some View {
        self.modifier(Modifiers.Debug.Perform(toPerform))
    }
}
