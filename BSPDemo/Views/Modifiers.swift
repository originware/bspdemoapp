//
//  Views/Modifiers.swift
//  BSPDemo
//
//  Created by Terry Stillone on 16/12/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation
import SwiftUI

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The custom Content View, View Modifiers.
///

struct Modifiers
{
    struct Stylers
    {
        struct BodyText: ViewModifier
        {
            func body(content: Content) -> some View {

                content
                        .multilineTextAlignment(.leading)
                        .font(.title3)
                        .foregroundColor(.black)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal, Styles.Sizes.ButtonPaddingUnit / 3)
                        .background(Color(uiColor: UIColor(red: 0.6, green: 0.6, blue: 0.9, alpha: 0.3)))
                        .clipShape(RoundedRectangle(cornerRadius: Styles.Sizes.TextViewCornerRadius, style: .continuous))
                        .overlay(
                                RoundedRectangle(cornerRadius: Styles.Sizes.TextViewCornerRadius, style: .continuous)
                                        .stroke(Color.gray, lineWidth: Styles.Sizes.TextViewIndent * 2 * 0.75)
                                        .offset(x: Styles.Sizes.TextViewIndent * 0.75, y: Styles.Sizes.TextViewIndent * 0.75)
                                        .clipShape(RoundedRectangle(cornerRadius: Styles.Sizes.TextViewCornerRadius, style: .continuous))
                        )
                        .overlay(
                                RoundedRectangle(cornerRadius: Styles.Sizes.TextViewCornerRadius, style: .continuous)
                                        .stroke(Color.white, lineWidth: Styles.Sizes.TextViewIndent * 2 * 0.75)
                                        .offset(x: -Styles.Sizes.TextViewIndent, y: -Styles.Sizes.TextViewIndent)
                                        .clipShape(RoundedRectangle(cornerRadius: Styles.Sizes.TextViewCornerRadius, style: .continuous))
                        )
                        .padding(.leading, Styles.Sizes.ButtonPaddingUnit / 2)
            }
        }

        struct AutomationText: ViewModifier
        {
            func body(content: Content) -> some View {

                content
                        .multilineTextAlignment(.leading)
                        .foregroundColor(.orange)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal, 10)
                        .padding(.vertical, 6)
                        .background(.clear)
                        .overlay(RoundedRectangle(cornerRadius: 7, style: .continuous).stroke(.red, lineWidth: 1))
                        .padding(.leading, 15)
            }
        }
         
        struct LegendText: ViewModifier
        {
            func body(content: Content) -> some View {

                content
                        .font(.callout)
                        .foregroundColor(.red)
                        .padding(.vertical, 7)
                        .padding(.horizontal, 10)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color(uiColor: Styles.Colors.LegendBackground))
                        )
                        .padding(20)
                        .transition(AnyTransition.scale.combined(with: .opacity).animation(.easeInOut(duration: 0.6)))
            }
        }
    }

    struct Controllers
    {
        struct MagnifyControlNative: ViewModifier
        {
            let                 viewPortBounds:  CGSize
            @State var          contentBounds =  CGRect.zero
            @ObservedObject var renderService : GraphRenderPipeline.RenderService
            @ObservedObject var panZoomModel:    PanZoomModel

            // Scaling
            @State private  var m_globalScale       = CGFloat(1.0)

            // Drag
            @State private  var m_globalDragOffset  = CGSize.zero

            // Gesture State
            @GestureState private var m_dragState    = CGSize.zero
            @GestureState private var m_magnifyState = CGFloat(1.0)

            func isConstrained() -> Bool
            {
                contentBounds.size.width < (viewPortBounds.width + 15) && contentBounds.size.height < (viewPortBounds.height + 15)
            }

            func body(content: Content) -> some View {

                let magnificationGesture = MagnificationGesture()

                        .onChanged { scale in

                            panZoomModel.zoom = scale
                        }
                        .updating($m_magnifyState) { (value, state, transaction) in

                            state = value
                        }
                        .onEnded { scale in
                            
                            // Update global scale from local scale change.
                            m_globalScale          *= scale

                            do {  // Update contentBounds.

                                let width  = viewPortBounds.width * m_globalScale
                                let height = viewPortBounds.height * m_globalScale

                                contentBounds.size    = CGSize(width: width, height: height)
                                contentBounds.origin  = CGPoint(x: (viewPortBounds.width - width) / 2, y: (viewPortBounds.height - height) / 2)
                            }

                            let isCurrentlyConstrained = isConstrained()

                            if isCurrentlyConstrained
                            {
                                // Reset global drag offset to center.
                                m_globalDragOffset = CGSize.zero
                            }

                            // Update Pan/Zoom mode.
                            panZoomModel.mode      = isCurrentlyConstrained ? .eZoomOnly : .ePanAndZoom
                            panZoomModel.zoom      = m_globalScale
                        }

                let dragGesture = DragGesture()
                        
                        .updating($m_dragState) { (value, state, transaction) in

                            if !isConstrained()
                            {
                                state = value.translation
                            }
                        }
                        .onEnded { value in

                            if !isConstrained()
                            {
                                // Update global drag offset from local change.
                                m_globalDragOffset += value.translation
                            }
                        }

                    content
                        .onChange(of: renderService.graphIdentifier) { (value) in

                            // When a graph is re-drawn, reset scaling and re-center.
                            m_globalScale       = 1
                            m_globalDragOffset  = CGSize.zero

                            contentBounds       = CGRect(origin: CGPoint.zero, size: viewPortBounds)
                            panZoomModel.zoom   = 1
                            panZoomModel.mode   = .eZoomOnly
                        }
                        .scaleEffect(m_globalScale * m_magnifyState)
                        .offset(x: m_globalDragOffset.width + m_dragState.width, y: m_globalDragOffset.height + m_dragState.height)
                        .gesture(dragGesture.simultaneously(with: magnificationGesture))
            }
        }
    }

    struct Debug
    {
        /// Print a debug message
        struct Print: ViewModifier
        {
            init(_ message : String)
            {
                print(message)
            }

            func body(content: Content) -> some View {
                content
            }
        }

        /// Perform a debug action
        struct Perform: ViewModifier
        {
            init(_ toPerform : () -> Void)
            {
                toPerform()
            }

            func body(content: Content) -> some View {
                content
            }
        }
    }
}
