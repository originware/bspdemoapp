//
//  Main/ContentView.swift
//  BSPDemo
//
//  Created by Terry Stillone on 23/11/21.
//  Copyright © 2022 Originware. All rights reserved.
//

import SwiftUI
import WebKit
import PencilKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The main content view for the App.
///

struct ContentView: View
{
    @EnvironmentObject var presentationActor:       PresentationActor
    @EnvironmentObject var presentationScopeStack:  ScopeStack
    @EnvironmentObject var mainViewModel:           MainViewModel

    @StateObject       var panZoomModel = PanZoomModel()
    @State             var headerSize = CGSize.zero

    @GestureState var listeningButtonLongPressTapGestureState = AvatarBehaviourModel.eAvatarListenState.eIsListening

    var body: some View {
        
        VStack(spacing: 0) {

            // Header Views
            ZStack(alignment: .topLeading){

                // Title Header and Edging
                VStack(spacing: 0) {

                    // Title Header
                    Views.HeaderView(headerSize: $headerSize)
                         .background(Rectangle().foregroundColor(Color(uiColor: Styles.Colors.HeaderBackground)).ignoresSafeArea())

                    // Title Header Edging.
                    Rectangle()
                            .frame(maxHeight: 4)
                            .foregroundColor(Color(uiColor: UIColor(red: 0.4, green: 0.4, blue: 0.7, alpha: 0.5)))
                            .blur(radius: 6)
                }

                // Avatar
                RepresentedUIViews.AvatarView(avatarUIView: presentationActor.avatarUIView)
                                  .frame(width: headerSize.width * 0.1, height: Styles.Sizes.AvatarHeight, alignment: .center)
                                  .padding(.horizontal, 10)
                                  .background(Color.black)
                                  .padding(.top, Styles.Sizes.AvatarTopSpacing)
                                  .background(in: RoundedRectangle(cornerRadius: 20, style: .circular).inset(by: 10))
                                  .shadow(color: .gray, radius: 5, x: 2, y: 2)
                                  .padding(.leading, headerSize.width * 0.048)
                                  .padding(.trailing, 2)
                                  .padding(.top, Styles.Sizes.AvatarTopSpacing * 4)
                                  .padding(.bottom, Styles.Sizes.AvatarBottomSpacing)

            }.overlay(

                     // Set the state headerSize
                     GeometryReader { geometry in

                         Color.clear
                              .onAppear {

                                  headerSize = geometry.size
                              }
                              .onChange(of: Styles.Sizes.WindowWidth) { _ in

                                  headerSize = geometry.size
                              }
                     }
             )

            // Control Views
            VStack {

                // Recognition Text and Listen Button
                HStack(spacing: Styles.Sizes.ButtonPaddingUnit / 2) {

                    // Recognition Text
                    ZStack(alignment: .topLeading) {

                        RepresentedUIControllers.UITextViewControllerRepresentable($mainViewModel.recognitionText, indentTemplate: mainViewModel.indentTemplate, presentationActor: presentationActor)
                                                .fixedSize(horizontal: false, vertical: true)
                                                .modifier(Modifiers.Stylers.BodyText())
                                                .accessibilityLabel("/mainView/textFields/recognitionBody")
                    }

                    // Listen Button
                    Button("Listen ") {

                        presentationActor.notifiers.presentation.event.send(.eOnListenButtonEvent(.ePressUp))
                    }
                            .buttonStyle(Styles.ButtonStyles.Bordered(color: .green))

                            .simultaneousGesture(
                                    LongPressGesture(minimumDuration: 1.0)
                                            .updating($listeningButtonLongPressTapGestureState, body: { (currentState, state, transaction) in

                                                switch currentState {

                                                    case true:

                                                        state = .eIsListening
                                                        presentationActor.notifiers.presentation.event.send(.eOnListenButtonEvent(.ePressDown))

                                                    case false:

                                                        state = .eIsNotListening
                                                }
                                            })
                            )
                            .accessibilityLabel("/mainView/buttons/listen")
                }

                // Response Text and Cancel Button
                HStack(spacing: Styles.Sizes.ButtonPaddingUnit / 2) {

                    // Response Text
                    ZStack(alignment: .topLeading) {

                        RepresentedUIControllers.UITextViewControllerRepresentable($mainViewModel.responseText, indentTemplate: mainViewModel.indentTemplate, presentationActor: nil)
                                                .fixedSize(horizontal: false, vertical: true)
                                                .modifier(Modifiers.Stylers.BodyText())
                                                .accessibilityLabel("/mainView/textFields/responseBody")
                    }

                    // Cancel Button
                    Button("Cancel") {

                        presentationActor.notifiers.presentation.event.send(.eOnCancelButtonPressed)
                    }
                            .buttonStyle(Styles.ButtonStyles.Bordered(color: .orange))
                            .accessibilityLabel("/mainView/buttons/cancel")
                }
            }

            // Stacked Views
            GeometryReader { geometry in

                // Stack Views: Graph (with Pen Canvas), Message and WebKit
                ZStack(alignment: .bottomTrailing) {

                    switch presentationScopeStack.topView
                    {
                        case .eGraph:

                            ZStack {

                                GraphRenderPipeline.ViewRenderService.Views.GraphView(
                                                           graphSize:    presentationActor.graphRenderPipeline.renderService.canvasFrame?.size,
                                                           nodeViews:    presentationActor.graphRenderPipeline.renderService.nodeViews,
                                                           edgeViews:    presentationActor.graphRenderPipeline.renderService.edgeViews
                                                   )
                                                   .padding(presentationActor.graphViewModel.graphLayoutModel?.graphLayout.margin ?? 0)

                                Views.CanvasView(pencilCanvasModel: presentationActor.pencilCanvasModel)
                            }
                                    .magnifyControlNative(viewPortBounds: geometry.size, panZoomModel: panZoomModel, presentationActor: presentationActor)
                                    .clipped()
                                    .padding(20)
                                    .contentShape(Styles.Shapes.GeometryShape(geometry: geometry))
                                    .background(.white)
                                    .padding(10)

                            Text(Styles.Text.formatLegendTitle("Graph View") + AttributedString("\n[Behaviour stack index: \(presentationScopeStack.topIndex)]\n", attributes: Styles.legendBody) + panZoomModel.formatted)
                                    .modifier(Modifiers.Stylers.LegendText())

                        case .eMessageView(let messageView):

                            messageView
                                    .padding(20)
                                    .transition(AnyTransition.scale.combined(with: .opacity).animation(.easeInOut(duration: 0.6)))
                                    .ignoresSafeArea()

                            Text(Styles.Text.formatLegendTitle("Message View") + AttributedString("[Behaviour stack index: \(presentationScopeStack.topIndex)]", attributes: Styles.legendBody))
                                    .modifier(Modifiers.Stylers.LegendText())

                        case .eWebView(let webViewWithDismiss):

                            webViewWithDismiss
                                    .frame(maxHeight: geometry.size.height)
                                    .padding(20)
                                    .transition(AnyTransition.scale.combined(with: .opacity).animation(.easeInOut(duration: 0.6)))

                            Text(Styles.Text.formatLegendTitle("Web URL View") + AttributedString("\n[Behaviour stack index: \(presentationScopeStack.topIndex)]", attributes: Styles.legendBody))
                                    .modifier(Modifiers.Stylers.LegendText())
                    }

                    if let automationViewModel = presentationActor.automationViewModel
                    {
                        Views.AutomationView(presentationActor: presentationActor,
                                             automationViewModel : automationViewModel,
                                             automationRequest: automationViewModel.automationRequest,
                                             automationReply: automationViewModel.automationReply)
                             .zIndex(2)
                    }
                }
            }

                    .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                    .inset(by: 10)
                                    .stroke(Color(uiColor: Styles.Colors.Border), lineWidth: 1.1)
                    )
        }
        .background(Views.SelectiveImage(iOS: "Background Image - iOS", mac: "Background Image - mac"), alignment: .top)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Content Support
///
///

struct Styles
{
    struct Colors
    {
        static let Background       = UIColor(red: 242 / 255, green: 242 / 255, blue: 255 / 255, alpha: 1.0)
        static let HeaderBackground = UIColor(red: 70 / 255, green: 70 / 255, blue: 150 / 255, alpha: 0.2)
        static let Border           = UIColor(red: 100 / 255, green: 0, blue: 190 / 255, alpha: 1.0)
        static let Shadow           = UIColor(red: 200 / 255, green: 0 / 255, blue: 200 / 255, alpha: 0.2)
        static let LegendBackground = UIColor(red: 255.0 / 255.0, green: 178.0 / 255.0, blue: 102.0 / 255, alpha: 0.1)
    }

    struct Graph
    {
        static let BaseColor                = UIColor(red: 0.9, green: 0.1, blue: 0.1, alpha: 1.0)
        static let FramingColor             = UIColor(red: 0.9, green: 0.5, blue: 0.0, alpha: 1.0)
        static let ExpressionColor          = UIColor(red: 0.1, green: 0.6, blue: 0.0, alpha: 1.0)
        static let ContentColor             = UIColor(red: 0.2, green: 0.1, blue: 1.0, alpha: 1.0)
        static let NonMatchedWordTokenColor = UIColor(red: 0.6, green: 0.0, blue: 0.7, alpha: 1.0)
        static let MatchedWordTokenColor    = UIColor(red: 0.9, green: 0.4, blue: 0.1, alpha: 1.0)
        static let MatchedWordStripeColor   = UIColor.red

        static let EdgeColor          = UIColor(red: 0, green: 0, blue: 0.5, alpha: 0.3)

        static let BackgroundColor    = UIColor(red: 254.0 / 255, green: 250.0 / 255, blue: 254.0 / 255, alpha: 1.0)

        static func getNodeFont(forSize: CGFloat) -> UIFont
        {
            UIFont.systemFont(ofSize: forSize)
        }

        static func getNodeBoldFont(forSize: CGFloat) -> UIFont
        {
            UIFont.boldSystemFont(ofSize: forSize)
        }

        static func getNodeItalicFont(forSize: CGFloat) -> UIFont
        {
            UIFont.italicSystemFont(ofSize: forSize)
        }
    }

    struct Shapes
    {
        struct GeometryShape : Shape
        {
            let geometry : GeometryProxy
            
            func path(in rect: CGRect) -> Path
            {
                let rect = geometry.frame(in: .local)
                
                return Path(rect)
            }
        }
    }

    struct ButtonStyles
    {
        struct Bordered: ButtonStyle
        {
            let color : Color

            @State var buttonWidth = Styles.Sizes.ButtonWidth
            @State var buttonPaddingUnit = Styles.Sizes.ButtonWidth

            func makeBody(configuration: Self.Configuration) -> some View {
                
                configuration.label
                
                    .font(.title2)
                    .scaledToFit()
                    .minimumScaleFactor(0.1)
                    .frame(maxWidth: buttonWidth)
                    .padding(.horizontal, Styles.Sizes.ButtonPaddingUnit)
                    .padding(.vertical, buttonPaddingUnit / 10)
                    .background(configuration.isPressed ? .red : color)
                    .foregroundColor(.white)
                    .cornerRadius(Styles.Sizes.ButtonPaddingUnit)
                    .padding(buttonPaddingUnit / 20)
                    .overlay(
                        RoundedRectangle(cornerRadius: Styles.Sizes.ButtonPaddingUnit)
                            .stroke(configuration.isPressed ? .red : color, lineWidth: buttonPaddingUnit / 20)
                    )
                    .padding(.horizontal, buttonPaddingUnit / 4)
                    .padding(.vertical, buttonPaddingUnit / 40)
            }
        }
    }

    struct Text
    {
        static func formatLegendTitle(_ text: String) -> AttributedString
        {
            AttributedString(text, attributes : Styles.legendTitle)
        }
    }
    
    struct Sizes
    {
        static var WindowWidth            = UIScreen.main.bounds.width
        static var WindowHeight           = UIScreen.main.bounds.height
        static var WindowDiagonal         = (UIScreen.main.bounds.height + UIScreen.main.bounds.width) / 2
        static var SafeAreaWidth          = UIScreen.main.bounds.width
        static var SafeAreaHeight         = UIScreen.main.bounds.height

        static var HaveSetSafeAreaSize    = false

        static var AvatarHeight : CGFloat               { SafeAreaHeight * 0.117 }
        static var TitleHeight : CGFloat                { SafeAreaHeight * 0.09 }
        static var TextViewWidth : CGFloat              { SafeAreaWidth * 0.8 }
        static var TextViewCornerRadius : CGFloat       { 15 }
        static var TextViewIndent : CGFloat             { 1 }
        static var ButtonWidth : CGFloat                { SafeAreaWidth * 0.05 }
        static var ButtonPaddingUnit : CGFloat          { SafeAreaWidth * 0.03 }


#if targetEnvironment(macCatalyst)
        static var AvatarTopSpacing : CGFloat           { WindowHeight * 0.0022 }
        static var AvatarBottomSpacing : CGFloat        { WindowHeight * 0.0009 }
#else
        static var AvatarTopSpacing : CGFloat           { WindowHeight * 0.0009 }
        static var AvatarBottomSpacing : CGFloat        { WindowHeight * 0.009 }
#endif
    }

#if targetEnvironment(macCatalyst)

    static let FontUnitSize                 = Sizes.WindowDiagonal * 0.0010

    static let AgentTitleFont               = UIFont.boldSystemFont(ofSize:   FontUnitSize * 24)
    static let AgentTitleColor              = UIColor(red: 128.0 / 255.0, green: 0.0, blue: 128.0 / 255, alpha: 1.0)


    static let TextViewBodyHighlightFont    = UIFont.systemFont(ofSize:       FontUnitSize * 18)
    static let TextViewBodyRegularFont      = UIFont.systemFont(ofSize:       FontUnitSize * 18)
    static let TextViewAnnotationFont       = UIFont.systemFont(ofSize:       FontUnitSize * 16)
    static let TextViewAnnotationColor      = UIColor(red: 0.4, green: 0.4, blue: 0.6, alpha: 1.0)

    static let LegendTitleFont              = UIFont.boldSystemFont(ofSize:   FontUnitSize * 16)
    static let LegendSubTitleFont           = UIFont.boldSystemFont(ofSize:   FontUnitSize * 15)
    static let LegendBodyFont               = UIFont.boldSystemFont(ofSize:   FontUnitSize * 14)
    static let LegendItalicFont             = UIFont.italicSystemFont(ofSize: FontUnitSize * 14)
    static let LegendColor                  = UIColor(red: 0.5, green: 0.0, blue: 0.8, alpha: 1.0)
    static let LegendAlertColor             = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)

#else
    
    static let FontUnitSize                 = Sizes.WindowDiagonal * 0.0011

    static let AgentTitleFont               = UIFont.boldSystemFont(ofSize:   FontUnitSize * 20)
    static let AgentTitleColor              = UIColor(red: 128.0 / 255.0, green: 0.0, blue: 128.0 / 255, alpha: 1.0)

    static let TextViewBodyHighlightFont    = UIFont.boldSystemFont(ofSize:   FontUnitSize * 18)
    static let TextViewBodyRegularFont      = UIFont.boldSystemFont(ofSize:   FontUnitSize * 18)
    static let TextViewAnnotationFont       = UIFont.systemFont(ofSize:       FontUnitSize * 15)
    static let TextViewAnnotationColor      = UIColor(red: 0.4, green: 0.4, blue: 0.6, alpha: 1.0)
    
    static let LegendTitleFont              = UIFont.boldSystemFont(ofSize:   FontUnitSize * 16)
    static let LegendSubTitleFont           = UIFont.boldSystemFont(ofSize:   FontUnitSize * 15)
    static let LegendBodyFont               = UIFont.boldSystemFont(ofSize:   FontUnitSize * 14)
    static let LegendItalicFont             = UIFont.italicSystemFont(ofSize: FontUnitSize * 14)
    static let LegendColor                  = UIColor(red: 0.5, green: 0.0, blue: 0.8, alpha: 1.0)
    static let LegendAlertColor             = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)

#endif

    static let annotationTextStyle    = AttributeContainer.font(Styles.TextViewAnnotationFont).foregroundColor(Styles.TextViewAnnotationColor)
    static let alertTextStyle         = AttributeContainer.font(Styles.TextViewAnnotationFont).foregroundColor(.red)
    static let bodyTextStyle          = AttributeContainer.font(Styles.TextViewBodyRegularFont)
    static let highlightBodyTextStyle = AttributeContainer.font(Styles.TextViewBodyHighlightFont).foregroundColor(UIColor.purple).underlineColor(UIColor.purple).underlineStyle(.double)

    static let legendTitle            = AttributeContainer.font(Styles.LegendTitleFont).foregroundColor(Styles.LegendAlertColor)
    static let legendSubTitle         = AttributeContainer.font(Styles.LegendSubTitleFont).foregroundColor(Styles.LegendColor)
    static let legendBody             = AttributeContainer.font(Styles.LegendSubTitleFont).foregroundColor(Styles.LegendColor)
    static let zoomPanBody            = AttributeContainer.font(Styles.LegendItalicFont).foregroundColor(Styles.LegendColor)
    static let zoomPanAlert           = AttributeContainer.font(Styles.LegendItalicFont).foregroundColor(Styles.LegendAlertColor)
}

class PanZoomModel : ObservableObject
{
    enum eMode
    {
        case eZoomOnly
        case ePanAndZoom
    }

    private static let ModeString = AttributedString("Mode: ", attributes: Styles.legendSubTitle)
    private static let ZoomString = AttributedString("Zoom: ", attributes: Styles.legendSubTitle)
    private static let ZoomOnlyString = AttributedString("Zoom Only\n", attributes: Styles.zoomPanAlert)
    private static let ZoomAndPanString = AttributedString("Zoom And Pan\n", attributes: Styles.zoomPanBody)

    private static let ZoomOnlyTemplate   = ModeString + ZoomOnlyString + ZoomString
    private static let ZoomAndPanTemplate = ModeString + ZoomAndPanString + ZoomString

    @Published var zoom: CGFloat = CGFloat(1)
    @Published var mode: eMode   = eMode.eZoomOnly

    var formatted : AttributedString
    {
        switch mode
        {
            case .eZoomOnly:

                return PanZoomModel.ZoomOnlyTemplate + AttributedString(String(format: "x %0.2f", zoom), attributes: Styles.zoomPanBody)

            case .ePanAndZoom:

                return PanZoomModel.ZoomAndPanTemplate + AttributedString(String(format: "x %0.2f", zoom), attributes: Styles.zoomPanBody)
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Previews
///

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        Group {
            
            ContentView()
                //.environmentObject(presentationActor)
                .previewDevice(PreviewDevice(rawValue: "iPad Pro (9.7-inch)"))
                .previewInterfaceOrientation(.landscapeLeft)
        }
    }
}
