//
//  Views/AvatarUIView.swift
//  BSPDemo
//
//  Created by Terry Stillone on 30/11/21.
//  Copyright (c) 2021 Originware. All rights reserved.
//

import Foundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// This Avatar Animation code was ported from the Originware Dronenaut App code.
///

public protocol IAnimationModelItem: AnyObject, CustomStringConvertible
{
    typealias AnimationNotify = (eAnimationReply) -> Void
    typealias AnimationHandler = (AnimationNotify?) -> Void

    typealias AnimationCancelHandler = (AnimationNotify?) -> Void
    typealias AnimationPauseResumeHandler = (Bool, AnimationNotify?) -> Void

    var name :                  String                      { get }
    var op:                     eAvatarGestureAnimationOp   { get }
    var animationSequenceID:    Int                         { get set }
    var animationNotifyHandler: AnimationNotify?            { get set }

    func run()
    func cancel()
    func pauseResume(doPause: Bool)
}

public enum eAvatarGesture: Equatable
{
    case eAssemble
    case eDisassemble
    case eWait
    case eHandWave(String)
    case eListen(eListenEndType)
    case eSpeak(String)
    case eAlert(String)
    case eSalute(String)
    case ePause(TimeInterval, Int?, Int?)
    case eReposition(ePosition)
    case ePerformAction(() -> Void, TimeInterval?, Int?, Int?)
    case ePerformOnGroupCancel(() -> Void, Int?, Int?)

    public enum eListenEndType
    {
        case eEndBySilenceDetection
        case eEndByTouchContactEnd
        case eSimulated
    }

    public enum ePosition: Equatable
    {
        case eNormal
        case eOperationsDisplay

        var description : String
        {
            switch self
            {
                case .eNormal:              return "normal"
                case .eOperationsDisplay:   return "operations display"
            }
        }
    }

    public static func ==(lhs : eAvatarGesture, rhs : eAvatarGesture) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eAssemble, .eAssemble):                                          return true
            case (.eDisassemble, .eDisassemble):                                    return true
            case (.eWait, .eWait):                                                  return true
            case (.eHandWave(let lhsText), .eHandWave(let rhsText)):                return lhsText == rhsText
            case (.eListen, .eListen):                                              return true
            case (.eSpeak(let lhsText), .eSpeak(let rhsText)):                      return lhsText == rhsText
            case (.eAlert(let lhsText), .eAlert(let rhsText)):                      return lhsText == rhsText
            case (.eSalute(let lhsText), .eSalute(let rhsText)):                    return lhsText == rhsText
            case (.eReposition(let lhsPosition), .eReposition(let rhsPosition)):    return lhsPosition == rhsPosition

            case (.ePause(let lhsDuration, _, let lhsGroupID), .ePause(let rhsDuration, _, let rhsGroupID)):
                return (lhsDuration == rhsDuration) && (lhsGroupID == rhsGroupID)

            default: return false
        }
    }

    public static func ~=(lhs : eAvatarGesture, rhs : eAvatarGesture) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eAssemble, .eAssemble):          return true
            case (.eDisassemble, .eDisassemble):    return true
            case (.eWait, .eWait):                  return true
            case (.eHandWave, .eHandWave):          return true
            case (.eListen, .eListen):              return true
            case (.eSpeak, .eSpeak):                return true
            case (.eAlert, .eAlert):                return true
            case (.eSalute, .eSalute):              return true
            case (.eReposition, .eReposition):      return true
            case (.ePause, .ePause):                return true
            default: return false
        }
    }
}

public final class AvatarUIView : UIView
{
    final class Layers : NSObject
    {
        var capLayer :         ShapeLayerWithAnimation! = nil
        var headLayer :        ShapeLayerWithAnimation! = nil
        var bodyLayer :        ShapeLayerWithAnimation! = nil

        public var capFillColor = AvatarUIView.Styles.capFillColorViolet()

        var bounds = CGRect.zero

        fileprivate func createLayers(parentLayer : CALayer, isCompactViewLandscape: Bool, colorMode: eViewColorCast)
        {
            capLayer = createShapeLayer(parentLayer.bounds)
            headLayer = createShapeLayer(parentLayer.bounds)
            bodyLayer = createShapeLayer(parentLayer.bounds)

            capLayer.lineWidth = 2
            capLayer.strokeColor = Styles.capStrokeColor(colorMode)
            capLayer.fillColor = capFillColor
            capLayer.shadowColor = Styles.capShadowColor(colorMode)
            capLayer.shadowOffset = CGSize(width: 1, height: 1.5)
            capLayer.shadowOpacity = 0.6
            capLayer.shadowRadius = 2

            headLayer.lineWidth = isCompactViewLandscape ? 1 : 3
            headLayer.strokeColor = Styles.strokeColor(colorMode)
            headLayer.fillColor = Styles.fillColor(colorMode)
            headLayer.shadowColor = Styles.shadowColor(colorMode)

            headLayer.shadowOffset = CGSize(width: 2.5, height: 4.5)
            headLayer.shadowOpacity = 0.7
            headLayer.shadowRadius = 4

            bodyLayer.lineWidth = isCompactViewLandscape ? 1 : 3
            bodyLayer.strokeColor = Styles.strokeColor(colorMode)
            bodyLayer.fillColor = Styles.fillColor(colorMode)

            parentLayer.addSublayer(headLayer)
            parentLayer.addSublayer(capLayer)
            parentLayer.addSublayer(bodyLayer)

            bounds = parentLayer.bounds
        }

        fileprivate func destroyLayers()
        {
            headLayer?.removeFromSuperlayer()
            capLayer?.removeFromSuperlayer()
            bodyLayer?.removeFromSuperlayer()

            headLayer = nil
            capLayer = nil
            bodyLayer = nil
        }

        internal func updateLayers(colorMode: eViewColorCast = .eSaturated)
        {
            capLayer.strokeColor = Styles.capStrokeColor(colorMode)
            capLayer.fillColor = capFillColor

            headLayer.strokeColor = Styles.strokeColor(colorMode)
            headLayer.fillColor = Styles.fillColor(colorMode)

            bodyLayer.strokeColor = Styles.strokeColor(colorMode)
            bodyLayer.fillColor = Styles.fillColor(colorMode)
        }
        
        private func createShapeLayer(_ bounds : CGRect) -> ShapeLayerWithAnimation
        {
            let layer = ShapeLayerWithAnimation()

            layer.opacity = 1.0
            layer.backgroundColor = UIColor.clear.cgColor
            layer.frame = bounds

            return layer
        }
    }

    final class Animations
    {
        enum eLayer
        {
            case eCapLayer
            case eHeadLayer
            case eBodyLayer

            static func targetLayers(_ forGesture: eAvatarGestureAnimationOp) -> [eLayer]
            {
                switch forGesture
                {
                    case .eDisassemble:             return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eAssemble:                return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eWait:                    return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eWaveLeft:                return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eWaveRight:               return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eSpeak:                   return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eAlert:                   return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eSalute:                  return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eListen:                  return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .ePause:                   return [.eCapLayer, .eHeadLayer, .eBodyLayer]
                    case .eReposition:              return []
                    case .eNop:                     return []
                }
            }
        }

        static let MorphAnimationKey =      "MorphKey"
        static let AlphaAnimationKey =      "AlphahKey"

        fileprivate let capLayer :         ShapeLayerWithAnimation
        fileprivate let headLayer :        ShapeLayerWithAnimation
        fileprivate let bodyLayer :        ShapeLayerWithAnimation

        fileprivate let avatarBounds :     CGRect
        fileprivate var m_isCompactDevice: Bool
        fileprivate var m_isCompactView: Bool
        fileprivate var m_avatarOffset = CGPoint()
        private let m_frame : CGRect

        fileprivate init(frame: CGRect, isCompactDevice: Bool, isCompactViewPortrait: Bool, capLayer : ShapeLayerWithAnimation, headLayer : ShapeLayerWithAnimation, bodyLayer : ShapeLayerWithAnimation)
        {
            self.m_frame = frame
            self.capLayer = capLayer
            self.headLayer = headLayer
            self.bodyLayer = bodyLayer
            self.avatarBounds = Outlines.avatarBounds()
            self.m_isCompactDevice = isCompactDevice
            self.m_isCompactView = isCompactViewPortrait

            m_avatarOffset = getAvatarOffset(forPosition: .eNormal, frame: m_frame)

            capLayer.currentPath = Outlines.transformUIBezierPath(avatarBounds, capLayer.bounds, m_avatarOffset, Outlines.capPath(.eDisassemble)!)
            headLayer.currentPath = Outlines.transformUIBezierPath(avatarBounds, headLayer.bounds, m_avatarOffset, Outlines.headPath(.eDisassemble)!)
            bodyLayer.currentPath = Outlines.transformUIBezierPath(avatarBounds, bodyLayer.bounds, m_avatarOffset, Outlines.bodyPath(.eDisassemble)!)
        }

        func createGestureAnimation(_ duration: TimeInterval, _ fromGesture: eAvatarGestureAnimationOp, _ toGesture: eAvatarGestureAnimationOp) -> IAnimationModelItem
        {
            let animation = createGestureOpAnimation(duration, fromGesture, toGesture)

            return animation
        }

        func createGestureOpAnimation(_ duration: TimeInterval, _ fromAnimationOp: eAvatarGestureAnimationOp, _ toAnimationOp: eAvatarGestureAnimationOp) -> IAnimationModelItem
        {
            func createGestureAnimation() -> IAnimationModelItem
            {
                let targetLayers = Set(Animations.eLayer.targetLayers(fromAnimationOp) + Animations.eLayer.targetLayers(toAnimationOp))
                let avatarBounds = self.avatarBounds
                let description = "\(fromAnimationOp.description) -> \(toAnimationOp.description)"

                let animation = GestureAnimation(description, op: toAnimationOp)
                let handler : GestureAnimation.AnimationHandler = { [weak animation] (reply) in

                    DispatchQueue.main.async(execute: { [weak self, weak animation] in

                        guard let strongSelf = self, let strongAnimation = animation else { return }

                        func transformPath(_ bounds: CGRect, _ path: UIBezierPath?) -> UIBezierPath?
                        {
                            guard let path = path else
                            {
                                return nil
                            }

                            return Outlines.transformUIBezierPath(avatarBounds, bounds, strongSelf.m_avatarOffset, path)
                        }

                        var animationLayerCount = 0

                        // Check if the animation uses the Head Layer.
                        if targetLayers.contains(.eHeadLayer)
                        {
                            let headLayer = strongSelf.headLayer

                            if let toHeadPath = transformPath(headLayer.bounds, Outlines.headPath(toAnimationOp))
                            {
                                let delegate = strongAnimation
                                let _        = headLayer.createMorphAnimation(duration, delegate, toHeadPath)

                                animationLayerCount += 1
                            }
                        }

                        // Check if the animation uses the Body Layer.
                        if targetLayers.contains(.eBodyLayer)
                        {
                            let bodyLayer = strongSelf.bodyLayer

                            if let toBodyPath = transformPath(bodyLayer.bounds, Outlines.bodyPath(toAnimationOp))
                            {
                                let delegate = (animationLayerCount == 0) ? strongAnimation : nil
                                let _        = bodyLayer.createMorphAnimation(duration, delegate, toBodyPath)

                                animationLayerCount += 1
                            }
                        }

                        // Check if the animation uses the Cap Layer.
                        if targetLayers.contains(.eCapLayer)
                        {
                            let capLayer = strongSelf.capLayer

                            if let toCapPath = transformPath(capLayer.bounds, Outlines.capPath(toAnimationOp))
                            {
                                let delegate = (animationLayerCount == 0) ? strongAnimation : nil
                                let _        = capLayer.createMorphAnimation(duration, delegate, toCapPath)

                                animationLayerCount += 1
                            }
                        }

                        if case .eReposition(let logicalPosition) = toAnimationOp
                        {
                            strongSelf.m_avatarOffset = strongSelf.getAvatarOffset(forPosition: logicalPosition, frame: strongSelf.m_frame)
                        }

                        // Check if no layers were animated.
                        if animationLayerCount == 0
                        {
                            func waitAnimation()
                            {
                                let layer = strongSelf.headLayer
                                let opacity = layer.opacity

                                let _ = layer.createAlphaAnimation(duration, strongAnimation, opacity, opacity)
                            }

                            // Nop and move gestures may not generate a direct layer change, so insert an dummy animation that dos nothing.
                            waitAnimation()
                        }
                    })
                }

                animation.runHandler = handler
                animation.cancelHandler = { [weak self] in

                    guard let strongSelf = self else { return }

                    strongSelf.capLayer.removeAllAnimations()
                    strongSelf.headLayer.removeAllAnimations()
                    strongSelf.bodyLayer.removeAllAnimations()
                }

                animation.pauseResumeHandler = { [weak self] (doPause) in

                    guard doPause, let strongSelf = self else { return }

                    strongSelf.capLayer.removeAllAnimations()
                    strongSelf.headLayer.removeAllAnimations()
                    strongSelf.bodyLayer.removeAllAnimations()
                }

                return animation
            }

            return createGestureAnimation()
        }

        func getAvatarOffset(forPosition position: eAvatarGesture.ePosition, frame: CGRect) -> CGPoint
        {
            switch position
            {
                case .eNormal:

                    if !m_isCompactDevice
                    {
                        return CGPoint(x: 3, y: 11)
                    }
                    else if !m_isCompactView
                    {
                        return CGPoint(x: 0, y: 10)
                    }
                    else
                    {
                        return CGPoint(x: 0, y: 30)
                    }

                case .eOperationsDisplay:

                    return m_isCompactDevice ? CGPoint(x: 0, y: 17) : CGPoint(x: -75, y: 40)
            }
        }
    }

    internal struct Styles
    {
        static func shadow() -> NSShadow
        {
            let shadow = NSShadow()

            shadow.shadowColor = UIColor.black.withAlphaComponent(0.19)
            shadow.shadowOffset = CGSize(width: 2, height: 2)
            shadow.shadowBlurRadius = 6

            return shadow
        }

        static func strokeColor(_ colorMode : eViewColorCast) -> CGColor
        {
            UIColor(red: 0.61, green: 0.80, blue: 1.0, alpha: 0.8).transform(colorMode).cgColor
        }

        static func fillColor(_ colorMode : eViewColorCast) -> CGColor
        {
            UIColor(red: 0.85, green: 0.85, blue: 0.95, alpha: 1.0).transform(colorMode).cgColor
        }

        static func shadowColor(_ colorMode : eViewColorCast) -> CGColor
        {
            UIColor(red: 0.2, green: 0.0, blue: 0.3, alpha: 1.0).transform(colorMode).cgColor
        }

        static func capStrokeColor(_ colorMode : eViewColorCast) -> CGColor
        {
            UIColor.black.cgColor
        }

        static func capFillColorViolet(_ colorMode : eViewColorCast = .eSaturated) -> CGColor
        {
            UIColor(red: 180.0 / 255.0, green: 30.0 / 255.0, blue: 200.0 / 255.0 , alpha: 1.0).transform(colorMode).cgColor
        }

        static func capFillColorBlue(_ colorMode : eViewColorCast = .eSaturated) -> CGColor
        {
            UIColor(red: 50.0 / 255.0, green: 95.0 / 255.0, blue: 130.0 / 255.0 , alpha: 1.0).transform(colorMode).cgColor
        }

        static func capFillColorGreen(_ colorMode : eViewColorCast = .eSaturated) -> CGColor
        {
            UIColor(red: 90.0 / 255.0, green: 200.0 / 255.0, blue: 120.0 / 255.0 , alpha: 1.0).transform(colorMode).cgColor
        }

        static func capShadowColor(_ colorMode : eViewColorCast) -> CGColor
        {
            UIColor(red: 0.0, green: 0.0, blue: 0.3 , alpha: 1.0).transform(colorMode).cgColor
        }
    }

    /// The layer animations management.
    var animations: Animations! = nil

    /// Layer management.
    var layers                  = Layers()

    /// Color Mode of the view.
    private var m_colorCast = eViewColorCast.eSaturated

    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    public required override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    public override func willMove(toSuperview newSuperview: UIView?)
    {
        if newSuperview != nil
        {
            setNeedsDisplay()
        }
        else
        {
            layers.destroyLayers()
        }
    }

    public override func layoutSubviews()
    {
        super.layoutSubviews()

        if let superview = superview,
           frame != superview.bounds
        {
            frame = superview.bounds
        }
        
        if layers.bounds != layer.bounds
        {

            // Create Layers and Animations.
            layers.destroyLayers()
            layers.createLayers(parentLayer: layer, isCompactViewLandscape: self.isCompactViewLandscapeOrExternalScreen, colorMode: .eSaturated)
            animations = Animations(frame: frame, isCompactDevice : self.isCompactDevice, isCompactViewPortrait: self.isCompactViewPortrait, capLayer: layers.capLayer, headLayer: layers.headLayer, bodyLayer: layers.bodyLayer)
        }
    }

    fileprivate struct Outlines
    {
        static func avatarBounds() -> CGRect
        {
            let gestures: [eAvatarGestureAnimationOp] = [.eDisassemble, .eWait, .eListen, .eSpeak ]
            var bounds = CGRect.zero

            for gesture in gestures
            {
                for path in [capPath(gesture), headPath(gesture), bodyPath(gesture)]
                {
                    guard let pathBounds = path?.bounds else { continue }

                    bounds = bounds.union(pathBounds)
                }
            }

            return bounds
        }

        static func transformUIBezierPath(_ avatarBounds: CGRect, _ bounds: CGRect, _ avatarPosition : CGPoint, _ path : UIBezierPath) -> UIBezierPath
        {
            let margin = CGFloat(0.0)
            var scaleX = CGFloat(1.0)
            var scaleY = CGFloat(1.0)

            if bounds.width < (avatarBounds.width + margin * 2)
            {
                scaleX = bounds.width / (avatarBounds.width + margin * 2)
            }

            if bounds.height < (avatarBounds.height + margin * 2)
            {
                scaleY = bounds.height / (avatarBounds.height + margin * 2)
            }

            if (scaleX != 1.0) || (scaleY != 1.0)
            {
                let minScale = min(scaleX, scaleY)

                scaleX = minScale * 1.1
                scaleY = minScale * 1.2
            }

            let margin2 = margin * 2
            let xOffset = margin2 + (bounds.width - (margin2 + avatarBounds.width) * scaleX) / 2.0
            let yOffset = margin + (bounds.height - (margin2 + avatarBounds.height) * scaleY) / 2.0

            if scaleX != 1.0
            {
                path.apply(CGAffineTransform(scaleX: scaleX, y: scaleY))
            }

            path.apply(CGAffineTransform(translationX: xOffset + avatarPosition.x, y: yOffset + avatarPosition.y))

            return path
        }

        static func capPath(_ gestureOp: eAvatarGestureAnimationOp) -> UIBezierPath?
        {
            let capShape = UIBezierPath()

            switch gestureOp
            {
                case .eAssemble:

                    return capPath(.eWait)

                case .eDisassemble:

                    capShape.move(to: CGPoint(x: -80.26104324433153, y: -414.0478808946574))
                    capShape.addQuadCurve(to: CGPoint(x: -64.22022022810691, y: -416.37405276964626), controlPoint: CGPoint(x: -57.90449548652857, y: -416.97756839462625))
                    capShape.addQuadCurve(to: CGPoint(x: -30.216532103706292, y: -416.02053714478564), controlPoint: CGPoint(x: -70.53594496968525, y: -415.7705371446662))
                    capShape.addQuadCurve(to: CGPoint(x: 23.131779983345695, y: -415.71584964483867), controlPoint: CGPoint(x: 10.10288076227266, y: -416.27053714490506))
                    capShape.addQuadCurve(to: CGPoint(x: 43.92026432201487, y: -415.3349902696797), controlPoint: CGPoint(x: 36.16067920441873, y: -415.1611621447723))
                    capShape.addQuadCurve(to: CGPoint(x: 71.1331510650411, y: -415.6806933947168), controlPoint: CGPoint(x: 51.67984943961101, y: -415.50881839458714))
                    capShape.addQuadCurve(to: CGPoint(x: 38.17897865482749, y: -415.5752246447788), controlPoint: CGPoint(x: 90.5864526904712, y: -415.85256839484646))
                    capShape.addQuadCurve(to: CGPoint(x: -58.423043191475365, y: -413.2080371446999), controlPoint: CGPoint(x: -14.22849538081623, y: -415.2978808947112))
                    capShape.addQuadCurve(to: CGPoint(x: -80.26104324433153, y: -414.0478808946574), controlPoint: CGPoint(x: -102.6175910021345, y: -411.11819339468855))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 146.37053318145112, y:41.55622147854682))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()
                    brimShape.move(to: CGPoint(x: -42.078255514143194, y: -421.62000692755805))
                    brimShape.addQuadCurve(to: CGPoint(x: -6.4176416647786, y: -422.92469442762206), controlPoint: CGPoint(x: -17.863168717091785, y: -420.8289913025317))
                    brimShape.addQuadCurve(to: CGPoint(x: -16.643097110142335, y: -424.86805380262115), controlPoint: CGPoint(x: 5.027885387534585, y: -425.0203975527124))
                    brimShape.addQuadCurve(to: CGPoint(x: -52.30371095950693, y: -423.56336630255714), controlPoint: CGPoint(x: -38.31407960781925, y: -424.71571005252986))
                    brimShape.addQuadCurve(to: CGPoint(x: -42.078255514143194, y: -421.62000692755805), controlPoint: CGPoint(x: -66.2933423111946, y: -422.4110225525844))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 115.40984792603408, y:37.0))
                    brimShape.apply(CGAffineTransform(scaleX: 0.9957151228525574, y: 1.0))

                    capShape.append(brimShape)

                case .eWait, .ePause:

                    capShape.move(to: CGPoint(x: -101.92813835207542, y: 12.227329944108888))
                    capShape.addQuadCurve(to: CGPoint(x: -78.25880382288022, y: -5.910220047111906), controlPoint: CGPoint(x: -60.83468950872964, y: 0.04153141318553821))
                    capShape.addQuadCurve(to: CGPoint(x: -55.37821042374114, y: -23.539025514119604), controlPoint: CGPoint(x: -95.68291813703081, y: -11.86197150740935))
                    capShape.addQuadCurve(to: CGPoint(x: 33.708152905387905, y: -24.494197588729286), controlPoint: CGPoint(x: -15.073502710451471, y: -35.21607952082986))
                    capShape.addQuadCurve(to: CGPoint(x: 68.52634879482245, y: -7.855674564502467), controlPoint: CGPoint(x: 82.48980852122727, y: -13.77231565662871))
                    capShape.addQuadCurve(to: CGPoint(x: 96.0656655177585, y: 7.931934736880635), controlPoint: CGPoint(x: 54.56288906841763, y: -1.9390334723762237))
                    capShape.addQuadCurve(to: CGPoint(x: 67.83939391273111, y: 29.243916295431344), controlPoint: CGPoint(x: 137.56844196709937, y: 17.802902946137493))
                    capShape.addQuadCurve(to: CGPoint(x: -72.45562066852918, y: 32.54902905987872), controlPoint: CGPoint(x: -1.8896541416371662, y: 40.684929644725194))
                    capShape.addQuadCurve(to: CGPoint(x: -101.92813835207542, y: 12.227329944108888), controlPoint: CGPoint(x: -143.0215871954212, y: 24.413128475032238))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 146.37053318145112, y:41.55622147854682))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()
                    brimShape.move(to: CGPoint(x: -56.57396158380502, y: -27.00455892948611))
                    brimShape.addQuadCurve(to: CGPoint(x: 7.384663580029299, y: -25.323894866985956), controlPoint: CGPoint(x: -22.943531196183205, y: -42.750652679502075))
                    brimShape.addQuadCurve(to: CGPoint(x: 8.002545503163459, y: -11.786785491928534), controlPoint: CGPoint(x: 37.7128583562418, y: -7.897137054469836))
                    brimShape.addQuadCurve(to: CGPoint(x: -55.956079660670866, y: -13.467449554428685), controlPoint: CGPoint(x: -21.707767349914885, y: -15.67643392938723))
                    brimShape.addQuadCurve(to: CGPoint(x: -56.57396158380502, y: -27.00455892948611), controlPoint: CGPoint(x: -90.20439197142684, y: -11.25846517947014))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 115.40984792603408, y:37.0))
                    brimShape.apply(CGAffineTransform(scaleX: 0.9957151228525574, y: 1.0))

                    capShape.append(brimShape)

                case .eWaveLeft:

                    capShape.move(to: CGPoint(x: -96.92493245288871, y: 7.963771385871967))
                    capShape.addQuadCurve(to: CGPoint(x: -77.63303219693695, y: -5.327950291490744), controlPoint: CGPoint(x: -58.42954923469629, y: -1.3638868021397235))
                    capShape.addQuadCurve(to: CGPoint(x: -49.498528799234926, y: -20.68734607350663), controlPoint: CGPoint(x: -96.83651515917762, y: -9.292013780841765))
                    capShape.addQuadCurve(to: CGPoint(x: 44.47383263455016, y: -23.508937456287818), controlPoint: CGPoint(x: -2.1605424392922274, y: -32.0826783661715))
                    capShape.addQuadCurve(to: CGPoint(x: 70.63348381529613, y: -10.48436239540087), controlPoint: CGPoint(x: 91.10820770839256, y: -14.935196546404134))
                    capShape.addQuadCurve(to: CGPoint(x: 94.56325515450065, y: -0.48457847503016005), controlPoint: CGPoint(x: 50.15875992219969, y: -6.033528244397607))
                    capShape.addQuadCurve(to: CGPoint(x: 73.05663371851753, y: 19.33550701677886), controlPoint: CGPoint(x: 138.9677503868016, y: 5.0643712943372865))
                    capShape.addQuadCurve(to: CGPoint(x: -64.13739931042385, y: 25.449036156552047), controlPoint: CGPoint(x: 7.1455170502334635, y: 33.60664273922043))
                    capShape.addQuadCurve(to: CGPoint(x: -96.92493245288871, y: 7.963771385871967), controlPoint: CGPoint(x: -135.42031567108114, y: 17.291429573883658))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 154.39053368323854, y:21.40387481367415))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -54.81812909002541, y: -25.31561390783252))
                    brimShape.addQuadCurve(to: CGPoint(x: 15.417222472450634, y: -28.514725846307698), controlPoint: CGPoint(x: -21.583754090038266, y: -42.91912953283084))
                    brimShape.addQuadCurve(to: CGPoint(x: 18.07542559745402, y: -14.480617684382644), controlPoint: CGPoint(x: 52.41819903493953, y: -14.110322159784563))
                    brimShape.addQuadCurve(to: CGPoint(x: -52.15992596502202, y: -11.281505745907467), controlPoint: CGPoint(x: -16.267347840031494, y: -14.850913208980726))
                    brimShape.addQuadCurve(to: CGPoint(x: -54.81812909002541, y: -25.31561390783252), controlPoint: CGPoint(x: -88.05250409001255, y: -7.712098282834209))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 114.07031250000972, y:13.832031250007176))

                    capShape.append(brimShape)

                case .eWaveRight:

                    capShape.move(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893))
                    capShape.addQuadCurve(to: CGPoint(x: -86.11686452435777, y: -9.470103810081078), controlPoint: CGPoint(x: -62.29360456498624, y: -5.312378523385172))
                    capShape.addQuadCurve(to: CGPoint(x: -48.07168384042224, y: -22.815312699181984), controlPoint: CGPoint(x: -109.94012448372929, y: -13.627829096776985))
                    capShape.addQuadCurve(to: CGPoint(x: 51.195783395306265, y: -18.781259015135234), controlPoint: CGPoint(x: 13.796756802884813, y: -32.00279630158698))
                    capShape.addQuadCurve(to: CGPoint(x: 71.57884952807268, y: -3.7493776005298534), controlPoint: CGPoint(x: 88.59480998772771, y: -5.559721728683483))
                    capShape.addQuadCurve(to: CGPoint(x: 93.09157778555041, y: 11.358042176462936), controlPoint: CGPoint(x: 54.56288906841763, y: -1.9390334723762237))
                    capShape.addQuadCurve(to: CGPoint(x: 56.449585353070745, y: 30.11045111433173), controlPoint: CGPoint(x: 131.6202665026832, y: 24.655117825302096))
                    capShape.addQuadCurve(to: CGPoint(x: -86.42002875370818, y: 24.39443120123356), controlPoint: CGPoint(x: -18.7210957965417, y: 35.56578440336136))
                    capShape.addQuadCurve(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893), controlPoint: CGPoint(x: -154.11896171087466, y: 13.223077999105751))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 154.39053368323854, y:21.40387481367415))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -55.38844159003172, y: -28.487275284800802))
                    brimShape.addQuadCurve(to: CGPoint(x: 14.992417784961207, y: -22.86129872230211), controlPoint: CGPoint(x: -18.27125409004132, y: -40.78805653481537))
                    brimShape.addQuadCurve(to: CGPoint(x: 13.631089659961589, y: -9.36129872229047), controlPoint: CGPoint(x: 48.256089659963735, y: -4.934540909788852))
                    brimShape.addQuadCurve(to: CGPoint(x: -56.749769715031334, y: -14.98727528478916), controlPoint: CGPoint(x: -20.993910340040557, y: -13.788056534792087))
                    brimShape.addQuadCurve(to: CGPoint(x: -55.38844159003172, y: -28.487275284800802), controlPoint: CGPoint(x: -92.50562909002211, y: -16.186494034786232))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 114.07031250000972, y:13.832031250007176))

                    capShape.append(brimShape)

                case .eListen:
                    capShape.move(to: CGPoint(x: -108.8321252885641, y: -3.5649750651344245))
                    capShape.addQuadCurve(to: CGPoint(x: -81.31664937305027, y: -17.51513501188548), controlPoint: CGPoint(x: -68.38245497234401, y: -6.608885011886103))
                    capShape.addQuadCurve(to: CGPoint(x: -37.38292795286257, y: -33.99462719936458), controlPoint: CGPoint(x: -94.25084377375654, y: -28.421385011884855))
                    capShape.addQuadCurve(to: CGPoint(x: 57.315093364536914, y: -22.43091551550861), controlPoint: CGPoint(x: 19.484987868031393, y: -39.5678693868443))
                    capShape.addQuadCurve(to: CGPoint(x: 75.12891482101223, y: 0.9645922969726217), controlPoint: CGPoint(x: 95.14519886104243, y: -5.293961644172921))
                    capShape.addQuadCurve(to: CGPoint(x: 86.28448952274678, y: 24.171388425620073), controlPoint: CGPoint(x: 55.11263078098203, y: 7.223146238118164))
                    capShape.addQuadCurve(to: CGPoint(x: 43.94522466836071, y: 41.44189623810899), controlPoint: CGPoint(x: 117.45634826451153, y: 41.119630613121984))
                    capShape.addQuadCurve(to: CGPoint(x: -89.42384726628715, y: 20.621548372356624), controlPoint: CGPoint(x: -29.56589892779012, y: 41.76416186309599))
                    capShape.addQuadCurve(to: CGPoint(x: -108.8321252885641, y: -3.5649750651344245), controlPoint: CGPoint(x: -149.28179560478418, y: -0.5210651183827468))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 134.85759203342204, y:24.820404164952947))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -52.673597840064886, y: -33.16394154448429))
                    brimShape.addQuadCurve(to: CGPoint(x: 16.99827715992239, y: -17.261902970269478), controlPoint: CGPoint(x: -8.050550965075784, y: -42.875855606987926))
                    brimShape.addQuadCurve(to: CGPoint(x: 12.889878722444537, y: -3.249431771265879), controlPoint: CGPoint(x: 42.04710528492057, y: 8.352049666448968))
                    brimShape.addQuadCurve(to: CGPoint(x: -56.78199627754274, y: -19.15147034548069), controlPoint: CGPoint(x: -16.267347840031494, y: -14.850913208980726))
                    brimShape.addQuadCurve(to: CGPoint(x: -52.673597840064886, y: -33.16394154448429), controlPoint: CGPoint(x: -97.29664471505399, y: -23.452027481980654))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 107.03906249976804, y:24.4218749999869))

                    capShape.append(brimShape)

                case .eSpeak:
                    capShape.move(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893))
                    capShape.addQuadCurve(to: CGPoint(x: -75.78862274446837, y: -12.3301205694519), controlPoint: CGPoint(x: -62.29360456498624, y: -5.312378523385172))
                    capShape.addQuadCurve(to: CGPoint(x: -39.348015717842536, y: -25.997126695041374), controlPoint: CGPoint(x: -89.2836409239505, y: -19.347862615518626))
                    capShape.addQuadCurve(to: CGPoint(x: 50.17978859941322, y: -18.963255878852625), controlPoint: CGPoint(x: 10.587609488265427, y: -32.64639077456412))
                    capShape.addQuadCurve(to: CGPoint(x: 72.16742838948932, y: -3.6095772277586775), controlPoint: CGPoint(x: 89.77196771056101, y: -5.280120983141131))
                    capShape.addQuadCurve(to: CGPoint(x: 93.09157778555041, y: 11.358042176462936), controlPoint: CGPoint(x: 54.56288906841763, y: -1.9390334723762237))
                    capShape.addQuadCurve(to: CGPoint(x: 56.449585353070745, y: 30.11045111433173), controlPoint: CGPoint(x: 131.6202665026832, y: 24.655117825302096))
                    capShape.addQuadCurve(to: CGPoint(x: -86.42002875370818, y: 24.39443120123356), controlPoint: CGPoint(x: -18.7210957965417, y: 35.56578440336136))
                    capShape.addQuadCurve(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893), controlPoint: CGPoint(x: -154.11896171087466, y: 13.223077999105751))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 154.39053368323854, y:21.40387481367415))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -54.807058838572246, y: -27.407038849849826))
                    brimShape.addQuadCurve(to: CGPoint(x: 10.644113036416508, y: -25.180476349840543), controlPoint: CGPoint(x: -21.799894837100087, y: -39.87434880162868))
                    brimShape.addQuadCurve(to: CGPoint(x: 13.410386534950804, y: -12.668758553516568), controlPoint: CGPoint(x: 43.0881209099331, y: -10.48660389805241))
                    brimShape.addQuadCurve(to: CGPoint(x: -52.04078534003795, y: -14.895321053525851), controlPoint: CGPoint(x: -16.267347840031494, y: -14.850913208980726))
                    brimShape.addQuadCurve(to: CGPoint(x: -54.807058838572246, y: -27.407038849849826), controlPoint: CGPoint(x: -87.81422284004441, y: -14.939728898070975))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 114.07031250000972, y:13.832031250007176))

                    capShape.append(brimShape)

                case .eAlert:
                    capShape.move(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893))
                    capShape.addQuadCurve(to: CGPoint(x: -75.78862274446837, y: -12.3301205694519), controlPoint: CGPoint(x: -62.29360456498624, y: -5.312378523385172))
                    capShape.addQuadCurve(to: CGPoint(x: -39.348015717842536, y: -25.997126695041374), controlPoint: CGPoint(x: -89.2836409239505, y: -19.347862615518626))
                    capShape.addQuadCurve(to: CGPoint(x: 50.17978859941322, y: -18.963255878852625), controlPoint: CGPoint(x: 10.587609488265427, y: -32.64639077456412))
                    capShape.addQuadCurve(to: CGPoint(x: 72.16742838948932, y: -3.6095772277586775), controlPoint: CGPoint(x: 89.77196771056101, y: -5.280120983141131))
                    capShape.addQuadCurve(to: CGPoint(x: 93.09157778555041, y: 11.358042176462936), controlPoint: CGPoint(x: 54.56288906841763, y: -1.9390334723762237))
                    capShape.addQuadCurve(to: CGPoint(x: 56.449585353070745, y: 30.11045111433173), controlPoint: CGPoint(x: 131.6202665026832, y: 24.655117825302096))
                    capShape.addQuadCurve(to: CGPoint(x: -86.42002875370818, y: 24.39443120123356), controlPoint: CGPoint(x: -18.7210957965417, y: 35.56578440336136))
                    capShape.addQuadCurve(to: CGPoint(x: -108.20628313793046, y: 3.9553497378602893), controlPoint: CGPoint(x: -154.11896171087466, y: 13.223077999105751))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 154.88790741171488, y:21.231025461216788))
                    capShape.apply(CGAffineTransform(scaleX: 0.6290103531340573, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -56.823984650328875, y: -26.58355252173135))
                    brimShape.addQuadCurve(to: CGPoint(x: 14.181874724655648, y: -21.286677521729953), controlPoint: CGPoint(x: -21.799894837100087, y: -39.87434880162868))
                    brimShape.addQuadCurve(to: CGPoint(x: 16.948148223189946, y: -8.774959725405976), controlPoint: CGPoint(x: 50.16364428641138, y: -2.699006241831226))
                    brimShape.addQuadCurve(to: CGPoint(x: -54.05771115179458, y: -14.071834725407374), controlPoint: CGPoint(x: -16.267347840031494, y: -14.850913208980726))
                    brimShape.addQuadCurve(to: CGPoint(x: -56.823984650328875, y: -26.58355252173135), controlPoint: CGPoint(x: -91.84807446355767, y: -13.29275624183402))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 115.46874999996598, y:16.140625000046015))

                    capShape.append(brimShape)

                case .eSalute:
                    capShape.move(to: CGPoint(x: -113.83312579409181, y: 4.089753382592839))
                    capShape.addQuadCurve(to: CGPoint(x: -87.38098269004888, y: -12.791105992408543), controlPoint: CGPoint(x: -64.99027909541608, y: -4.367277867413643))
                    capShape.addQuadCurve(to: CGPoint(x: -53.38971486991467, y: -26.804777867416078), controlPoint: CGPoint(x: -109.77168628468168, y: -21.214934117403445))
                    capShape.addQuadCurve(to: CGPoint(x: 42.74103253303981, y: -23.08346863702871), controlPoint: CGPoint(x: 2.9922565448523333, y: -32.39462161742871))
                    capShape.addQuadCurve(to: CGPoint(x: 70.56251418720123, y: -7.501437387015745), controlPoint: CGPoint(x: 82.48980852122727, y: -13.77231565662871))
                    capShape.addQuadCurve(to: CGPoint(x: 97.23371254555082, y: 11.39834713259631), controlPoint: CGPoint(x: 58.63521985317518, y: -1.2305591174027803))
                    capShape.addQuadCurve(to: CGPoint(x: 61.89484170781906, y: 29.83584713260648), controlPoint: CGPoint(x: 135.83220523792644, y: 24.0272533825954))
                    capShape.addQuadCurve(to: CGPoint(x: -87.35924715752795, y: 24.09561275760844), controlPoint: CGPoint(x: -12.042521822288323, y: 35.64444088261756))
                    capShape.addQuadCurve(to: CGPoint(x: -113.83312579409181, y: 4.089753382592839), controlPoint: CGPoint(x: -162.67597249276756, y: 12.546784632599321))
                    capShape.close()
                    capShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    capShape.apply(CGAffineTransform(translationX: 146.37053318145112, y:41.55622147854682))
                    capShape.apply(CGAffineTransform(scaleX: 0.601287617585103, y: 1.0))

                    let brimShape = UIBezierPath()

                    brimShape.move(to: CGPoint(x: -58.41191511388665, y: -29.625164398215407))
                    brimShape.addQuadCurve(to: CGPoint(x: 9.43740463576745, y: -23.35368002321514), controlPoint: CGPoint(x: -20.134620358322913, y: -37.25797689821171))
                    brimShape.addQuadCurve(to: CGPoint(x: 8.650831139971464, y: -12.562908538802901), controlPoint: CGPoint(x: 39.00942962985781, y: -9.44938314821857))
                    brimShape.addQuadCurve(to: CGPoint(x: -59.19848860968264, y: -18.834392913803168), controlPoint: CGPoint(x: -21.707767349914885, y: -15.67643392938723))
                    brimShape.addQuadCurve(to: CGPoint(x: -58.41191511388665, y: -29.625164398215407), controlPoint: CGPoint(x: -96.6892098694504, y: -21.992351898219106))
                    brimShape.close()
                    brimShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    brimShape.apply(CGAffineTransform(translationX: 115.40984792603408, y:37.0))
                    brimShape.apply(CGAffineTransform(scaleX: 0.9957151228525574, y: 1.0))

                    capShape.append(brimShape)

                case .eReposition:
                    return capPath(.eWait)

                case .eNop:
                    return nil
            }

            return capShape
        }

        static func headPath(_ gestureOp: eAvatarGestureAnimationOp) -> UIBezierPath?
        {
            let headShape = UIBezierPath()

            switch gestureOp
            {
                case .eAssemble:

                    return headPath(.eWait)

                case .eDisassemble:

                    headShape.move(to: CGPoint(x: 0.5253906250581437, y: -350.0993862152012))
                    headShape.addQuadCurve(to: CGPoint(x: -21.830078124906, y: -349.759542465265), controlPoint: CGPoint(x: -14.626953124906791, y: -349.71852684027357))
                    headShape.addQuadCurve(to: CGPoint(x: 22.12109375008838, y: -351.1267299652933), controlPoint: CGPoint(x: -29.033203124905206, y: -349.80055809025635))
                    headShape.addQuadCurve(to: CGPoint(x: 55.39257812507614, y: -352.8357143401916), controlPoint: CGPoint(x: 73.27539062508197, y: -352.4529018403302))
                    headShape.addQuadCurve(to: CGPoint(x: 26.593750000046697, y: -351.84938621509093), controlPoint: CGPoint(x: 37.509765625070315, y: -353.218526840053))
                    headShape.addQuadCurve(to: CGPoint(x: 0.5253906250581437, y: -350.0993862152012), controlPoint: CGPoint(x: 15.677734375023078, y: -350.48024559012885))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 82.41210937494066, y:107.04492187503189))

                case .eWait, .ePause:

                    headShape.move(to: CGPoint(x: -29.30338124568459, y: 59.52823854502934))
                    headShape.addQuadCurve(to: CGPoint(x: -40.033610483555115, y: -21.981092328754304), controlPoint: CGPoint(x: -57.98714284227441, y: 17.061640034230095))
                    headShape.addQuadCurve(to: CGPoint(x: 23.92578125011108, y: -54.25429344170486), controlPoint: CGPoint(x: -22.080078124835822, y: -61.0238246917387))
                    headShape.addQuadCurve(to: CGPoint(x: 69.82040105199252, y: -6.207853192957927), controlPoint: CGPoint(x: 69.93164062505798, y: -47.484762191671024))
                    headShape.addQuadCurve(to: CGPoint(x: 34.54477091491614, y: 68.53194643079188), controlPoint: CGPoint(x: 69.70916147892706, y: 35.06905580575517))
                    headShape.addQuadCurve(to: CGPoint(x: -29.30338124568459, y: 59.52823854502934), controlPoint: CGPoint(x: -0.6196196490947727, y: 101.99483705582858))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 82.41210937494066, y:107.04492187503189))

                case .eWaveLeft:

                    headShape.move(to: CGPoint(x: -27.902751108695064, y: 59.141629343262224))
                    headShape.addQuadCurve(to: CGPoint(x: -43.58072499569815, y: -20.444417704990176), controlPoint: CGPoint(x: -57.98714284227441, y: 17.061640034230095))
                    headShape.addQuadCurve(to: CGPoint(x: 16.612065175369338, y: -54.62280547110579), controlPoint: CGPoint(x: -29.174307149121887, y: -57.95047544421045))
                    headShape.addQuadCurve(to: CGPoint(x: 66.05379948939381, y: -8.113039846122987), controlPoint: CGPoint(x: 62.39843749986056, y: -51.295135498001144))
                    headShape.addQuadCurve(to: CGPoint(x: 35.94540105190567, y: 68.14533722902476), controlPoint: CGPoint(x: 69.70916147892706, y: 35.06905580575517))
                    headShape.addQuadCurve(to: CGPoint(x: -27.902751108695064, y: 59.141629343262224), controlPoint: CGPoint(x: 2.181640624884281, y: 101.22161865229435))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 87.45703125010667, y:86.41015625000284))

                case .eWaveRight:

                    headShape.move(to: CGPoint(x: -27.09318079622262, y: 58.9744616797031))
                    headShape.addQuadCurve(to: CGPoint(x: -43.58072499569815, y: -20.444417704990176), controlPoint: CGPoint(x: -57.98714284227441, y: 17.061640034230095))
                    headShape.addQuadCurve(to: CGPoint(x: 18.2136276754013, y: -52.75425230950381), controlPoint: CGPoint(x: -29.174307149121887, y: -57.95047544421045))
                    headShape.addQuadCurve(to: CGPoint(x: 66.8233307394629, y: -6.521830434551575), controlPoint: CGPoint(x: 65.60156249992448, y: -47.55802917479717))
                    headShape.addQuadCurve(to: CGPoint(x: 35.92294011441525, y: 67.70082581543505), controlPoint: CGPoint(x: 68.04509897900132, y: 34.51436830569402))
                    headShape.addQuadCurve(to: CGPoint(x: -27.09318079622262, y: 58.9744616797031), controlPoint: CGPoint(x: 3.8007812498291713, y: 100.8872833251761))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 87.45703125010667, y:86.41015625000284))

                case .eListen:

                    headShape.move(to: CGPoint(x: -40.14843750017171, y: 59.00469970704528))
                    headShape.addQuadCurve(to: CGPoint(x: -42.98632812510374, y: -21.592956543022783), controlPoint: CGPoint(x: -59.87109375018647, y: 18.950012207118135))
                    headShape.addQuadCurve(to: CGPoint(x: 18.805664062430235, y: -56.53735351574498), controlPoint: CGPoint(x: -26.10156250002101, y: -62.1359252931637))
                    headShape.addQuadCurve(to: CGPoint(x: 66.71102605190427, y: -7.93486296628555), controlPoint: CGPoint(x: 63.71289062488148, y: -50.93878173832627))
                    headShape.addQuadCurve(to: CGPoint(x: 24.641690114385057, y: 67.0642215063638), controlPoint: CGPoint(x: 69.70916147892706, y: 35.06905580575517))
                    headShape.addQuadCurve(to: CGPoint(x: -40.14843750017171, y: 59.00469970704528), controlPoint: CGPoint(x: -20.425781250156945, y: 99.05938720697243))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 87.45703125010667, y:86.41015625000284))

                case .eSpeak:

                    headShape.move(to: CGPoint(x: -29.30338124568459, y: 59.52823854502934))
                    headShape.addQuadCurve(to: CGPoint(x: -43.58072499569815, y: -20.444417704990176), controlPoint: CGPoint(x: -57.98714284227441, y: 17.061640034230095))
                    headShape.addQuadCurve(to: CGPoint(x: 20.271142680080914, y: -52.502278079423235), controlPoint: CGPoint(x: -29.174307149121887, y: -57.95047544421045))
                    headShape.addQuadCurve(to: CGPoint(x: 69.71287699410539, y: -5.99251245444043), controlPoint: CGPoint(x: 69.71659250928371, y: -47.05408071463603))
                    headShape.addQuadCurve(to: CGPoint(x: 34.54477091491614, y: 68.53194643079188), controlPoint: CGPoint(x: 69.70916147892706, y: 35.06905580575517))
                    headShape.addQuadCurve(to: CGPoint(x: -29.30338124568459, y: 59.52823854502934), controlPoint: CGPoint(x: -0.6196196490947727, y: 101.99483705582858))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 87.45703125010667, y:86.41015625000284))

                case .eAlert:

                    headShape.move(to: CGPoint(x: -24.74218750013825, y: 63.46215820309067))
                    headShape.addQuadCurve(to: CGPoint(x: -39.69335937514859, y: -16.56518554689794), controlPoint: CGPoint(x: -53.91015625012527, y: 22.32348632808987))
                    headShape.addQuadCurve(to: CGPoint(x: 19.314453124830173, y: -53.52416992188918), controlPoint: CGPoint(x: -25.47656250017191, y: -55.45385742188575))
                    headShape.addQuadCurve(to: CGPoint(x: 66.57812499984009, y: -9.143310546880816), controlPoint: CGPoint(x: 64.10546874983226, y: -51.59448242189262))
                    headShape.addQuadCurve(to: CGPoint(x: 36.73828124984834, y: 68.95434570311123), controlPoint: CGPoint(x: 69.05078124984792, y: 33.30786132813099))
                    headShape.addQuadCurve(to: CGPoint(x: -24.74218750013825, y: 63.46215820309067), controlPoint: CGPoint(x: 4.425781249848768, y: 104.60083007809146))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 87.45703125010667, y:86.41015625000284))

                case .eSalute:

                    headShape.move(to: CGPoint(x: -29.349609374913623, y: 62.23941993720832))
                    headShape.addQuadCurve(to: CGPoint(x: -45.25976562488994, y: -21.26643943776231), controlPoint: CGPoint(x: -57.66992187490494, y: 19.837076187232))
                    headShape.addQuadCurve(to: CGPoint(x: 15.351562500146422, y: -57.16292381279466), controlPoint: CGPoint(x: -32.84960937487493, y: -62.36995506275662))
                    headShape.addQuadCurve(to: CGPoint(x: 65.56640625014677, y: -9.453939437818832), controlPoint: CGPoint(x: 63.55273437516777, y: -51.9558925628327))
                    headShape.addQuadCurve(to: CGPoint(x: 33.27539062510173, y: 68.84488868718984), controlPoint: CGPoint(x: 67.58007812512577, y: 33.04801368719504))
                    headShape.addQuadCurve(to: CGPoint(x: -29.349609374913623, y: 62.23941993720832), controlPoint: CGPoint(x: -1.0292968749223093, y: 104.64176368718464))
                    headShape.close()
                    headShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    headShape.apply(CGAffineTransform(translationX: 82.41210937494066, y:107.04492187503189))

                case .eReposition:
                    return headPath(.eWait)

                case .eNop:
                    return nil
            }


            return headShape
        }

        static func bodyPath(_ gestureOp: eAvatarGestureAnimationOp) -> UIBezierPath?
        {
            let bodyShape = UIBezierPath()

            switch gestureOp
            {
                case .eAssemble:

                    return bodyPath(.eWait)

                case .eDisassemble:

                    bodyShape.move(to: CGPoint(x: 34.23593139636209, y: -238.75780010221033))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.784759521353024, y: -238.02342510210616), controlPoint: CGPoint(x: 42.21640014636546, y: -239.06444072717738))
                    bodyShape.addQuadCurve(to: CGPoint(x: 65.7554626464235, y: -237.14061260227788), controlPoint: CGPoint(x: 59.35311889634059, y: -236.98240947703496))
                    bodyShape.addQuadCurve(to: CGPoint(x: 79.33163452151993, y: -237.47264385250224), controlPoint: CGPoint(x: 72.15780639650639, y: -237.29881572752083))
                    bodyShape.addQuadCurve(to: CGPoint(x: 92.61093139646975, y: -238.27147197737912), controlPoint: CGPoint(x: 86.50546264653349, y: -237.64647197748369))
                    bodyShape.addQuadCurve(to: CGPoint(x: 102.51327514636135, y: -238.1601438522188), controlPoint: CGPoint(x: 98.71640014640602, y: -238.89647197727453))
                    bodyShape.addQuadCurve(to: CGPoint(x: 98.07577514639667, y: -237.2792844772831), controlPoint: CGPoint(x: 106.31015014631669, y: -237.42381572716306))
                    bodyShape.addQuadCurve(to: CGPoint(x: 53.38241577144429, y: -238.54295635231958), controlPoint: CGPoint(x: 89.84140014647664, y: -237.13475322740314))
                    bodyShape.addQuadCurve(to: CGPoint(x: -25.312896728609992, y: -236.19530010220848), controlPoint: CGPoint(x: 16.923431396411942, y: -239.95115947723602))
                    bodyShape.addQuadCurve(to: CGPoint(x: -62.16641235362699, y: -234.04295635217147), controlPoint: CGPoint(x: -67.54922485363193, y: -232.43944072718094))
                    bodyShape.addQuadCurve(to: CGPoint(x: -52.930084228620366, y: -235.27928447719805), controlPoint: CGPoint(x: -56.78359985362205, y: -235.64647197716204))
                    bodyShape.addQuadCurve(to: CGPoint(x: -43.23672485364712, y: -235.21678447724045), controlPoint: CGPoint(x: -49.07656860361868, y: -234.91209697723406))
                    bodyShape.addQuadCurve(to: CGPoint(x: -30.965240478644073, y: -235.84178447722968), controlPoint: CGPoint(x: -37.396881103675554, y: -235.52147197724685))
                    bodyShape.addQuadCurve(to: CGPoint(x: -18.56289672859791, y: -236.63475322733603), controlPoint: CGPoint(x: -24.533599853612593, y: -236.1620969772125))
                    bodyShape.addQuadCurve(to: CGPoint(x: -5.1683654785863915, y: -237.39061260237645), controlPoint: CGPoint(x: -12.592193603583226, y: -237.10740947745958))
                    bodyShape.addQuadCurve(to: CGPoint(x: 14.255462646384586, y: -238.06248760226828), controlPoint: CGPoint(x: 2.255462646410443, y: -237.67381572729332))
                    bodyShape.addQuadCurve(to: CGPoint(x: 34.23593139636209, y: -238.75780010221033), controlPoint: CGPoint(x: 26.25546264635873, y: -238.45115947724327))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 73.59609985362619, y:220.97635173797926))

                case .eWait, .ePause:

                    bodyShape.move(to: CGPoint(x: 50.188079833816914, y: -183.48175811758088))
                    bodyShape.addQuadCurve(to: CGPoint(x: 74.69296264637248, y: -161.11192607868043), controlPoint: CGPoint(x: 73.3453063963132, y: -184.54161357853644))
                    bodyShape.addQuadCurve(to: CGPoint(x: 86.35897827138197, y: -137.8248167036948), controlPoint: CGPoint(x: 76.04061889643177, y: -137.68223857882438))
                    bodyShape.addQuadCurve(to: CGPoint(x: 100.8921813964001, y: -110.03587627407772), controlPoint: CGPoint(x: 96.67733764633219, y: -137.96739482856526))
                    bodyShape.addQuadCurve(to: CGPoint(x: 106.86483764642726, y: -61.903063774108894), controlPoint: CGPoint(x: 105.107025146468, y: -82.10435771959018))
                    bodyShape.addQuadCurve(to: CGPoint(x: 106.57968139640951, y: -19.693957328740044), controlPoint: CGPoint(x: 108.62265014638652, y: -41.701769828627604))
                    bodyShape.addQuadCurve(to: CGPoint(x: 99.28671264642603, y: 27.509167671240707), controlPoint: CGPoint(x: 104.5367126464325, y: 2.313855171147516))
                    bodyShape.addQuadCurve(to: CGPoint(x: 59.30038452140104, y: 50.681042671298755), controlPoint: CGPoint(x: 94.03671264641955, y: 52.7044801713339))
                    bodyShape.addQuadCurve(to: CGPoint(x: -12.705474853638002, y: 50.91541767129267), controlPoint: CGPoint(x: 24.56405639638254, y: 48.65760517126361))
                    bodyShape.addQuadCurve(to: CGPoint(x: -54.92617797862843, y: 30.456433296303466), controlPoint: CGPoint(x: -49.975006103658544, y: 53.173230171321734))
                    bodyShape.addQuadCurve(to: CGPoint(x: -60.24649047862928, y: -13.592394828733426), controlPoint: CGPoint(x: -59.87734985359831, y: 7.739636421285198))
                    bodyShape.addQuadCurve(to: CGPoint(x: -60.0726623536343, y: -61.03575420375953), controlPoint: CGPoint(x: -60.61563110366025, y: -34.92442607875205))
                    bodyShape.addQuadCurve(to: CGPoint(x: -53.86367797862086, y: -110.51622295381495), controlPoint: CGPoint(x: -59.52969360360835, y: -87.14708232876701))
                    bodyShape.addQuadCurve(to: CGPoint(x: -39.20156860366893, y: -135.59044170371448), controlPoint: CGPoint(x: -48.197662353633376, y: -133.8853635788629))
                    bodyShape.addQuadCurve(to: CGPoint(x: -32.51406860366172, y: -158.83458232878374), controlPoint: CGPoint(x: -30.205474853704494, y: -137.29551982856603))
                    bodyShape.addQuadCurve(to: CGPoint(x: -3.8959045411491644, y: -181.3977737428134), controlPoint: CGPoint(x: -34.822662353618945, y: -180.37364482900145))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.188079833816914, y: -183.48175811758088), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 73.59609985362619, y:220.97635173797926))

                case .eWaveLeft:

                    bodyShape.move(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896))
                    bodyShape.addQuadCurve(to: CGPoint(x: 74.62557983388382, y: -146.73655414599529), controlPoint: CGPoint(x: 74.66757202145989, y: -188.53518390695388))
                    bodyShape.addQuadCurve(to: CGPoint(x: 98.35897827130952, y: -127.54925251014679), controlPoint: CGPoint(x: 74.58358764630773, y: -104.93792438503667))
                    bodyShape.addQuadCurve(to: CGPoint(x: 117.50936889632794, y: -108.51800251025325), controlPoint: CGPoint(x: 122.13436889631133, y: -150.1605806352569))
                    bodyShape.addQuadCurve(to: CGPoint(x: 111.66952514637596, y: -46.34026813530286), controlPoint: CGPoint(x: 112.88436889634457, y: -66.8754243852496))
                    bodyShape.addQuadCurve(to: CGPoint(x: 108.71835327137563, y: -4.492611885260462), controlPoint: CGPoint(x: 110.45468139640735, y: -25.805111885356112))
                    bodyShape.addQuadCurve(to: CGPoint(x: 102.73202514629408, y: 35.70465373983387), controlPoint: CGPoint(x: 106.98202514634391, y: 16.819888114835187))
                    bodyShape.addQuadCurve(to: CGPoint(x: 67.60409545883056, y: 50.15846538532744), controlPoint: CGPoint(x: 98.48202514624424, y: 54.58941936483255))
                    bodyShape.addQuadCurve(to: CGPoint(x: 15.458587646381726, y: 45.12008953085214), controlPoint: CGPoint(x: 36.72616577141689, y: 45.72751140582233))
                    bodyShape.addQuadCurve(to: CGPoint(x: -19.178131103687132, y: 43.262667655851665), controlPoint: CGPoint(x: -5.808990478653435, y: 44.512667655881955))
                    bodyShape.addQuadCurve(to: CGPoint(x: -99.07363891619487, y: 110.56080913529124), controlPoint: CGPoint(x: -32.54727172872083, y: 42.012667655821375))
                    bodyShape.addQuadCurve(to: CGPoint(x: -131.07266235367752, y: 117.7554349898034), controlPoint: CGPoint(x: -165.6000061036689, y: 179.10895061476108))
                    bodyShape.addQuadCurve(to: CGPoint(x: -64.35195922870534, y: 23.46051311491476), controlPoint: CGPoint(x: -96.5453186036861, y: 56.401919364845725))
                    bodyShape.addQuadCurve(to: CGPoint(x: -32.26992797870922, y: -58.44573688513583), controlPoint: CGPoint(x: -32.15859985372458, y: -9.480893135016203))
                    bodyShape.addQuadCurve(to: CGPoint(x: -24.93106079117306, y: -147.7560853959365), controlPoint: CGPoint(x: -32.38125610369386, y: -107.41058063525546))
                    bodyShape.addQuadCurve(to: CGPoint(x: 4.774993896334177, y: -185.26174640662146), controlPoint: CGPoint(x: -17.480865478652262, y: -188.10159015661756))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 77.63320922863035, y:201.83377361284667))

                case .eWaveRight:

                    bodyShape.move(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896))
                    bodyShape.addQuadCurve(to: CGPoint(x: 75.36190795887802, y: -145.12310504938353), controlPoint: CGPoint(x: 74.66757202145989, y: -188.53518390695388))
                    bodyShape.addQuadCurve(to: CGPoint(x: 94.58163452128412, y: -117.15623188027602), controlPoint: CGPoint(x: 76.05624389629614, y: -101.71102619181315))
                    bodyShape.addQuadCurve(to: CGPoint(x: 111.31307983376345, y: -101.65375995639796), controlPoint: CGPoint(x: 113.10702514627208, y: -132.6014375687389))
                    bodyShape.addQuadCurve(to: CGPoint(x: 108.67147827129263, y: -48.63772296905149), controlPoint: CGPoint(x: 109.51913452125484, y: -70.70608234405702))
                    bodyShape.addQuadCurve(to: CGPoint(x: 106.8076497154294, y: -4.7491738475394705), controlPoint: CGPoint(x: 107.82382202133041, y: -26.569363594045953))
                    bodyShape.addQuadCurve(to: CGPoint(x: 102.10550127790147, y: 33.73088291510268), controlPoint: CGPoint(x: 105.7914774095284, y: 17.071015898967012))
                    bodyShape.addQuadCurve(to: CGPoint(x: 67.5728454588457, y: 48.05913066853034), controlPoint: CGPoint(x: 98.41952514627454, y: 50.39074993123835))
                    bodyShape.addQuadCurve(to: CGPoint(x: 12.45370483384227, y: 41.711474418522), controlPoint: CGPoint(x: 36.72616577141689, y: 45.72751140582233))
                    bodyShape.addQuadCurve(to: CGPoint(x: -30.408599853727324, y: 38.54504680625954), controlPoint: CGPoint(x: -11.818756103732348, y: 37.69543743122168))
                    bodyShape.addQuadCurve(to: CGPoint(x: -64.291412353719, y: 134.14845561978456), controlPoint: CGPoint(x: -48.9984436037223, y: 39.39465618129741))
                    bodyShape.addQuadCurve(to: CGPoint(x: -82.2718811037095, y: 141.72842693316983), controlPoint: CGPoint(x: -79.58438110371569, y: 228.9022550582717))
                    bodyShape.addQuadCurve(to: CGPoint(x: -64.41543579113838, y: 18.180117606930168), controlPoint: CGPoint(x: -84.95938110370332, y: 54.554598808067965))
                    bodyShape.addQuadCurve(to: CGPoint(x: -41.44277954115283, y: -66.39019489298356), controlPoint: CGPoint(x: -43.87149047857342, y: -18.19436359420763))
                    bodyShape.addQuadCurve(to: CGPoint(x: -28.24746704119225, y: -151.34380817418852), controlPoint: CGPoint(x: -39.01406860373224, y: -114.58602619175949))
                    bodyShape.addQuadCurve(to: CGPoint(x: 4.774993896334177, y: -185.26174640662146), controlPoint: CGPoint(x: -17.480865478652262, y: -188.10159015661756))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 77.63320922863035, y:201.83377361284667))

                case .eListen:

                    bodyShape.move(to: CGPoint(x: 62.69686889642064, y: -181.26022052782054))
                    bodyShape.addQuadCurve(to: CGPoint(x: 86.00155639642182, y: -136.2182893754015), controlPoint: CGPoint(x: 88.75741577138639, y: -181.6831331253182))
                    bodyShape.addQuadCurve(to: CGPoint(x: 74.12655639645506, y: -34.440945625443206), controlPoint: CGPoint(x: 83.24569702145726, y: -90.7534456254848))
                    bodyShape.addQuadCurve(to: CGPoint(x: 91.821868896482, y: -25.269070625457644), controlPoint: CGPoint(x: 65.00741577145286, y: 21.87155437459839))
                    bodyShape.addQuadCurve(to: CGPoint(x: 134.39413452141795, y: -86.90188312537549), controlPoint: CGPoint(x: 118.63632202151115, y: -72.40969562551368))
                    bodyShape.addQuadCurve(to: CGPoint(x: 132.72811889637876, y: -43.77688312532055), controlPoint: CGPoint(x: 150.15194702132476, y: -101.39407062523729))
                    bodyShape.addQuadCurve(to: CGPoint(x: 97.42733764633994, y: 35.188998222249694), controlPoint: CGPoint(x: 115.30429077143273, y: 13.84030437459618))
                    bodyShape.addQuadCurve(to: CGPoint(x: 58.13827514633201, y: 51.132601737862764), controlPoint: CGPoint(x: 79.55038452124714, y: 56.5376920699032))
                    bodyShape.addQuadCurve(to: CGPoint(x: 15.872650146398609, y: 42.68454265588217), controlPoint: CGPoint(x: 36.72616577141689, y: 45.72751140582233))
                    bodyShape.addQuadCurve(to: CGPoint(x: -37.17227172863167, y: 40.99978923791968), controlPoint: CGPoint(x: -4.98086547861967, y: 39.64157390594201))
                    bodyShape.addQuadCurve(to: CGPoint(x: -52.697662353628225, y: 101.55917644488235), controlPoint: CGPoint(x: -69.36367797864366, y: 42.358004569897346))
                    bodyShape.addQuadCurve(to: CGPoint(x: -69.17031860363969, y: 113.38534831984725), controlPoint: CGPoint(x: -36.031646728612785, y: 160.76034831986735))
                    bodyShape.addQuadCurve(to: CGPoint(x: -72.43594360365218, y: 25.80813884716345), controlPoint: CGPoint(x: -102.3089904786666, y: 66.01034831982716))
                    bodyShape.addQuadCurve(to: CGPoint(x: -35.68008422859552, y: -55.970242500457644), controlPoint: CGPoint(x: -42.56289672863775, y: -14.394070625500262))
                    bodyShape.addQuadCurve(to: CGPoint(x: -26.574615478585795, y: -142.2124300004027), controlPoint: CGPoint(x: -28.797271728553284, y: -97.54641437541503))
                    bodyShape.addQuadCurve(to: CGPoint(x: 6.142181396418287, y: -183.85787677785663), controlPoint: CGPoint(x: -24.351959228618306, y: -186.87844562539036))
                    bodyShape.addQuadCurve(to: CGPoint(x: 62.69686889642064, y: -181.26022052782054), controlPoint: CGPoint(x: 36.63632202145488, y: -180.83730793032288))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 77.63320922863035, y:201.83377361284667))

                case .eSpeak:
                    bodyShape.move(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896))
                    bodyShape.addQuadCurve(to: CGPoint(x: 75.83554077141821, y: -146.55227375051595), controlPoint: CGPoint(x: 74.66757202145989, y: -188.53518390695388))
                    bodyShape.addQuadCurve(to: CGPoint(x: 79.54159545890488, y: -78.54714679730769), controlPoint: CGPoint(x: 77.00350952137651, y: -104.569363594078))
                    bodyShape.addQuadCurve(to: CGPoint(x: 86.99569702147033, y: -34.927273750602424), controlPoint: CGPoint(x: 82.07968139643323, y: -52.52493000053738))
                    bodyShape.addQuadCurve(to: CGPoint(x: 106.09335327150454, y: 7.336398124377716), controlPoint: CGPoint(x: 91.91171264650743, y: -17.329617500667467))
                    bodyShape.addQuadCurve(to: CGPoint(x: 81.64218139652444, y: 86.00827312436897), controlPoint: CGPoint(x: 120.27499389650166, y: 32.0024137494229))
                    bodyShape.addQuadCurve(to: CGPoint(x: 66.3433532715148, y: 85.02194499936371), controlPoint: CGPoint(x: 43.009368896547215, y: 140.01413249931502))
                    bodyShape.addQuadCurve(to: CGPoint(x: 72.54452514650569, y: 33.52389812436177), controlPoint: CGPoint(x: 89.67733764648239, y: 30.029757499412412))
                    bodyShape.addQuadCurve(to: CGPoint(x: 26.968353271546494, y: 41.62155437428339), controlPoint: CGPoint(x: 55.41171264652898, y: 37.01803874931113))
                    bodyShape.addQuadCurve(to: CGPoint(x: -31.426177978573413, y: 35.61374187446389), controlPoint: CGPoint(x: -1.4750061034359945, y: 46.22506999925565))
                    bodyShape.addQuadCurve(to: CGPoint(x: -21.676177978676694, y: 68.98874187465321), controlPoint: CGPoint(x: -61.37734985371083, y: 25.00241374967213))
                    bodyShape.addQuadCurve(to: CGPoint(x: -43.56484985364477, y: 72.78171062461246), controlPoint: CGPoint(x: 18.024993896357444, y: 112.97506999963429))
                    bodyShape.addQuadCurve(to: CGPoint(x: -69.21914672865762, y: 4.357882499575652), controlPoint: CGPoint(x: -105.15469360364699, y: 32.58835124959063))
                    bodyShape.addQuadCurve(to: CGPoint(x: -34.87442016613376, y: -66.86355304735768), controlPoint: CGPoint(x: -33.28359985366824, y: -23.872586250439326))
                    bodyShape.addQuadCurve(to: CGPoint(x: -26.973052978625766, y: -148.9780550004468), controlPoint: CGPoint(x: -36.46524047859927, y: -109.85451984427604))
                    bodyShape.addQuadCurve(to: CGPoint(x: 4.774993896334177, y: -185.26174640662146), controlPoint: CGPoint(x: -17.480865478652262, y: -188.10159015661756))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.84921264639026, y: -185.4785432817896), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 77.63320922863035, y:201.83377361284667))

                case .eAlert:
                    bodyShape.move(to: CGPoint(x: 52.75057983386077, y: -184.5073213578753))
                    bodyShape.addQuadCurve(to: CGPoint(x: 72.22421264639453, y: -74.24313068403092), controlPoint: CGPoint(x: 78.47030639640091, y: -186.59274005912528))
                    bodyShape.addQuadCurve(to: CGPoint(x: 81.59335327139489, y: -16.086880683893412), controlPoint: CGPoint(x: 65.97811889638814, y: 38.106478691063444))
                    bodyShape.addQuadCurve(to: CGPoint(x: 111.22225952143607, y: -99.97359943402454), controlPoint: CGPoint(x: 97.20858764640164, y: -70.28024005885027))
                    bodyShape.addQuadCurve(to: CGPoint(x: 124.3062438963922, y: -101.67477130909049), controlPoint: CGPoint(x: 125.2359313964705, y: -129.6669588091988))
                    bodyShape.addQuadCurve(to: CGPoint(x: 116.53475952139942, y: -40.42281818410402), controlPoint: CGPoint(x: 123.3765563963139, y: -73.68258380898219))
                    bodyShape.addQuadCurve(to: CGPoint(x: 105.13436889640204, y: 16.667025565915136), controlPoint: CGPoint(x: 109.69296264648494, y: -7.163052559225854))
                    bodyShape.addQuadCurve(to: CGPoint(x: 68.51913452139334, y: 40.49710369102764), controlPoint: CGPoint(x: 100.57577514631913, y: 40.497103691056125))
                    bodyShape.addQuadCurve(to: CGPoint(x: 11.939056396422888, y: 37.979525565960486), controlPoint: CGPoint(x: 36.462493896467564, y: 40.497103690999154))
                    bodyShape.addQuadCurve(to: CGPoint(x: -32.57656860365749, y: 53.670931815956536), controlPoint: CGPoint(x: -12.584381103621787, y: 35.46194744092182))
                    bodyShape.addQuadCurve(to: CGPoint(x: -79.10391235364833, y: 123.58890056597697), controlPoint: CGPoint(x: -52.568756103693204, y: 71.87991619099125))
                    bodyShape.addQuadCurve(to: CGPoint(x: -97.91055297860169, y: 127.15140056600244), controlPoint: CGPoint(x: -105.63906860360346, y: 175.2978849409627))
                    bodyShape.addQuadCurve(to: CGPoint(x: -65.13906860362471, y: 44.233431816039), controlPoint: CGPoint(x: -90.18203735359991, y: 79.00491619104218))
                    bodyShape.addQuadCurve(to: CGPoint(x: -35.750396728697794, y: -51.02676105501867), controlPoint: CGPoint(x: -40.09609985364951, y: 9.461947441035818))
                    bodyShape.addQuadCurve(to: CGPoint(x: -28.662506103676094, y: -147.49160480510056), controlPoint: CGPoint(x: -31.404693603746082, y: -111.51546955107315))
                    bodyShape.addQuadCurve(to: CGPoint(x: 0.5552673338572554, y: -182.94482135787666), controlPoint: CGPoint(x: -25.920318603606106, y: -183.46774005912798))
                    bodyShape.addQuadCurve(to: CGPoint(x: 52.75057983386077, y: -184.5073213578753), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 77.63320922863035, y:201.83377361284667))

                case .eSalute:
                    bodyShape.move(to: CGPoint(x: 50.188079833816914, y: -183.48175811758088))
                    bodyShape.addQuadCurve(to: CGPoint(x: 73.79647827122355, y: -160.07600688875561), controlPoint: CGPoint(x: 73.3453063963132, y: -184.54161357853644))
                    bodyShape.addQuadCurve(to: CGPoint(x: 85.46249389623304, y: -136.78889751377), controlPoint: CGPoint(x: 74.2476501461339, y: -135.61040019897476))
                    bodyShape.addQuadCurve(to: CGPoint(x: 97.5523376461922, y: -109.51155376382), controlPoint: CGPoint(x: 96.67733764633219, y: -137.96739482856526))
                    bodyShape.addQuadCurve(to: CGPoint(x: 100.75350952104577, y: -60.506884574015125), controlPoint: CGPoint(x: 98.42733764605222, y: -81.05571269907475))
                    bodyShape.addQuadCurve(to: CGPoint(x: 100.23983764617596, y: -19.903368949155567), controlPoint: CGPoint(x: 103.07968139603932, y: -39.9580564489555))
                    bodyShape.addQuadCurve(to: CGPoint(x: 92.08749389624153, y: 21.873974800710688), controlPoint: CGPoint(x: 97.3999938963126, y: 0.15131855064436905))
                    bodyShape.addQuadCurve(to: CGPoint(x: 57.01718139612713, y: 43.01459980082066), controlPoint: CGPoint(x: 86.77499389617046, y: 43.596631050777006))
                    bodyShape.addQuadCurve(to: CGPoint(x: -5.635162353898021, y: 35.06342792577695), controlPoint: CGPoint(x: 27.2593688960838, y: 42.43256855086432))
                    bodyShape.addQuadCurve(to: CGPoint(x: -57.767974853874435, y: 54.4188966757358), controlPoint: CGPoint(x: -38.52969360387984, y: 27.69428730068958))
                    bodyShape.addQuadCurve(to: CGPoint(x: -24.459381103861077, y: 119.00678730086304), controlPoint: CGPoint(x: -77.00625610386902, y: 81.14350605078202))
                    bodyShape.addQuadCurve(to: CGPoint(x: -46.80703735384627, y: 122.28413105096823), controlPoint: CGPoint(x: 28.087493896146867, y: 156.87006855094404))
                    bodyShape.addQuadCurve(to: CGPoint(x: -92.59609985384299, y: 49.954052926012594), controlPoint: CGPoint(x: -121.7015686038394, y: 87.69819355099241))
                    bodyShape.addQuadCurve(to: CGPoint(x: -50.06094360385685, y: -11.00493144896231), controlPoint: CGPoint(x: -63.49063110384655, y: 12.209912301032773))
                    bodyShape.addQuadCurve(to: CGPoint(x: -35.72695922874304, y: -107.29671001397942), controlPoint: CGPoint(x: -36.631256103867145, y: -34.21977519895739))
                    bodyShape.addQuadCurve(to: CGPoint(x: -3.8959045411491644, y: -181.3977737428134), controlPoint: CGPoint(x: -34.822662353618945, y: -180.37364482900145))
                    bodyShape.addQuadCurve(to: CGPoint(x: 50.188079833816914, y: -183.48175811758088), controlPoint: CGPoint(x: 27.030853271320616, y: -182.42190265662532))
                    bodyShape.close()
                    bodyShape.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
                    bodyShape.apply(CGAffineTransform(translationX: 73.59609985362619, y:220.97635173797926))

                case .eReposition:
                    return bodyPath(.eWait)

                case .eNop:
                    return nil
            }

            return bodyShape
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ShapeLayer: ShapeLayerWithAnimation with animation definitions.
///

final class ShapeLayerWithAnimation: CAShapeLayer
{
    fileprivate var currentPath = UIBezierPath()

    fileprivate func createMorphAnimation(_ duration: TimeInterval, _ delegate : CAAnimationDelegate?, _ toPath: UIBezierPath) -> CAAnimation
    {
        let morphPathAnimation = CABasicAnimation(keyPath: "path")

        morphPathAnimation.fromValue = currentPath.cgPath
        morphPathAnimation.toValue = toPath.cgPath
        morphPathAnimation.duration = duration
        morphPathAnimation.isRemovedOnCompletion = false
        morphPathAnimation.autoreverses = false
        morphPathAnimation.fillMode = CAMediaTimingFillMode.both
        morphPathAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)

        if let delegate = delegate
        {
            morphPathAnimation.delegate = delegate
        }

        currentPath = toPath

        self.add(morphPathAnimation, forKey: AvatarUIView.Animations.MorphAnimationKey)

        return morphPathAnimation
    }

    fileprivate func createAlphaAnimation(_ duration: TimeInterval, _ delegate : CAAnimationDelegate?, _ fromAlpha: Float, _ toAlpha: Float) -> CAAnimation
    {
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")

        alphaAnimation.fromValue = fromAlpha
        alphaAnimation.toValue = toAlpha
        alphaAnimation.duration = duration
        alphaAnimation.isRemovedOnCompletion = false
        alphaAnimation.autoreverses = false
        alphaAnimation.fillMode = CAMediaTimingFillMode.both
        alphaAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

        if let delegate = delegate
        {
            alphaAnimation.delegate = delegate
        }

        self.add(alphaAnimation, forKey: AvatarUIView.Animations.AlphaAnimationKey)

        return alphaAnimation
    }
}

public enum eAnimationReply
{
    case eDidBeginAnimation(IAnimationModelItem)
    case eDidEndAnimation(IAnimationModelItem)
    case eDidCancelGesture(IAnimationModelItem)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eViewColorCast: The color cast for the non-Overlay and Manual Flight views.
///

enum eViewColorCast
{
    case eSaturated
    case eDesaturated
    case eAlert
}

enum ePosition: Equatable
{
    case eNormal
    case eOperationsDisplay

    var description : String
    {
        switch self
        {
            case .eNormal:              return "normal"
            case .eOperationsDisplay:   return "operations display"
        }
    }
}

public enum eAvatarGestureAnimationOp: CustomStringConvertible, Hashable
{
    case eAssemble
    case eDisassemble
    case eWait
    case eWaveLeft
    case eWaveRight
    case eListen
    case eSpeak
    case eAlert
    case eSalute
    case ePause
    case eReposition(eAvatarGesture.ePosition)
    case eNop

    var hashingValue : Int
    {
        switch self
        {
            case .eDisassemble:                     return 1
            case .eAssemble:                        return 2
            case .eWait:                            return 3
            case .eWaveLeft:                        return 4
            case .eWaveRight:                       return 5
            case .eSpeak:                           return 6
            case .eAlert:                           return 7
            case .eSalute:                          return 8
            case .eListen:                          return 9
            case .ePause:                           return 10
            case .eReposition:                      return 11
            case .eNop:                             return 12
        }
    }

    public func hash(into hasher: inout Hasher)
    {
        hasher.combine(hashingValue)
    }

    public var description : String
    {
        switch self
        {
            case .eDisassemble:                     return "Disassemble"
            case .eAssemble:                        return "Assemble"
            case .eWait:                            return "Wait"
            case .eWaveLeft:                        return "Wave Left"
            case .eWaveRight:                       return "Wave Right"
            case .eSpeak:                           return "Speak"
            case .eAlert:                           return "Alert"
            case .eSalute:                          return "Salute"
            case .eListen:                          return "Listen"
            case .ePause:                           return "Pause"
            case .eReposition:                      return "Move"
            case .eNop:                             return "Nop"
        }
    }

    public static func ==(lhs: eAvatarGestureAnimationOp, rhs: eAvatarGestureAnimationOp) -> Bool
    {
        switch (lhs, rhs)
        {
            case (.eAssemble, .eAssemble):                                          return true
            case (.eDisassemble, .eDisassemble):                                    return true
            case (.eWait, .eWait):                                                  return true
            case (.eWaveLeft, .eWaveLeft):                                          return true
            case (.eWaveRight, .eWaveRight):                                        return true
            case (.eListen, .eListen):                                              return true
            case (.eSpeak, .eSpeak):                                                return true
            case (.eAlert, .eAlert):                                                return true
            case (.eSalute, .eSalute):                                              return true
            case (.eReposition(let lhsPosition), .eReposition(let rhsPosition)):    return lhsPosition == rhsPosition
            case (.ePause, .ePause):                                                return true
            case (.eNop, .eNop):                                                    return true

            default: return false
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxGestureAnimation: IRxAnimation implementation for an avatar gesture.
///

final class GestureAnimation: NSObject, IAnimationModelItem, CAAnimationDelegate
{
    typealias AnimationNotify = (eAnimationReply) -> Void
    typealias AnimationHandler = (AnimationNotify?) -> Void
    typealias CancelHandler = () -> Void
    typealias PauseResumeHandler = (Bool) -> Void

    private static var animationIDCounter = 0
    
    let name :                  String
    var op:                     eAvatarGestureAnimationOp
    var animationSequenceID:    Int
    var runHandler:             AnimationHandler?   = nil
    var cancelHandler:          CancelHandler?      = nil
    var pauseResumeHandler:     PauseResumeHandler? = nil
    var animationNotifyHandler: AnimationNotify?    = nil

    override var description: String
    {
        "GestureAnimation(name: \(name))"
    }

    init(_ name : String, op: eAvatarGestureAnimationOp)
    {
        self.name = name
        self.op = op
        self.animationSequenceID = GestureAnimation.animationIDCounter
        
        GestureAnimation.animationIDCounter += 1
    }

    func run()
    {
        runHandler?(animationNotifyHandler)
    }

    func cancel()
    {
        cancelHandler?()
    }

    func pauseResume(doPause: Bool)
    {
        pauseResumeHandler?(doPause)
    }

    public func animationDidStart(_ anim: CAAnimation)
    {
        animationNotifyHandler?(eAnimationReply.eDidBeginAnimation(self))
    }

    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool)
    {
        animationNotifyHandler?(eAnimationReply.eDidEndAnimation(self))
    }

    public static func ==(lhs : GestureAnimation, rhs: GestureAnimation) -> Bool
    {
        lhs.name == rhs.name
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

extension UIColor // eViewColorMode
{
    func transform(_ withColorMode : eViewColorCast) -> UIColor
    {
        switch withColorMode
        {
            case .eSaturated:     return self

            case .eDesaturated:

                var brightness: CGFloat = 0
                var alpha: CGFloat = 0

                getHue(nil, saturation: nil, brightness: &brightness, alpha: &alpha)

                return UIColor(white: brightness * 0.4, alpha: alpha)

            case .eAlert:

                var red: CGFloat = 0
                var green: CGFloat = 0
                var blue: CGFloat = 0
                var alpha: CGFloat = 0

                getRed(&red, green: &green, blue: &blue, alpha: &alpha)

                if self == UIColor(red: 0.61, green: 0.80, blue: 1.0, alpha: 0.8)
                {
                    return UIColor(red: 0.8, green: 0.2, blue: 0.4, alpha: 0.4)
                }

                if red < 0.3
                {
                    red = 0.3
                }

                return UIColor(red: red, green: green, blue: blue, alpha: alpha)
        }
    }

    func blend(_ factor : CGFloat, _ other: UIColor) -> UIColor
    {
        let factor1 = 1.0 - factor

        var (r1, g1, b1, a1) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        var (r2, g2, b2, a2) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))

        func blend(_ value1: CGFloat, _ value2: CGFloat) -> CGFloat
        {
            factor * value1 + factor1 * value2
        }

        self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        other.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        return UIColor(red: blend(r1, r2), green: blend(g1, g2), blue: blend(b1, b2), alpha: blend(a1, a2))
    }
}

extension UIView
{
    var isCompactViewPortrait: Bool
    {
        traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass != .compact
    }

    var isCompactViewLandscape: Bool
    {
        traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .compact
    }

    var isCompactDevice : Bool
    {
        traitCollection.horizontalSizeClass == .compact || traitCollection.verticalSizeClass == .compact
    }

    var isCompactViewLandscapeOrExternalScreen : Bool
    {
        true
    }
}

