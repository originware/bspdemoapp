
//
//  GraphViewModelTests.swift
//  BSPDemoTests
//
//  Created by Terry Stillone on 23/11/21.
//

import XCTest

class GraphViewModelTests: XCTestCase
{
    typealias Node = GraphViewModel.Node
    typealias Edge = GraphViewModel.Edge
    typealias RankPosition = GraphLayoutModel.RankPosition
    
    let NodeA = NSAttributedString("Node A\nNode Category Test\nComment")
    let NodeB = NSAttributedString("Node B\nNode Category Test\nComment")
    let NodeC = NSAttributedString("Node C\nNode Category Test\nComment")
    let NodeD = NSAttributedString("Node D\nNode Category Test\nComment")
    let NodeE = NSAttributedString("Node E\nNode Category Test\nComment")
    let NodeF = NSAttributedString("Node F\nNode Category Test\nComment")
    let NodeG = NSAttributedString("Node G\nNode Category Test\nComment")
    let NodeH = NSAttributedString("Node H\nNode Category Test\nComment")
    let NodeI = NSAttributedString("Node I\nNode Category Test\nComment")
    
    fileprivate enum eConfiguration
    {
        case eSingleTreeWithSingleNode
        case eSingleTreeWithTwoNodes
        case eSingleTreeWithTwoChildren
        case eSingleTreeWithThreeChildren
        case eSingleTreeWithWithUnbalancedLeftSide1
        case eSingleTreeWithWithUnbalancedLeftSide2
        
        fileprivate var rootNodeCount : Int
        {
            switch self
            {
                case .eSingleTreeWithSingleNode:                return 1
                case .eSingleTreeWithTwoNodes:                  return 1
                case .eSingleTreeWithTwoChildren:               return 1
                case .eSingleTreeWithThreeChildren:             return 1
                case .eSingleTreeWithWithUnbalancedLeftSide1:   return 1
                case .eSingleTreeWithWithUnbalancedLeftSide2:   return 1
            }
        }
        
        fileprivate var nodeCount : Int
        {
            switch self
            {
                case .eSingleTreeWithSingleNode:                return 1
                case .eSingleTreeWithTwoNodes:                  return 2
                case .eSingleTreeWithTwoChildren:               return 3
                case .eSingleTreeWithThreeChildren:             return 4
                case .eSingleTreeWithWithUnbalancedLeftSide1:   return 6
                case .eSingleTreeWithWithUnbalancedLeftSide2:   return 9
            }
        }
        
        fileprivate var edgeCount : Int
        {
            switch self
            {
                case .eSingleTreeWithSingleNode:                return 0
                case .eSingleTreeWithTwoNodes:                  return 1
                case .eSingleTreeWithTwoChildren:               return 2
                case .eSingleTreeWithThreeChildren:             return 3
                case .eSingleTreeWithWithUnbalancedLeftSide1:   return 5
                case .eSingleTreeWithWithUnbalancedLeftSide2:   return 8
            }
        }
        
        fileprivate var rowCount : Int
        {
            switch self
            {
                case .eSingleTreeWithSingleNode:                return 1
                case .eSingleTreeWithTwoNodes:                  return 2
                case .eSingleTreeWithTwoChildren:               return 2
                case .eSingleTreeWithThreeChildren:             return 2
                case .eSingleTreeWithWithUnbalancedLeftSide1:   return 3
                case .eSingleTreeWithWithUnbalancedLeftSide2:   return 3
            }
        }
        
        fileprivate var columnCount : Int
        {
            switch self
            {
                case .eSingleTreeWithSingleNode:                return 1
                case .eSingleTreeWithTwoNodes:                  return 1
                case .eSingleTreeWithTwoChildren:               return 3
                case .eSingleTreeWithThreeChildren:             return 3
                case .eSingleTreeWithWithUnbalancedLeftSide1:   return 5
                case .eSingleTreeWithWithUnbalancedLeftSide2:   return 7
            }
        }
    }
    
    fileprivate struct RankTestItem
    {
        let config : eConfiguration
        let ranks : [RankPosition : NSAttributedString]
    }
    
    override func setUp()
    {
        continueAfterFailure = false
    }

    func testRanking()
    {
        let testItems : ContiguousArray<RankTestItem> = [

            RankTestItem(config: .eSingleTreeWithSingleNode, ranks: [

                RankPosition(row: 0, column: 0, isVirtualColumn: false) : NodeA,
            ]),

            RankTestItem(config: .eSingleTreeWithTwoNodes, ranks: [

                RankPosition(row: 0, column: 0, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
            ]),

            RankTestItem(config: .eSingleTreeWithTwoChildren, ranks: [

                RankPosition(row: 0, column: 1, isVirtualColumn: true) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 2, isVirtualColumn: false) : NodeC
            ]),

            RankTestItem(config: .eSingleTreeWithThreeChildren, ranks: [

                RankPosition(row: 0, column: 1, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeC,
                RankPosition(row: 1, column: 2, isVirtualColumn: false) : NodeD
            ]),

            RankTestItem(config: .eSingleTreeWithWithUnbalancedLeftSide1, ranks: [

                RankPosition(row: 0, column: 3, isVirtualColumn: true) : NodeA,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeB,
                RankPosition(row: 2, column: 4, isVirtualColumn: false) : NodeC,
                RankPosition(row: 2, column: 0, isVirtualColumn: false) : NodeD,
                RankPosition(row: 2, column: 1, isVirtualColumn: false) : NodeE,
                RankPosition(row: 2, column: 2, isVirtualColumn: false) : NodeF,
            ]),

            RankTestItem(config: .eSingleTreeWithWithUnbalancedLeftSide2, ranks: [

                RankPosition(row: 0, column: 4, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeB,
                RankPosition(row: 2, column: 6, isVirtualColumn: false) : NodeC,
                RankPosition(row: 2, column: 0, isVirtualColumn: false) : NodeD,
                RankPosition(row: 2, column: 1, isVirtualColumn: false) : NodeE,
                RankPosition(row: 2, column: 2, isVirtualColumn: false) : NodeF,

                RankPosition(row: 1, column: 4, isVirtualColumn: true) : NodeG,
                RankPosition(row: 2, column: 3, isVirtualColumn: false) : NodeH,
                RankPosition(row: 2, column: 5, isVirtualColumn: false) : NodeI,
            ]),
        ]
        
        for testItem in testItems
        {
            let graphViewModel = GraphViewModel()
            let renderPipeline = GraphRenderPipeline()
            
            configure(graphViewModel: graphViewModel, withConfiguration: testItem.config)
            
            let graphLayoutModel = renderPipeline.request(graphViewModel: graphViewModel)
            
            XCTAssertTrue(graphLayoutModel.nodeCells.count == testItem.config.nodeCount, "Expected \(testItem.config.nodeCount) NodeCells but got: \(graphLayoutModel.nodeCells.count)")
            XCTAssertTrue(graphLayoutModel.edgeCells.count == testItem.config.edgeCount, "Expected \(testItem.config.edgeCount) EdgeCells but got: \(graphLayoutModel.edgeCells.count)")
            XCTAssertTrue(graphLayoutModel.rootNodeCells.count == testItem.config.rootNodeCount, "Expected \(testItem.config.rootNodeCount) RootNodeCells but got: \(graphLayoutModel.rootNodeCells.count)")
            
            for (rank, title) in testItem.ranks
            {
                XCTAssertTrue(graphLayoutModel.rankToNodeCell[rank]?.title == title, "Expected node with title \(title) to have rank \(rank)")
            }
        }
    }

    func testSizingAnPositioning()
    {
        let testItems : ContiguousArray<RankTestItem> = [

            RankTestItem(config: .eSingleTreeWithSingleNode, ranks: [

                RankPosition(row: 0, column: 0, isVirtualColumn: false) : NodeA,
            ]),

            RankTestItem(config: .eSingleTreeWithTwoNodes, ranks: [

                RankPosition(row: 0, column: 0, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
            ]),

            RankTestItem(config: .eSingleTreeWithTwoChildren, ranks: [

                RankPosition(row: 0, column: 1, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 2, isVirtualColumn: false) : NodeC
            ]),

            RankTestItem(config: .eSingleTreeWithThreeChildren, ranks: [

                RankPosition(row: 0, column: 1, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 0, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeC,
                RankPosition(row: 1, column: 2, isVirtualColumn: false) : NodeD
            ]),

            RankTestItem(config: .eSingleTreeWithWithUnbalancedLeftSide1, ranks: [

                RankPosition(row: 0, column: 3, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 4, isVirtualColumn: false) : NodeC,
                RankPosition(row: 2, column: 0, isVirtualColumn: false) : NodeD,
                RankPosition(row: 2, column: 1, isVirtualColumn: false) : NodeE,
                RankPosition(row: 2, column: 2, isVirtualColumn: false) : NodeF,
            ]),

            RankTestItem(config: .eSingleTreeWithWithUnbalancedLeftSide2, ranks: [

                RankPosition(row: 0, column: 4, isVirtualColumn: false) : NodeA,
                RankPosition(row: 1, column: 1, isVirtualColumn: false) : NodeB,
                RankPosition(row: 1, column: 6, isVirtualColumn: false) : NodeC,
                RankPosition(row: 2, column: 0, isVirtualColumn: false) : NodeD,
                RankPosition(row: 2, column: 1, isVirtualColumn: false) : NodeE,
                RankPosition(row: 2, column: 2, isVirtualColumn: false) : NodeF,

                RankPosition(row: 1, column: 4, isVirtualColumn: false) : NodeG,
                RankPosition(row: 2, column: 3, isVirtualColumn: false) : NodeH,
                RankPosition(row: 2, column: 5, isVirtualColumn: false) : NodeI,
            ]),
        ]

        for testItem in testItems
        {
            let graphViewModel = GraphViewModel()
            let renderPipeline = GraphRenderPipeline()

            configure(graphViewModel: graphViewModel, withConfiguration: testItem.config)

            let graphLayoutModel = renderPipeline.request(graphViewModel: graphViewModel)
            let graphLayout = graphLayoutModel.graphLayout

            XCTAssertTrue(graphLayout.rowCount == testItem.config.rowCount, "Expected \(testItem.config.rowCount) row count but got: \(graphLayout.rowCount )")
            XCTAssertTrue(graphLayout.columnCount == testItem.config.columnCount, "Expected \(testItem.config.columnCount) column count but got: \(graphLayout.columnCount )")

            for row in 0..<graphLayout.rowCount
            {
                XCTAssertNotNil(graphLayout.rowHeights[row], "Expected rowHeights to have an entry for row \(row)")
            }

            for column in 0..<graphLayout.columnCount
            {
                if !graphLayout.virtualColumns.contains(column)
                {
                    XCTAssertNotNil(graphLayout.columnWidths[column], "Expected columnWidths to have an entry for column \(column)")
                }
            }

            for nodeCell in graphLayoutModel.nodeCells
            {
                XCTAssertNotNil(nodeCell.sizing?.cellBounds, "Expected NodeCell for title \(nodeCell.title) to have a bounds")
                XCTAssertNotNil(nodeCell.frame, "Expected NodeCell for title \(nodeCell.title) to have a position")
            }

            var lastRowPosition : CGFloat? = nil
            var lastColumnPosition : CGFloat? = nil
            var nodeCellCount = 0

            func check(_ row: Int, _ column: Int)
            {
                let rank = RankPosition(row: row, column: column, isVirtualColumn: false)

                if let cellNode = graphLayoutModel.rankToNodeCell[rank]
                {
                    let frame = cellNode.frame!

                    if let lastColumnPosition = lastColumnPosition
                    {
                        XCTAssertTrue(lastColumnPosition < frame.origin.x, "Expected cell frame to be to the right of the precious row cell")
                    }
                    else if let lastRowPosition = lastRowPosition
                    {
                        XCTAssertTrue(lastRowPosition > frame.origin.y, "Expected cell frame to be below of the precious column cell")
                    }

                    lastRowPosition = frame.origin.y
                    lastColumnPosition = frame.origin.x

                    nodeCellCount += 1
                }
            }

            for row in 0..<graphLayout.rowCount
            {
                lastColumnPosition = nil

                for column in 0..<graphLayout.columnCount
                {
                    check(row, column)
                }
            }

            XCTAssertTrue(nodeCellCount == graphLayoutModel.nodeCells.count, "Expected rankToNodeCell to include all cells")
        }
    }

    private func configure(graphViewModel: GraphViewModel, withConfiguration configuration: eConfiguration)
    {
        switch configuration
        {
            case .eSingleTreeWithSingleNode:

                let node1 = Node(groupId: 0, title: NodeA)

                graphViewModel.add(node: node1)

            case .eSingleTreeWithTwoNodes:

                let node1 = Node(groupId: 0, title: NodeA)
                let node2 = Node(groupId: 0, title: NodeB)
                let edge12 = Edge(from: node1, to: node2, fromNodeEdgeIndex: 0)

                graphViewModel.add(node: node1)
                graphViewModel.add(node: node2)

                graphViewModel.add(edge: edge12)

            case .eSingleTreeWithTwoChildren:

                let node1 = Node(groupId: 0, title: NodeA)
                let node2 = Node(groupId: 0, title: NodeB)
                let node3 = Node(groupId: 0, title: NodeC)
                let edge12 = Edge(from: node1, to: node2, fromNodeEdgeIndex: 0)
                let edge13 = Edge(from: node1, to: node3, fromNodeEdgeIndex: 1)

                graphViewModel.add(node: node1)
                graphViewModel.add(node: node2)
                graphViewModel.add(node: node3)

                graphViewModel.add(edge: edge12)
                graphViewModel.add(edge: edge13)

            case .eSingleTreeWithThreeChildren:

                let node1 = Node(groupId: 0, title: NodeA)
                let node2 = Node(groupId: 0, title: NodeB)
                let node3 = Node(groupId: 0, title: NodeC)
                let node4 = Node(groupId: 0, title: NodeD)
                let edge12 = Edge(from: node1, to: node2, fromNodeEdgeIndex: 0)
                let edge13 = Edge(from: node1, to: node3, fromNodeEdgeIndex: 1)
                let edge14 = Edge(from: node1, to: node4, fromNodeEdgeIndex: 2)

                graphViewModel.add(node: node1)
                graphViewModel.add(node: node2)
                graphViewModel.add(node: node3)
                graphViewModel.add(node: node4)

                graphViewModel.add(edge: edge12)
                graphViewModel.add(edge: edge13)
                graphViewModel.add(edge: edge14)

            case .eSingleTreeWithWithUnbalancedLeftSide1:

                let node1 = Node(groupId: 0, title: NodeA)
                let node2 = Node(groupId: 0, title: NodeB)
                let node3 = Node(groupId: 0, title: NodeC)
                let node4 = Node(groupId: 0, title: NodeD)
                let node5 = Node(groupId: 0, title: NodeE)
                let node6 = Node(groupId: 0, title: NodeF)

                let edge12 = Edge(from: node1, to: node2, fromNodeEdgeIndex: 0)
                let edge13 = Edge(from: node1, to: node3, fromNodeEdgeIndex: 1)
                let edge24 = Edge(from: node2, to: node4, fromNodeEdgeIndex: 0)
                let edge25 = Edge(from: node2, to: node5, fromNodeEdgeIndex: 1)
                let edge26 = Edge(from: node2, to: node6, fromNodeEdgeIndex: 2)

                graphViewModel.add(node: node1)
                graphViewModel.add(node: node2)
                graphViewModel.add(node: node3)
                graphViewModel.add(node: node4)
                graphViewModel.add(node: node5)
                graphViewModel.add(node: node6)

                graphViewModel.add(edge: edge12)
                graphViewModel.add(edge: edge13)
                graphViewModel.add(edge: edge24)
                graphViewModel.add(edge: edge25)
                graphViewModel.add(edge: edge26)

            case .eSingleTreeWithWithUnbalancedLeftSide2:

                let node1 = Node(groupId: 0, title: NodeA)
                let node2 = Node(groupId: 0, title: NodeB)
                let node3 = Node(groupId: 0, title: NodeC)
                let node4 = Node(groupId: 0, title: NodeD)
                let node5 = Node(groupId: 0, title: NodeE)
                let node6 = Node(groupId: 0, title: NodeF)
                let node7 = Node(groupId: 0, title: NodeG)
                let node8 = Node(groupId: 0, title: NodeH)
                let node9 = Node(groupId: 0, title: NodeI)

                let edge12 = Edge(from: node1, to: node2, fromNodeEdgeIndex: 0)
                let edge17 = Edge(from: node1, to: node7, fromNodeEdgeIndex: 1)
                let edge13 = Edge(from: node1, to: node3, fromNodeEdgeIndex: 2)
                let edge24 = Edge(from: node2, to: node4, fromNodeEdgeIndex: 0)
                let edge25 = Edge(from: node2, to: node5, fromNodeEdgeIndex: 1)
                let edge26 = Edge(from: node2, to: node6, fromNodeEdgeIndex: 2)

                let edge78 = Edge(from: node7, to: node8, fromNodeEdgeIndex: 0)
                let edge79 = Edge(from: node7, to: node9, fromNodeEdgeIndex: 1)

                graphViewModel.add(node: node1)
                graphViewModel.add(node: node2)
                graphViewModel.add(node: node3)
                graphViewModel.add(node: node4)
                graphViewModel.add(node: node5)
                graphViewModel.add(node: node6)
                graphViewModel.add(node: node7)
                graphViewModel.add(node: node8)
                graphViewModel.add(node: node9)

                graphViewModel.add(edge: edge12)
                graphViewModel.add(edge: edge17)
                graphViewModel.add(edge: edge13)
                graphViewModel.add(edge: edge24)
                graphViewModel.add(edge: edge25)
                graphViewModel.add(edge: edge26)
                graphViewModel.add(edge: edge78)
                graphViewModel.add(edge: edge79)
        }
    }
}
