//
//  BehaviourModelTests.swift
//  BSPDemoTests
//
//  Created by Terry Stillone on 23/11/21.
//

import XCTest
import CoreLocation
import Algorithms

class BehaviourTests: XCTestCase
{
    override func setUp()
    {
        continueAfterFailure = false
    }
    
    func testMicrophoneUnavailability() async
    {
        let notifications : [ServiceActor.ServiceDirectory.eFacilityStatus] = [
       
           .eMicrophone(.eFailure("Microphone authorization denied")),
           .eSpeechRecognitionService(.eOnlineOperation("test")),
           .eReachabilityService(.eIsReachable("wifi", "ssid"))
        ]
      
        await runSpeechRecognitionUnavailability(notifications: notifications)
    }
    
    func testSpeechRecognitionNotAuthorized() async
    {
        let notifications : [ServiceActor.ServiceDirectory.eFacilityStatus] = [
       
           .eMicrophone(.eIsAvailable),
           .eSpeechRecognitionService(.eIsNotAvailable("Speech Recognition authorization denied")),
           .eReachabilityService(.eIsReachable("wifi", "ssid"))
        ]
      
        await runSpeechRecognitionUnavailability(notifications: notifications)
    }
    
    func testSpeechRecognitionOnlineAvailability() async
    {
        let notifications : [ServiceActor.ServiceDirectory.eFacilityStatus] = [
       
           .eMicrophone(.eIsAvailable),
           .eSpeechRecognitionService(.eOfflineOperation),
           .eReachabilityService(.eIsReachable("wifi", "ssid"))
        ]
      
        await runSpeechRecognitionAvailability(notifications: notifications, traceMatch : "eFacilityEvent(eSpeechRecognitionAvailabilityChange(eOnlineOperation(wifi SSID ssid)))")
    }
    
    func testSpeechRecognitionOfflineAvailability() async
    {
        let notifications : [ServiceActor.ServiceDirectory.eFacilityStatus] = [
       
           .eMicrophone(.eIsAvailable),
           .eSpeechRecognitionService(.eOfflineOperation),
           .eReachabilityService(.eIsNotReachable)
        ]
      
        await runSpeechRecognitionAvailability(notifications: notifications, traceMatch : "eFacilityEvent(eSpeechRecognitionAvailabilityChange(eOfflineOperation))")
    }

    func testCommandBehaviourModel() async
    {
        typealias VocalResult = BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.VocalResult
        
        let builder = BehaviourModelBuilder(useNopNotifiers: true)
        let commandBehaviourModel = builder.behaviourActor.commandBehaviourModel
        let testItems : ContiguousArray<CommandTestItem> = [

            CommandTestItem("Are you there",                .eFailure("Sorry, the command \"Are you there\" is not recognized")),
            CommandTestItem("What is the current location", .eFailure("Location request not issued")),
            CommandTestItem("What is this location",        .eFailure("Location request not issued")),
            CommandTestItem("What is the address",          .eFailure("Location request not issued")),
            CommandTestItem("What is the time",             .eFailure("Temporal request not issued")),

            CommandTestItem("Display google",               .eFailure("Web request not issued")),
            CommandTestItem("Show google",                  .eFailure("Web request not issued")),

            CommandTestItem("Search wikipedia for time",    .eFailure("Web request not issued")),

            CommandTestItem("Message my location",          .eFailure("Location request not issued")),

            CommandTestItem("Remove web page",              .eFailure("Web request not issued")),
            CommandTestItem("Dismiss web page",             .eFailure("Web request not issued")),
            CommandTestItem("Hide web page",                .eFailure("Web request not issued")),

            CommandTestItem("Give me a sound check",        .eSuccess(VocalResult(vocalOps : [.eSpeak("Sound check"), .ePause(0.3), .eSpeak("Supercalifragilisticexpialidocious")]))),
            CommandTestItem("Recite the loose moose",       .eSuccess(ReciteExtracts.ReciteTexts["the loose moose"])),
            CommandTestItem("Recite the walrus and the carpenter",   .eSuccess(ReciteExtracts.ReciteTexts["the walrus and the carpenter"])),
            CommandTestItem("Recite sylvie and bruno",      .eSuccess(ReciteExtracts.ReciteTexts["silvie and bruno"])),

            CommandTestItem("Display pen controls",         .eFailure("Pencil request not issued")),
            CommandTestItem("Show pen controls",            .eFailure("Pencil request not issued")),

            CommandTestItem("Remove pen controls",          .eFailure("Pencil controls not dismissed")),
            CommandTestItem("Dismiss pen controls",         .eFailure("Pencil controls not dismissed")),
            CommandTestItem("Hide pen controls",            .eFailure("Pencil controls not dismissed")),
        ]

        for testItem in testItems
        {
            let result = await commandBehaviourModel.getResponse(vocalCommand: testItem.vocalCommand, recogStats: nil)

            XCTAssert(result == testItem.expectedResult, "Unexpected result for \"\(testItem.vocalCommand)\":\n\tExpected: \(testItem.expectedResult)\n\tGot: \(result)")
        }
    }
}

extension BehaviourTests
{
    func runSpeechRecognitionAvailability(notifications: [ServiceActor.ServiceDirectory.eFacilityStatus], traceMatch: String) async
    {
        var passCount = 0
        var failCount = 0

        func run(notifications : [ServiceActor.ServiceDirectory.eFacilityStatus]) async -> Bool
        {
            let builder = BehaviourModelBuilder(useNopNotifiers: true)
            let serviceEventNotifier = builder.notifierDirectory.service.event

            for notification in notifications
            {
                serviceEventNotifier.send(.eOnServiceAvailabilityChange(notification))
            }
           
            await builder.auditPublisher.sendCompletion()

            for await record in builder.storePublisher.logAsyncStream
            {
                if record.auditRecord.trace.contains("eFacilityEvent(eSpeechRecognitionAvailabilityChange")
                {
                    if record.auditRecord.trace.contains(traceMatch)
                    {
                        passCount += 1
                    }
                    else
                    {
                        failCount += 1
                    }
                }
            }
            
            return true
        }
   
        for perm in notifications.permutations(ofCount: nil)
        {
            if await run(notifications: perm) == false
            {
                break
            }
        }

        XCTAssertTrue(passCount == 6 && failCount == 0, "eSpeechRecognitionAvailabilityChange gave invalid result")
    }
    
    func runSpeechRecognitionUnavailability(notifications: [ServiceActor.ServiceDirectory.eFacilityStatus]) async
    {
        var passCount = 0
        var failCount = 0

        func run(notifications : [ServiceActor.ServiceDirectory.eFacilityStatus]) async -> Bool
        {
            let builder = BehaviourModelBuilder(useNopNotifiers: true)
            let serviceEventNotifier = builder.notifierDirectory.service.event
            
            for notification in notifications
            {
                serviceEventNotifier.send(.eOnServiceAvailabilityChange(notification))
            }
            
            await builder.auditPublisher.sendCompletion()

            for await record in builder.storePublisher.logAsyncStream
            {
                if record.auditRecord.trace.contains("eFacilityEvent(eSpeechRecognitionAvailabilityChange")
                {
                    if record.auditRecord.trace.contains("(eIsNotAvailable)")
                    {
                        passCount += 1
                    }
                    else
                    {
                        failCount += 1
                    }
                }
            }
            
            return true
        }
   
        for perm in notifications.permutations(ofCount: nil)
        {
            if await run(notifications: perm) == false
            {
                break
            }
        }

        if passCount == 0 && failCount == 0
        {
            XCTFail("eSpeechRecognitionAvailabilityChange did not give any results")
        }
        else
        {
            XCTAssertTrue(passCount == 6 && failCount == 0, "eSpeechRecognitionAvailabilityChange gave invalid result")
        }
    }
}

fileprivate struct BehaviourModelBuilder
{
    fileprivate let notifierDirectory : NotifierDirectory
    fileprivate let behaviourActor:     BehaviourActor
    fileprivate let auditPublisher :    Audit.AuditPublisher<Audit.TraceRecord>
    fileprivate let storePublisher : Audit.AuditStore<Audit.TraceRecord>
    
    fileprivate init(useNopNotifiers: Bool)
    {
        let mockLocation = CLLocation(latitude: -35.0976, longitude: 106.6682)
        let mockAddress = "Sydney, Australia"
        let testingConfig = AppConfig.TestingConfig(showAutomationView: false,
                                              mockServices: true,
                                              mockLocation: mockLocation,
                                              mockAddress: mockAddress)
        let config                 = AppConfig(commandResponderType: .eSample, enableAnnouncements: false, testingConfig: testingConfig)
        let notifierStore          = !useNopNotifiers ? NotifierStore() : NopNotifierStore()
        let notifierDirectory      = NotifierDirectory(notifierStore: notifierStore)
        let behaviourScopeStack    = !useNopNotifiers ? ScopeStack(uri: BehaviourActor.BehaviourStackURI, notifierDirectory: notifierDirectory)       : nil
        let presentationScopeStack = !useNopNotifiers ? ScopeStack(uri: PresentationActor.PresentationStackURI, notifierDirectory: notifierDirectory) : nil
        
        let appBuilder             = AppBuilder(config: config, notifierDirectory: notifierDirectory, behaviourScopeStack: behaviourScopeStack, presentationScopeStack: presentationScopeStack)
        
        self.notifierDirectory = notifierDirectory
        self.behaviourActor = BehaviourActor(appBuilder: appBuilder)
        self.auditPublisher = Audit.AuditPublisher<Audit.TraceRecord>()
        self.storePublisher = Audit.AuditStore<Audit.TraceRecord>(auditPublisher: auditPublisher)
        
        notifierDirectory.behaviour.meta.request.auditPublisher = auditPublisher
        notifierDirectory.behaviour.meta.event.auditPublisher = auditPublisher
    }
    
    fileprivate func buildExpectation(_ match: String) -> XCTestExpectation
    {
        let expectation = XCTestExpectation(description: "eFacilityEvent(eSpeechRecognitionAvailabilityChange")
        
        auditPublisher.onReceive( { (record) in
             
            if record.trace.contains("eFacilityEvent(eSpeechRecognitionAvailabilityChange")
            {
                 expectation.fulfill()
            }
        })
        
        return expectation
    }
}

fileprivate class NopNotifierStore: NotifierStore
{
    public override func build<T>(uri: String, type: T.Type) -> T
    {
        switch uri
        {
            case ServiceActor.ServiceRequestURI:

                return Notifiers.NullSyncNotifier<ServiceActor.eServiceRequest, ServiceActor.eServiceReply>(uri: ServiceActor.ServiceRequestURI, defaultResult: .eAcknowledge) as! T

            case ServiceActor.ServiceEventURI:

                return ServiceActor.ServiceEventNotifier(uri: ServiceActor.ServiceEventURI) as! T

            case ServiceActor.ServiceSpeechSynthURI:

                return Notifiers.NullQueuedNotifier<ServiceActor.eSpeechSynthRequest, Void>(uri: ServiceActor.ServiceSpeechSynthURI, defaultResult: ()) as! T

            case BehaviourActor.BehaviourMetaRequestURI:

                return BehaviourActor.BehaviourMetaRequestNotifier(uri: BehaviourActor.BehaviourMetaRequestURI) as! T

            case BehaviourActor.BehaviourMetaEventURI:

                return BehaviourActor.BehaviourMetaEventNotifier(uri: BehaviourActor.BehaviourMetaEventURI) as! T

            case PresentationActor.PresentationRequestURI:

                return Notifiers.NullAsyncNotifier<PresentationActor.ePresentationRequest, Void>(uri: PresentationActor.PresentationRequestURI, defaultResult: ()) as! T

            case PresentationActor.PresentationEventURI:

                return Notifiers.NullSyncNotifier<PresentationActor.ePresentationEvent, Void>(uri: PresentationActor.PresentationRequestURI, defaultResult: ()) as! T

            default:

                fatalError("Invalid Notifier URI")
        }
    }
}

fileprivate struct CommandTestItem
{
    fileprivate let vocalCommand: String
    fileprivate let expectedResult: BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult

    fileprivate init(_ vocalCommand: String, _ expectedResult: BehaviourActor.eBehaviourMetaEvent.eBehaviourCycle.eOperationalCycle.eResponseCycleEvent.eResult)
    {
        self.vocalCommand = vocalCommand
        self.expectedResult = expectedResult
    }
}
