 # README: The BSP (Behaviour, Service, Presentation) Demo App

Format             | Xcode Project from [Originware.com](http://www.originware.com)
:------------------| :----
App	Title		   | The **BSPDemoApp** (v1.0)
Platforms      	   | iOS 15.0 or later (device and simulator) & Mac Catalyst.
Language           | Swift 5.5 & SwiftUI 3
Requirements:  	   | Xcode 13.0 or later.
Apple Frameworks:  | AVFoundation and Speech (Speech Recognition and Speech Synthesis), Location, WebKit, Network and SystemConfiguration (Network Reachability Detection, Wifi SSID Detection), PencilKit, Combine, SwiftUI, UIKit, Foundation.
Packages:		   | Swift Algorithms, Swift Numerics
Provides:		   | Speech recognition, vocalisation, node tree graphing, avatar character animation, pencil drawing, web page serving, device location, the current time.


## Description

This App is a demonstrator for the **BSP (Behaviour, Service, Presentation) Design Model**. For a complete guide to **BSP**, please read the: [BSP Design Model Brief.pdf](https://originware.com/doc/BSP%20Design%20Model%20Brief.pdf)

Functionally, this is a vocal interaction App, which also graphs the recognised text.

This App "shell" is also used to demonstrate and graph **Originware**'s **Semantic Transcription Technology** but this particular App variant only performs very primitive keyword matching and graphing.

## Operating the App

To operate the App: **Press and Hold** the "Listen" button while vocalizing your chosen directive (see below for a list of commands). When you perform **Press-up**, speech recognition will complete and the App will vocalize its response.

During this process:

- The Recognition text field will present the result of speech recognition.
- The Response text field will present the response and underline each word as it is vocalized.
- The App avatar will indicate when the App is listening and when it is vocalizing, through a series of animated gestures.
- The Node Tree Graph view will display the words that were recognized (Note: in the Semantic Transcription version, the Semantic result is graphed).
- You can annotate the graph with a pencil.
- The Cancel button can be used to stop vocalizations and clear any pencil drawing.

The recognized **vocal command set** includes:

| Command                              | App Response                                        |  Behaviour Scope |
| ------------------------------------ | ------------------------------------------------    | ---------------- |
| What is your location                | Vocalize the current address                        |  Graph Scope     |
| What is the time                     | Vocalize the current time                           |  Graph Scope     |
| Search wikipedia for &lt;subject&gt; | Display the wikipedia page                          |  Web URL Scope   |
| Display google                       | Display the google www.google.com web page          |  Web URL Scope   |
| Remove web page                      | Any presented web page will be dismissed            |  Web URL Scope   |
| Message my location                  | Bring up the iMessage UI with your current location |  Message Scope   |
| Remove message view                  | Any message view will be dismissed                  |  Message Scope   |
| Give me a sound check                | Vocalize a sound level check 					     |  Graph Scope     |
| Recite the loose moose   		       | Recite The Loose Moose poem				         |  Graph Scope     |
| Recite the walrus and the carpenter  | Recite a Lewis Carroll extract					     |  Graph Scope     |
| Recite silvie and bruno              | Recite a Lewis Carroll extract				         |  Graph Scope     |
| Display pencil controls              | Display the pencil kit controls                     |  Graph Scope     |
| Remove pencil controls               | Remove the pencil kit controls                      |  Graph Scope     |

## Design

![Warning: App Interaction Diagram 1 not rendered](App Interaction Diagram1.png)

![Warning: App Interaction Diagram 2 not rendered](App Interaction Diagram2.png)

## Notes

- SwiftUI 3 currently does not support two finger panning. So you will have to use two fingers to zoom and then swap over to one finger panning.
- SwiftUI also currently does not support localized magnification. So pinching only zooms from the graph center.
- The AppUITests may fail with scroll errors when run on iPhone devices, please use iPad devices instead.
- Zoom and Pan are not available on MacCatalyst (with SwiftUI).

## Licensing

The source in this package is provided for under the Apache Version 2.0 license, see the [License.txt](License.txt) file.

## Enquiries

Please direct queries to [Terry Stillone](mailto:terry@originware.com)

## Screenshot

![Warning: App screen shot not rendered](ScreenShot.png)


```
Copyright (c) 2022 Originware. All rights reserved.
```
