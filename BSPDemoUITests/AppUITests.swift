//
//  BSPDemoUITests.swift
//  BSPDemoUITests
//
//  Created by Terry Stillone on 23/11/21.
//

import XCTest
import UIKit

class AppUITests: XCTestCase
{
    override func setUp()
    {
        continueAfterFailure = false
    }
    
    func testRecognitionToPresentationPathway() throws
    {
        guard let contentModel = ContentModel() else { XCTFail("Application failed to launch"); return }
        
        let testRunItems : ContiguousArray<TestRunItem> = [
            
            TestRunItem(speechRecognitionText: "Display google",           expectedAuditRecords: [
              "/actor/behaviour/notifier/meta/event;send;eBehaviourCycleEvent(eOperationalCycle(eResponseCycle(eBegin(Display google",
              "/actor/behaviour/notifier/meta/request;send;eAvatar(eSpeaking(eSpeak(Fetching web page, isAlert: false)))",
              "/actor/behaviour/notifier/meta/request;send;eAvatar(eSpeaking(eSpeak(Presenting web page, isAlert: false)))"
            ]),
 
            TestRunItem(speechRecognitionText: "Dismiss web page",         expectedAuditRecords: [
               "/actor/behaviour/notifier/meta/event;send;eBehaviourCycleEvent(eOperationalCycle(eResponseCycle(eBegin(Dismiss web page",
               "/actor/behaviour/notifier/meta/request;sendAsync;eAvatar(eSpeaking(eSpeak(Removing web page, isAlert: true)))",
           ]),
            
            TestRunItem(speechRecognitionText: "Are you there",             expectedAuditRecords: [
                "/actor/behaviour/notifier/meta/event;send;eBehaviourCycleEvent(eOperationalCycle(eResponseCycle(eBegin(Are you there",
                "/actor/behaviour/notifier/meta/event;sendAsync;eBehaviourCycleEvent(eOperationalCycle(eResponseCycle(eEnd(eFailure(\"Sorry, the command \\\"Are you there .\\\" is not recognized",
                "/actor/presentation/notifier/request;sendAsync;eSet(eRecognitionTextField(Recognized: Are you there",
                "/actor/presentation/notifier/request;send;eSet(eResponseTextField(Remark:        Sorry, the command \"Are you there .\" is not recognized))",
            ]),

            TestRunItem(speechRecognitionText: "What is the current address",  expectedAuditRecords: [
                "/actor/presentation/notifier/request;sendAsync;eSet(eRecognitionTextField(Recognized: What is the current address",
                "/actor/presentation/notifier/request;send;eSet(eResponseTextField(Remark:        The address is Sydney, Australia)",
                "/actor/behaviour/notifier/meta/request;sendAsync;eAvatar(eSpeaking(eSpeak(The address is Sydney, Australia, isAlert: false)))"
            ]),

            TestRunItem(speechRecognitionText: "What is the time",  expectedAuditRecords: [
                "/actor/presentation/notifier/request;sendAsync;eSet(eRecognitionTextField(Recognized: What is the time",
                "/actor/presentation/notifier/request;send;eSet(eResponseTextField(Remark:        The time is 2:30 PM))",
                "/actor/behaviour/notifier/meta/request;sendAsync;eAvatar(eSpeaking(eSpeak(The time is 2:30 PM, isAlert: false)))"
            ])
        ]

        runTest(contentModel: contentModel, doCancel: false, testRunItems: testRunItems)
    }

    func testListenAndThenCancel() throws
    {
        guard let contentModel = ContentModel() else { XCTFail("Application failed to launch"); return }

        let testRunItems : ContiguousArray<TestRunItem> = [

            TestRunItem(speechRecognitionText: "Get the address", expectedAuditRecords: [
                
                "[Cancel Vocal Command]",
                "!progressEvent(eOnUtterance("
             ]),
        ]

        runTest(contentModel: contentModel, doCancel: true, testRunItems: testRunItems)
    }

    private func runTest(contentModel: ContentModel, doCancel: Bool, testRunItems : ContiguousArray<TestRunItem>)
    {
        // Allow any permissions.
        addUIInterruptionMonitor(withDescription: "System Dialog") { (alert) -> Bool in
            
            let okButton    = alert.buttons["OK"]
            let allowButton = alert.buttons["Allow"]
            
            if okButton.exists
            {
                okButton.tap()
            }

            if allowButton.exists
            {
                allowButton.tap()
            }
            
            return true
        }
        
        contentModel.app.tap()
    
        // Run each test item
        for testRunItem in testRunItems
        {
            func waitForReply(_ text: String)
            {
                let predicate = NSPredicate(block: { _, _ -> Bool in
                    
                    guard let content = contentModel.automationReply.value as? String,
                          !content.isEmpty else { return false }
                    
                    let result = content.contains(text)

                    return result
                })
                
                let waitExpectation = expectation(for: predicate, evaluatedWith: contentModel.automationReply, handler: nil)
                
                contentModel.automationReply.tap2()

                if case .timedOut = XCTWaiter.wait(for: [waitExpectation], timeout: 10.0)
                {
                    XCTFail("automationReply was not set")
                }
            }
            
            func waitForRequest(_ text: String)
            {
                let predicate = NSPredicate(block: { _, _ -> Bool in
                    
                    guard let content = contentModel.automationRequest.value as? String,
                          !content.isEmpty else { return false }
                    
                    return content.hasSuffix(text)
                })
                
                let waitExpectation = expectation(for: predicate, evaluatedWith: contentModel.automationRequest, handler: nil)
                
                contentModel.automationRequest.tap2()

                if case .timedOut = XCTWaiter.wait(for: [waitExpectation], timeout: 10.0)
                {
                    XCTFail("automationReply was not set")
                }
            }
            
            func wait(for timeInterval : TimeInterval)
            {
                let _ = XCTWaiter.wait(for: [XCTestExpectation(description: "wait")], timeout: timeInterval)
            }

            let automationCommand = "begin:" + testRunItem.speechRecognitionText + " ."
            
            contentModel.automationRequest.typeIn(text: automationCommand)
            
            waitForRequest(".")
            waitForReply(automationCommand + ":ack")

            if doCancel
            {
                contentModel.listenButton.press(forDuration: 0.05)
                contentModel.cancelButton.tap()
                wait(for: 3.0)
            }
            else
            {
                contentModel.listenButton.press(forDuration: 0.1)
                wait(for: 3.0)
            }
     
            contentModel.automationRequest.typeIn(text: ":end.")
            waitForRequest(".")
    
            waitForReply(automationCommand + ":end.:ack")
       
            if let aggregatedAuditText = contentModel.automationReply.value as? String
            {
                for expectedAuditText in testRunItem.expectedAuditRecords
                {
                    let isExpected = !expectedAuditText.hasPrefix("!")
                    let effectiveAuditText = isExpected ? expectedAuditText : String(expectedAuditText.dropFirst())
                    let doesContains = aggregatedAuditText.contains(effectiveAuditText)
                    
                    switch (isExpected, doesContains)
                    {
                        case (true, let doesContain):
                            
                            if !doesContain
                            {
                                print(">>>>> test failure debug")
                                
                                let fields = expectedAuditText.components(separatedBy: ";")
                                
                                for field in fields
                                {
                                    if aggregatedAuditText.contains(field)
                                    {
                                        print("    >>>>> contains \(field)")
                                    }
                                    else
                                    {
                                        print("    >>>>> does not contains \(field)")
                                    }
                                }
                                
                                print("    >>>>> wanted: \(expectedAuditText)")
                                print("    >>>>> got: \(aggregatedAuditText)")
                            }
                            
                            XCTAssertTrue(doesContain, "Expected auditText: \"\(effectiveAuditText)\" is missing")
                            
                        case (false, let doesContain):

                            XCTAssertTrue(!doesContain, "Expected auditText: \"\(effectiveAuditText)\" to be missing")
                    }
                }
            }
            
            contentModel.automationRequest.typeIn(text: ":clear.")
            contentModel.automationReply.tap()
            contentModel.automationRequest.tap()
        }
    }
    
    public struct TestRunItem
    {
        public let speechRecognitionText : String
        public let expectedAuditRecords  : ContiguousArray<String>
    }

    public class ContentModel
    {
        public let app : XCUIApplication
        
        public private(set) var listenButton : XCUIElement
        public private(set) var cancelButton : XCUIElement
        public private(set) var recognitionBodyText : XCUIElement
        public private(set) var responseBodyText : XCUIElement
        
        public private(set) var automationRequest : XCUIElement 
        public private(set) var automationReply: XCUIElement
        
        public init?()
        {
            let app = XCUIApplication()
            let device = XCUIDevice.shared
            
            app.launchEnvironment = ["AutoCorrection": "Disabled"]
            app.launchArguments = ["-mock-services", "-show-automationView", "-disable-announcements"]

            // Uncomment if needed for testing interruption handler
            //app.resetAuthorizationStatus(for: .microphone)
            
            device.orientation = UIDeviceOrientation.landscapeRight
            app.launch()

            self.app                    = app
            self.listenButton           = app.buttons["/mainView/buttons/listen"]
            self.cancelButton           = app.buttons["/mainView/buttons/cancel"]
            self.recognitionBodyText    = app.otherElements["/mainView/textFields/recognitionBody"]
            self.responseBodyText       = app.otherElements["/mainView/textFields/responseBody"]
            self.automationRequest      = app.textFields["/automationView/textFields/automationRequest"]
            self.automationReply        = app.textFields["/automationView/textFields/automationReply"]
            
            guard listenButton.exists,
                  cancelButton.exists,
                  recognitionBodyText.exists,
                  responseBodyText.exists,
                  automationRequest.exists else { return nil }
        }
    }
}

extension XCUIElement
{
    public func tap2()
    {
        self.tap()
        self.tap()
    }
    
    public func typeIn(text: String)
    {
        self.tap()
        self.tap()

        let components = text.components(separatedBy: " ")
  
        for (index, component) in components.enumerated()
        {
            if index == 0
            {
                typeText(component)
            }
            else
            {
                typeText(" " + component)
            }
        }
    }
}
